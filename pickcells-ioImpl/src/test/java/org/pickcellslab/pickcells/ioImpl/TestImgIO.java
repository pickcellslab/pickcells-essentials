package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.junit.Test;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;

import com.alee.laf.WebLookAndFeel;

import net.imglib2.Dimensions;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.util.Intervals;

public class TestImgIO {


	private static final ImgIO io = new PickCellsImgIO();
	//private static final Simple charts = new Charts(Collections.singletonList(new JFSimpleHistogramFactory()));

	@Test
	public void testConversion(){



		double xCal = 0.2, yCal = 0.3, zCal = 0.5, cCal = 1, tCal = 2.5;

		int[] xyz = new int[]{Image.x,Image.y,Image.z};
		double[] xyzCal = new double[]{xCal,yCal,zCal};

		int[] order = ImgsChecker.convert(xyz);
		assertTrue(order[Image.x] == 0);
		assertTrue(order[Image.y] == 1);
		assertTrue(order[Image.c] == -1);
		assertTrue(order[Image.z] == 2);
		assertTrue(order[Image.t] == -1);

		double[] cal = ImgsChecker.convert(order, xyzCal);
		assertTrue(cal[Image.x] == xCal);
		assertTrue(cal[Image.y] == yCal);
		assertTrue(cal[Image.c] == 1);
		assertTrue(cal[Image.z] == zCal);
		assertTrue(cal[Image.t] == 1);


		int[] xytc = new int[]{Image.x,Image.y,Image.t,Image.c};
		double[] xytcCal = new double[]{xCal,yCal,tCal,cCal};

		order = ImgsChecker.convert(xytc);
		assertTrue(order[Image.x] == 0);
		assertTrue(order[Image.y] == 1);
		assertTrue(order[Image.c] == 3);
		assertTrue(order[Image.z] == -1);
		assertTrue(order[Image.t] == 2);

		cal = ImgsChecker.convert(order, xytcCal);
		assertTrue(cal[Image.x] == xCal);
		assertTrue(cal[Image.y] == yCal);
		assertTrue(cal[Image.c] == cCal);
		assertTrue(cal[Image.z] == 1);
		assertTrue(cal[Image.t] == tCal);


		int[] xytzc = new int[]{Image.x,Image.y,Image.z,Image.t,Image.c};
		order = ImgsChecker.convert(xytzc);
		assertTrue(order[Image.x] == 0);
		assertTrue(order[Image.y] == 1);
		assertTrue(order[Image.c] == 4);
		assertTrue(order[Image.z] == 2);
		assertTrue(order[Image.t] == 3);

	}

	//@Test
	public void testCreateTmpImage(){
		Dimensions dims = Intervals.createMinMax(0,0,0,0,0, 1024,1024,2,50,20);
		io.createImg(dims, new FloatType());
	}


	//@Test
	public void testFolderHandler(){

		//Setup the look and feel
		try {
			UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebLookAndFeel.initializeManagers ();

		ImgsChooser chooser = io.createChooserBuilder()
				.setTitle("Chooser Test")
				.build();
		chooser.setModal(true);
		chooser.setVisible(true);


		if(!chooser.wasCancelled()){

			ImgFileList list = chooser.getChosenList();
			for(int i = 0; i<list.numDataSets(); i++)
				for(int j = 0; j<list.numImages(i); j++){
					list.load(i, j);
					System.out.println("Name : "+list.name(i,j));
					final MinimalImageInfo info = list.getMinimalInfo(i, j);
					System.out.println("-----------------------------");
					System.out.println(info);
				}


		}


	}











	//@Test
	public <T extends NativeType<T> & RealType<T>> void testImgCreation() throws InterruptedException, IOException{




		//Setup the look and feel
		try {
			UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebLookAndFeel.initializeManagers ();





		// Choose an Image file to Load and visualize

		ImgsChooser chooser = io.createChooserBuilder()
				.setTitle("Chooser Test")
				.build();
		chooser.setModal(true);
		chooser.setVisible(true);


		if(chooser.wasCancelled())	return;



		ImgFileList list = chooser.getChosenList();

		System.out.println("Number of datasets = "+list.numDataSets());
		System.out.println("Number of images = "+list.numImages());

		ImgsChecker checker = 
				io.createCheckerBuilder()
				.addCheck(ImgsChecker.calibration)
				.addCheck(ImgsChecker.ordering)
				.addCheck(ImgsChecker.nChannels)
				.addCheck(ImgsChecker.extension)
				.addCheck(ImgsChecker.sUnit)
				.addCheck(ImgsChecker.bitDepth)
				.build(list);

		System.out.println("Chosen list is consistent : "+checker.isConsistent());

		for(AKey<?> k : checker.tested()){
			System.out.println(k.name+" = ");
			Object o = checker.value(k);
			if(o.getClass().isArray())
				for(int i = 0; i<Array.getLength(o); i++)
					System.out.print(Array.get(o, i)+" ");
			else
				System.out.print(checker.value(k));
		}




		int[] order = ImgsChecker.convert(checker.value(ImgsChecker.ordering,list,0,0));
		//int[] order = checker.value(ImgsChecker.ordering,list,0,0);
		double[] cal = ImgsChecker.convert(order, checker.value(ImgsChecker.calibration,list,0,0));



		System.out.println("Order after conversion = " + Arrays.toString(order));
		System.out.println("Calibration after conversion = " + Arrays.toString(cal));

		MinimalImageInfo info = list.getMinimalInfo(0,0);
		Img<T> img = list.load(0, 0);



		// Create the visualisation

		ImageViewer<T> view = new ImageViewer<>(img,ImgDimensions.createDefaultColorTables(img, order[Image.c]), order);
		JFrame vf = view.displayWithChannelsController("Spot Detection Preview ",new String[]{"Grays"});
		vf.pack();
		vf.setLocationRelativeTo(null);

		vf.setVisible(true);

		Thread.sleep(100000);

		long[] dims = new long[img.numDimensions()];
		img.dimensions(dims);



		// Save the image frame by frame
		/*

		ImgWriter<T> w = io.createWriter(info, img.firstElement(), list.path(0)+File.separator+"CreatedFromTest"+io.standard(img));


		System.out.println("Channel number = "+w.channelNumber());
		System.out.println("Frame number = "+w.frameNumber());

		RandomAccessibleInterval<T> channel = img;
		RandomAccessibleInterval<T> slice = null;
		// Get the first time point
		for(int c = 0; c<w.channelNumber(); c++){

			//Cut original image along channel
			int[] reorder = order;
			if(w.channelNumber()>1){
				channel = Views.hyperSlice(img, order[Image.c], c);
				reorder = Image.removeDimension(reorder, Image.c);
				System.out.println("After Channel removed : ");
			}

			for(int t = 0; t<w.frameNumber(); t++){
				RandomAccessibleInterval<T> frame = w.createNewChannelFrame();
				//Copy the frame
				slice = channel;
				if(w.frameNumber()>1){
					slice = Views.hyperSlice(channel, reorder[Image.t], t);
				}
				ImgDimensions.copy(slice, frame);

				long[] l = new long[frame.numDimensions()];
				frame.dimensions(l);
				System.out.println("created frame sizes : "+Arrays.toString(l));
				w.writeChannelFrame(frame, c, t);
			}
		}
		w.close();



		// Test the creation of ImgFileList from file
		ImgWriter<T> overwriter = 
				io.imgFileListFrom(Collections.singletonList(new File(list.path(0)+File.separator+"CreatedFromTest"+io.standard(img))))
				.createWriterHandle(0,0);


		// Simply get one plane from img
		if(order[Image.z]!=-1){
			slice = Views.hyperSlice(slice, 2, 0);
		}


		//Set all to 1
		T type = img.firstElement().createVariable();
		type.setReal(type.getMaxValue());
		img.forEach(p->p.set(type));

		//Overwrite
		overwriter.writePlane(slice, (int)img.dimension(order[Image.z])/2, 0, 0);

		//Close
		overwriter.close();
		 */
	}




	//@Test
	public <T extends NativeType<T> & RealType<T>> void testImageViewer2() throws InterruptedException, IOException{




		//Setup the look and feel
		try {
			UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebLookAndFeel.initializeManagers ();






		// Choose an Image file to Load and visualize

		ImgsChooser chooser = io.createChooserBuilder()
				.setTitle("Chooser Test")
				.build();
		chooser.setModal(true);
		chooser.setVisible(true);


		if(chooser.wasCancelled())	return;



		ImgFileList list = chooser.getChosenList();

		System.out.println("Number of datasets = "+list.numDataSets());
		System.out.println("Number of images = "+list.numImages());

		ImgsChecker checker = 
				io.createCheckerBuilder()
				.addCheck(ImgsChecker.calibration)
				.addCheck(ImgsChecker.ordering)
				.addCheck(ImgsChecker.nChannels)
				.addCheck(ImgsChecker.extension)
				.addCheck(ImgsChecker.sUnit)
				.addCheck(ImgsChecker.bitDepth)
				.build(list);

		System.out.println("Chosen list is consistent : "+checker.isConsistent());

		for(AKey<?> k : checker.tested()){
			System.out.println(k.name+" = ");
			Object o = checker.value(k);
			if(o.getClass().isArray())
				for(int i = 0; i<Array.getLength(o); i++)
					System.out.print(Array.get(o, i)+" ");
			else
				System.out.print(checker.value(k));
		}




		int[] order = ImgsChecker.convert(checker.value(ImgsChecker.ordering,list,0,0));
		//int[] order = checker.value(ImgsChecker.ordering,list,0,0);
		double[] cal = ImgsChecker.convert(order, checker.value(ImgsChecker.calibration,list,0,0));



		System.out.println("Order after conversion = " + Arrays.toString(order));
		System.out.println("Calibration after conversion = " + Arrays.toString(cal));

		MinimalImageInfo info = list.getMinimalInfo(0,0);
		Img<T> img = list.load(0, 0);


		/*
		// Create the visualisation
		System.out.println("Image loaded");

		ImageDisplay view = new ImageDiplayFactoryImpl(new JFSimpleHistogramFactory()).newDisplay();
		view.addImage(info, img);
		JFrame vf = new JFrame();
		vf.setContentPane(view.getView());
		vf.pack();
		vf.setLocationRelativeTo(null);

		vf.setVisible(true);

		Thread.sleep(5000);

		//view.removeImage(0);

		//Img<UnsignedByteType> img2 = io.createImg(img, new UnsignedByteType());
		//ImgDimensions.copyRealAndContrastWithOutput(img, img2);
		//view.addImage(info, img2);






		Thread.sleep(300000);
		 */
	}


}
