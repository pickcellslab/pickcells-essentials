package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Predicate;

import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgWriter;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;

import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Writer;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;

/**
 * Implementation of {@link ImgFileList} using the Scifio library
 * 
 * @author Guillaume Blin
 *
 */
public class PickCellsImgFiles implements ImgFileList{


	private final List<DiskImgHandler> datasets;
	
	PickCellsImgFiles(List<DiskImgHandler> datasets){
		this.datasets = datasets;
	}

	@Override
	public int numImages() {
		int counter = 0;
		for(DiskImgHandler i : datasets)
			counter+=i.numImages();
		return counter;
	}




	@Override
	public int numImages(int i) {
		return datasets.get(i).numImages();
	}




	@Override
	public int numDataSets() {
		return datasets.size();
	}




	@Override
	public String path(int dataset) {
		return datasets.get(dataset).getDataSetPath();
	}




	@Override
	public String name(int dataset) {
		return datasets.get(dataset).getDataSetName();
	}




	@Override
	public String name(int dataset, int image) {
		return datasets.get(dataset).getName(image);
	}




	@Override
	public String name(String dataset, int image) {
		Optional<DiskImgHandler> h = datasets.stream().filter(d->d.getDataSetName().equals(dataset)).findFirst();
		if(!h.isPresent())
			throw new NoSuchElementException("Unknown dataset : " + dataset);
		return h.get().getName(image);
	}




	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends NativeType<T>> Img<T> load(int dataset, int image) {
		return (Img) datasets.get(dataset).load(image);	
	}




	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <T extends NativeType<T>> Img<T> load(String dataset, int image) {
		Optional<DiskImgHandler> h = datasets.stream().filter(d->d.getDataSetName().equals(dataset)).findFirst();
		if(!h.isPresent())
			throw new NoSuchElementException("Unknown dataset : " + dataset);
		return (Img) h.get().load(image);
	}




	@Override
	public ImgFileList subList(int dataset, Predicate<Integer> images) {
		List<DiskImgHandler> list = new ArrayList<>(1);
		list.add(datasets.get(dataset).create(images));
		return new PickCellsImgFiles(list);
	}




	@Override
	public void merge(ImgFileList other) {
		// TODO Auto-generated method stub
		throw new RuntimeException("Not implemented yet!");
	}

	ImageMetadata metadata(int d, int i){
		return datasets.get(d).imageMetadata(i);
	}
	
	List<ImageMetadata> metadata(){
		List<ImageMetadata> mds = new ArrayList<>();
		for(DiskImgHandler h : datasets)
			mds.addAll(h.imageMetaData());
		return mds;
	}

	@Override
	public MinimalImageInfo getMinimalInfo(int i, int j) {

		ImageMetadata imd = metadata(i,j);		
		
		int[] order = ImgsChecker.convert(PickCellsChecker.value(ImgsChecker.ordering, imd));
		double[] cal = PickCellsChecker.value(ImgsChecker.calibration, imd);
		long[] dims = PickCellsChecker.value(ImgsChecker.dimensions, imd);
		String[] units = PickCellsChecker.value(ImgsChecker.units, imd);
		
		return new MinimalImageInfo(order, dims, cal, units);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends NativeType<T>> T getType(int i, int j) {
		ImageMetadata imd = metadata(i,j);
		return (T) Context.createType(imd);
	}

	@Override
	public <T extends NativeType<T>> ImgWriter<T> createWriterHandle(int i, int j) throws IOException {
		try {
			Writer w = datasets.get(i).createWriterHandle(j);
			return new PickCellsImgWriter<T>(w,getType(i,j));
		} catch (FormatException e) {
			throw new IOException(e);
		}
		
	}

	

}
