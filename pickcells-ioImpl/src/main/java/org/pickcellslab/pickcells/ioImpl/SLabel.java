package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * foundationj-ui
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JLabel;
import javax.swing.JToolTip;

/**
 * A JLabel which displays its tooltip inside a scrollable JTextPane
 * 
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("serial")
public class SLabel extends JLabel{
	 @Override  
     public JToolTip createToolTip() {  
         JScrollableToolTip tip = new JScrollableToolTip(200, 200);  
         tip.setComponent(this);  
         return tip;  
     } 
}
