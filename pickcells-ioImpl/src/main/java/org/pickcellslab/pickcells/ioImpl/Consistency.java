package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.lang.reflect.Array;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.scif.ImageMetadata;
import net.imagej.axis.CalibratedAxis;

/**
 * This {@link FunctionalInterface} defines objects capable of checking that a list of 
 * {@link ImageMetadata} are consistent for a particular value
 *  
 * @author Guillaume Blin
 *
 * @param <T> The type of the value that is checked
 */
@FunctionalInterface
public interface Consistency<T>{

	static Logger log = LoggerFactory.getLogger(Consistency.class);

	/**
	 * Finds and return the value this Consistency is defined for
	 * @param metadata The {@link ImageMetadata} to test
	 * @return The value of interest
	 */
	T findValue(ImageMetadata metadata);
	/**
	 * Checks if all the values are consistent
	 * @param values The list of values to be checked
	 * @return {@code true} if the values are consistent, {@code false} otherwise
	 */
	public default boolean check(List<ImageMetadata> data) {		
		if(data.isEmpty())
			return false;
		return check(data,findValue(data.get(0)));
	}

	/**
	 * Checks if all the values are consistent with the provided value
	 * @param <V>
	 * @param v The value to check against
	 * @param values The list of values to be checked
	 * @return {@code true} if the values are consistent, {@code false} otherwise
	 */
	public default boolean check(List<ImageMetadata> data, Object o) {
		if(data.isEmpty())
			return false;
		List<T> values = data.stream().map(d->findValue(d)).collect(Collectors.toList());		
		if(o.getClass().isArray()){
			return values.stream().allMatch(v->{
				boolean isEqual = true;
				if(!v.getClass().isArray())
					return false;
				if(Array.getLength(o)!= Array.getLength(v))
					return false;
				for(int x = 0; x<Array.getLength(o); x++){
					if(!Array.get(o, x).equals(Array.get(v,x))){
						isEqual = false;
						break;
					}
				}
				return isEqual;
			});
		}
		else
			return values.stream().allMatch(v->v.equals(o));
	}


	/**
	 * @return A {@link Consistency} which will check if the number of dimensions is consistent 
	 */
	public static Consistency<long[]> dimensions(){
		return (i) -> {
			long[] dims = new long[i.getAxes().size()];
			for(int j = 0; j<dims.length; j++)
				dims[j] = i.getAxisLength(j);	
			return dims;
		};
	}


	/**
	 * @return A {@link Consistency} which will check if the ordering of the dimensions in the images are consistent
	 */
	public static Consistency<int[]> ordering(){	
		return (i)->{
			List<CalibratedAxis> axes = i.getAxes();
			int[] order = new int[axes.size()];		
			for(int d = 0; d<order.length; d++){
				switch(axes.get(d).type().getLabel()){
				case "X":order[d] = Image.x; break;
				case "Y": order[d] = Image.y; break;
				case "Colors" : order[d] = Image.c; break;
				case "Channel": order[d] = Image.c; break;
				case "Z": order[d] = Image.z; break;
				case "Time": order[d] = Image.t; break;
				case "T": order[d] = Image.t; break;
				default : order[d] = Image.z; // Put in Z dimension if unknown (nrrd)
				}
			}
			return order;	
		};
	}


	/**
	 * @return A {@link Consistency} which will check the calibration of the n-dimensional images 
	 * returning a double[numAxes] with image ordering of axes
	 */
	public static Consistency<double[]> calibration(){	
		return (i)->{
		
			List<CalibratedAxis> axes = i.getAxes();
			double[] cal = new double[axes.size()];
			for(int d = 0; d<cal.length; d++){
					cal[d] = Math.round(axes.get(d).calibratedValue(1) * 100.0) / 100.0;	
					System.out.println("Calibration : "+axes.get(d).type().getLabel()+ " ; "+axes.get(d).calibratedValue(1));
				//}
			}
			return cal;	
		};
	}

	/**
	 * @return A {@link Consistency} which will check the number of channels
	 */
	public static Consistency<Integer> channels(){	
		return (i)->{
			List<CalibratedAxis> axes = i.getAxes();
			for(int d = 0; d<axes.size(); d++)
				if(axes.get(d).type().getLabel().equals("Channel") || axes.get(d).type().getLabel().equals("Colors"))
					return (int) i.getAxisLength(d);
			return 1; //TODO throw exception
		};
	}



	/**
	 * @return A {@link Consistency} which will check if the length of a specific dimension is consistent
	 */
	public static Consistency<Integer> dimensionLength(int d){					
		return i-> (int)i.getAxisLength(d);
	}


	/**
	 * @return A {@link Consistency} which will check if images all have the same pixel type.
	 * Note that the returned int corresponds to {@link io.scif.util.FormatTools} fields.
	 */
	public static Consistency<Integer> pxType(){					
		return i->i.getPixelType();
	}

	public static Consistency<String> ext(){		
		return (i) -> {
			String extension = i.getName();
			if(extension == null)
				return "library";
			else{
				int j = extension.lastIndexOf('.');
				if (j > 0) {
					extension = i.getName().substring(j+1);
				}
				return extension;
			}
		};
	}

	public static Consistency<?> sUnit(){		
		return (i) -> {
			CalibratedAxis axis = getAxis(Image.x, i).orElse(null);
			if(null == axis)
				return "Not available"; // TODO throw exception
			if(axis.unit() == null)
				return "Unknown";
			return axis.unit();
		};
	}

	public static Consistency<?> tUnit(){
		return (i) -> {

			CalibratedAxis axis = getAxis(Image.t, i).orElse(null);
			if(null == axis)
				return "Not available";
			if(axis.unit() == null)
				return "Unknown";
			return axis.unit();

		};
	}
	
	public static Consistency<?> units(){
		return (i) -> {

			String[] units = new String[i.getAxes().size()];
			for(int j = 0; j<units.length; j++)				
				units[j] = i.getAxis(j).unit();	
			
			return units;

		};
	}


	public static Optional<CalibratedAxis> getAxis(int d, ImageMetadata md){

		String label = "";
		switch(d){
		case Image.x: label = "X"; break;
		case Image.y: label = "Y"; break;
		case Image.c: label = "Channel"; break;
		case Image.z: label = "Z"; break;
		case Image.t: label = "Time"; break;
		default  : return Optional.empty();
		}

		for(int i = 0; i<md.getAxes().size(); i++)
			if(md.getAxes().get(i).type().getLabel().equals(label))
				return Optional.of(md.getAxes().get(i));		

		//Check for an alternative channel name 
		if(d == Image.c)
			for(int i = 0; i<md.getAxes().size(); i++)
				if(md.getAxes().get(i).type().getLabel().equals("Colors"))
					return Optional.of(md.getAxes().get(i));	

		return Optional.empty();
	}
	



}
