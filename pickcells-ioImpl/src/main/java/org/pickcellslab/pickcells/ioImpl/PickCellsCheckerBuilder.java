package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsCheckerBuilder;

public class PickCellsCheckerBuilder implements ImgsCheckerBuilder {

	
	private Map<AKey<?>,Consistency<?>> options = new HashMap<>();
	private Map<AKey<?>,Object> fixedValues = new HashMap<>();
	private Map<AKey<?>,Consistency<?>> fixedChecks = new HashMap<>();
	
	
	
	@Override
	public ImgsCheckerBuilder addCheck(AKey<?> property) {
		if(!PickCellsChecker.repository.containsKey(property))
			throw new IllegalArgumentException("Unsupported property : "+ property.name);
		options.put(property,PickCellsChecker.repository.get(property));
		return this;
	}

	@Override
	public ImgsCheckerBuilder addCheck(AKey<?> property, int index) {
		if(property != ImgsChecker.calibration)
			throw new IllegalArgumentException("Unsupported property : "+ property.name);
		options.put(property, i -> i.getAxes().get(index).calibratedValue(1));
		return this;
	}

	@Override
	public <V> ImgsCheckerBuilder addCheck(AKey<V> property, V value) {
		if(!PickCellsChecker.repository.containsKey(property))
			throw new IllegalArgumentException("Unsupported property : "+ property.name);
		fixedValues.put(property, value);
		fixedChecks.put(property, PickCellsChecker.repository.get(property));
		return this;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public <V> ImgsCheckerBuilder addCheck(AKey<V[]> property, V value, int index) {
		if(property != (AKey)ImgsChecker.calibration)
			throw new IllegalArgumentException("Unsupported property : "+ property.name);
		fixedValues.put(property, value);
		fixedChecks.put(property, i -> i.getAxes().get(index).calibratedValue(1));
		return this;
	}

	@Override
	public ImgsChecker build(ImgFileList list) {
		return new PickCellsChecker(list,options,fixedValues,fixedChecks);
	}
	
	

}
