package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.util.function.Predicate;

import org.pickcellslab.pickcells.api.img.io.ImgsChooser;
import org.pickcellslab.pickcells.api.img.io.ImgsChooserBuilder;

public class PickCellsChooserBuilder implements ImgsChooserBuilder {

	private String title = "Image Chooser";
	private String home = System.getProperty("user.dir");


	@Override
	public ImgsChooserBuilder setTitle(String title) {
		this.title = title;
		return this;
	}

	@Override
	public ImgsChooserBuilder setHomeDirectory(String path) {
		if(new File(path).isDirectory())			
			home = path;
		return this;
	}

	@Override
	public ImgsChooserBuilder setFileFilter(Predicate<String> filter) {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public ImgsChooserBuilder setFileNumber(Predicate<Integer> number) {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public ImgsChooser build() {
		return new PickCellsImgsChooser(title, home);
	}

}
