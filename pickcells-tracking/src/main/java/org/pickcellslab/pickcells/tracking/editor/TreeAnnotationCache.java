package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.img.view.Annotation;

public class TreeAnnotationCache {

	private final Map<NodeItem, DefaultNodeAnnotation> nodeCache = new ConcurrentHashMap<>();
	private final Map<Link, DefaultLinkAnnotation> linkCache = new ConcurrentHashMap<>();
	private final Map<Integer, DefaultTreeAnnotation> treeCache = new ConcurrentHashMap<>(); //Only full trees are cached
	private final Map<Integer, List<DefaultPathAnnotation>> branchCache = new ConcurrentHashMap<>(); //Only Branches are cached

	private final Set<Integer> trackIds;

	private final MutableInt ids = new MutableInt();


	private TrackSelectionMode mode = TrackSelectionMode.LINKS;


	public TreeAnnotationCache(Set<Integer> ids) {
		trackIds = ids;
	}


	public synchronized void setSelectionMode(TrackSelectionMode mode) {
		if(this.mode!=mode){
			this.mode = mode;
			//update status of all nodes and links
			nodeCache.values().forEach(n->n.updateMorphing());
			linkCache.values().forEach(l->l.updateMorphing());
		}
	}


	public TrackSelectionMode getSelectionMode() {
		return mode;
	}

	
	public Stream<Annotation> treeStream() {
		return (Stream)treeCache.values().stream();
	}

	public Stream<Annotation> branchStream() {
		return branchCache.values().stream().flatMap(l->l.stream());
	};
	
	

	public synchronized DefaultNodeAnnotation get(NodeItem node){
		if(node==null)	return null;
		DefaultNodeAnnotation na = nodeCache.get(node);
		if(na==null){
			na = new DefaultNodeAnnotation(node, this);
			nodeCache.put(node, na);
		}
		return na;
	}

	public synchronized DefaultLinkAnnotation get(Link link){
		if(link==null)	return null;
		DefaultLinkAnnotation la = linkCache.get(link);
		//System.out.println("TreeAnnotationCache return link : "+la);
		if(la==null){
			la = new DefaultLinkAnnotation(this, link);
			linkCache.put(link, la);
		}
		return la;
	}



	synchronized int newTrackID(){
		while(trackIds.contains(ids.intValue()))
			ids.increment();
		trackIds.add(ids.intValue());
		return ids.intValue();
	}


	synchronized void delete(DefaultLinkAnnotation la) {
		treeCache.remove(la.link().getAttribute(Keys.trackID).get());
		linkCache.remove(la.link());
	}





	public synchronized DefaultTreeAnnotation getFullTree(DefaultNodeAnnotation node) {
		final int trackId = node.getAttribute(Keys.trackID).orElse(-1);
		DefaultTreeAnnotation t = treeCache.get(trackId);
		if(t == null){
			t = new DefaultTreeAnnotation(this, node, Direction.BOTH);
			treeCache.put(trackId, t);
		}
		return t;
	}

	
	public synchronized DefaultPathAnnotation getPath(DefaultNodeAnnotation node){
		final int trackId = node.getAttribute(Keys.trackID).orElse(-1);
		List<DefaultPathAnnotation> list = branchCache.get(trackId);		
		if(list == null){
			list = new ArrayList<>();
			branchCache.put(trackId, list);
		}
		DefaultPathAnnotation branch = null;
		for(DefaultPathAnnotation path : list){
			if(path.contains(node)){
				branch = path;
				break;
			}
		}
		if(branch == null){
			branch = new DefaultPathAnnotation(this, node, false);
			//System.out.println(branch+" created with "+branch.nodesCount()+" nodes from TreeAnnotationCache");
			list.add(branch);
		}
		return branch;
	}


	//TODO getBranchFor LinkAnnotation




}
