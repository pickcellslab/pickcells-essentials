package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;

import net.imglib2.type.numeric.ARGBType;

public interface NodeAnnotation extends NodeItem, MorphableAnnotation {

	public enum NodeStatus implements AnnotationStatus {
		
		NODE_ROOT{
			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 200, 0, 200);
			}
		},
		
		NODE_DIVIDING{
			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 0, 255, 200);
			}
		},
		
		NODE_LEAF{
			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 255, 0, 200);
			}
		},
		
		NODE_SINGLE{
			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 0, 205, 255);
			}
		};
		
		
	}
	
	public default boolean hasTag(String tag) {
		throw new RuntimeException("Not implemented");
	}

	public boolean isRoot();

	public boolean isLeaf();
	
	public boolean isDivision();
	
	public boolean isInnerNode();
	
	
	public Stream<LinkAnnotation> upstreamLinks(int depth);
	
	
	public TreeAnnotation upstreamTree();
	
	/**
	 * @return A {@link TreeAnnotation} down to the reachable leaf nodes of this {@link NodeAnnotation}
	 */
	public TreeAnnotation downStreamTree();	
	
	public TreeAnnotation fullTree();
	
	
	/**
	 * @return The {@link PathAnnotation} from root to this {@link NodeAnnotation}
	 */
	public PathAnnotation upStreamPath();

		
	
	

	
	
}
