package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;

import net.imglib2.type.numeric.ARGBType;

public interface LinkAnnotation extends Link, MorphableAnnotation {

	public enum LinkStatus implements AnnotationStatus{

		LINK_DELETED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 0, 0, 255);
			}

		}, 		
		LINK_HIGHLIGHTED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 255, 0, 255);
			}

		}, 
		LINK_DIMMED{

			@Override
			public int preferredARGB() {
				return ARGBType.rgba(150, 150, 150, 150);
			}

		},
		
	}



	public PathAnnotation path();

}
