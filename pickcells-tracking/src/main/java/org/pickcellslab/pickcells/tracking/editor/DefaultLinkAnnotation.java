package org.pickcellslab.pickcells.tracking.editor;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;

public class DefaultLinkAnnotation implements LinkAnnotation {

	private final Link l;
	private final TreeAnnotationCache cache;
	private AnnotationStatus status = AnnotationStatus.DESELECTED;
	private MorphableAnnotation morphing;

	DefaultLinkAnnotation(TreeAnnotationCache cache, Link l) {
		this.l = l;
		this.cache = cache;
		morphing = this;
	}


	@Override
	public NodeItem source() {
		return l.source();
	}

	@Override
	public NodeItem target() {
		return l.target();
	}

	@Override
	public void delete() {

		//Get the source to use as root for recoloring
		final NodeItem source = l.source();


		final NodeItem target = l.target();


		cache.delete(this);

		l.delete();

		final int newID = cache.newTrackID();

		final DefaultTraversal<NodeItem, Link> tr = 
				Traversers.breadthfirst(
						source,
						Traversers.newConstraints().fromMinDepth().toMaxDepth()
						.traverseLink(Links.TRACK, Direction.BOTH).includeAllNodes())
				.traverse();


		for(NodeItem n : tr.nodes()){
			n.setAttribute(Keys.trackID, newID);
		}

		for(Link l : tr.edges()){
			l.setAttribute(Keys.trackID, newID);
		}



		//Update states
		cache.get(source).updateMorphing();
		cache.get(target).updateMorphing();		
		this.status = LinkStatus.LINK_DELETED;


	}


	public void updateMorphing() {
		if(l.source()==null)
			return;
		if(cache.getSelectionMode() == TrackSelectionMode.TREES){
			final TreeAnnotation tree = cache.getFullTree(cache.get(l.source()));
			morphing = tree;
		}else if(cache.getSelectionMode() == TrackSelectionMode.BRANCHES){
			final PathAnnotation path = cache.getPath(cache.get(l.target()));
			morphing = path;
		}
		else
			morphing = this;
	}





	@Override
	public PathAnnotation path() {		
		return new DefaultPathAnnotation(cache, this, true);
	}


	@Override
	public Stream<AKey<?>> properties() {
		return l.getValidAttributeKeys();
	}


	@Override
	public <E> E getProperty(AKey<E> k) {
		return l.getAttribute(k).get();
	}


	@Override
	public AnnotationStatus status() {
		return status;
	}


	Link link() {
		return l;
	}



	@Override
	public void toggleSelected(List<LinkAnnotation> linkList, List<NodeAnnotation> nodesList) {
		if(status!=AnnotationStatus.SELECTED){
			status = AnnotationStatus.SELECTED;
			linkList.add(this);
		}
		else{
			status = AnnotationStatus.DESELECTED;
			linkList.remove(this);
		}
	}


	@Override
	public String toString(){
		return l.toString();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return l.minimal();
	}


	@Override
	public String declaredType() {
		return l.declaredType();
	}

	
	@Override
	public String representedType() {
		return l.declaredType();
	}
	

	@Override
	public Stream<AKey<?>> getValidAttributeKeys() {
		return l.getValidAttributeKeys();
	}


	@Override
	public <T> Optional<T> getAttribute(AKey<T> key) {
		return l.getAttribute(key);
	}


	@Override
	public <T> void setAttribute(AKey<T> key, T v) {
		throw new RuntimeException("Forbidden operation");
	}


	@Override
	public void removeAttribute(AKey<?> key) {
		throw new RuntimeException("Forbidden operation");
	}


	public MorphableAnnotation getCurrentMorphing(){
		checkMorphing();
		return morphing;
	}

	
	
	private void checkMorphing() {
		boolean update = false;
		switch(cache.getSelectionMode()){
		case BRANCHES: update = !(morphing instanceof PathAnnotation);
			break;
		case LINKS:	update = morphing != this;
			break;
		case TREES:	update = !(morphing instanceof TreeAnnotation);
			break;
		
		}
		if(update)
			updateMorphing();
	}


}
