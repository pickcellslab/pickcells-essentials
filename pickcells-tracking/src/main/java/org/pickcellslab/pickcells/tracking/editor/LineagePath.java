package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequence;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;

@Data(typeId="LineagePath")
@SameScopeAs(Image.class)
@GraphSequence(LineagePathHints.class)
public class LineagePath extends DataNode implements GraphSequencePointer{

	public static String toLeafLink = "HAS_LEAF";

	protected static final List<AKey<?>> keys = new ArrayList<>(6);

	static{	
		keys.add(idKey);
		keys.add(DataNode.declaredTypeKey);
	}

	@SuppressWarnings("unused")
	private LineagePath() {/*Database use only*/
		System.out.println("LineagePath regenerated from database");
		
	}


	public LineagePath(LineageTree tree, DefaultNodeAnnotation leaf) {
		
		//Set path type
		this.setAttribute(DataNode.declaredTypeKey,  tree.getRoot().declaredType()+" Path");
		
		// Create link to tree
		new DataLink(LineageTree.toPathLink, tree, this, true);
		//Create link to Root
		new DataLink(LineageTree.toRootLink, this, tree.getRoot(), true);
		//Create link to Leaf
		new DataLink(toLeafLink, this, leaf.data(), true);

		//Copy computed features from path annotation
		final PathAnnotation path = leaf.upStreamPath();
		path.properties().forEach(k->Optional.ofNullable(path.getProperty(k)).ifPresent(a->attributes.put(k, a)));
		

	}


	@Override
	public String declaredType(){
		return (String)attributes.get(DataNode.declaredTypeKey);
	}


	@Override
	public void consume(DataAccess access, Consumer<Path<NodeItem, Link>> consumer) throws DataAccessException {
		access.queryFactory().readPaths(this.toString()).with(Actions.consumerAction(consumer, null)).inOneSet()
		.useTraversal(
				TraversalDefinition.newDefinition()
				.startFrom(DataRegistry.typeIdFor(LineagePath.class)).having(DataItem.idKey, this.getAttribute(DataItem.idKey).get())
				.fromDepth(1).toMaxDepth()
				.traverseLink(Links.TRACK, Direction.INCOMING)
				.and(LineagePath.toLeafLink, Direction.OUTGOING)
				.includeAllNodes().build()
				)
		.run();
	}




	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}


	public NodeItem getLeaf() {
		return typeMap.get(toLeafLink).iterator().next().target();
	}



}
