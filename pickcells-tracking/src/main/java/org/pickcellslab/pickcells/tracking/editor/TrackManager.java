package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.threads.ModeratedThread;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.editors.AnnotationManagerWritable;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.view.AbstractAnnotationManager;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatusListener;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;

import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.EllipseNeighborhood;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.view.Views;

public class TrackManager extends AbstractAnnotationManager implements AnnotationManagerWritable{



	private final List<AnnotationStatusListener> statusLstrs = new ArrayList<>();

	private int radius = 2;

	private int zPos, tPos, visibleFrameDepth;
	private boolean annotationsVisible = true;

	private final TrackContainer tracks;


	private RandomAccessibleInterval<IntType> drawnNodes;
	private RandomAccessibleInterval<IntType> drawnLinks;

	private final List<LinkAnnotation> selectedLinks = new ArrayList<>();
	private final List<NodeAnnotation> selectedNodes = new ArrayList<>();



	private final ModeratedThread redrawThread = new ModeratedThread(()->redraw());


	public TrackManager(ImgIO io, TrackContainer tracks) {
		this.tracks = tracks;
		visibleFrameDepth = 4;
		drawnNodes = io.createImg(new FinalInterval(tracks.annotationsInfo().removeDimension(Image.z).imageDimensions()), new IntType());
		drawnLinks = io.createImg(drawnNodes, new IntType());
		redrawThread.start();
	}



	@Override
	public MinimalImageInfo annotatedImageInfo() {
		return tracks.annotationsInfo(); 
	}

	@Override
	public List<AnnotationStatus> possibleAnnotationStates() {
		List<AnnotationStatus> list = new ArrayList<>();
		list.add(AnnotationStatus.DESELECTED);
		list.add(AnnotationStatus.SELECTED);
		list.addAll(Arrays.asList(NodeAnnotation.NodeStatus.values()));
		list.addAll(Arrays.asList(LinkAnnotation.LinkStatus.values()));
		return list;
	}



	@Override
	public Map<String,List<Dimension<Annotation, ?>>> possibleAnnotationDimensions(Predicate<Dimension> filter, boolean decompose) {
		return tracks.possibleDimensions(filter, decompose);		
	}




	@Override
	public Annotation getAnnotationAt(long[] clickPos) {
		// check Links first
		final long[] pos = Arrays.copyOf(clickPos, 2);		
		final RandomAccess<IntType> la = Views.extendZero(drawnLinks).randomAccess();
		la.setPosition(pos);
		if(la.get().get()!=0){
			return tracks.getLink(la.get().get()).getCurrentMorphing();
		}
		else{//try nodes
			final RandomAccess<IntType> na = Views.extendZero(drawnNodes).randomAccess();
			na.setPosition(pos);
			if(na.get().get()!=0){
				return tracks.getNode(na.get().get()).getCurrentMorphing();
			}
		}
		return null;
	}




	@Override
	public void toggleSelectedAt(long[] pos) {
		final MorphableAnnotation sa = (MorphableAnnotation) getAnnotationAt(pos);
		toggleSelected(sa);
	}

	@Override
	public void toggleSelected(Annotation sa) {

		if(sa==null)	return;

		((MorphableAnnotation) sa).toggleSelected(selectedLinks, selectedNodes);	

		statusLstrs.forEach(l->l.statusChanged(sa));
		this.fireAnnotationRedrawRequired(drawnNodes);
	}



	@Override
	public void inverseSelection() {
		// TODO Auto-generated method stub

	}

	@Override
	public void clearSelection() {
		if(mode == TrackSelectionMode.LINKS){
			new ArrayList<>(selectedLinks).forEach(l->l.toggleSelected(selectedLinks, selectedNodes));
			new ArrayList<>(selectedNodes).forEach(n->n.toggleSelected(selectedLinks, selectedNodes));
		}
		selectedLinks.clear();
		selectedNodes.clear();
		//System.out.println("TrackManager selection cleared!");
		this.fireAnnotationRedrawRequired(drawnNodes);
	}




	@Override
	public boolean annotationsAreVisible() {
		return annotationsVisible;
	}

	@Override
	public void toggleAnnotationsVisible() {
		annotationsVisible = !annotationsVisible;
		this.fireAnnotationRedrawRequired(drawnNodes);
	}

	@Override
	public void addAnnotationStatusListener(AnnotationStatusListener l) {
		statusLstrs.add(l);
	}

	@Override
	public void removeAnnotationStatusListener(AnnotationStatusListener l) {
		statusLstrs.remove(l);
	}

	@Override
	public Stream<Annotation> annotations(long[] min, long[] max) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stream<? extends Annotation> stream(String type) {
		return tracks.stream(type);
	}

	@Override
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer) {
		produceCurrentPlaneIteration(stop, consumer, drawnNodes);
	}

	@Override
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer, Interval itrv) {

		final long[] pos = new long[2];

		final Cursor<IntType> nCursor = Views.interval(drawnNodes, itrv).cursor();
		final Cursor<IntType> lCursor = Views.interval(drawnLinks, itrv).localizingCursor();

		//System.out.println("Producing plane");
		while(nCursor.hasNext()){
			final IntType  lT = lCursor.next();		nCursor.fwd();
			lCursor.localize(pos);
			if(lT.get()==0){
				if(nCursor.get().get()==0)
					consumer.accept(pos, null);
				else
					consumer.accept(pos, tracks.getNode(nCursor.get().get()).getCurrentMorphing());
			}
			else
				consumer.accept(pos, tracks.getLink(lT.get()).getCurrentMorphing());				
		}
		//System.out.println("Producing plane done");

	}





	private void redraw() {

		long b = System.currentTimeMillis();

		Views.iterable(drawnNodes).forEach(t->t.setZero());
		Views.iterable(drawnLinks).forEach(t->t.setZero());

		//drawnNodes = StaticImgIO.createImg(new FinalInterval(tracks.annotationsInfo().removeDimension(Image.z).imageDimensions()), new IntType());
		//drawnLinks = StaticImgIO.createImg(drawnNodes, new IntType());

		//System.out.println("Planes creation took "+(System.currentTimeMillis()-b)+"ms");


		//final RandomAccess<IntType> nAccess = drawnNodes.randomAccess();
		final RandomAccess<IntType> lAccess = drawnLinks.randomAccess();

		final EllipseNeighborhood<IntType> ellipse = new EllipseNeighborhood<>(drawnNodes, new long[]{0,0}, new long[]{radius, radius});

		tracks.nodesAt(tPos, zPos).forEach(n->{
			ellipse.setPosition(n.getProperty(Keys.location));
			int value = n.getProperty(DataItem.idKey);
			ellipse.forEach(t->t.set(value));
			//ImgGeometry.drawCircle(n.getProperty(Keys.location), radius, nAccess, new IntType(n.getProperty(DataItem.idKey)));
			n.upstreamLinks(visibleFrameDepth).forEach(l->{
				final long[] o = Arrays.copyOf(l.source().getAttribute(Keys.location).get(),2);
				final long[] h = Arrays.copyOf(l.target().getAttribute(Keys.location).get(),2);
				ImgGeometry.drawLine(lAccess, o, h, new IntType(l.getProperty(DataItem.idKey)));
			});
		});

		//TODO Finally active paths and trees
		this.fireAnnotationRedrawRequired(drawnNodes);
	}






	@Override
	public void positionHasChanged(ImageDisplay source, DisplayPosition dp) {
		switch(dp){
		case FRAME: tPos = source.currentTimeFrame();
		break;
		case SLICE:	zPos = source.currentZSlice();
		break;		
		}
		redrawThread.requestRerun();
	}




	@Override
	public void setDragged(Annotation a, long[] newPos) {}



	@Override
	public void setSelectedAsDeleted() {
		tracks.delete(selectedLinks);
		selectedLinks.clear();
		redrawThread.requestRerun();
	}


	@Override
	public void undeleteAll() {
		// TODO Auto-generated method stub

	}

	public void linkSelected() {

		if(selectedNodes.size()!=2){
			JOptionPane.showMessageDialog(null, "2 nodes must be selected to allow linkage, current selection : "+selectedNodes.size());
			return;
		}

		final DefaultNodeAnnotation na1 = (DefaultNodeAnnotation) selectedNodes.get(0);
		final DefaultNodeAnnotation na2 = (DefaultNodeAnnotation) selectedNodes.get(1);

		if(na1.data().frame()==na2.data().frame()){
			JOptionPane.showMessageDialog(null, "The 2 selected nodes cannot be on the same time point");
			return;
		}

		final DefaultNodeAnnotation source = na1.data().frame()>na2.data().frame() ? na2 : na1;
		final DefaultNodeAnnotation target = source == na1 ? na2 : na1;

		if(source.getDegree(Direction.OUTGOING, Links.TRACK)>1){
			JOptionPane.showMessageDialog(null, source+" is already a division");
			return;
		}

		if(target.getDegree(Direction.INCOMING, Links.TRACK)!=0){
			JOptionPane.showMessageDialog(null, target+" is already connected");
			return;
		}

		tracks.createLink(source.data(), target.data());

		source.toggleSelected(selectedLinks, selectedNodes);
		target.toggleSelected(selectedLinks, selectedNodes);

		this.fireAnnotationRedrawRequired(drawnNodes);

	}



	@Override
	public boolean undoAt(long[] pos) {
		// TODO Auto-generated method stub
		return false;
	}




	@Override
	public void commitChanges() {

		try {

			tracks.commitEdits();	

			WebNotification wn = NotificationManager.showNotification("Changes saved to the database");
			wn.setDisplayTime(1000);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}



	public void setZVisibilityDepth(int value) {
		tracks.setZRadius(value);
		redrawThread.requestRerun();
	}



	public void setFrameVisibilityDepth(int value) {
		this.visibleFrameDepth = value;
		redrawThread.requestRerun();
	}



	public void setNodeSize(int intValue) {
		this.radius = intValue;
		redrawThread.requestRerun();
	}


	private TrackSelectionMode mode = TrackSelectionMode.LINKS;


	public void setTrackSelectionMode(TrackSelectionMode mode) {
		//Clear selection without triggering redraw for now
		//new ArrayList<>(selectedLinks).forEach(l->l.toggleSelected(selectedLinks, selectedNodes));
		//new ArrayList<>(selectedNodes).forEach(n->n.toggleSelected(selectedLinks, selectedNodes));
		selectedLinks.clear();
		selectedNodes.clear();
		this.mode = mode;
		tracks.setSelectionMode(mode);
		redrawThread.requestRerun();
	}



	public void createReport(DataAccess access) {
		tracks.createReport();
	}


}
