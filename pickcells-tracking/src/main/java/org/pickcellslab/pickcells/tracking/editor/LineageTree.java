package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequence;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;

@Data(typeId="LineageTree")
@SameScopeAs(Image.class)
@GraphSequence(LineageTreeHints.class)
public class LineageTree extends DataNode implements GraphSequencePointer{


	public static String toImageLink = "TRACKED_FROM", toRootLink = "HAS_ROOT", toPathLink = "HAS_PATH";


	protected static final List<AKey<?>> keys = new ArrayList<>(6);

	static{	
		keys.add(idKey);
		keys.add(DataNode.declaredTypeKey);
	}


	@SuppressWarnings("unused")
	private LineageTree() {/*Database use*/}


	public LineageTree(final DefaultTreeAnnotation tree) {


		//Copy tree attributes 
		tree.properties().forEach(k->attributes.put(k, tree.getProperty(k)));

		// Create link towards root
		final ImageLocated root = tree.getRoot().data();

		//Set tree type
		this.setAttribute(DataNode.declaredTypeKey, root.declaredType()+" Lineage");

		new DataLink(toRootLink, this, root, true);

		//Create links towards Image
		final Image image = root.image();
		new DataLink(toImageLink, this, image, true);

		// Create Paths objects
		tree.leaves().forEach(l->new LineagePath(this, (DefaultNodeAnnotation) l));


	}




	@Override
	public String declaredType(){
		return (String)attributes.get(DataNode.declaredTypeKey);
	}




	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}





	@Override
	public void consume(DataAccess access, Consumer<Path<NodeItem, Link>> consumer) throws DataAccessException {

		access.queryFactory().readPaths(this.toString()).with(Actions.consumerAction(consumer, null)).inOneSet()
		.useTraversal(
				TraversalDefinition.newDefinition()
				.startFrom(DataRegistry.typeIdFor(LineageTree.class))
				.having(DataItem.idKey, this.getAttribute(DataItem.idKey).get())
				.fromDepth(1).toMaxDepth()
				.traverseLink(Links.TRACK, Direction.OUTGOING)
				.and(toRootLink, Direction.OUTGOING)
				.includeAllNodes().build()
				)
		.run();

	}


	public NodeItem getRoot() { 
		if(this.typeMap.get(toRootLink).isEmpty())
			return null;		
		return typeMap.get(toRootLink).iterator().next().target();
	}


	public List<NodeItem> leaves(){
		final List<NodeItem> leaves = new ArrayList<>();
		for(Link pathLink : typeMap.get(toPathLink)) {
			final LineagePath path = (LineagePath)pathLink.target();
			leaves.add(path.getLeaf());
		}
		return leaves;
	}

	
	public void visitTreeFromRoot(Consumer<Link> visitor) {
		if(visitor == null)
			return;
		
		final LinkedList<NodeItem> queue = new LinkedList<>();
		final Consumer<NodeItem> queueMaintenance = (n)-> queue.addLast(n);
		queue.add(getRoot());
		while(!queue.isEmpty()) {
			queue.pollFirst().getLinks(Direction.OUTGOING, Links.TRACK).forEach(l->{
				queueMaintenance.accept(l.target());
				visitor.accept(l);
			});
		}
	}




}
