package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyDimension;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequenceHints;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;


/**
 * {@link GraphSequenceHints} for {@link LineageTree}s
 * 
 * @author Guillaume Blin
 *
 */
public class LineageTreeHints implements GraphSequenceHints {



	@Override
	public List<MetaQueryable> encounteredQueryable(DataAccess access, String type) {

		final List<MetaQueryable> list = new ArrayList<>(2);

		try{
			//Look for lineage Trees in the database with the specified type

			//Get the root
			final ImageLocated il =
					access.queryFactory().regenerate(DataRegistry.typeIdFor(LineageTree.class)).toDepth(1)
					.traverseLink(LineageTree.toRootLink, Direction.OUTGOING).includeAllNodes().regenerateAllKeys()
					.doNotGetAll().useFilter(P.isNodeWithDeclaredType(type)).run().getOneDependency(ImageLocated.class).orElse(null);
			if(il!=null){
				list.add(access.metaModel().getMetaModel(il));
			}	
			//Add links (which are always tracks)
			list.add(access.metaModel().getMetaLinks(Links.TRACK).get(0));
		}
		catch(DataAccessException e){

		}

		return list;
	}
	
	
	
	
	@Override
	public Dimension<DataItem, ?> generatedDimension() {
		return new KeyDimension<>(ImageLocated.frameKey, "Time");
	}

	

	@Override
	public void consumeFromDatabaseItem(NodeItem dbItem, Consumer<StepTraverser<NodeItem, Link>> action) {
		final TraverserConstraints tc = Traversers.newConstraints()
				.fromDepth(1).toMaxDepth()
				.traverseLink(Links.TRACK, Direction.OUTGOING)
				.and(LineageTree.toRootLink, Direction.OUTGOING)
				.includeAllNodes();
				action.accept(Traversers.breadthfirst(dbItem, tc));		
	}

	
	
	

}
