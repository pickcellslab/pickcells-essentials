package org.pickcellslab.pickcells.tracking.tracker;

import java.net.URL;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.foundationj.datamodel.tools.ShortestPath;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.optim.algorithms.HungarianAlgorithm;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.pickcellslab.pickcells.tracking.features.TrackingDescriptors;

@Module
public class SimpleTracker extends AbstractAnalysis {




	// Link types used by the tracker ***********
	static final String trackType = Links.TRACK, potType = "POTENTIAL"; 

	// AKeys used by the tracker ***********
	public static final AKey<Float> cost = AKey.get("Tracking Cost", Float.class);
	private static final AKey<Float> divScore = AKey.get("Division Score", Float.class);
	//private static final AKey<Integer> trackingEvent = AKey.get("Tracking Event", Integer.class);
	private static final AKey<Boolean> isMinCost = AKey.get("isMinCost", Boolean.class);

	static final AKey<Boolean> isAssigned = AKey.get("IS ASSIGNED", Boolean.class);


	// User defined parameters ***********
	private BiFunction<Link, Double, Float> costFunction;
	private static int allowedFrameJumps = 5;
	private static int minBranchDuration = 4;
	private static int minDivisionGap = 100;


	private final double maxDistance = 15;



	// Reusable Lambdas *******************

	private final Predicate<Link> sourceIsDivision = l->l.source().getDegree(Direction.OUTGOING, trackType) == 2;

	private final static Comparator<Link> costComparator = (l1,l2)-> Float.compare(l1.getAttribute(cost).get(),l2.getAttribute(cost).get());




	// DataAccess ***************
	private final DataAccess access;

	private final UITheme theme;



	public SimpleTracker(DataAccess access, UITheme theme) {
		this.access = access;
		this.theme = theme;
	}






	@Override
	public void launch() throws AnalysisException {


		try{


			// 1- Get all image nodes with their segmentation node(s) (but not further)
			List<Image> images = UsefulQueries.imagesWithSegmentations(access);

			// Make sure there are images in the database
			if(images.isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no images found in the database yet");
				return;
			}

			// Make sure these images have been segmented
			if(images.get(0).segmentations().isEmpty()){
				JOptionPane.showMessageDialog(null, "Images have not been segmented yet");
				return;
			}


			//Allow the user to choose which type of object to track
			Set<String> segmented = new HashSet<>();
			images.get(0).segmentations().forEach(s->{
				for(String clazz : s.associatiedObjects())
					segmented.add(clazz);				
			});


			String choice = null;

			// if segmented is empty
			if(segmented.isEmpty()){
				JOptionPane.showMessageDialog(null, "I have found segmentation results but no data are yet attached. Run Feature Computer first.");
				return;
			}
			// If there is only one ask for confirmation
			else if(segmented.size() == 1){
				choice = segmented.iterator().next();
				int c = JOptionPane.showConfirmDialog(null, 
						"There exist one type of object supported by this module: "+choice+
						"/n Proceed?");
				if(c!=JOptionPane.OK_OPTION)
					return;
			}
			else{ // If there are more request choice
				JComboBox<String> box = new JComboBox<>();
				for(String s : segmented)
					box.addItem(s);

				int c = JOptionPane.showConfirmDialog(null, box);
				if(c!=JOptionPane.OK_OPTION)
					return;
				else
					choice = (String) box.getSelectedItem();
			}


			//labelLink = images.get(0).getSegmentation(choice).get().linkType();


			Class<? extends ImageLocated> choiceClazz = DataModel.getPrototype(access.dataRegistry(), choice).getClass();

			MetaClass choiceMC = access.metaModel().getMetaModel(choice);



			// *************************** Obtain options for our tracker *****************************************************
			final SimpleTrackerOptionDialog dialog = new SimpleTrackerOptionDialog(theme, choiceMC);
			if(JOptionPane.showConfirmDialog(null, dialog)==JOptionPane.CANCEL_OPTION)
				return;

			costFunction = dialog.getCostFunction(access, maxDistance);
			allowedFrameJumps = dialog.getAllowedFrameJumps();
			minBranchDuration = dialog.getMinBranchDuration();
			minDivisionGap = dialog.getMinDivisionGap();



			// *************************** Now for each Image -> Perform Tracking *****************************************************



			for(Image img : images){



				RegeneratedItems items = 
						UsefulQueries.imageToImageLocated(access, choiceMC, P.keyTest(ImageLocated.frameKey, Op.Logical.EQUALS, 0), img.getAttribute(WritableDataItem.idKey).get());


				// ----------- First create the graph of potential connections

				@SuppressWarnings({ "unchecked", "rawtypes" })
				List<SegmentedObject> prev = 
				(List)
				items
				.getDependencies(choiceClazz).collect(Collectors.toList());

				//Set<SegmentedObject> all = new HashSet<>(prev);


				List<DescriptiveStatistics> minStats = new ArrayList<>(this.allowedFrameJumps);
				for(int i = 0; i<allowedFrameJumps; i++)
					minStats.add(new DescriptiveStatistics());


				List<Link> potList = new ArrayList<>();

				for(int t = 1; t<img.frames(); t++){


					System.out.println("Linking Frame "+t);

					@SuppressWarnings({ "unchecked", "rawtypes" })
					List<SegmentedObject> current =
					(List)
					UsefulQueries.imageToImageLocated(access, choiceMC, P.keyTest(ImageLocated.frameKey, Op.Logical.EQUALS, t), img.getAttribute(DataItem.idKey).get())
					.getDependencies(choiceClazz).collect(Collectors.toList());

					System.out.println("Number of retrieved object for current frame: "+current.size());


					if(prev.isEmpty() || current.isEmpty()){
						prev.addAll(current);	
						filterPrevious(prev, t, minStats);
						//all.addAll(current);
						continue;
					}



					// Create the graph
					// connectSetsHungarian(prev, current, gen);
					connectSetsQueue(prev, current, potList);

					// Remove from prev the areas from slices too far above
					prev.addAll(current);	
					filterPrevious(prev,  t, minStats);	



				}// END of itr over frames		




				//perform filtering for last frames
				for(int t = (int) img.frames(); t<img.frames()+allowedFrameJumps; t++){
					filterPrevious(prev,  t, minStats);
				};



				System.out.println("Graph of potential Assignments built with "+potList.size()+" links");




				final List<NodeItem> trackList = relaxedTracking((int) img.frames(), potList, minStats.get(1));

				System.out.println("trackList size after relaxed Tracking = "+trackList.size());




				final TraverserConstraints tc = Traversers.newConstraints().fromMinDepth().toMaxDepth().traverseLink(trackType, Direction.OUTGOING).includeAllNodes();

				final StorageBoxBuilder box = access.queryFactory().store();

				trackList.forEach(n->box.add(n,tc));


				box.run();






			}// END of itr over images








		}catch(Exception e){
			throw new AnalysisException("An error occured while running Simple Tracker", e);
		}
	}









	static List<NodeItem> relaxedTracking(int numFrames, List<Link> potList, DescriptiveStatistics stats){

		createPrimaryAssignment(potList, stats);	
		return buildAndConnectBranches(numFrames, potList);

	}









	// Note This can create assignment mistakes
	// For example if a cell moves and comes back to its initial position in the next frame
	// one frame is going to be skipped because the score will be higher for the jumping link 
	// However these mistakes are corrected during the buildAndConnectBranches procedure
	static void createPrimaryAssignment(List<Link> potList, DescriptiveStatistics stats){

		final Map<Integer, LinkedList<Link>> potMap = potList.stream()
				.collect(Collectors.groupingBy(l->jumpSize(l), Collectors.toCollection(LinkedList::new)));

		final int rounds = 3;

		final double base = stats.getPercentile(75);
		final double IQR = base-stats.getPercentile(25);

		double step = (IQR * 2 ) / rounds;

		for(int i = 1; i<=rounds; i++){

			double threshold = base + (step * i);

			System.out.println("Threshold = "+threshold);


			for(int js = 1; js<=allowedFrameJumps; js++){

				final LinkedList<Link> queue = potMap.get(js);
				if(queue==null)
					continue;

				Collections.sort(queue, costComparator);

				System.out.println("Queue Size for js = "+js+" -> "+queue.size());

				queue.forEach(l->{
					if(l.getAttribute(cost).get() < threshold){
						if(l.source().getLinks(Direction.OUTGOING, potType).filter(out->isAssigned(out)).count() == 0)
							if(l.target().getLinks(Direction.INCOMING, potType).filter(in->isAssigned(in)).count() == 0)
								l.setAttribute(isAssigned, true);
					}
				});

			}

		}// Primary Assignment Created!
	}









	static List<NodeItem> buildAndConnectBranches(int numFrames, List<Link> potList) {


		/********************* Branches connections ****************/


		//Labelling and root connection

		final Map<Integer, Branch> branches = new HashMap<>();



		final MutableInt gen = new MutableInt();
		potList.stream().filter(l->isAssigned(l))
		.sorted((l1,l2) -> Integer.compare(l1.source().getAttribute(ImageLocated.frameKey).get(), l2.source().getAttribute(ImageLocated.frameKey).get()))
		.forEach(l->
		{
			Optional<Integer> id = l.source().getAttribute(Keys.trackID);
			if(!id.isPresent()){
				gen.increment();
				branches.put(gen.intValue(), new Branch(l, gen.intValue()));
			}

		});

		//FIXME single node not considered!!




		//Create a list of branches sorted by duration
		List<Branch> sortedBranches = new ArrayList<>(branches.values());
		Collections.sort(sortedBranches, (b1,b2)->Integer.compare(b1.duration(),b2.duration()));




		//************  First identify small branches that should probably be fused into larger branches (VALIDATED)

		final List<Link> divisionLinks = new ArrayList<>();

		sortedBranches.stream()
		//.filter(b->b.duration()<=this.minBranchDuration)
		.forEach(b->{


			System.out.println("Branch with root "+b.root()+ " is now being tested");

			if(!branches.containsKey(b.trackId()))
				return;


			final Link rootPot = b.rootPotential();
			if(rootPot==null)
				return;

			// Get the frame location of the leaf of the other
			// if the other leaf is above our root, simply append
			final Branch other = branches.get(rootPot.source().getAttribute(Keys.trackID).get());
			if(other.leaf().frame() < b.root().frame()){
				other.append(b, rootPot);
				branches.remove(b.trackId());
				System.out.println("Branch with root "+other.root()+" appended");
				return;
			}			


			// Check for split/fusion or overlooked move
			final Link leafPot = b.leafPotential();
			if(leafPot != null && rootPot.source().getAttribute(Keys.trackID).orElse(-1) == leafPot.target().getAttribute(Keys.trackID).orElse(-2)){
				// Both root and leaf are looking to be attached to the same other branch					
				//if(other!=null)//FIX ME why would this happen?
				if(other.insert(rootPot, leafPot, b)) 
					branches.remove(b.trackId());

				return;

			}


			// If leafPot is null or rootPot and leafPot reach different tracks then 2 cases: 
			// 1- Over-segmentation event has lead to split track
			// 2- Division is present
			else{// Is the leaf of the other branch reaching potentially the current branch? (split track case)
				final Link otherLeafPot = other.leafPotential();
				if(otherLeafPot != null){
					if(otherLeafPot.target().getAttribute(Keys.trackID).orElse(-2)==b.trackId()){
						other.fuse(b, rootPot, otherLeafPot);
						branches.remove(b.trackId());							
						return;
					}
				}
			}



			// If we reach here then we need to look for division
			divisionLinks.add(rootPot);
			//rootPot.source().setAttribute(trackingEvent, 10);

		});



		// For each division link check if there is no potential link to a leaf instead and if yes -> append
		divisionLinks.stream().filter(l->l.target().getAttribute(Keys.trackID).isPresent()).forEach(l->{
			final Integer id0 = l.target().getAttribute(Keys.trackID).orElse(null);
			if(id0!=null && branches.containsKey(id0)){
				final Link toLeaf = branches.get(id0).rootToPotentialLeaf();
				if(toLeaf != null){
					final Integer id1 = toLeaf.source().getAttribute(Keys.trackID).orElse(null);
					final Integer id2 = toLeaf.target().getAttribute(Keys.trackID).orElse(null);
					if(id1!=null && id2 !=null && branches.get(id1)!=null && branches.get(id2)!=null)
						branches.get(id1).append(branches.get(id2), toLeaf);
				}
			}
		});


		// Now compute the division score for each potential division (not assigned in previous step)

		divisionLinks.stream().filter(l->!isAssigned(l) && l.source().getAttribute(Keys.trackID).isPresent()).forEach(l->{
			double angle = getAngleForDivision(l, l.source().getLinks(Direction.OUTGOING, potType).filter(a->isAssigned(a)).findAny().get());
			float score = 
					(float) (angle - Math.abs(
							branches.get(l.target().getAttribute(Keys.trackID).get()).duration()
							- branches.get(l.source().getAttribute(Keys.trackID).get()).remainingDurationFrom(l.source())));
			l.source().setAttribute(divScore, score);			
		});



		divisionLinks.stream().filter(l->!isAssigned(l) && l.source().getAttribute(Keys.trackID).isPresent())
		.sorted((l1,l2)->Float.compare(l2.source().getAttribute(divScore).get(), l1.source().getAttribute(divScore).get()))
		.forEach(l->{
			final Integer id1 = l.source().getAttribute(Keys.trackID).orElse(null);
			final Integer id2 = l.target().getAttribute(Keys.trackID).orElse(null);
			if(id1!=null && id2 !=null && branches.get(id1)!=null && branches.get(id2)!=null)
				branches.get(id1).divideOrFuse(l, branches.get(id2));			
		});


		// De assign branches < minBranchDuration
		branches.values().stream().filter(b->b.duration()<minBranchDuration).forEach(b->b.delete());

		final Map<Integer,NodeItem> rootMap = new HashMap<>();

		//Create final Tracks and compute final features.
		potList.stream().filter(l->isAssigned(l))
		.forEach(l->{
			final Link t = new DataLink(Links.TRACK, l.source(),l.target(), true);
			t.setAttribute(cost, l.getAttribute(cost).get());
			t.setAttribute(Keys.trackID, l.getAttribute(Keys.trackID).get());
			writeInstantaneousAngle(l.source().getLinks(Direction.INCOMING, potType).filter(p->isAssigned(p)).findAny().orElse(null), t);

			if(!rootMap.containsKey(l.getAttribute(Keys.trackID).get()))
				rootMap.put(l.getAttribute(Keys.trackID).get(), getRoot(l));

		});



		return new ArrayList<>(rootMap.values());


	}





	static boolean isAssigned(Link l){
		return l.getAttribute(isAssigned).orElse(false);
	}






	static NodeItem getRoot(Link l){

		if(!isAssigned(l))
			return null;

		Link link = l;
		NodeItem n = null;
		while(link!=null){
			n = link.source();
			link = n.getLinks(Direction.INCOMING, potType).filter(p->isAssigned(p)).findAny().orElse(null);
		}
		return n;
	}










	static int upstreamDivision(Link l) {
		NodeItem dividing = null;
		Link link = l;
		NodeItem n = null;
		while(link!=null){
			System.out.println("Searching for "+link);
			n = link.source();
			if(n.getLinks(Direction.OUTGOING, potType).filter(p->isAssigned(p)).count()>1){
				dividing = n;
				break;
			}

			link = n.getLinks(Direction.INCOMING, potType).filter(p->isAssigned(p)).findAny().orElse(null);
		}
		int v =  dividing == null ? Integer.MAX_VALUE : ((ImageLocated) l.source()).frame() - ((ImageLocated) dividing).frame();
		System.out.println("Upstream division for "+l+" = "+v);
		return v;
	}







	static int downstreamDivision(Link l) {
		NodeItem dividing = null;
		Link link = l;
		NodeItem n = null;
		while(link!=null){
			n = link.target();
			if(n.getLinks(Direction.OUTGOING, potType).filter(p->isAssigned(p)).count()>1){
				dividing = n;
				break;
			}

			link = n.getLinks(Direction.OUTGOING, potType).filter(p->isAssigned(p)).findAny().orElse(null);
		}
		int v = dividing == null ? Integer.MAX_VALUE : ((ImageLocated) dividing).frame() - ((ImageLocated) link.source()).frame();
		System.out.println("Downstream division for "+l+" = "+v);
		return v;
	}








	private static double getAngleForDivision(Link l1, Link l2){

		double angle = 0;

		final double[] o = ((ImageLocated) l1.source()).centroid();
		final double[] h1 = ((ImageLocated) l1.target()).centroid();
		final double[] h2 = ((ImageLocated) l2.target()).centroid();

		if(o.length==2){
			final Vector2D v1 = new Vector2D(h1[0]-o[0], h1[1]-o[1]);
			final Vector2D v2 = new Vector2D(h2[0]-o[0], h2[1]-o[1]);
			if(v1.getNorm()==0 || v2.getNorm() == 0)
				return 0;
			angle = Vector2D.angle(v1, v2);
		}
		else{
			final Vector3D v1 = new Vector3D(h1[0]-o[0], h1[1]-o[1], h1[2]-o[2]);
			final Vector3D v2 = new Vector3D(h2[0]-o[0], h2[1]-o[1], h2[2]-o[2]);
			if(v1.getNorm()==0 || v2.getNorm() == 0)
				return 0;
			angle = Vector3D.angle(v1, v2);
		}		

		return FastMath.toDegrees(angle);
	}




	public static void writeInstantaneousAngle(Link l1, Link l2){

		double angle = 0, dist = 0;

		final double[] o2 = ((ImageLocated) l2.source()).centroid();
		final double[] h2 = ((ImageLocated) l2.target()).centroid();


		if(l1!=null){

			final double[] o1 = ((ImageLocated) l1.source()).centroid();
			final double[] h1 = ((ImageLocated) l1.target()).centroid();

			if(o1.length==2){
				final Vector2D v1 = new Vector2D(h1[0]-o1[0], h1[1]-o1[1]);
				final Vector2D v2 = new Vector2D(h2[0]-o2[0], h2[1]-o2[1]);
				if(!(v1.getNorm()==0 || v2.getNorm() == 0))
					angle = Vector2D.angle(v1, v2);
				dist = v2.getNorm();
			}
			else{
				final Vector3D v1 = new Vector3D(h1[0]-o1[0], h1[1]-o1[1], h1[2]-o1[2]);
				final Vector3D v2 = new Vector3D(h2[0]-o2[0], h2[1]-o2[1], h2[2]-o2[2]);
				if(!(v1.getNorm()==0 || v2.getNorm() == 0))
					angle = Vector3D.angle(v1, v2);
				dist = v2.getNorm();
			}		

		}
		else{// if l1 is null
			dist = MathArrays.distance(o2, h2);
		}

		l2.setAttribute(TrackingDescriptors.instantAngle, FastMath.toDegrees(angle));
		l2.setAttribute(TrackingDescriptors.distance, dist);
	}




	/**
	 * Removes shapes which are located at t-allowedFrameJump in time. For each shape that is removed,
	 * the method also updates the {@link DescriptiveStatistics}  
	 * @param prev The List of {@link SegmentedObject} that need to be filtered
	 * @param t The current time frame
	 * @param stats A List of {@link DescriptiveStatistics} (one stat per time frame in the jump size)
	 */
	private void filterPrevious(List<SegmentedObject> prev, int t, List<DescriptiveStatistics> stats) {


		System.out.println("prev size BEFORE filtering for frame "+t+" = "+prev.size());

		Iterator<SegmentedObject> it = prev.iterator();
		while(it.hasNext()){

			final SegmentedObject so = it.next();

			if(so.frame() <= (t-allowedFrameJumps)){

				// Take this opportunity to flag the links with the lowest cost (among their jump range)

				for(int i = 1 ; i<=allowedFrameJumps; i++){
					final int jumpSize = i;
					so.getLinks(Direction.OUTGOING, potType)
					.filter(l->jumpSize(l)==jumpSize)
					.sorted(costComparator)
					.findFirst()
					.ifPresent(l->{
						stats.get(jumpSize-1).addValue(l.getAttribute(cost).get());
						l.setAttribute(isMinCost, true);
					});
				}


				// Remove from the list
				it.remove();

			}

		}

		System.out.println("prev size AFTER filtering for frame "+t+" = "+prev.size());

	}



	private static int jumpSize(Link l){
		return ((ImageLocated) l.target()).frame()-((ImageLocated) l.source()).frame();
	}






	private void connectSetsQueue(List<SegmentedObject> prev, List<SegmentedObject> current, List<Link> queue) {

		// Build the graph of potential links

		System.out.println("Linking "+prev.size()+" / "+current.size());

		for(int p = 0; p<prev.size(); p++){
			for(int c = 0; c<current.size(); c++){

				//Check inter-centroid distance
				double dis = MathArrays.distance(prev.get(p).getPoint(), current.get(c).getPoint());
				//System.out.println("Distance =  "+dis);
				if(dis<maxDistance){
					//Create link and add to queue
					Link l = new DataLink(potType, prev.get(p), current.get(c), true);					
					l.setAttribute(cost, costFunction.apply(l, dis));
					//l.setAttribute(isJump, (current.get(c).frame()-prev.get(p).frame()>1));
					queue.add(l);
				}

			}
		}


	}







	private void connectSetsHungarian(List<SegmentedObject> prev, List<SegmentedObject> current, AtomicInteger gen) {

		// For now simple linear assignment problem solved by hungarian algorithm

		// Cost = > inter centroid distances weighted by bbox jaccard index
		System.out.println("Linking "+prev.size()+" / "+current.size());
		float[][] matrix = new float[prev.size()][current.size()];
		for(int p = 0; p<prev.size(); p++){
			for(int c = 0; c<current.size(); c++){

				matrix[p][c] = (float) (MathArrays.distance(prev.get(p).getPoint(), current.get(c).getPoint())
						* (1-
								ImgGeometry.boundingBoxJaccard(
										prev.get(p).getAttribute(Keys.bbMin).get(),
										prev.get(p).getAttribute(Keys.bbMax).get(),
										current.get(c).getAttribute(Keys.bbMin).get(),
										current.get(c).getAttribute(Keys.bbMax).get())
								));

			}
		}


		// - Find the optimum assignment
		HungarianAlgorithm algorithm = new HungarianAlgorithm(matrix);
		int[] assignment = algorithm.execute();

		for(int n = 0; n<prev.size(); n++){
			int index = assignment[n]; // index in current
			if(index!=-1 && matrix[n][index] <= 15){
				Link l = new DataLink(trackType, prev.get(n), current.get(index), true);
				int id = prev.get(n).getAttribute(Keys.trackID).orElse(-1);
				if(id==-1){
					id = gen.incrementAndGet();
					prev.get(n).setAttribute(Keys.trackID,id);					
				}
				current.get(index).setAttribute(Keys.trackID,id);
				l.setAttribute(Keys.trackID, id);
				l.setAttribute(cost, matrix[n][index]);
			}
		}


	}





	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/tracking_icon.png"));
	}



	@Override
	public String[] categories() {
		return new String[]{"Tracking"};
	}

	@Override
	public String name() {
		return "Simple Tracker";
	}

	@Override
	public String description() {
		return "Track objects, allowing for divisions, missing objects over several frames and object disappearance.";
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public Icon[] icons() {
		return new Icon[4];
	}

	@Override
	public String[] steps() {
		return new String[]{"Reading DB", "Primary Assignment", "Finding Divisions", "Done"};
	}







	private final static Comparator<Link> frameComparator = (l1,l2)-> Integer.compare(((ImageLocated) l1.source()).frame(), ((ImageLocated) l2.source()).frame());

	private final static Comparator<Link> jumpComparator = 	(l1,l2)-> Integer.compare(jumpSize(l1), jumpSize(l2));



	private static class Branch{


		private static final TraverserConstraints biDirPotentialsAssigned = Traversers.newConstraints()
				.fromMinDepth().toMaxDepth()
				.acceptLinks((l,d)->l.declaredType().equals(potType) && isAssigned(l))
				.includeAllNodes();

		private static final TraverserConstraints upstreamPotentialsAssigned = Traversers.newConstraints()
				.fromMinDepth().toMaxDepth()
				.acceptLinks((l,d)->l.declaredType().equals(potType) && d==Direction.INCOMING && isAssigned(l))
				.includeAllNodes();


		private final List<Link> links;
		private ImageLocated root;
		private final int id;

		private boolean isDividing = false;


		private Branch(final Link l, final int id){

			this.id = id;

			final DefaultTraversal<NodeItem, Link> tr = Traversers.breadthfirst(l.source(), biDirPotentialsAssigned).traverse();
			this.links = tr.edges.stream().sorted(frameComparator).collect(Collectors.toList());
			recolor(id);
			this.root = root();

		}




		public void delete() {
			links.forEach(l->{
				l.removeAttribute(Keys.trackID);
				l.removeAttribute(isAssigned);
			});
			links.clear();
		}




		private Branch(final ImageLocated single, final int id){

			this.id = id;
			this.root = single;
			this.links = new LinkedList<>();
			recolor(id);

		}




		private Integer trackId() {
			return id;
		}




		private ImageLocated root(){
			if(root == null)
				root = (ImageLocated) links.get(0).source();
			return root;
		}




		private ImageLocated leaf(){
			return  links.isEmpty() ? root : (ImageLocated) links.get(links.size()-1).target();
		}




		private void recolor(int newId){
			if(isDividing){
				final DefaultTraversal<NodeItem, Link> tr = Traversers.breadthfirst(root(), biDirPotentialsAssigned).traverse();
				tr.edges().forEach(link->{
					link.setAttribute(Keys.trackID, newId);
					link.source().setAttribute(Keys.trackID, newId);	
					link.target().setAttribute(Keys.trackID, newId);
				});
			}
			else
				for (Link link : links){
					link.setAttribute(Keys.trackID, newId);	
					link.source().setAttribute(Keys.trackID, newId);	
					link.target().setAttribute(Keys.trackID, newId);
				}
		}


		private Link rootPotential(){
			return root().getLinks(Direction.INCOMING, potType)
					.filter(l->l.source().getAttribute(Keys.trackID).isPresent())
					.sorted(costComparator).sorted(jumpComparator).findFirst().orElse(null);
		}



		public Link rootToPotentialLeaf() {
			return root().getLinks(Direction.INCOMING, potType)
					.filter(l->l.source().getAttribute(Keys.trackID).isPresent())
					.filter(l->l.source().getLinks(Direction.INCOMING, potType).filter(out->isAssigned(out)).count()==0)
					.sorted(costComparator).sorted(jumpComparator).findFirst().orElse(null);
		}





		private Link leafPotential(){
			return leaf().getLinks(Direction.OUTGOING, potType)
					.filter(l->l.target().getAttribute(Keys.trackID).isPresent())
					.sorted(costComparator).sorted(jumpComparator).findFirst().orElse(null);
		}


		private int duration(){
			if(isDividing){
				final DefaultTraversal<NodeItem, Link> tr = Traversers.breadthfirst(root(), biDirPotentialsAssigned).traverse();
				int last = tr.nodes().stream().mapToInt(n->((ImageLocated) n).frame()).max().orElse(0);
				return last - root().frame();
			}
			else
				return leaf().frame()-root().frame();
		}






		private int remainingDurationFrom(NodeItem node) {
			if(isDividing){
				final DefaultTraversal<NodeItem, Link> tr = Traversers.breadthfirst(root(), biDirPotentialsAssigned).traverse();
				int last = tr.nodes().stream().mapToInt(n->((ImageLocated) n).frame()).max().orElse(0);
				return last - ((ImageLocated) node).frame();
			}
			else
				return leaf().frame() - ((ImageLocated) node).frame();
		}





		private int nodeNumber(){
			return links.size()+1;
		}





		private void append(Branch branch, Link rootLink) {

			branch.recolor(id);
			//rootLink.source().setAttribute(trackingEvent, 1);
			rootLink.setAttribute(isAssigned, true);	
			rootLink.setAttribute(Keys.trackID, id);
			links.add(rootLink);
			links.addAll(branch.links); 
			//Should be in order already

		}








		private boolean insert(Link rootLink, Link leafLink, Branch toInsert){
			// To distinguish between split/fusion or overlooked move
			// -> Check the size of the other branch vs the number of nodes traversed between rootLink source and leafLink target
			final List<Link> path = new ShortestPath(leafLink.target(), rootLink.source(), upstreamPotentialsAssigned).call().orElse(null);
			if(path==null){
				System.out.println("WARNING: This should Not happen!");
				return false;
			}

			//System.out.println("Checking insertion : path size = "+path.size()+" ; toInsert size = "+toInsert.nodeNumber());

			if(path.size() < toInsert.nodeNumber()){// We need to insert the new path

				path.forEach(l->{
					l.setAttribute(isAssigned, false);
					links.remove(l);
				});


				rootLink.setAttribute(isAssigned, true);	rootLink.setAttribute(Keys.trackID, id);
				leafLink.setAttribute(isAssigned, true);	leafLink.setAttribute(Keys.trackID, id);


				//rootLink.source().setAttribute(trackingEvent, 2);
				//leafLink.target().setAttribute(trackingEvent, 2);

				links.add(rootLink);
				links.add(leafLink);

				toInsert.recolor(id);

				Collections.sort(links, frameComparator);

				System.out.println("Branch with root "+root()+" -> insertion accepted");
				return true;

			}//Otherwise just ignore as the other branch is likely the result of oversegmentation.
			//TODO set other branch as visited?

			System.out.println("Branch with root "+root()+" -> insertion refused");

			return false;

		}


		/**
		 * Fuses this upstream branch to the provided downstream branch, using the lowest cost provided link
		 * @param downstream
		 * @param rootPot
		 * @param otherLeafPot
		 */
		private void fuse(Branch downstream, Link rootLink, Link upstreamLeafLink) {

			System.out.println("Fusing Branch with root "+ root()+" with downstream branch with root "+downstream.root());
			System.out.println("Choice offered between "+ rootLink+" and "+upstreamLeafLink);

			if(rootLink.getAttribute(cost).get() < upstreamLeafLink.getAttribute(cost).get()){

				fuseWithrootLink(rootLink, downstream);

			}// END of fusing from root link


			else{

				//System.out.println("Choice is "+ upstreamLeafLink);

				//fuse from otherLeafLink
				//upstreamLeafLink.source().setAttribute(trackingEvent, 4);
				upstreamLeafLink.setAttribute(isAssigned, true);	
				upstreamLeafLink.setAttribute(Keys.trackID, id);

				upstreamLeafLink.target().setAttribute(Keys.trackID, id);

				//Remove downstream's links which are upstream of upstreamLeafLink
				final Iterator<Link> it = downstream.links.iterator();
				boolean remove = true;
				while(it.hasNext()){
					Link link = it.next();
					if(remove && link.source() == upstreamLeafLink.target()){
						remove=false;
					}
					if(remove){
						it.remove();
						link.setAttribute(isAssigned, false);
						link.removeAttribute(Keys.trackID);
						link.source().removeAttribute(Keys.trackID);
						//	System.out.println("Track ID removed from "+link.source());
					}					
				}


				links.add(upstreamLeafLink);
				links.addAll(downstream.links);
				downstream.recolor(id);

				System.out.println("Branch is now : ");
				links.forEach(l->System.out.print(" | "+l+" "+isAssigned(l)));
				System.out.println();

			}

		}



		private void fuseWithrootLink(Link rootLink, Branch downstream) {
			//System.out.println("Choice is "+ rootLink);

			//fuse from rootLink
			//rootLink.source().setAttribute(trackingEvent, 3);
			rootLink.setAttribute(isAssigned, true);	
			rootLink.setAttribute(Keys.trackID, id);

			// Find the node the rootLink points to and remove further links
			if(rootLink.source() == root()){
				links.forEach(l->{
					l.setAttribute(isAssigned, false);
					l.removeAttribute(Keys.trackID);
					//link.source().removeAttribute(Keys.trackID);
					l.target().removeAttribute(Keys.trackID);
					//System.out.println("Track ID removed from "+l.target());
				});
				links.clear();
			}
			else{
				final Iterator<Link> it = links.iterator();
				boolean remove = false;
				while(it.hasNext()){
					Link link = it.next();
					if(remove){
						it.remove();
						link.setAttribute(isAssigned, false);
						link.removeAttribute(Keys.trackID);
						//link.source().removeAttribute(Keys.trackID);
						link.target().removeAttribute(Keys.trackID);
						//System.out.println("Track ID removed from "+link.target());
					}
					else if(link.target() == rootLink.source()){
						remove=true;
					}
				}
			}

			links.add(rootLink);
			downstream.recolor(id);
			links.addAll(downstream.links);

			//System.out.println("Branch is now : ");
			//links.forEach(l->System.out.print(" | "+l));
			//System.out.println();

		}







		private boolean divideOrFuse(Link l, Branch downstream) {

			//First of all check if both potential downstream branches are long enough
			if(this.remainingDurationFrom(l.target()) >= minBranchDuration && downstream.duration() >= minBranchDuration){

				if(upstreamDivision(l)>minDivisionGap && downstreamDivision(l)>minDivisionGap){
					//Then divide!

					downstream.recolor(id);
					l.setAttribute(isAssigned, true);	
					l.setAttribute(Keys.trackID, id);
					isDividing = true;
					System.out.println("Division Accepted");
					return true;
				}

			}		
			else if(downstream.duration() >= minBranchDuration){
				//Check this branch average cost
				final DoubleSummaryStatistics stats = links.stream().mapToDouble(link->link.getAttribute(cost).get()).summaryStatistics();
				if(l.getAttribute(cost).get()< stats.getAverage()+(stats.getMax()-stats.getAverage())/2){
					this.fuseWithrootLink(l, downstream);
					System.out.println("Fusion accepted after division rejection");
					return true;
				}
			}

			System.out.println("Divide and Fusion rejected");

			return false;

		}






	}



	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}






	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}






	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}






	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}






	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}





}
