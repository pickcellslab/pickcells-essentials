package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.Meta;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.tracking.tracker.SimpleTracker;

public class DefaultTrackContainer implements TrackContainer {


	private int objectDepth = 20;

	private final TreeAnnotationCache cache;

	private final MinimalImageInfo info;

	private final Map<Integer, List<ImageLocated>> frameNodeMap;

	private final Map<Integer,ImageLocated> nodeIdMap = new HashMap<>();
	private final Map<Integer,Link> linkIdMap = new HashMap<>();

	private final Set<Integer> deletedLinks = new HashSet<>();
	private final Set<NodeItem> editedRoots = new HashSet<>();

	private final DataAccess access;

	private final String nodeType, linkType;



	public <V,E> DefaultTrackContainer(Map<Integer, List<ImageLocated>> frameNodeMap, MinimalImageInfo info, DataAccess access){

		this.frameNodeMap = frameNodeMap;

		this.access = access;

		this.info = info.removeDimension(Image.c).removeDimension(Image.t);

		//Build the id maps
		final Set<Integer> trackIds = new HashSet<>();
		for(List<ImageLocated> list : frameNodeMap.values()){
			for(ImageLocated il : list){
				nodeIdMap.put(il.getAttribute(DataItem.idKey).get(), il);
				il.getLinks(Direction.OUTGOING, Links.TRACK).forEach(l->linkIdMap.put(l.getAttribute(DataItem.idKey).get(), l));
				il.getAttribute(Keys.trackID).ifPresent(i->trackIds.add(i));
			}
		}

		cache = new TreeAnnotationCache(trackIds);

		if(!nodeIdMap.isEmpty())
			nodeType = nodeIdMap.values().iterator().next().declaredType();
		else
			nodeType = null;

		if(!linkIdMap.isEmpty())
			linkType = linkIdMap.values().iterator().next().declaredType();
		else
			linkType = null;

		/*
		// only correcting for old database
		linkIdMap.values().forEach(e->{
			if(!e.getAttribute(SimpleTracker.cost).isPresent()) {
				e.setAttribute(SimpleTracker.cost, 0f);
			}
			// instantAngle and travelled distance
			//System.out.println("Writing angles and distance");
			Link prev = e.source().getLinks(Direction.INCOMING, Links.TRACK).findAny().orElse(null);
			SimpleTracker.writeInstantaneousAngle(prev, e);

		});


		System.out.println("Number of frames = "+frameNodeMap.size());
		System.out.println("Number of nodes = "+nodeIdMap.size());
		System.out.println("Number of links = "+linkIdMap.size());
		 */

	}






	public TreeAnnotationCache getCache(){
		return cache;
	}	




	@Override
	public synchronized void setSelectionMode(TrackSelectionMode mode) {
		cache.setSelectionMode(mode);
	}





	@Override
	public Map<String, List<Dimension<Annotation, ?>>> possibleDimensions(Predicate<Dimension> filter, boolean decompose) {

		final Map<String, List<Dimension<Annotation, ?>>> map = new LinkedHashMap<>(); // linked to keep order of types	

		if(!nodeIdMap.isEmpty()){
			final ImageLocated il = nodeIdMap.values().iterator().next();
			final List<Dimension<Annotation, ?>> list = createList(access.metaModel(), il, filter, decompose);
			if(!list.isEmpty()){
				map.put(il.declaredType(), list);
			}
		}

		if(!linkIdMap.isEmpty()){
			final Link link = linkIdMap.values().iterator().next();
			final List<Dimension<Annotation, ?>> list = createList(access.metaModel(), link, filter, decompose);
			if(!list.isEmpty()){
				map.put(link.declaredType(), list);
			}
		}

		map.put(DefaultPathAnnotation.staticType(), DefaultPathAnnotation.possibleDimensions());	

		return map;
	}



	private List<Dimension<Annotation, ?>> createList(Meta meta, DataItem so, Predicate<Dimension> filter, boolean decompose){

		final List<Dimension<Annotation, ?>> list = new ArrayList<>();
		/* Removed since Annotation interface no longer extends DataItem
		//Check if documentation is available
		MetaQueryable mq = null;
		if(meta!=null)
			mq = meta.getMetaModel(so);	

		if(mq!=null){
			if(!decompose)
				mq.getReadables().map(mr->mr.getRaw()).filter(filter).forEach(d->list.add(d));
			else{
				mq.getReadables().forEach(mr->{
					for(int i = 0; i<mr.numDimensions(); i++){
						final Dimension<Annotation, ?> dim = mr.getDimension(i);
						if(filter.test(dim))
							list.add(dim);
					}
				});
			}
		}
		else{
		*/
			if(!decompose)
				so.getValidAttributeKeys().forEach(k->{
					final Dimension<Annotation, ?> dim = Annotation.createDimension((AKey)k, so.getAttribute(k).get());
					if(filter.test(dim))
						list.add(dim);
				});

			else
				so.getValidAttributeKeys().forEach(k->{
					final List<Dimension<Annotation, ?>> dims = Annotation.createDecomposedDimension((AKey)k, so.getAttribute(k).get());
					for(Dimension<Annotation, ?> dim : dims)
						if(filter.test(dim))
							list.add(dim);
				});
		//}
		return list;
	}






	@Override
	public Stream<Annotation> stream(String type) {
		if(type==null)	return Stream.empty();
		if(type.equals(nodeType))
			return nodeIdMap.values().stream().map(n->cache.get(n));
		else if(type.equals(linkType))
			return linkIdMap.values().stream().map(l->cache.get(l));
		else if(type.equals(DefaultTreeAnnotation.staticType()))
			return cache.treeStream();
		else if(type.equals(DefaultPathAnnotation.staticType()))
			return cache.branchStream();
		return Stream.empty();
	}





	@Override
	public Stream<NodeAnnotation> nodesAt(int frame, int zPos) { 
		if(frameNodeMap.get(frame)==null)
			return Stream.empty();
		return frameNodeMap.get(frame).stream()
				.filter(getNodePredicate(zPos)).map(n->cache.get(n));
	}

	@Override
	public Stream<LinkAnnotation> linksAt(int frame, int zPos) {
		if(frameNodeMap.get(frame)==null)
			return Stream.empty();
		return frameNodeMap.get(frame).stream().flatMap(il->il.getLinks(Direction.INCOMING, Links.TRACK))
				.filter(getLinkPredicate(zPos)).map(il->cache.get(il));
	}




	@Override
	public Stream<TreeAnnotation> activeTrees() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Stream<PathAnnotation> activePaths() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public MinimalImageInfo annotationsInfo() {
		return info;
	}

	@Override
	public NodeAnnotation getNode(int i) {
		return cache.get(nodeIdMap.get(i));
	}

	@Override
	public LinkAnnotation getLink(int i) {
		return cache.get(linkIdMap.get(i));
	}


	private Predicate<ImageLocated> getNodePredicate(final int zPos){
		if(info.dimension(Image.z)>1)
			return il -> Math.abs(il.location()[2] - zPos) < objectDepth;
			else
				return il -> true;
	}

	private Predicate<Link> getLinkPredicate(int zPos) {

		if(info.dimension(Image.z)>1){
			final Predicate<ImageLocated> nodePredicate = getNodePredicate(zPos);
			return l -> {
				final ImageLocated sIl = (ImageLocated) l.source();
				final ImageLocated tIl = (ImageLocated) l.target();
				return nodePredicate.test(sIl) || nodePredicate.test(tIl);
			};
		}
		else
			return il->true;
	}


	@Override
	public void setZRadius(int value) {
		objectDepth = value;
	}




	@Override
	public void delete(List<LinkAnnotation> selectedLinks) {
		selectedLinks.forEach(l->{
			final Integer dbId = l.getAttribute(DataItem.idKey).get();
			linkIdMap.remove(dbId);
			deletedLinks.add(dbId);		
			editedRoots.add(l.source());
			editedRoots.add(l.target());
			l.delete();		
		});
	}




	@Override
	public void createLink(NodeItem source, NodeItem target) {

		// Create the link
		final Link l = new DataLink(Links.TRACK, source, target, true);

		// Now we need to rewrite track ids
		final Integer sTrackId =  source.getAttribute(Keys.trackID).orElse(null);


		if(sTrackId!=null){//Recolor downstream
			l.setAttribute(Keys.trackID, sTrackId);
			final Traversal<NodeItem, Link> tr = cache.get(target).downStreamTree().breadthFirst().traverse();
			tr.nodes().forEach(n->n.setAttribute(Keys.trackID, sTrackId));
			tr.edges().forEach(e->e.setAttribute(Keys.trackID, sTrackId));
		}
		else{
			final Integer tTrackId =  target.getAttribute(Keys.trackID).orElse(null);
			if(tTrackId!=null){
				l.setAttribute(Keys.trackID, tTrackId);
				final Traversal<NodeItem, Link> tr = cache.get(source).upstreamTree().breadthFirst().traverse();
				tr.nodes().forEach(n->n.setAttribute(Keys.trackID, tTrackId));
				tr.edges().forEach(e->e.setAttribute(Keys.trackID, tTrackId));
			}
			else{//Both were without any track so create a new one
				final int newId = cache.newTrackID();
				l.setAttribute(Keys.trackID, newId);
				source.setAttribute(Keys.trackID, newId);
				target.setAttribute(Keys.trackID, newId);
			}
		}

		// Add features
		// cost is 0 since this was manually added
		l.setAttribute(SimpleTracker.cost, 0f);
		// instantAngle and travelled distance
		Link prev = l.source().getLinks(Direction.INCOMING, Links.TRACK).findAny().orElse(null);
		SimpleTracker.writeInstantaneousAngle(prev, l);



		editedRoots.add(source);

		//we need to commit edits in order to obtain an id
		try {
			commitEdits();
			linkIdMap.put(l.getAttribute(DataItem.idKey).get(), l);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}






	@Override
	public void commitEdits() throws DataAccessException {

		// delete all deleted links
		access.queryFactory()
		.deleteLinks(Links.TRACK).completely()
		.useFilter(P.setContains(DataItem.idKey, deletedLinks))
		.run();

		deletedLinks.clear();

		//commit edits
		final TraverserConstraints tc = 
				Traversers.newConstraints().fromMinDepth().toMaxDepth()
				.traverseLink(Links.TRACK, Direction.BOTH).includeAllNodes();

		for(NodeItem root : editedRoots){
			access.queryFactory().store().add(root, tc).run();
		}

		editedRoots.clear();

	}




	@Override
	public void createReport() {

		
		final TraverserConstraints tc = Traversers.newConstraints().fromMinDepth().toDepth(3)
				.traverseLink(LineageTree.toImageLink, Direction.OUTGOING)
				.and(LineageTree.toRootLink, Direction.OUTGOING)
				.and(LineageTree.toPathLink, Direction.OUTGOING)
				.and(LineagePath.toLeafLink, Direction.OUTGOING)
				.includeAllNodes();
	

		final StorageBoxBuilder box = access.queryFactory().store();
		nodeIdMap.values().stream().filter(n->n.getDegree(Direction.INCOMING, Links.TRACK)==0)
		.filter(n->n.getAttribute(Keys.trackID).isPresent())
		.map(il-> cache.get(il).fullTree())
		.map(annotation->new LineageTree(annotation))
		.forEach(tree -> box.add(tree, tc));
		try {
			box.run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




}
