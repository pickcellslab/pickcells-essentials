package org.pickcellslab.pickcells.tracking.interactions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.TraversalDefinition;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequence;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequencePointer;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.tracking.editor.LineageTree;
import org.pickcellslab.pickcells.tracking.features.TrackingDescriptors;

@Data(typeId="LineageBranch")
@SameScopeAs(Image.class)
@GraphSequence(LineageBranchHints.class)
public class LineageBranch extends DataNode implements GraphSequencePointer{

	//private static final Logger log = LoggerFactory.getLogger(LineageBranch.class);


	static final String toStartLink = "STARTS_WITH", toEndLink = "ENDS_WITH",
			daughterLink="DAUGHTER", sisterLink="SISTER", belongsToLink = "BELONGS_TO";
	static final AKey<Integer> branchIdKey = AKey.get("Branch ID", int.class);
	static final AKey<Integer> branchDepthKey = AKey.get("Branch Depth", int.class);

	//static final AKey<Integer> roundTrackVisitsKey = AKey.get("Tracks visited by Branch", int.class);
	//static final AKey<Integer> roundBranchVisitsKey = AKey.get("Branches visited by Branch", int.class);




	private LineageBranch(){/*db use*/}


	private LineageBranch(NodeItem first, int depth) {		
		Objects.requireNonNull(first, "first is null");		

		final int branchID = first.getAttribute(DataItem.idKey).get();
		this.setAttribute(branchIdKey, branchID);
		this.setAttribute(Keys.trackID, first.getAttribute(Keys.trackID).get());
		this.setAttribute(branchDepthKey, depth);

		NodeItem last = first;
		while(last!=null) {
			final NodeItem n = last;
			n.setAttribute(branchIdKey, branchID);
			if(n.getDegree(Direction.OUTGOING, Links.TRACK)!=1) { // either dividing or leaf
				last = n;
				break;
			}
			else {
				last = n.getLinks(Direction.OUTGOING, Links.TRACK).findFirst().get().target();
			}
		}

		new DataLink(toStartLink, this, first, true);		
		new DataLink(toEndLink, this, last, true);

		/*
		this.setAttribute(AKey.get("Duration", int.class), 
				last.getAttribute(ImageLocated.frameKey).orElse(0)
				- first.getAttribute(ImageLocated.frameKey).orElse(0)); 
		 */
		// Add descriptors
		final List<Link> linkList = new ArrayList<>();
		this.visitLinks(l->linkList.add(l));
		TrackingDescriptors.setDescriptors(this, linkList);
				

	}



	public boolean hasUpstreamBranches() {
		return first().getDegree(Direction.INCOMING, Links.TRACK) != 0;
	}


	public boolean hasDownstreamBranches() {
		return last().getDegree(Direction.OUTGOING, Links.TRACK) != 0;
	}





	/**
	 * Creates {@link LineageBranch}es for the given {@link LineageTree} and annotate
	 * nodes with the branch id they belong to. This call will also create SISTER and DAUGHTER
	 * relationships between the branches of the given LineageTree
	 * @param tree
	 * @return A List of all the branches created on the given {@link LineageTree} sorted by depth.
	 */
	public static List<LineageBranch> getBranches(LineageTree tree){
		final List<LineageBranch> branches = new ArrayList<>();

		final LinkedList<LineageBranch> queue = new LinkedList<>();
		queue.add(new LineageBranch(tree.getRoot(), 0));
		while(!queue.isEmpty()) {
			final LineageBranch b = queue.poll();
			if(b.getDuration()!=0) {
				final NodeItem leafOrDiv = b.last();
				if(leafOrDiv.getDegree(Direction.OUTGOING, Links.TRACK)==2) {
					leafOrDiv.getLinks(Direction.OUTGOING, Links.TRACK)
					.forEach(l->queue.add(new LineageBranch(l.target(), (int) b.attributes.get(branchDepthKey)+1)));
				
					// Create relationships
					final LineageBranch d1 = queue.get(queue.size()-1);
					final LineageBranch d2 = queue.get(queue.size()-2);
					new DataLink(daughterLink, b, d1, true);
					new DataLink(daughterLink, b, d2, true);
					new DataLink(sisterLink, d1, d2, true);
				}
			}
			branches.add(b);
			new DataLink(belongsToLink, b, tree, true);
		}

		return branches;
	}





	public void visitAndPropagateDownStream(Consumer<LineageBranch> visitor) {
		visitor.accept(this);
		directDownstreamBranches().forEach(branch -> branch.visitAndPropagateDownStream(visitor));
	}

	public <E> void visitAndPropagateDownStream(BiFunction<LineageBranch, E, E> visitor, E first) {
		final E out = visitor.apply(this, first);
		directDownstreamBranches().forEach(branch -> branch.visitAndPropagateDownStream(visitor, out));
	}


	private Stream<LineageBranch> directDownstreamBranches(){

		final NodeItem leafOrDiv = last();	
		if(leafOrDiv.getDegree(Direction.OUTGOING, Links.TRACK)==2) {
			return
					leafOrDiv.getLinks(Direction.OUTGOING, Links.TRACK)
					.map(l->(LineageBranch) l.target()
							.getLinks(Direction.INCOMING, toStartLink)
							.findFirst().get()
							.source());
		}
		else
			return Stream.empty();
	}




	public void visitNodes(Consumer<NodeItem> visitor) {
		final NodeItem first = first();
		final NodeItem last = last();
		NodeItem next = first;
		while(next!=null) {
			visitor.accept(next);
			next = next.getLinks(Direction.OUTGOING, Links.TRACK).findFirst().get().target();
			if(next == last) {
				visitor.accept(next);
				break;
			}
		}
	}

	public void visitLinks(Consumer<Link> visitor) {
		final NodeItem first = first();
		final NodeItem last = last();
		Link next = first.getLinks(Direction.OUTGOING, Links.TRACK).findFirst().orElse(null);
		while(next!=null) {
			visitor.accept(next);
			if(next.target() == last)
				break;
			else
				next = next.target().getLinks(Direction.OUTGOING, Links.TRACK).findFirst().orElse(null);
		}
	}



	public NodeItem first() {
		return this.typeMap.get(toStartLink).iterator().next().target();
	}

	public NodeItem last() {
		return this.typeMap.get(toEndLink).iterator().next().target();
	}	
	
	
	

	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(idKey, TrackingDescriptors.duration, branchIdKey, Keys.trackID);
	}


	public int getTrackId() {
		return (int) attributes.get(Keys.trackID);
	}

	public int getBranchId() {
		return (int) attributes.get(branchIdKey);
	}

	public int getDuration() {
		return (int) attributes.get(TrackingDescriptors.duration);
	}



	@Override
	public void consume(DataAccess access, Consumer<Path<NodeItem, Link>> action) throws DataAccessException {
		access.queryFactory().readPaths(this.toString()).with(Actions.consumerAction(action, null)).inOneSet()
		.useTraversal(
				TraversalDefinition.newDefinition()
				.startFrom(DataRegistry.typeIdFor(LineageBranch.class)).having(DataItem.idKey, this.getAttribute(DataItem.idKey).get())
				.fromDepth(1).toMaxDepth()
				.traverseLink(Links.TRACK, Direction.OUTGOING)
				.and(LineageBranch.toStartLink, Direction.OUTGOING)
				.withOneEvaluation(
						F.connections()
						.includeLinks(Direction.INCOMING, LineageBranch.toEndLink)
						.whereAdjacent()
						.include(LineageBranch.class)
						.with(DataItem.idKey, Op.Logical.EQUALS, getAttribute(DataItem.idKey).get())
						.create().count().equalsTo(0l)
						,
						Decision.INCLUDE_AND_CONTINUE, Decision.INCLUDE_AND_STOP)
				.build()
				)
		.run();

	}

}
