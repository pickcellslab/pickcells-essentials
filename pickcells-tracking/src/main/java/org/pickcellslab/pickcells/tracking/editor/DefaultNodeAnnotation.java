package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;

public class DefaultNodeAnnotation implements NodeAnnotation {

	private final TreeAnnotationCache cache;
	private final ImageLocated data;

	private MorphableAnnotation morphing;
	private AnnotationStatus status;


	public DefaultNodeAnnotation(NodeItem node, TreeAnnotationCache cache) {
		this.cache = cache;
		data = (ImageLocated) node;		
		status = getDeselectedStatus();
		morphing = this;
	}

	private AnnotationStatus getDeselectedStatus() {
		//get Status
		if(cache.getSelectionMode()==TrackSelectionMode.LINKS){

			if(data.getDegree(Direction.OUTGOING, Links.TRACK)>1)
				return NodeStatus.NODE_DIVIDING;
			else if(data.getDegree(Direction.OUTGOING, Links.TRACK)==0 && data.getDegree(Direction.INCOMING, Links.TRACK)==0)
				return NodeStatus.NODE_SINGLE;
			else if(data.getDegree(Direction.OUTGOING, Links.TRACK)==0)
				return NodeStatus.NODE_LEAF;
			else if(data.getDegree(Direction.INCOMING, Links.TRACK)==0)
				return NodeStatus.NODE_ROOT;

		}

		return AnnotationStatus.DESELECTED;
	}

	@Override
	public Stream<Link> links() {
		return data.links();
	}


	@Override
	public Stream<AKey<?>> properties() {
		return data.getValidAttributeKeys();
	}

	@Override
	public <E> E getProperty(AKey<E> k) {
		return data.getAttribute(k).get();
	}

	@Override
	public AnnotationStatus status() {
		return status;
	}


	@Override
	public boolean isRoot() {
		return data.getDegree(Direction.INCOMING, Links.TRACK) == 0;
	}

	@Override
	public boolean isLeaf() {
		return data.getDegree(Direction.OUTGOING, Links.TRACK)==0;
	}

	@Override
	public boolean isDivision() {
		return data.getDegree(Direction.OUTGOING, Links.TRACK)==2;
	}

	@Override
	public boolean isInnerNode() {
		return data.getDegree(Direction.OUTGOING, Links.TRACK)==1 && data.getDegree(Direction.INCOMING, Links.TRACK)==1;
	}


	@Override
	public Stream<LinkAnnotation> upstreamLinks(int depth) {
		final List<LinkAnnotation> list = new ArrayList<>();
		int count = 0;
		Link l = data.getLinks(Direction.INCOMING, Links.TRACK).findAny().orElse(null);		
		while(l!=null && count<depth){
			list.add(cache.get(l));
			count++;
			l = l.source().getLinks(Direction.INCOMING, Links.TRACK).findAny().orElse(null);
		}
		return list.stream();
	}



	@Override
	public TreeAnnotation downStreamTree() {
		return new DefaultTreeAnnotation(cache, this, Direction.OUTGOING);
	}

	@Override
	public TreeAnnotation upstreamTree() {
		return new DefaultTreeAnnotation(cache, this, Direction.INCOMING);
	}


	@Override
	public DefaultTreeAnnotation fullTree() {
		return cache.getFullTree(this);
	}



	@Override
	public PathAnnotation upStreamPath() {
		return new DefaultPathAnnotation(cache, this, true);
	}



	
	ImageLocated data(){
		return data;
	}



	@Override
	public void toggleSelected(List<LinkAnnotation> linkList, List<NodeAnnotation> nodesList) {
		if(status!=AnnotationStatus.SELECTED){
			status = AnnotationStatus.SELECTED;
			nodesList.add(this);
			//System.out.println(this+" added to selection (DefaultNodeAnnotation)");
		}
		else{
			status = getDeselectedStatus();
			nodesList.remove(this);
			//System.out.println(this+" removed from list : "+nodesList.remove(this));
		}
	}


	@Override
	public String toString(){
		return data.toString();
	}

	@Override
	public Stream<AKey<?>> minimal() {
		return data.minimal();
	}

	@Override
	public String declaredType() {
		return data.declaredType();
	}

	@Override
	public Stream<AKey<?>> getValidAttributeKeys() {
		return data.getValidAttributeKeys();
	}

	@Override
	public <T> Optional<T> getAttribute(AKey<T> key) {
		return data.getAttribute(key);
	}

	@Override
	public String typeId() {
		return data.typeId();
	}
	
	@Override
	public String representedType() {
		return data.typeId();
	}
	

	@Override
	public int getDegree(Direction direction, String linkType) {
		return data.getDegree(direction, linkType);
	}

	@Override
	public Stream<Link> getLinks(Direction direction, String... linkTypes) {
		return data.getLinks(direction, linkTypes);
	}



	@Override
	public Collection<Link> removeLink(Predicate<Link> predicate, boolean updateNeighbour) {
		throw new RuntimeException("Forbidden operation");
	}



	@Override
	public boolean addLink(Link link) {
		throw new RuntimeException("Forbidden operation");
	}

	@Override
	public boolean removeLink(Link link, boolean updateNeighbour) {
		throw new RuntimeException("Forbidden operation");
	}


	@Override
	public Link addOutgoing(String type, NodeItem target) {
		throw new RuntimeException("Forbidden operation");
	}

	@Override
	public Link addIncoming(String type, NodeItem target) {
		throw new RuntimeException("Forbidden operation");
	}

	@Override
	public <T> void setAttribute(AKey<T> key, T v) {
		throw new RuntimeException("Forbidden operation");
	}

	@Override
	public void removeAttribute(AKey<?> key) {
		throw new RuntimeException("Forbidden operation");
	}

	public void updateMorphing() {
		if(cache.getSelectionMode() == TrackSelectionMode.TREES){
			final TreeAnnotation tree = cache.getFullTree(this);
			morphing = tree;
		}
		else if(cache.getSelectionMode() == TrackSelectionMode.BRANCHES){
			final PathAnnotation path = cache.getPath(this);
			morphing = path;
		}
		else
			morphing = this;
		
		status = getDeselectedStatus();
	}


	public MorphableAnnotation getCurrentMorphing(){
		checkMorphing();
		return morphing;
	}

	
	
	private void checkMorphing() {
		boolean update = false;
		switch(cache.getSelectionMode()){
		case BRANCHES: update = !(morphing instanceof PathAnnotation);
			break;
		case LINKS:	update = morphing != this;
			break;
		case TREES:	update = !(morphing instanceof TreeAnnotation);
			break;
		
		}
		if(update)
			updateMorphing();
	}


}
