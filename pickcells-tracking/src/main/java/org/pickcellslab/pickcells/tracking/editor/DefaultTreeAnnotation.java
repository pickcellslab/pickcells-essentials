package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyDimension;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.Traversal;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.datamodel.tools.Traversers.BreadthFirstWithConstraints;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;
import org.pickcellslab.pickcells.tracking.features.TrackingDescriptors;

public class DefaultTreeAnnotation implements TreeAnnotation {

	
	private static String type = "Lineage Tree";
	
	protected static AKey<Integer> divNumber = AKey.get("Number of Divisions", Integer.class);
	protected static AKey<Integer> durMax = AKey.get("Max Duration", Integer.class);
	protected static AKey<Integer> durMin = AKey.get("Min Duration", Integer.class);
	protected static AKey<Integer> durTotal = AKey.get("Total Duration", Integer.class);
	
	
	protected static List<AKey<?>> possibleKeys = new ArrayList<>();
	static{
		possibleKeys.add(divNumber);
		possibleKeys.add(durMax);
		possibleKeys.add(durMin);
		possibleKeys.add(durTotal);
		possibleKeys.add(Keys.trackID);
		possibleKeys.add(TrackingDescriptors.msd);
		possibleKeys.add(TrackingDescriptors.dirChange);
		possibleKeys.add(TrackingDescriptors.velocity);
		possibleKeys.add(TrackingDescriptors.totalDis);
	}
	
	
	private final TreeAnnotationCache cache;
	
	private final DefaultNodeAnnotation root;
	private final DefaultNodeAnnotation origin;
	private final Direction dir;

	private final Set<NodeItem> nodes;

	private AnnotationStatus status = AnnotationStatus.DESELECTED;
	
	
	private Map<AKey<?>,Object> properties = new HashMap<>();
	
	
	public DefaultTreeAnnotation(TreeAnnotationCache cache, DefaultNodeAnnotation node, Direction dir) {

		this.cache = cache;
		this.dir = dir;
		this.origin = node;
		
		if(dir==Direction.OUTGOING || node.isRoot())
			root = node;
		else 
			root = (DefaultNodeAnnotation) new DefaultPathAnnotation(cache, node, true).first();		

		final DefaultTraversal<NodeItem,Link> tr = createBFSFromOrigin().traverse();
		nodes = tr.nodes();
		nodes.add(node.data());
		computeFeatures(tr);
		//System.out.println("DefaultTreeAnnotation created : "+toString());
	}

	
	private void computeFeatures(DefaultTraversal<NodeItem,Link> tr){
		
		int minLeafFrame = Integer.MAX_VALUE, maxLeafFrame = 0, div = 0;
		
		for(NodeItem n : nodes){
			final DefaultNodeAnnotation na = cache.get(n);
			if(na.isDivision())
				div++;
			if(na.isLeaf()){
				minLeafFrame = FastMath.min(minLeafFrame, na.data().frame());
				maxLeafFrame = FastMath.max(maxLeafFrame, na.data().frame());
			}
		}		
		properties.put(divNumber, div);
		properties.put(durMax, maxLeafFrame-root.data().frame());
		properties.put(durMin, minLeafFrame-root.data().frame());
		properties.put(Keys.trackID, root.data().getAttribute(Keys.trackID).orElse(null));
		
		// Add tracking descriptors
		final List<Link> linkList = new ArrayList<>(tr.edges());
		Collections.sort(linkList, (l1,l2)->l1.source().getAttribute(ImageLocated.frameKey).get()
				.compareTo(l2.source().getAttribute(ImageLocated.frameKey).get()));
		TrackingDescriptors.getDescriptors(properties, linkList);
		//remove netDistance which does not make much sense for tree
		properties.remove(TrackingDescriptors.netDis);
		
		// change the key for total duration
		properties.put(durTotal, properties.remove(TrackingDescriptors.duration));
		
		/*
		System.out.println("===============================");
		System.out.println("DefaultTreeAnnotation created");
		System.out.println("===============================");
		System.out.println("Number of nodes : "+nodes.size());
		System.out.println("Number of links : "+linkList.size());
		if(!linkList.isEmpty()) {
			System.out.println("First frame : "+linkList.get(0).source().getAttribute(ImageLocated.frameKey).get());
			System.out.println("Last frame : "+linkList.get(linkList.size()-1).target().getAttribute(ImageLocated.frameKey).get());
			System.out.println("** Last Link Properties **");
			linkList.get(linkList.size()-1).getValidAttributeKeys()
			.forEach(k->System.out.println(k.name+" : "+linkList.get(linkList.size()-1).getAttribute(k).get()));
		}
		
		System.out.println("** Tree Properties **");
		properties.forEach((k,v)->System.out.println(k.name+" : "+Objects.toString(v)));
		*/
		
	}
	
	
	
	@Override
	public Stream<AKey<?>> properties() {
		return possibleKeys.stream();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> E getProperty(AKey<E> k) {
		return (E) properties.get(k);
	}

	@Override
	public AnnotationStatus status() {
		return status;
	}



	@Override
	public DefaultNodeAnnotation getRoot() {
		return root;
	}

	@Override
	public Stream<NodeAnnotation> divisions() {
		return nodes.stream().map(n->(NodeAnnotation)cache.get(n)).filter(na->na.isDivision());
	}

	@Override
	public Stream<NodeAnnotation> leaves() {
		return nodes.stream().map(n->(NodeAnnotation)cache.get(n)).filter(na->na.isLeaf());
	}

	@Override
	public  BreadthFirstWithConstraints breadthFirst() {
		return createBFSFromRoot();
	}
	
	private  BreadthFirstWithConstraints createBFSFromRoot() {
		

		if(dir == Direction.INCOMING){ // upstream of origin
			return Traversers.breadthfirst(
					root.data(),
					Traversers.newConstraints().fromMinDepth().toMaxDepth()
					.traverseLink(Links.TRACK, Direction.OUTGOING)
					.withOneEvaluation(F.select(DataItem.idKey).equalsTo(origin.getAttribute(DataItem.idKey).get()),
							Decision.INCLUDE_AND_STOP,
							Decision.INCLUDE_AND_CONTINUE)
					);
		}
		else{// whole tree or downstream of origin
			return Traversers.breadthfirst(
					origin.data(),
					Traversers.newConstraints().fromMinDepth().toMaxDepth()
					.traverseLink(Links.TRACK, dir)
					.includeAllNodes()
					);
		}
		
	}






	private BreadthFirstWithConstraints createBFSFromOrigin(){
		
		if(dir == Direction.INCOMING){
			return Traversers.breadthfirst(
					origin.data(),
					Traversers.newConstraints().fromMinDepth().toMaxDepth()
					.traverseLink(Links.TRACK, Direction.BOTH)
					.withOneEvaluation(F.select(DataItem.idKey).equalsTo(origin.getAttribute(DataItem.idKey).get()),
							Decision.INCLUDE_AND_STOP,
							Decision.INCLUDE_AND_CONTINUE)
					);
		}
		else{
			return Traversers.breadthfirst(
					origin.data(),
					Traversers.newConstraints().fromMinDepth().toMaxDepth()
					.traverseLink(Links.TRACK, dir)
					.includeAllNodes()
					);
		}
	}







	@Override
	public MorphableAnnotation getCurrentMorphing() {
		return this;
	}






	@Override
	public void toggleSelected(List<LinkAnnotation> linkList, List<NodeAnnotation> nodesList) {
		if(status == AnnotationStatus.SELECTED){
			status = AnnotationStatus.DESELECTED;
			final Traversal<NodeItem, Link> tr = this.breadthFirst().traverse();
			tr.nodes().forEach(n->nodesList.remove(cache.get(n)));
			tr.edges().forEach(l->linkList.remove(cache.get(l)));
			//System.out.println(this+" deselected!");
			//System.out.println(" Number of selected nodes : "+nodesList.size());
			//System.out.println(" Number of selected links : "+linkList.size());
		}
		else{
			status = AnnotationStatus.SELECTED;
			final Traversal<NodeItem, Link> tr = this.breadthFirst().traverse();
			tr.nodes().forEach(n->nodesList.add(cache.get(n)));
			tr.edges().forEach(l->linkList.add(cache.get(l)));
			//System.out.println(this+" selected!");
			//System.out.println(" Number of selected nodes : "+nodesList.size());
			//System.out.println(" Number of selected links : "+linkList.size());
		}
	}



	@Override
	public String toString(){
		return "Lineage Tree "+properties.get(Keys.trackID);
	}
	
	
	

	public static String staticType() {
		return type;
	}






	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Dimension<? super Annotation, ?>> possibleDimensions() {
		return (List)possibleKeys.stream().map(k->(Dimension)new KeyDimension(k, k.name)).collect(Collectors.toList());
	}


	@Override
	public String representedType() {
		return "Lineage Tree";
	}


	

}
