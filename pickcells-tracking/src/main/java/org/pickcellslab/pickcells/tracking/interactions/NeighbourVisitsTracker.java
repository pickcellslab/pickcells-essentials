package org.pickcellslab.pickcells.tracking.interactions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Sets;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;

class NeighbourVisitsTracker {

	private final Map<Integer, Integer> since;
	private final Map<Integer, Mean> time;
	private final String adjacentLink;
	private final AKey<Integer> idKey;

	private int lastFrame;

	public NeighbourVisitsTracker(String adjacentLink, AKey<Integer> idKey) {
		this(new HashMap<>(), new HashMap<>(), adjacentLink, idKey);
	}


	private NeighbourVisitsTracker(
			Map<Integer, Integer> since,
			Map<Integer, Mean> time,
			String adjacentLink,
			AKey<Integer> idKey) {
		Objects.requireNonNull(adjacentLink, "The provided type of links is null");
		Objects.requireNonNull(idKey, "The provided AKey for ids is null");
		this.adjacentLink = adjacentLink;
		this.idKey = idKey;
		this.since = new HashMap<>(since);
		this.time = new HashMap<>(time);
	}


	public void addNode(NodeItem node) {

		lastFrame = node.getAttribute(ImageLocated.frameKey).get();

		final Set<Integer> currentNeighbours = new HashSet<>();


		node.getLinks(Direction.BOTH, adjacentLink)
		.map(l->l.source()==node ? l.target() : l.source())
		.forEach(n->{

			final Integer id = n.getAttribute(idKey).orElse(-1);
			currentNeighbours.add(id);

			// Have we already visited this neighbour ?
			if(!time.containsKey(id)) {
				initVisits(id, n.getAttribute(ImageLocated.frameKey).orElse(0));
			}
			else {
				updateVisits(id, n.getAttribute(ImageLocated.frameKey).orElse(0));
			}			
		});

		checkLostNeighbours(currentNeighbours, node.getAttribute(ImageLocated.frameKey).orElse(0));

	}



	private void initVisits(Integer id, Integer frame) {
		//visits.put(id, new MutableInt(1));
		since.put(id, frame);
		time.put(id, new Mean());
	}




	private void updateVisits(Integer id, Integer currentFrame) {
		// Were we visiting this neighbour in previous frame
		final Integer sinceFrame = since.get(id);
		if(sinceFrame == null) {
			since.put(id, currentFrame);
			if(time.get(id) == null)
				throw new RuntimeException("time is empty for id "+Objects.toString(id));
		}
	}


	private void checkLostNeighbours(Set<Integer> currentNeighbours, Integer frame) {
		final Set<Integer> lost = Sets.difference(since.keySet(), currentNeighbours);
		for(Integer l : lost) {
			final Mean mean = time.get(l);
			final Integer s = since.remove(l); // Could have been removed already?
			//if(s!=null)
			System.out.println("NeighbourVisitsTracker will increment Mean : "+ frame +" ; "+ s);
			mean.increment(frame - s);
		}
	}


	public NeighbourVisitsTracker duplicate() {
		return new NeighbourVisitsTracker(since, time, adjacentLink, idKey);
	}


	public int totalNumberOfVisitedNeighbours() {
		return time.size();
	}


	public NeighbourVisitsResult result() {
		return new NeighbourVisitsResult(since, time, lastFrame);
	}





	class NeighbourVisitsResult{

		private final Map<Integer, Integer> visits;
		private final Map<Integer, Double> time;

		private NeighbourVisitsResult(Map<Integer, Integer> since, Map<Integer, Mean> time, int lastFrame) {
			this.visits = new HashMap<>();
			this.time = new HashMap<>();
			time.forEach((k,m)->{
				final Integer seenSince = since.get(k);
				Mean mean = m;
				if(seenSince!=null) {
					mean = m.copy();
					mean.increment(lastFrame-seenSince);
				}
				this.time.put(k, mean.getResult());
				visits.put(k, (int)mean.getN());
			});

		}


		public int totalNumberOfVisitedNeighbours() {
			return visits.size();
		}


		public int numberOfVisits(int id) {
			return visits.get(id);
		}

		public double averageVisitTime(int id) {
			return time.get(id);
		}


		public Stream<Integer> getVisitedIds() {
			return visits.keySet().stream();
		}



	}


}
