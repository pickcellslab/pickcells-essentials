package org.pickcellslab.pickcells.tracking.interactions;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyDimension;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.StepTraverser;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.sequences.GraphSequenceHints;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;

public class LineageBranchHints implements GraphSequenceHints{

	@Override
	public List<MetaQueryable> encounteredQueryable(DataAccess access, String type) {

		final List<MetaQueryable> list = new ArrayList<>(2);

		try{
			//Look for lineage branch in the database with the specified type

			//Get the root
			final ImageLocated il =
					access.queryFactory().regenerate(DataRegistry.typeIdFor(LineageBranch.class)).toDepth(1)
					.traverseLink(LineageBranch.toEndLink, Direction.OUTGOING).includeAllNodes().regenerateAllKeys()
					.doNotGetAll().useFilter(P.isNodeWithDeclaredType(type)).run().getOneDependency(ImageLocated.class).orElse(null);
			if(il!=null){
				list.add(access.metaModel().getMetaModel(il));
			}	
			//Add links (which are always tracks)
			list.add(access.metaModel().getMetaLinks(Links.TRACK).get(0));
		}
		catch(DataAccessException e){

		}

		return list;
	}

	@Override
	public Dimension<DataItem, ?> generatedDimension() {
		return new KeyDimension<>(ImageLocated.frameKey, "Time");
	}


	@Override
	public void consumeFromDatabaseItem(NodeItem dbItem, Consumer<StepTraverser<NodeItem, Link>> action) {
		/*
		final TraverserConstraints tc = Traversers.newConstraints()
				.fromDepth(1).toMaxDepth()
				.traverseLink(Links.TRACK, Direction.OUTGOING)
				.and(LineageBranch.toRootLink, Direction.OUTGOING)
				.withOneEvaluation(
						P.test( 
								F.streamLinks()
								.next(
										F.filter(
												P.sourcePasses(
														P.keyTest(DataItem.idKey, 
																Op.Logical.EQUALS,
																dbItem.getAttribute(DataItem.idKey).get())
														)
												)

										.next(F.count()))
								, 0), 
						Decision.INCLUDE_AND_CONTINUE, Decision.INCLUDE_AND_STOP);
		 */

		final TraverserConstraints tc = Traversers.newConstraints()
				.fromDepth(1).toMaxDepth()
				.traverseLink(Links.TRACK, Direction.OUTGOING)
				.and(LineageBranch.toStartLink, Direction.OUTGOING)
				.withOneEvaluation(
						F.connections()
						.includeLinks(Direction.INCOMING, LineageBranch.toEndLink)
						.whereAdjacent()
						.include(LineageBranch.class)
						.with(DataItem.idKey, Op.Logical.EQUALS, dbItem.getAttribute(DataItem.idKey).get())
						.create().count().equalsTo(0l),
						Decision.INCLUDE_AND_CONTINUE, Decision.INCLUDE_AND_STOP);

		action.accept(Traversers.breadthfirst(dbItem, tc));	
	}

}
