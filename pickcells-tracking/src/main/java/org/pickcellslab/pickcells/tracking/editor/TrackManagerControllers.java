package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEventModel.SelectionMode;

public class TrackManagerControllers {

	
	public static void createControls(TrackManager mgr, ImageDisplay view){
		
		view.getMouseModel().setSelectionMode(SelectionMode.MULTIPLE);

		// Register Deleltion
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl D"), "Delete", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.setSelectedAsDeleted();		
				System.out.println("TrackManagerControllers -> TrackManager notified for deletion");
			}			
		});
		
		
		view.getMouseModel().setSelectionMode(SelectionMode.MULTIPLE);

		// Register Link Creation
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl L"), "Link", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.linkSelected();		
				System.out.println("TrackManagerControllers -> TrackManager notified for linkage");
			}			
		});
		
		
	}
	
	
}
