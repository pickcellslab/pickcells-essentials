package org.pickcellslab.pickcells.tracking.interactions;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.Sets;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.ActivationListener;
import org.pickcellslab.pickcells.api.app.modules.MenuEntry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Nucleus;
import org.pickcellslab.pickcells.tracking.editor.LineageTree;
import org.pickcellslab.pickcells.tracking.interactions.NeighbourVisitsTracker.NeighbourVisitsResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.managers.notification.NotificationManager;

@Module
public class NeighbourExchange implements MenuEntry{

	private static final Logger log = LoggerFactory.getLogger(NeighbourExchange.class);


	private final DataAccess access;
	private final UITheme theme;
	

	private static final String adjacentLink = "All "+Links.ADJACENT_TO;
	private static final String visitsLink = "VISITS";
	private static final String visitsTrackLink = "VISITS_TRACK";

	static final AKey<Integer> visitedTracks = AKey.get("Unique Tracks Visited", int.class);	
	static final AKey<Integer> visitedBranches = AKey.get("Unique Branches Visited", int.class);

	static final AKey<Integer> visitsNumber = AKey.get("Number of Visits", int.class);
	static final AKey<Double> visitsTime = AKey.get("Average Visit Time", double.class);

	static final AKey<Integer> symDifBranch = AKey.get("Distinct branch visits", int.class);
	static final AKey<Integer> itrsctBranch = AKey.get("Branch visits in common", int.class);

	static final AKey<Integer> symDifTree = AKey.get("Distinct track visits", int.class);
	static final AKey<Integer> itrsctTree = AKey.get("Track visits in common", int.class);

	public NeighbourExchange(DataAccess access, UITheme theme) {
		this.access = access;
		this.theme = theme;
	}


	@Override
	public void launch() throws Exception {

		// TODO check requirements and optimise code notably by avoiding creating so many lambdas

		//-------------------------------------------------------------------------//
		//          First Pass
		//-------------------------------------------------------------------------//

		final AKey<Integer> nNeighboursKey = AKey.get("Number of Neighbours", int.class);
		final AKey<Integer> nLossKey = AKey.get("Number of Neighbours Lost", int.class);
		final AKey<Integer> nGainKey = AKey.get("Number of Neighbours Gained", int.class);



		// Collect all trees
		final Map<Integer, LineageTree> treeMap = new HashMap<>();
		access.queryFactory().regenerate(DataRegistry.typeIdFor(LineageTree.class))
		.toDepth(0).traverseAllLinks().includeAllNodes().regenerateAllKeys().getAll()
		.getAllItemsFor(LineageTree.class)
		.forEach(tree->treeMap.put(tree.getAttribute(Keys.trackID).get(), tree));



		for(LineageTree t : treeMap.values()) {

			final int currentTrackId = t.getAttribute(Keys.trackID).get();
			final RegeneratedItems items = getFullTree(t.getAttribute(DataItem.idKey).get(), currentTrackId);

			log.debug("full tree regenerated with Track Id = "+currentTrackId);

			log.debug("tree contains : "+ items.getDependencies(Nucleus.class).count());

			items.getDependencies(Nucleus.class)
			.filter(n -> n!=null && (n.getAttribute(Keys.trackID).orElse(-1).equals(currentTrackId)))
			.forEach(currentTimeNucleus->{

				// Number of neighbours
				final int neighbours = currentTimeNucleus.getDegree(Direction.BOTH, adjacentLink);
				currentTimeNucleus.setAttribute(nNeighboursKey, neighbours);

				// Number of loss and gains
				currentTimeNucleus.getLinks(Direction.INCOMING, Links.TRACK).findAny().ifPresent(trackLink->{

					final NodeItem previousTimeNucleus = trackLink.source();
					final Set<Integer> previous =
							previousTimeNucleus.getLinks(Direction.BOTH, adjacentLink)
							.map(neighbourLink-> neighbourLink.source()==previousTimeNucleus ? 
									neighbourLink.target().getAttribute(Keys.trackID).orElse(-1): neighbourLink.source().getAttribute(Keys.trackID).orElse(-1))
							.collect(Collectors.toSet());
					final Set<Integer> current =
							currentTimeNucleus.getLinks(Direction.BOTH, adjacentLink)
							.map(neighbourLink-> neighbourLink.source()==currentTimeNucleus ? 
									neighbourLink.target().getAttribute(Keys.trackID).orElse(-1): neighbourLink.source().getAttribute(Keys.trackID).orElse(-1))
							.collect(Collectors.toSet());

					final int nLoss = Sets.difference(previous, current).size();
					final int nGain = Sets.difference(current, previous).size();

					trackLink.setAttribute(nLossKey, nLoss);
					trackLink.setAttribute(nGainKey, nGain);


				});

			});

			log.debug("Basic nuclei features done");


			// Now create branches

			final LineageTree fullTree = items.getOneTarget(LineageTree.class).get();
			final List<LineageBranch> branches = LineageBranch.getBranches(fullTree);

			log.debug("Branches created");


			// Compute tree features and set number of visited trees (trackIds) at each time point
			final NeighbourVisitsTracker treeVisits = new NeighbourVisitsTracker(adjacentLink, Keys.trackID);
			final BiFunction<LineageBranch, NeighbourVisitsTracker, NeighbourVisitsTracker> visitor = (b,nv)->{


				log.debug("Visiting Branch "+b.getBranchId());

				// Set Node feature
				NeighbourVisitsTracker visitCounts = nv.duplicate();				
				// Update the treeVisits
				b.visitNodes(n->{

					///log.debug("Visiting Node "+n);

					visitCounts.addNode(n);
					n.setAttribute(visitedTracks, visitCounts.totalNumberOfVisitedNeighbours());

					treeVisits.addNode(n);

				});

				return visitCounts;

			};

			final LineageBranch firstBranch = branches.get(0);
			final NeighbourVisitsTracker tpVisits = new NeighbourVisitsTracker(adjacentLink, Keys.trackID);
			firstBranch.visitAndPropagateDownStream(visitor, tpVisits);

			log.debug("Neighbour visits set for tree");

			// Set the number of visited trees as an attribute in tree
			t.setAttribute(visitedTracks, treeVisits.totalNumberOfVisitedNeighbours());

			// Now update the graph of interactions between trees
			final NeighbourVisitsResult treeVisitsResult = treeVisits.result();
			treeVisitsResult.getVisitedIds().forEach(trackId->{
				final LineageTree toLink = treeMap.get(trackId);
				if(toLink != null) {// null if interaction id is -1 when a neighbour cell is not part of a tree

					// Do not add the link if toLink already has visitLinks (to avoid redundancies)
					if(toLink.getDegree(Direction.OUTGOING, visitsLink)==0) {
						final Link l = new DataLink(visitsLink, t, toLink, true);
						l.setAttribute(visitsTime, treeVisitsResult.averageVisitTime(trackId));
						l.setAttribute(visitsNumber, treeVisitsResult.numberOfVisits(trackId));
					}
				}
			});

			log.debug(fullTree+" done");

			// 3- Store the tree with its new branches
			access.queryFactory().store().add(fullTree).run();


		}// END of Iteration through Lineage trees


		log.debug("Starting to store the graph of tree interactions");

		// Save the graph of interactions between trees
		final StorageBoxBuilder box = access.queryFactory().store();
		final TraverserConstraints tc = 
				Traversers.newConstraints().fromMinDepth().toDepth(1)
				.traverseLink(visitsLink, Direction.BOTH)
				.includeAllNodes();
		for(LineageTree t : treeMap.values()) {
			box.add(t,tc);
		}

		box.run();



		//-------------------------------------------------------------------------//
		//          Second Pass
		//-------------------------------------------------------------------------//


		// 2- Create the graph of interaction between branches

		log.debug("SECOND PASS");

		// Restore all branches which will use for the graph of interactions

		// Create a Map for branches using their branchIds as keys
		final Map<Integer, LineageBranch> branchMap = 
				access.queryFactory().regenerate(DataRegistry.typeIdFor(LineageBranch.class))
				.toDepth(1) // Include SISTER - DAUGHTER linkTypes
				.traverseLink(LineageBranch.daughterLink, Direction.BOTH)
				.and(LineageBranch.sisterLink, Direction.BOTH)
				.includeAllNodes().regenerateAllKeys().getAll()
				.getAllItemsFor(LineageBranch.class)
				.collect(Collectors.toMap(b->b.getAttribute(LineageBranch.branchIdKey).get(), b->b));




		for(LineageTree t : treeMap.values()) {

			final int currentTrackId = t.getAttribute(Keys.trackID).get();
			final RegeneratedItems items = getFullTreeWithBranches(t.getAttribute(DataItem.idKey).get(), currentTrackId);

			log.debug("full tree regenerated with Track Id = "+currentTrackId);

			log.debug("tree contains : "+ items.getDependencies(Nucleus.class).count());


			items.getDependencies(LineageBranch.class)
			.forEach(branchWithNuclei->{


				// Write to nuclei the number of unique branches visited
				final NeighbourVisitsTracker branchVisitsTracker = new NeighbourVisitsTracker(adjacentLink, LineageBranch.branchIdKey);
				// And also keep track of the unique trees visited by each branch
				final NeighbourVisitsTracker treeVisitsTracker = new NeighbourVisitsTracker(adjacentLink, Keys.trackID);
				branchWithNuclei.visitNodes(n->{
					treeVisitsTracker.addNode(n);
					branchVisitsTracker.addNode(n);
					n.setAttribute(visitedBranches, branchVisitsTracker.totalNumberOfVisitedNeighbours());
				});

				final LineageBranch branchForGraph = branchMap.get(branchWithNuclei.getBranchId());
				branchForGraph.setAttribute(visitedBranches, branchVisitsTracker.totalNumberOfVisitedNeighbours());

				// Now create links between branches to store how branches interact with one another
				final NeighbourVisitsResult branchVisitsResult = branchVisitsTracker.result(); 
				branchVisitsResult.getVisitedIds().forEach(visitedId->{
					final LineageBranch visitedBranch = branchMap.get(visitedId);
					if(visitedBranch!=null) {
						// Do not add the link if visitedBranch already has visitLinks (to avoid redundancies)
						if(visitedBranch.getDegree(Direction.OUTGOING, visitsLink)==0) {
							final Link link = new DataLink(visitsLink, branchForGraph, visitedBranch, true);
							link.setAttribute(visitsNumber, branchVisitsResult.numberOfVisits(visitedId));
							link.setAttribute(visitsTime, branchVisitsResult.averageVisitTime(visitedId));
						}
					}
				});

				// And create links between branches and trees to store how branches interact with other trees
				final NeighbourVisitsResult treeVisitsResult = treeVisitsTracker.result();
				treeVisitsResult.getVisitedIds().forEach(visitedId->{
					final LineageTree visitedTree = treeMap.get(visitedId);
					if(visitedTree!=null) {
						// Do not add the link if visitedTree already has visitsTrackLink (to avoid redundancies)
						if(visitedTree.getDegree(Direction.OUTGOING, visitsTrackLink)==0) {
							final Link link = new DataLink(visitsTrackLink, branchForGraph, visitedTree, true);
							link.setAttribute(visitsNumber, treeVisitsResult.numberOfVisits(visitedId));
							link.setAttribute(visitsTime, treeVisitsResult.averageVisitTime(visitedId));
						}
					}
				});

			});// END of iteration through branches of the tree

			// Store Nuclei updates
			access.queryFactory().store().addAll(items.getDependencies(Nucleus.class).collect(Collectors.toList())).run();


		}// END of Iteration through Lineage trees



		// Finally compute symmetric dif and intersection for 
		// interactions between Daughter and Sister branches
		branchMap.values().forEach(branch->{		
			branch.getLinks(Direction.OUTGOING, LineageBranch.daughterLink)
			.forEach(l->computeSetFeatures(l));
			branch.getLinks(Direction.OUTGOING, LineageBranch.sisterLink).findFirst()
			.ifPresent(l->computeSetFeatures(l));			
		});





		log.debug("Starting to store the graph of lineage interactions");

		// Save the graph of interactions between branches and trees
		final StorageBoxBuilder box2 = access.queryFactory().store();
		final TraverserConstraints tc2 = 
				Traversers.newConstraints().fromMinDepth().toDepth(1)
				.traverseLink(visitsLink, Direction.BOTH)
				.and(LineageBranch.sisterLink, Direction.BOTH)
				.and(LineageBranch.daughterLink, Direction.BOTH)
				.and(visitsTrackLink, Direction.BOTH)
				.includeAllNodes();
		for(LineageBranch b : branchMap.values()) {
			box2.add(b,tc2);
		}

		box2.run();



		NotificationManager.showNotification("Neighbours Exchange Done", theme.icon(IconID.Misc.THUMBS_UP, 16));
	}




	//private final Function<Link,Integer> targetId = v->v.target().getAttribute(DataItem.idKey).get();

	private void computeSetFeatures(Link l) {
		// Visits on other branches
		final Set<Integer> sourceSet1 = getVisitedIds(l.source(), visitsLink);
		final Set<Integer> targetSet1 = getVisitedIds(l.target(), visitsLink);

		// Create Attributes
		l.setAttribute(symDifBranch, Sets.symDifference(sourceSet1, targetSet1).size());
		l.setAttribute(itrsctBranch, Sets.intersection(sourceSet1, targetSet1).size());

		// Visits on other branches
		final Set<Integer> sourceSet2 =getVisitedIds(l.source(), visitsTrackLink);
		final Set<Integer> targetSet2 =getVisitedIds(l.target(), visitsTrackLink);

		// Create Attributes
		l.setAttribute(symDifTree, Sets.symDifference(sourceSet2, targetSet2).size());
		l.setAttribute(itrsctTree, Sets.intersection(sourceSet2, targetSet2).size());

	}





	private Set<Integer> getVisitedIds(NodeItem node, String linkType) {
		return node.getLinks(Direction.BOTH, linkType) 
				.map(v->{
					final NodeItem adj = v.source()==node ? v.target() : v.source();
					return adj.getAttribute(DataItem.idKey).get();
				})
				.collect(Collectors.toSet());
	}




	private RegeneratedItems getFullTree(int treeId, int trackId) throws DataAccessException {
		return 
				access.queryFactory().regenerate(DataRegistry.typeIdFor(LineageTree.class))
				.toMaxDepth()
				.traverseLink(LineageTree.toPathLink, Direction.OUTGOING)
				.and(LineageTree.toRootLink, Direction.OUTGOING)
				.and(Links.TRACK, Direction.OUTGOING)
				.and(adjacentLink, Direction.BOTH)
				.withOneEvaluation(// Include neighbours at depth 1 but do not go beyond!
						P.isDeclaredType(DataRegistry.typeIdFor(Nucleus.class))
						.and(F.select(Keys.trackID).distinctFrom(trackId)), 
						// If node is nucleus and trackid is distinct from the tree trackID, then
						Decision.INCLUDE_AND_STOP, /* otherwise*/ Decision.INCLUDE_AND_CONTINUE)
				.regenerateAllKeys()
				.doNotGetAll()
				.useFilter(F.select(DataItem.idKey).equalsTo(treeId))
				.run();
	}


	private RegeneratedItems getFullTreeWithBranches(int treeId, int trackId) throws DataAccessException {
		return 
				access.queryFactory().regenerate(DataRegistry.typeIdFor(LineageTree.class))
				.toMaxDepth()
				.traverseLink(LineageTree.toPathLink, Direction.OUTGOING)
				.and(LineageTree.toRootLink, Direction.OUTGOING)
				.and(Links.TRACK, Direction.OUTGOING)
				.and(LineageBranch.toStartLink, Direction.INCOMING)
				.and(LineageBranch.toEndLink, Direction.OUTGOING)
				.and(adjacentLink, Direction.BOTH)
				.withOneEvaluation(// Include neighbours at depth 1 but do not go beyond!
						P.isDeclaredType(DataRegistry.typeIdFor(Nucleus.class))
						.and(F.select(Keys.trackID).distinctFrom(trackId)), 
						// If node is nucleus and trackid is distinct from the tree trackID, then
						Decision.INCLUDE_AND_STOP, /* otherwise*/ Decision.INCLUDE_AND_CONTINUE)
				.regenerateAllKeys()
				.doNotGetAll()
				.useFilter(F.select(DataItem.idKey).equalsTo(treeId))
				.run();
	}




	@Override
	public void start() {
		// Nothing to do

	}

	@Override
	public void registerListener(ActivationListener lst) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String name() {
		return "Neighbours Exchange";
	}

	@Override
	public String authors() {
		return "Guillaume Blin";
	}

	@Override
	public String licence() {
		return "GPLv3";
	}

	@Override
	public String description() {
		return "<HTML>Requires Lineage (TRACK links) and neighbourhood graph (ADJACENT TO links)"
				+ "<br>Computes Features reflecting neighbour exchanges.</HTML>";
	}

	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] menuPath() {
		return new String[] {"Utilities", "Neighbours Exchange"};
	}

}