package org.pickcellslab.pickcells.tracking.features;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.util.MathArrays;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;

public final class TrackingDescriptors {

	
	public static AKey<Integer> duration = AKey.get("Duration", Integer.class);
	public static AKey<Double> totalDis = AKey.get("Total Distance", Double.class);
	public static AKey<Double> netDis = AKey.get("Net Distance", Double.class);
	public static AKey<Double> msd = AKey.get("Mean Squared Displacement", Double.class);
	public static AKey<Double> velocity = AKey.get("Average Velocity", Double.class);
	
	public static AKey<Double> dirChange = AKey.get("Average Directional Change", Double.class);
	
	public static AKey<Double> distance = AKey.get("Travelled Distance", Double.class);
	public static AKey<Double> instantAngle = AKey.get("Instantaneous Angle", Double.class);
	
	
	private TrackingDescriptors() {};
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void setDescriptors(WritableDataItem container, List<? extends Link> linkList) {
		final Map<AKey<?>, Object> map = new HashMap<>();
		getDescriptors(map, linkList);
		map.forEach((k,v)->{
			container.setAttribute((AKey)k, v);
		});
	}
	
	
	
	public static void getDescriptors(Map<AKey<?>,Object> properties, List<? extends Link> linkList) {

		if(linkList.isEmpty()){
			properties.put(duration, 0);
			properties.put(msd, 0d);
			properties.put(dirChange, 0d);
			properties.put(velocity, 0d);
			properties.put(totalDis, 0d);
			properties.put(netDis, 0d);
		}
		else{

			final double[] rootCentroid =linkList.get(0).source().getAttribute(Keys.centroid).get();
			double totalDistance = 0;
			double totalDistanceToRoot = 0;
			double angles = 0;
			int numFrames = 0;
			for(Link link : linkList){
				numFrames += link.target().getAttribute(ImageLocated.frameKey).get()-link.source().getAttribute(ImageLocated.frameKey).get();
				final double distanceToRoot = MathArrays.distance(rootCentroid, link.target().getAttribute(Keys.centroid).get());
				totalDistanceToRoot += distanceToRoot;
				totalDistance += link.getAttribute(distance)
						.orElse(MathArrays.distance(link.source().getAttribute(Keys.centroid).get(), 
								link.target().getAttribute(Keys.centroid).get()));
				angles += link.getAttribute(instantAngle).orElse(0d);
			}
			properties.put(duration, numFrames);
			properties.put(totalDis, totalDistance);
			properties.put(dirChange, angles/linkList.size());
			properties.put(netDis, MathArrays.distance(rootCentroid, 
					linkList.get(linkList.size()-1).target().getAttribute(Keys.centroid).get()));
			properties.put(msd, totalDistanceToRoot/numFrames);
			properties.put(velocity, totalDistance/numFrames);
			properties.put(Keys.trackID, linkList.get(0).source().getAttribute(Keys.trackID).orElse(null));

		}
	}
	
}
