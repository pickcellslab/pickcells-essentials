package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.view.Annotation;

public interface TrackContainer {

	
	// ------------------     Streams for specific Image Locations    ----------------------- //
	
	public Stream<NodeAnnotation> nodesAt(int frame, int zPos);

	public Stream<LinkAnnotation> linksAt(int frame, int zPos);
	
	public Stream<TreeAnnotation> activeTrees();
	
	public Stream<PathAnnotation> activePaths();

	

	public MinimalImageInfo annotationsInfo();

	public NodeAnnotation getNode(int i);
	
	public LinkAnnotation getLink(int i);

	public void setZRadius(int value);


	public Stream<Annotation> stream(String type);

	public Map<String, List<Dimension<Annotation, ?>>> possibleDimensions(Predicate<Dimension> filter, boolean decompose);

	public void delete(List<LinkAnnotation> selectedLinks);

	public void createLink(NodeItem source, NodeItem target);

	public void commitEdits() throws DataAccessException;

	public void setSelectionMode(TrackSelectionMode mode);


	public void createReport();


}
