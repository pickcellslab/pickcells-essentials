package org.pickcellslab.pickcells.tracking.editor;

import java.awt.BorderLayout;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.ImageDisplayFactory;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.pickcellslab.pickcells.tracking.tracker.DisplayedTrackChoiceDialog;

import com.alee.managers.notification.NotificationManager;

import net.imglib2.img.Img;


@Module
public class TrackEditor implements DBViewFactory{

	private final ImageDisplayFactory dispFctry;
	private final DBViewHelp helper;
	private final ImgIO io;
	private final UITheme theme;

	public TrackEditor(ImageDisplayFactory dispFctry, ImgIO io, DBViewHelp helper, UITheme theme) {
		this.dispFctry = dispFctry;
		this.helper = helper;
		this.io = io;
		this.theme = theme;
	}


	@Override
	public String name() {
		return "Track Editor";
	}

	@Override
	public String description() {
		return "This module allows to visualize and edit tracking outputs";
	}

	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}

	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) throws ViewFactoryException {

		final ImageDisplay display = dispFctry.newDisplay();

		// Create Controllers
		final JPanel toolBar = 
				helper.newToolBar()
				.addButton(theme.icon(IconID.Misc.DATA_TABLE, 16), "Select the Tracks to be loaded", l->{

					//Check that there are tracks in the database
					final List<MetaLink> links = access.metaModel().getMetaLinks(Links.TRACK);
					if(links.isEmpty()){
						JOptionPane.showMessageDialog(null, "There are no tracks in the database");
						return;
					}						


					final DisplayedTrackChoiceDialog dialog = new DisplayedTrackChoiceDialog(theme, access);
					int response = JOptionPane.showConfirmDialog(display.getView(), dialog, "Image and tracks to load", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
					if(response == JOptionPane.OK_OPTION){
						//TODO close previous image and manager

						//Load the color image
						final Image color = dialog.getChosenImage();

						Img colorImg = null;
						try {
							colorImg = io.open(color);

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							return;
						}

						display.addImage(color.getMinimalInfo(), colorImg);


						final MetaClass imageLocatedType = dialog.getChosenImageLocatedType();

						RegeneratedItems ri;
						try {

							ri = UsefulQueries.imageToImageLocated(access, imageLocatedType, color.getAttribute(WritableDataItem.idKey).get(),
									Integer.MAX_VALUE, Links.TRACK, Direction.OUTGOING);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return;
						}

						final Map<Integer, List<ImageLocated>> map = ri.getDependencies(ImageLocated.class).collect(Collectors.groupingBy(il->il.frame()));
						final DefaultTrackContainer dtc = new DefaultTrackContainer(map, color.getMinimalInfo(), access);
						final TrackManager mgr = new TrackManager(io, dtc);

						display.addAnnotationLayer("Tracks", mgr);

						TrackManagerControllers.createControls(mgr, display);

						//display.getMouseModel().setSelectionMode(SelectionMode.SINGLE);

					}
				})				
				.build();

		// Z visibility control
		final SpinnerNumberModel zModel = new SpinnerNumberModel(20, 0, 100, 1);
		final JSpinner zSpinner = new JSpinner(zModel);
		toolBar.add(new JLabel("    Z Visibility"));
		toolBar.add(zSpinner);
		zSpinner.addChangeListener(l-> {
			TrackManager mgr = (TrackManager) display.getAnnotationManager(0);
			if(mgr!=null){
				mgr.setZVisibilityDepth(zModel.getNumber().intValue());
			}
		});

		final SpinnerNumberModel tModel = new SpinnerNumberModel(4, 0, 100, 1);
		final JSpinner tSpinner = new JSpinner(tModel);
		toolBar.add(new JLabel("Time Visibility"));
		toolBar.add(tSpinner);
		tSpinner.addChangeListener(l-> {
			final TrackManager mgr = (TrackManager) display.getAnnotationManager(0);
			if(mgr!=null){
				mgr.setFrameVisibilityDepth(tModel.getNumber().intValue());
			}
		});


		final SpinnerNumberModel sModel = new SpinnerNumberModel(2, 1, 10, 1);
		final JSpinner sSpinner = new JSpinner(sModel);
		toolBar.add(new JLabel("Node Size"));
		toolBar.add(sSpinner);
		sSpinner.addChangeListener(l-> {
			final TrackManager mgr = (TrackManager) display.getAnnotationManager(0);
			if(mgr!=null){
				mgr.setNodeSize(sModel.getNumber().intValue());
			}
		});



		//Selection Mode
		toolBar.add(new JLabel("   Selection Mode"));
		final JComboBox<TrackSelectionMode> modeBox = new JComboBox<>(TrackSelectionMode.values());
		modeBox.setSelectedItem(TrackSelectionMode.LINKS);
		modeBox.addActionListener(l->((TrackManager) display.getAnnotationManager(0)).setTrackSelectionMode((TrackSelectionMode) modeBox.getSelectedItem()));

		toolBar.add(modeBox);


		//Save (Commit Changes button)
		final JButton saveBtn = new JButton(theme.icon(IconID.Files.SAVE, 16));
		toolBar.add(saveBtn);
		saveBtn.addActionListener(l->((TrackManager) display.getAnnotationManager(0)).commitChanges());

		final JButton reportBtn = new JButton(theme.icon(IconID.Misc.DATA_TABLE, 16));
		toolBar.add(reportBtn);
		reportBtn.addActionListener(l->{
			new Thread(()->{
				((TrackManager) display.getAnnotationManager(0)).createReport(access);
				NotificationManager.showNotification("Report Created", theme.icon(IconID.Misc.THUMBS_UP, 16));
			}).start();
		});


		//TODO movie button





		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(toolBar, BorderLayout.NORTH);
		panel.add(display.getView(), BorderLayout.CENTER);


		return new DefaultUIDocument(panel, name(), icon());
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/track_editor_icon.png"));
	}



	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

}
