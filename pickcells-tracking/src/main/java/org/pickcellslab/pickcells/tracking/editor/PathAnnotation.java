package org.pickcellslab.pickcells.tracking.editor;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.tools.Path;

/**
 * 
 * Represents a {@link Path} between 2 non inner {@link NodeAnnotation}s
 * @author Guillaume Blin
 *
 */
public interface PathAnnotation extends Path<NodeAnnotation,LinkAnnotation>, MorphableAnnotation {

	public static AKey<Double> msd = AKey.get("Mean Square Displacement", Double.class);
	
	public Stream<NodeAnnotation> nodesStream();
	
	public Stream<LinkAnnotation> edgesStream();
	
	public double meanSqrDist();

	public double averageVelocity();

}
