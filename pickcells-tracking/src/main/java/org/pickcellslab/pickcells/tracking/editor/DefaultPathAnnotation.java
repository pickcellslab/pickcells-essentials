package org.pickcellslab.pickcells.tracking.editor;

import static org.pickcellslab.pickcells.tracking.features.TrackingDescriptors.dirChange;
import static org.pickcellslab.pickcells.tracking.features.TrackingDescriptors.duration;
import static org.pickcellslab.pickcells.tracking.features.TrackingDescriptors.netDis;
import static org.pickcellslab.pickcells.tracking.features.TrackingDescriptors.totalDis;
import static org.pickcellslab.pickcells.tracking.features.TrackingDescriptors.velocity;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.api.img.view.AnnotationDimension;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;
import org.pickcellslab.pickcells.tracking.features.TrackingDescriptors;


public class DefaultPathAnnotation implements PathAnnotation {

	
	private static final ExplicitPredicate<NodeItem> borderNode = new ExplicitPredicate<NodeItem>(){

		@Override
		public boolean test(NodeItem t) {
			return t.getDegree(Direction.OUTGOING, Links.TRACK) < 2
					&&	t.getDegree(Direction.INCOMING, Links.TRACK) < 2;
		}

		@Override
		public String description() {
			return "Branch Nodes";
		}

	};	

	
	
	private static String type = "BRANCH";
	


	
	
	
	private static List<AKey<?>> keys = new ArrayList<>();
	static{
		keys.add(duration);
		keys.add(totalDis);
		keys.add(netDis);
		keys.add(dirChange);
		keys.add(msd);
		keys.add(velocity);
		keys.add(Keys.trackID);
	}


	private AnnotationStatus status = AnnotationStatus.DESELECTED;

	private final List<DefaultLinkAnnotation> linkList = new ArrayList<>();

	private final List<DefaultNodeAnnotation> nodeList = new ArrayList<>();

	private final Map<AKey<?>,Object> properties = new HashMap<>();



	/**
	 * Creates a new {@link PathAnnotation}
	 * @param cache
	 * @param la The link from which to identify the Path
	 * @param fullPath if true, The path will start from the root of the tree to the target of the given link, if false
	 * the path will be defined between the 2 non inner nodes (Root, leaf or division) reachable from the given {@link LinkAnnotation}	 
	 */
	DefaultPathAnnotation(TreeAnnotationCache cache, DefaultLinkAnnotation la, boolean fullPath) {
		this.build(cache, cache.get(la.target()), true);
	}



	/**
	 * @param cache
	 * @param n
	 * @param fullPath If true, the path will start from the root of the tree to the given node, if false
	 * the path will be defined between the 2 non inner nodes (Root, leaf or division) reachable from the given {@link LinkAnnotation}.
	 * If the given node is not an inner node, the upstream path will be created.	
	 */
	DefaultPathAnnotation(TreeAnnotationCache cache, DefaultNodeAnnotation n, boolean fullPath) {
		build(cache, n, fullPath);
	}





	private void build(TreeAnnotationCache cache, DefaultNodeAnnotation node, boolean fullPath) {


		if(!fullPath){
			//Build the path
			final DefaultTraversal<NodeItem, Link> tr = Traversers.breadthfirst(
					node.data(),
					Traversers.newConstraints().fromMinDepth().toMaxDepth().traverseLink(Links.TRACK, Direction.BOTH)
					.withEvaluations(borderNode, Decision.INCLUDE_AND_CONTINUE, Decision.INCLUDE_AND_STOP)
					.done()
					).traverse();

			// We assume the passed nodes are ImageLocated.
			tr.edges().forEach(e->linkList.add(cache.get(e)));
			tr.nodes().forEach(n->nodeList.add(cache.get(n)));
			

		}
		else{
			node.upstreamLinks(Integer.MAX_VALUE).forEach(l->{
				linkList.add(cache.get(l));
				nodeList.add(cache.get(l.source()));
				nodeList.add(cache.get(l.target()));
			});
			nodeList.add(node);			
		}

		Collections.sort(nodeList, 
				(n1,n2) -> Integer.compare(n1.data().frame(), n2.data().frame()));

		if(linkList.isEmpty())
			linkList.add(null);
		else
			Collections.sort(linkList, 
					(n1,n2) -> Integer.compare(((ImageLocated) n1.link().source()).frame(), ((ImageLocated) n2.link().source()).frame()));
		
		
	}




	private void checkFeatures() {
		if(!properties.isEmpty())	return;
		TrackingDescriptors.getDescriptors(properties, linkList);
	}





	@Override
	public NodeAnnotation first() {
		return nodeList.get(0);
	}

	@Override
	public NodeAnnotation last() {
		return nodeList.get(nodeList.size()-1);
	}

	@Override
	public LinkAnnotation firstEdge() {
		return linkList.get(0);
	}

	@Override
	public LinkAnnotation lastEdge() {
		return linkList.get(linkList.size()-1);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Iterator<NodeAnnotation> nodes() {
		return (Iterator) nodeList.iterator();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Iterator<LinkAnnotation> edges() {
		if(firstEdge()==null)
			return empty;
		return (Iterator) linkList.iterator();
	}

	@Override
	public int nodesCount() {
		return nodeList.size();
	}

	@Override
	public int edgesCount() {
		if(firstEdge()==null)
			return 0;
		return linkList.size();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Stream<NodeAnnotation> nodesStream() {
		return (Stream) nodeList.stream();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Stream<LinkAnnotation> edgesStream() {
		if(firstEdge()==null)
			return Stream.empty();
		return (Stream) linkList.stream();
	}

	@Override
	public double meanSqrDist() {
		checkFeatures();
		return (double) properties.get(msd);
	}


	@Override
	public double averageVelocity() {
		checkFeatures();
		return (double) properties.get(velocity);
	}

	@Override
	public Stream<AKey<?>> properties() {
		return keys.stream();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> E getProperty(AKey<E> k) {
		checkFeatures();
		return (E) properties.get(k);
	}

	@Override
	public AnnotationStatus status() {
		return status;
	}



	@Override
	public MorphableAnnotation getCurrentMorphing() {
		return this;
	}



	@Override
	public void toggleSelected(List<LinkAnnotation> linkList, List<NodeAnnotation> nodesList) {
		if(status == AnnotationStatus.SELECTED){
			status = AnnotationStatus.DESELECTED;
			linkList.removeAll(this.linkList);
			nodesList.removeAll(this.nodeList);
		}
		else{
			status = AnnotationStatus.SELECTED;	
			linkList.addAll(this.linkList);
			nodesList.addAll(this.nodeList);
		}
	}



	public boolean contains(DefaultNodeAnnotation node) {
		return nodeList.contains(node);
	}

	
	
	@Override
	public String toString(){
		return staticType()+" With Root "+first();
	}
	
	
	
	public static String staticType() {
		return type;
	}


	@Override
	public String representedType() {
		return type;
	}



	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Dimension<Annotation, ?>> possibleDimensions() {
		return (List)keys.stream().map(k->new AnnotationDimension<>(k, k.name)).collect(Collectors.toList());
	}

	
	
	@SuppressWarnings({ "rawtypes", "unused" })
	private static final Iterator empty = new Iterator() {

		@Override
		public boolean hasNext() {
			return false;
		}

		@Override
		public Object next() {
			return null;
		}
		
	};
	
}
