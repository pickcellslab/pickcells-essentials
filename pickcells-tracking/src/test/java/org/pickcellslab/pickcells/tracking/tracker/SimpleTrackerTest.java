package org.pickcellslab.pickcells.tracking.tracker;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.math3.util.MathArrays;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.datamodel.tools.Traversers.SimpleBreadthFirst;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;

public class SimpleTrackerTest {




	@Test
	public void testGetRoot(){

		// A -a-> B -a-> C -a-> D -a-> E
		//		  	     F -n-> 
		final long[] loc = new long[2];
		TestNode A = new TestNode(loc);	TestNode B = new TestNode(loc);	TestNode C = new TestNode(loc);
		TestNode D = new TestNode(loc);	TestNode E = new TestNode(loc);	TestNode F = new TestNode(loc);
		createPotLink(A, B, true);			createPotLink(B, C, true);			createPotLink(C, D, true);
		Link l1 = createPotLink(D, E, true);			Link l2 = createPotLink(F, D, false);

		Assert.assertTrue(SimpleTracker.getRoot(l1)==A);		
		Assert.assertTrue(SimpleTracker.getRoot(l2)==null);	

	}




	@Test
	public void testInsertion(){

		// A -a-> B -------a------> E -a->F
		//		  |-n-> C -a-> D -n-|
		final long[] loc = new long[2];
		TestNode A = new TestNode(loc);	TestNode B = new TestNode(1, loc);	TestNode C = new TestNode(2, loc);
		TestNode D = new TestNode(3, loc);	TestNode E = new TestNode(4, loc);	TestNode F = new TestNode(5, loc);

		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, B, true));		
		Link BE = createPotLink(B, E, true); 	list.add(BE);		
		list.add(createPotLink(E, F, true));
		list.add(createPotLink(C, D, true));

		Link BC = createPotLink(B, C, false);	list.add(BC);
		Link DE = createPotLink(D, E, false);	list.add(DE);


		Assert.assertTrue(SimpleTracker.getRoot(DE)==null);



		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(5, list);

		Assert.assertTrue(roots.size()==1);
		Assert.assertTrue(roots.get(0)==A);

		Assert.assertTrue(!SimpleTracker.isAssigned(BE));
		Assert.assertTrue(SimpleTracker.isAssigned(BC));
		Assert.assertTrue(SimpleTracker.isAssigned(DE));

		Assert.assertTrue(SimpleTracker.getRoot(DE)==A);


		list.forEach(l->Assert.assertTrue(A.getAttribute(Keys.trackID).get() == l.target().getAttribute(Keys.trackID).get()));

	}




	@Test
	public void testAppend(){

		// A -a-> B -n-> E -a-> F
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode B = new TestNode("B", 1, loc);	
		TestNode E = new TestNode("E", 3, loc);	TestNode F = new TestNode("F", 4, loc);

		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, B, true));
		list.add(createPotLink(B, E, false));
		list.add(createPotLink(E, F, true));

		Assert.assertTrue(SimpleTracker.getRoot(list.get(2))==E);

		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(5, list);

		Assert.assertTrue(roots.size()==1);
		Assert.assertTrue(roots.get(0)==A);

		Assert.assertTrue(SimpleTracker.getRoot(list.get(2))==A);


		list.forEach(l->Assert.assertTrue(A.getAttribute(Keys.trackID).get() == l.target().getAttribute(Keys.trackID).get()));

	}





	@Test
	public void testFuseSameLevel(){

		// A -a-> B -a-> C
		//				 D -a-> E -a-> F
		// (+ unassigned between BD and CE)
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode B = new TestNode("B", 1, loc);	TestNode C = new TestNode("C", 2, loc);
		TestNode D = new TestNode("D", 2, loc);	TestNode E = new TestNode("E", 3, loc);	TestNode F = new TestNode("F", 4, loc);

		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, B, true));		list.add(createPotLink(B, D, false));
		list.add(createPotLink(B, C, true));		list.add(createPotLink(C, E, false));
		list.add(createPotLink(D, E, true));
		list.add(createPotLink(E, F, true));

		Assert.assertTrue(SimpleTracker.getRoot(list.get(5))==D);

		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(5, list);


		SimpleBreadthFirst<NodeItem, Link> tr = Traversers.breadthFirst(A);
		TraversalStep<NodeItem, Link> step;
		while((step = tr.nextStep()) != null){
			System.out.println("Depth "+step.round+"----------------");
			step.edges.forEach(System.out::println);
		}


		Assert.assertTrue(roots.get(0)==A);
		Assert.assertTrue(roots.size()==1);
		

		Assert.assertTrue(SimpleTracker.getRoot(list.get(5))==A);


		list.stream().filter(l->l.target()!=D).forEach(l->Assert.assertTrue(A.getAttribute(Keys.trackID).get() == l.target().getAttribute(Keys.trackID).get()));

	}








	@Test
	public void testFuseWithRootPotential(){

		// A -a-> B -a-> C -a-> D
		//				 E -a-> F -a-> G
		// (+ unassigned between BE and DG)
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode B = new TestNode("B", 1, loc);	TestNode C = new TestNode("C", 2, loc);
		TestNode D = new TestNode("D", 3, loc);	TestNode E = new TestNode("E", 2, loc);	TestNode F = new TestNode("F", 3, loc);
		TestNode G = new TestNode("G", 4, new long[]{2,3});

		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, B, true));		list.add(createPotLink(B, E, false));
		list.add(createPotLink(B, C, true));		list.add(createPotLink(D, G, false));
		list.add(createPotLink(C, D, true));
		list.add(createPotLink(E, F, true));
		list.add(createPotLink(F, G, true));

		Assert.assertTrue(SimpleTracker.getRoot(list.get(6))==E);

		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(5, list);


		SimpleBreadthFirst<NodeItem, Link> tr = Traversers.breadthFirst(A);
		TraversalStep<NodeItem, Link> step;
		while((step = tr.nextStep()) != null){
			System.out.println("Depth "+step.round+"----------------");
			step.edges.forEach(System.out::println);
		}

		Assert.assertTrue(SimpleTracker.isAssigned(list.get(1))); // BE must now be assigned
		Assert.assertTrue(!SimpleTracker.isAssigned(list.get(4))); // CD should not be assigned anymore

		Assert.assertTrue(roots.size()==1);
		Assert.assertTrue(roots.get(0)==A);

		Assert.assertTrue(A.getAttribute(Keys.trackID).get() == G.getAttribute(Keys.trackID).get());

		Assert.assertTrue(SimpleTracker.getRoot(list.get(6))==A);


		list.stream().filter(l->l.target()!=C && l.target()!=D).forEach(l->Assert.assertTrue(A.getAttribute(Keys.trackID).get() == l.target().getAttribute(Keys.trackID).get()));
		Assert.assertTrue(!C.getAttribute(Keys.trackID).isPresent());
		Assert.assertTrue(!D.getAttribute(Keys.trackID).isPresent());
	}





	@Test
	public void testFuseWithRootPotentialSingleUpstreamNode(){

		// A -a-> C -a-> D
		//		  E -a-> F -a-> G
		// (+ unassigned between AE and DG)
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode C = new TestNode("C", 2, loc);
		TestNode D = new TestNode("D", 3, loc);	TestNode E = new TestNode("E", 2, loc);	
		TestNode F = new TestNode("F", 3, loc); TestNode G = new TestNode("G", 4, new long[]{2,3});

		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, C, true));		list.add(createPotLink(A, E, false));		
		list.add(createPotLink(C, D, true));		list.add(createPotLink(D, G, false));
		list.add(createPotLink(E, F, true));
		list.add(createPotLink(F, G, true));

		Assert.assertTrue(SimpleTracker.getRoot(list.get(5))==E);

		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(5, list);


		SimpleBreadthFirst<NodeItem, Link> tr = Traversers.breadthFirst(A);
		TraversalStep<NodeItem, Link> step;
		while((step = tr.nextStep()) != null){
			System.out.println("Depth "+step.round+"----------------");
			step.edges.forEach(System.out::println);
		}

		Assert.assertTrue(SimpleTracker.isAssigned(list.get(1))); // BE must now be assigned
		Assert.assertTrue(!SimpleTracker.isAssigned(list.get(2))); // CD should not be assigned anymore

		Assert.assertTrue(roots.size()==1);
		Assert.assertTrue(roots.get(0)==A);

		Assert.assertTrue(A.getAttribute(Keys.trackID).get() == G.getAttribute(Keys.trackID).get());

		Assert.assertTrue(SimpleTracker.getRoot(list.get(5))==A);


		list.stream().filter(l->l.target()!=D && l.target()!=C).forEach(l->Assert.assertTrue(A.getAttribute(Keys.trackID).get() == l.target().getAttribute(Keys.trackID).get()));
		Assert.assertTrue(!C.getAttribute(Keys.trackID).isPresent());
		Assert.assertTrue(!D.getAttribute(Keys.trackID).isPresent());
	}








	@Test
	public void testFuseWithUpstreamLeafPotential(){

		// A -a-> B -a-> C -a-> D
		//				 E -a-> F -a-> G
		// (+ unassigned between BE and DG)
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode B = new TestNode("B", 1, loc);	TestNode C = new TestNode("C", 2, loc);
		TestNode D = new TestNode("D", 3, loc);	TestNode E = new TestNode("E", 2, new long[]{2,3});	TestNode F = new TestNode("F", 3, loc);
		TestNode G = new TestNode("G", 4, loc);

		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, B, true));		list.add(createPotLink(B, E, false));
		list.add(createPotLink(B, C, true));		list.add(createPotLink(D, G, false));
		list.add(createPotLink(C, D, true));
		list.add(createPotLink(E, F, true));
		list.add(createPotLink(F, G, true));

		Assert.assertTrue(SimpleTracker.getRoot(list.get(6))==E);

		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(5, list);


		SimpleBreadthFirst<NodeItem, Link> tr = Traversers.breadthFirst(A);
		TraversalStep<NodeItem, Link> step;
		while((step = tr.nextStep()) != null){
			System.out.println("Depth "+step.round+"----------------");
			step.edges.forEach(System.out::println);
		}

		Assert.assertTrue(!SimpleTracker.isAssigned(list.get(1))); // BE must not be assigned
		Assert.assertTrue(!SimpleTracker.isAssigned(list.get(5))); // EF should not be assigned anymore
		Assert.assertTrue(SimpleTracker.isAssigned(list.get(3))); // DG should now be assigned

		Assert.assertTrue(roots.size()==1);
		Assert.assertTrue(roots.get(0)==A);

		Assert.assertTrue(A.getAttribute(Keys.trackID).get() == G.getAttribute(Keys.trackID).get());


		//list.stream().filter(l->l.target()!=C && l.target()!=D).forEach(l->Assert.assertTrue(A.getAttribute(Keys.trackID).get() == l.target().getAttribute(Keys.trackID).get()));
		Assert.assertTrue(!E.getAttribute(Keys.trackID).isPresent());
		Assert.assertTrue(!F.getAttribute(Keys.trackID).isPresent());
	}





	@Test
	public void testDivisionFinding(){

		// A -a-> B -a-> C -a-> D -a-> E -a-> F
		// 		  B -a-> G -a-> H -a-> I -a-> J
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode B = new TestNode("B", 1, loc);	TestNode C = new TestNode("C", 2, loc);
		TestNode D = new TestNode("D", 4, loc);	TestNode E = new TestNode("E", 5, loc);	TestNode F = new TestNode("F", 6, loc);
		TestNode G = new TestNode("G", 3, loc);	TestNode H = new TestNode("H", 4, loc);	TestNode I = new TestNode("I", 5, loc);
		TestNode J = new TestNode("J", 6, loc);


		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, B, true));	
		list.add(createPotLink(B, C, true));	list.add(createPotLink(B, G, true));
		list.add(createPotLink(C, D, true));	list.add(createPotLink(G, H, true));
		list.add(createPotLink(D, E, true));	list.add(createPotLink(H, I, true));
		list.add(createPotLink(E, F, true));	list.add(createPotLink(I, J, true));

		System.out.println(SimpleTracker.upstreamDivision(list.get(8)));
		System.out.println(SimpleTracker.downstreamDivision(list.get(0)));
	}



	@Test
	public void testDivision(){

		// A -a-> B -a-> C -a-> D -a-> E
		// 		  B -n-> G -a-> H -a-> I -a-> J
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode B = new TestNode("B", 1, loc);	TestNode C = new TestNode("C", 2, loc);
		TestNode D = new TestNode("D", 4, loc);	TestNode E = new TestNode("E", 5, loc);	TestNode F = new TestNode("F", 6, loc);
		TestNode G = new TestNode("G", 3, loc);	TestNode H = new TestNode("H", 4, loc);	TestNode I = new TestNode("I", 5, loc);
		TestNode J = new TestNode("J", 6, loc);


		List<Link> list = new ArrayList<>();
		list.add(createPotLink(A, B, true));	
		list.add(createPotLink(B, C, true));	list.add(createPotLink(B, G, false));
		list.add(createPotLink(C, D, true));	list.add(createPotLink(G, H, true));
		list.add(createPotLink(D, E, true));	list.add(createPotLink(H, I, true));
		/*list.add(createPotLink(E, F, true));*/	list.add(createPotLink(I, J, true));

		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(5, list);


		SimpleBreadthFirst<NodeItem, Link> tr = Traversers.breadthFirst(A);
		TraversalStep<NodeItem, Link> step;
		while((step = tr.nextStep()) != null){
			System.out.println("Depth "+step.round+"----------------");
			step.edges.forEach(System.out::println);
		}
	}

	
	
	
	
	
	@Test
	public void testDivisionSeparation(){

		// A -a-> B -a-> C -a-> D -a-> E
		// 		  B -n-> G -a-> H -a-> I -a-> J
		//						H -n-> L -a-> M
		final long[] loc = new long[2];
		TestNode A = new TestNode("A", 0, loc);	TestNode B = new TestNode("B", 1, loc);	TestNode C = new TestNode("C", 2, new long[]{-2,2});
		TestNode D = new TestNode("D", 4, loc);	TestNode E = new TestNode("E", 5, loc);	TestNode F = new TestNode("F", 6, loc);
		TestNode G = new TestNode("G", 3, new long[]{2,2});	TestNode H = new TestNode("H", 4, loc);	TestNode I = new TestNode("I", 5, loc);
		TestNode J = new TestNode("J", 6, loc);	TestNode L = new TestNode("L", 5, loc);	TestNode M = new TestNode("M", 6, loc);


		final List<Link> list = new ArrayList<>();
		
		list.add(createPotLink(A, B, true));	
		list.add(createPotLink(B, C, true));	list.add(createPotLink(B, G, false));
		list.add(createPotLink(C, D, true));	list.add(createPotLink(G, H, true));
		list.add(createPotLink(D, E, true));	list.add(createPotLink(H, I, true));
		
		list.add(createPotLink(I, J, true));
		list.add(createPotLink(H, L, false));
		list.add(createPotLink(L, M, true));
		
		
		final List<NodeItem> roots = SimpleTracker.buildAndConnectBranches(6, list);


		SimpleBreadthFirst<NodeItem, Link> tr = Traversers.breadthFirst(A);
		TraversalStep<NodeItem, Link> step;
		while((step = tr.nextStep()) != null){
			System.out.println("Depth "+step.round+"----------------");
			step.edges.forEach(System.out::println);
		}
	}

	
	
	





	private Link createPotLink(TestNode n1, TestNode n2, boolean assigned){
		Link l = new DataLink(SimpleTracker.potType, n1, n2, true);
		l.setAttribute(SimpleTracker.isAssigned, assigned);
		l.setAttribute(SimpleTracker.cost, (float)MathArrays.distance(n1.centroid(), n2.centroid()));
		return l;
	}



	private class TestNode extends DataNode implements ImageLocated{

		private final long[] loc;
		private final String name;


		public TestNode(long[] loc){
			this(0, loc);
		}

		public TestNode(int frame, long[] loc){
			this(null, frame, loc);
		}

		public TestNode(String name, int frame, long[] loc){
			this.setAttribute(frameKey,frame);
			this.name = name;
			this.loc = loc;
		}


		@Override
		public long[] location() {
			return loc;
		}

		@Override
		public double[] centroid() {
			double[] c = new double[loc.length];
			for(int i = 0; i<c.length; i++)
				c[i] = loc[i];
			return c;
		}



		@Override
		public Image image() {
			return null;
		}


		@Override
		public Stream<AKey<?>> minimal() {
			return Stream.empty();
		}


		@Override
		public String toString(){
			return name == null ? super.toString() : name;
		}

	}





}	
