package org.pickcellslab.pickcells.tracking.interactions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.Image.ImageBuilder;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.RawFile;
import org.pickcellslab.pickcells.tracking.editor.DefaultNodeAnnotation;
import org.pickcellslab.pickcells.tracking.editor.DefaultTreeAnnotation;
import org.pickcellslab.pickcells.tracking.editor.LineageTree;
import org.pickcellslab.pickcells.tracking.editor.TreeAnnotationCache;
import org.pickcellslab.pickcells.tracking.interactions.NeighbourVisitsTracker.NeighbourVisitsResult;

public class NeighbourVisitsTest {



	@Test
	public void testNeighbourVisits(){

		// Build a tree with neighbours
		// 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 10
		//					-> 11-> 12-> 13->   -> 14-> 15

		final List<NodeItem> path1 = new ArrayList<>();
		final List<NodeItem> path2 = new ArrayList<>();

		final NodeItem t1 = new Tracked("t", 1, 1, 1);	path1.add(t1);	path2.add(t1);
		final NodeItem t2 = new Tracked("t", 1, 1, 2);	new DataLink(Links.TRACK, t1, t2, true);	path1.add(t2);	path2.add(t2);
		final NodeItem t3 = new Tracked("t", 1, 1, 3);	new DataLink(Links.TRACK, t2, t3, true);	path1.add(t3);	path2.add(t3);
		final NodeItem t4 = new Tracked("t", 1, 1, 4);	new DataLink(Links.TRACK, t3, t4, true);	path1.add(t4);	path2.add(t4);
		final NodeItem t5 = new Tracked("t", 1, 2, 5);	new DataLink(Links.TRACK, t4, t5, true);	path1.add(t5);	path2.add(t5);
		final NodeItem t6 = new Tracked("t", 1, 2, 6);	new DataLink(Links.TRACK, t5, t6, true);	path1.add(t6);
		final NodeItem t7 = new Tracked("t", 1, 2, 7);	new DataLink(Links.TRACK, t6, t7, true);	path1.add(t7);
		final NodeItem t8 = new Tracked("t", 1, 2, 8);	new DataLink(Links.TRACK, t7, t8, true);	path1.add(t8);
		final NodeItem t9 = new Tracked("t", 1, 2, 9);	new DataLink(Links.TRACK, t8, t9, true);	path1.add(t9);
		final NodeItem t10 = new Tracked("t", 1, 2, 10);	new DataLink(Links.TRACK, t9, t10, true);	path1.add(t10);
		final NodeItem t11 = new Tracked("t", 1, 3, 5);	new DataLink(Links.TRACK, t4, t11, true);	path2.add(t11);
		final NodeItem t12 = new Tracked("t", 1, 3, 6);	new DataLink(Links.TRACK, t11, t12, true);	path2.add(t12);
		final NodeItem t13 = new Tracked("t", 1, 3, 7);	new DataLink(Links.TRACK, t12, t13, true);	path2.add(t13);
		final NodeItem t14 = new Tracked("t", 1, 3, 9);	new DataLink(Links.TRACK, t13, t14, true);	path2.add(t14);
		final NodeItem t15 = new Tracked("t", 1, 3, 10);	new DataLink(Links.TRACK, t14, t15, true);	path2.add(t15);


		final TreeAnnotationCache cache = new TreeAnnotationCache(Collections.singleton(1));
		final DefaultNodeAnnotation an = new DefaultNodeAnnotation(t1, cache);		

		final LineageTree tree = new LineageTree(new DefaultTreeAnnotation(cache, an, Direction.OUTGOING));
		Assert.assertTrue("Tree does not have 3 branches", LineageBranch.getBranches(tree).size()==3);

		// add neighbours (between 3 and 7)
		final NodeItem n1 = new Tracked("n", 2, 1, 3); 	new DataLink(Links.ADJACENT_TO, t3, n1, true);
		final NodeItem n2 = new Tracked("n", 2, 1, 4);	new DataLink(Links.ADJACENT_TO, t4, n2, true);
		final NodeItem n3 = new Tracked("n", 2, 1, 5);	new DataLink(Links.ADJACENT_TO, t5, n3, true);
		final NodeItem n4 = new Tracked("n", 2, 1, 6);	new DataLink(Links.ADJACENT_TO, t6, n4, true);
		final NodeItem n5 = new Tracked("n", 2, 1, 7);	new DataLink(Links.ADJACENT_TO, t7, n5, true);

		// add neighbours (between 11 and 15)
		final NodeItem n6 = new Tracked("n", 3, 1, 5); 	new DataLink(Links.ADJACENT_TO, t11, n6, true);
		final NodeItem n7 = new Tracked("n", 3, 1, 6);	new DataLink(Links.ADJACENT_TO, t12, n7, true);
		final NodeItem n8 = new Tracked("n", 3, 1, 7);	new DataLink(Links.ADJACENT_TO, t13, n8, true);
		final NodeItem n9 = new Tracked("n", 3, 1, 9);	new DataLink(Links.ADJACENT_TO, t14, n9, true);
		final NodeItem n10 = new Tracked("n", 3, 1, 10);	new DataLink(Links.ADJACENT_TO, t15, n10, true);


		final NeighbourVisitsTracker tracker1 = new NeighbourVisitsTracker(Links.ADJACENT_TO, Keys.trackID);
		path1.forEach(n->tracker1.addNode(n));

		Assert.assertTrue("tracker1 did not find 1 neighbour", tracker1.totalNumberOfVisitedNeighbours() == 1);

		final NeighbourVisitsResult result = tracker1.result();
		Assert.assertTrue("tracker1 did not find correct average visit time - "+result.averageVisitTime(2), result.averageVisitTime(2) == 5);
		Assert.assertTrue("tracker1 did not find correct number of visits", result.numberOfVisits(2) == 1);
		
		
		final NeighbourVisitsTracker tracker2 = new NeighbourVisitsTracker(Links.ADJACENT_TO, Keys.trackID);
		path2.forEach(n->tracker2.addNode(n));

		Assert.assertTrue("tracker2 did not find 1 neighbour", tracker2.totalNumberOfVisitedNeighbours() == 2);

		final NeighbourVisitsResult result2 = tracker2.result();
		Assert.assertTrue("tracker2 did not find correct average visit time - "+result2.averageVisitTime(3), result2.averageVisitTime(3) == 5);
		Assert.assertTrue("tracker2 did not find correct number of visits", result2.numberOfVisits(3) == 1);

	}








	@Data(typeId="Tracked")
	@Scope(name="Test")
	private static class Tracked extends DataNode implements ImageLocated{

		private static final Image image = new ImageBuilder("test", 512, 512, 1)
				.setUnits(0.5, 0.5, "um")
				.setRawFile(Mockito.mock(RawFile.class))
				.setLocation("Nowhere")
				.setChannel(0, "", "")
				.build();
		private static final Random r = new Random();
		private static int idMaker;

		@SuppressWarnings("unused")
		Tracked(){}
		
		Tracked(String namePrefix, int trackId, int branchId, int frame) {
			setAttribute(Keys.centroid, new double[] {r.nextDouble(), r.nextDouble()});
			setAttribute(Keys.name, "namePrefix "+frame);
			setAttribute(Keys.trackID,trackId);
			setAttribute(LineageBranch.branchIdKey, branchId);
			setAttribute(ImageLocated.frameKey, frame);
			setAttribute(DataItem.idKey,++idMaker);
		}


		@Override
		public Stream<AKey<?>> minimal() {
			return Stream.empty();
		}

		@Override
		public long[] location() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public double[] centroid() {
			return getAttribute(Keys.centroid).get();
		}

		@Override
		public Image image() {
			return image;
		}

		@Override
		public String toString() {
			return getAttribute(Keys.name).get();
		}

	}


}
