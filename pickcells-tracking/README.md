# Pickcells - Tracking

This project contains 3 pickcells modules dedicated to the analysis of time resolved datasets:

 * [SimpleTracker  (A cell tracker)](#simpletracker)
 * [TrackEditor (A graphic user interface for track editing)](#trackeditor)
 * [NeighbourExchange (An analysis of lineage trees interaction)](#neighbourexchange)


## SimpleTracker: 

>**Disclaimer: This module is still under development. It is sub-optimal in its current state and is subject to change**.

The purpose of the SimpleTracker is to use segmented shapes within individual time frames and to connect them over time in order to create lineage trees. The type of the links between connected shapes is 'TRACK':

```mermaid
graph LR

mother(Mother)-->t1((t1))
t1-->t2((t2))
t2-->div{Division}

div-->t3a((t3))
div-->t3b((t3))

t3a-->t4a((t4))
t3b-->t4b((t4))

t4a-->d1(Daughter 1)
t4b-->d2(Daughter 2)


classDef node fill:wheat,stroke:#333,stroke-width:1px;

linkStyle 0 stroke:red, stroke-width:2px;
linkStyle 1 stroke:red, stroke-width:2px;
linkStyle 2 stroke:red, stroke-width:2px;
linkStyle 3 stroke:red, stroke-width:2px;
linkStyle 4 stroke:red, stroke-width:2px;
linkStyle 5 stroke:red, stroke-width:2px;
linkStyle 6 stroke:red, stroke-width:2px;
linkStyle 7 stroke:red, stroke-width:2px;
linkStyle 8 stroke:red, stroke-width:2px;
```


##### Principle of the SimpleTracker

 1. **Graph of potential connections**

First, a graph of potential connections is created using the following rule:

For a shape at time t (S<sub>t</sub>) and a shape at time t+n (S<sub>t+n</sub>), we create a link if:

  a. The euclidian distance between the centroid of St and St+n < distance threshold  
  b. n < allowed frame jump (This enables the tracking of cells even if the upstream segmentation missed the shape for a few frames)
  
The distance threshold and the 'allowed frame jump' are set as user-defined parameters.

Upon creation, connections are associated with a similarity score equal to the Jaccard Index of the two shapes bounding boxes.


2. **Primary assignment**

Next, a first round of assignment is performed. The links with highest score and lowest time difference are assigned first with the condition that the source shape does not already possess an assigned outgoing link and that the target does not already possess an assigned incoming link.
This ensures that nodes in the generated tracks contain at most one child and one parent.

3. **Branches Connections**

If the initial segmentation is imperfect, the previous step generates a number of truncated tracks. For example if a cell is over-segmented on a frame the following tracks will be generated:

<pre>
    ------------------------------> time
     A -a-> B -a-> C
                   D -a-> E -a-> F
                   
    -a-> : assignment link
</pre>

SimpleTracker finds these patterns and corrects them.
In the example above, the outcome would be a new link between B and D if the score of B -a-> D is higher than C -a-> E  leaving out D unattached or a new link between C and E if the score of C -p-> E is higher than B -p-> D  leaving out C unattached

<pre>
    ------------------------------> time
     A -a-> B      C
            |- a-> D -a-> E -a-> F
	        
	          OR
	          
     A -a-> B -a-> C --a--v
                   D      E -a-> F	          
</pre>

Some of these patterns are listed in the SimpleTrackerTest class.



4. **Divisions**

Finally, when all small tracks have been merged, division events are created. This is done by linking tracks which do not start from the beginning of the movie with 
neighbour tracks starting with higher score connections first. A constraint is also used to avoid divisions to occur too often within the same lineage (user-defined parameter: minimum amount of time between two divisions).
 




---

### TrackEditor 

Once 'TRACK' links are available, the TrackEditor can be used to edit those links.  
When the user validates the tracking result, the TrackEditor generates [LineageTree](https://framagit.org/pickcellslab/pickcells-essentials/blob/develop/pickcells-tracking/src/main/java/org/pickcellslab/pickcells/tracking/editor/LineageTree.java#L55) and [LineagePath](https://framagit.org/pickcellslab/pickcells-essentials/blob/develop/pickcells-tracking/src/main/java/org/pickcellslab/pickcells/tracking/editor/LineagePath.java#L54) objects which store summary features such as average velocity, average duration, mean square displacement, etc... These objects are also useful to iterate over  specific node sequences.



```mermaid


graph LR

path1-->|Has Root|mother
lineage(Lineage Tree)-->|Has Root|mother
path2-->|Has Root|mother

lineage-->|Has Path|path1
lineage-->|Has Path|path2

path1(Lineage Path 1)-->|Has Leaf|d1
path2(Lineage Path 2)-->|Has Leaf|d2

mother(Mother)-->t1((t1))
t1-->t2((t2))
t2-->div{Division}

div-->t3a((t3))
div-->t3b((t3))

t3a-->t4a((t4))
t3b-->t4b((t4))

t4a-->d1(Daughter 1)
t4b-->d2(Daughter 2)

style lineage fill:tomato,stroke:#333,stroke-width:1px;
style path1 fill:whitesmoke,stroke:#333,stroke-width:1px;
style path2 fill:whitesmoke,stroke:#333,stroke-width:1px;

classDef tree fill:wheat,stroke:#333,stroke-width:1px;
class mother,t1,t2,div,t3a,t3b,t4a,t4b,d1,d2 tree;

linkStyle 0 stroke:lightblue, stroke-width:2px;
linkStyle 1 stroke:lightsteelblue, stroke-width:2px;
linkStyle 2 stroke:lightblue, stroke-width:2px;
linkStyle 3 stroke:tomato, stroke-width:2px;
linkStyle 4 stroke:tomato, stroke-width:2px;

linkStyle 5 stroke:olivedrab, stroke-width:2px;
linkStyle 6 stroke:olivedrab, stroke-width:2px;

```

---

### NeighbourExchange

This module updates the graph and computes descriptors to reflect how lineages interact with one another. It requires LineageTrees as input as well as links defining the neighbourhood graph at each time point:

Input Graph example : (N1, N2, N3 are neighbouring cells)

```mermaid

graph LR

subgraph Time1

n3t1((N3))
n1t1-.-|ADJACENT|c1t1
n2t1-.-|ADJACENT|c1t1
end

subgraph Time2
c1t2-.-|ADJACENT|n3t2((N3))
c1t2-.-|ADJACENT|n1t2
c1t2-.-|ADJACENT|n2t2

end


n3t1((N3))-->|TRACK|n3t2
n1t1((N1))-->|TRACK|n1t2((N1))
c1t1((Cell))-->|TRACK|c1t2((Cell))
n2t1((N2))-->|TRACK|n2t2((N2))

 

classDef node fill:wheat,stroke:#333,stroke-width:1px;
classDef branch1 fill:pink,stroke:#333,stroke-width:1px;
classDef branch2 fill:whitesmoke, stroke:#333,stroke-width:1px;
classDef branch3 fill:lavender, stroke:#333,stroke-width:1px;

class n1t1,n1t2 branch1;
class n2t1,n2t2 branch2;
class n3t1,n3t2 branch3;


linkStyle 5 stroke:lightsteelblue, stroke-width:2px;
linkStyle 6 stroke:palevioletred, stroke-width:2px;
linkStyle 7 stroke:orange, stroke-width:2px;
linkStyle 8 stroke:black, stroke-width:2px;

```

  
<br><br>

The module creates 'LineageBranch' objects which are linked to SegmentedObjects as follows:

```mermaid


graph LR

path1(LineagePath 1)-->|Has Root|mother
lineage(LineageTree) -->|Has Root|mother
path2(LineagePath 2)-->|Has Root|mother

lineage-->|Has Path|path1
lineage-->|Has Path|path2

path1-->|Has Leaf|d1
path2-->|Has Leaf|d2

mother(Mother)-->t1((t1))
t1-->t2((t2))
t2-->div{Division}

div-->t3a((t3))
div-->t3b((t3))

t3a-->t4a((t4))
t3b-->t4b((t4))

t4a-->d1(Daughter 1)
t4b-->d2(Daughter 2)


branch1(LineageBranch 1) -->|STARTS_WITH| mother
branch1 -->|ENDS_WITH| div

branch2(LineageBranch 2) -->|STARTS_WITH| t3a
branch2 -->|ENDS_WITH| d1

branch3(LineageBranch 3) -->|STARTS_WITH| t3b
branch3 -->|ENDS_WITH| d2


style lineage fill:#db5a5a,stroke:#333,stroke-width:1px;
style path1 fill:whitesmoke,stroke:#333,stroke-width:1px;
style path2 fill:whitesmoke,stroke:#333,stroke-width:1px;

classDef tree fill:wheat,stroke:#333,stroke-width:1px;
class mother,t1,t2,div,t3a,t3b,t4a,t4b,d1,d2 tree;


classDef branch fill:#bfe8bf,stroke:#333,stroke-width:1px;
class branch1,branch2,branch3 branch;


linkStyle 0 stroke:lightblue, stroke-width:2px;
linkStyle 1 stroke:lightsteelblue, stroke-width:2px;
linkStyle 2 stroke:lightblue, stroke-width:2px;
linkStyle 3 stroke:tomato, stroke-width:2px;
linkStyle 4 stroke:tomato, stroke-width:2px;

linkStyle 5 stroke:olivedrab, stroke-width:2px;
linkStyle 6 stroke:olivedrab, stroke-width:2px;

linkStyle 7 stroke:wheat, stroke-width:2px;
linkStyle 8 stroke:wheat, stroke-width:2px;
linkStyle 9 stroke:wheat, stroke-width:2px;
linkStyle 10 stroke:wheat, stroke-width:2px;
linkStyle 11 stroke:wheat, stroke-width:2px;
linkStyle 12 stroke:wheat, stroke-width:2px;
linkStyle 13 stroke:wheat, stroke-width:2px;
linkStyle 14 stroke:wheat, stroke-width:2px;

linkStyle 16 stroke:#c2abe7, stroke-width:2px;
linkStyle 17 stroke:#412234, stroke-width:2px;
linkStyle 18 stroke:#c2abe7, stroke-width:2px;
linkStyle 19 stroke:#412234, stroke-width:2px;
linkStyle 20 stroke:#c2abe7, stroke-width:2px;
linkStyle 21 stroke:#412234, stroke-width:2px;

```



And then creates a graph of interactions as shown below (Some links have been omitted for the sake of readability)

![neighbour_exchange_graph](/uploads/ac4232e87adb75e6edb73072d9f252b2/neighbour_exchange_graph.png)



<br><br>
__Computed features :__
 
  * In SegmentedObject Nodes:
    * Number of Neighbours Lost (NOT the net loss - identity of neighbours is taken into account)   
    * Number of Neighbours Gained (NOT the net gain - identity of neighbours is taken into account)

  * In LineageTrees and LineageBranch nodes:
    * Number of unique trees visited   
    * Number of unique branches visited
 
  * For each VISITS link:
    * Number of visits
    * Average visit time

  * For each SISTER or DAUGHTER link:
    * Number of visited branches that have been visited by only one of the the two branches (symmetric difference)
    * Number of branches that have been visited by the two branches (intersection)
    * Number of trees that have been visited by only one of the the two branches (symmetric difference)
    * Number of trees that have been visited by the two branches (intersection)
