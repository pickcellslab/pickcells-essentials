package org.pickcellslab.pickcells.boundaries;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.vecmath.Point3f;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.queryui.QueryableChoiceDialog;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.datamodel.types.Boundary;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.BinaryMorphology;
import org.pickcellslab.pickcells.api.img.process.LabelToBinaryConverter;
import org.pickcellslab.pickcells.api.img.process.Operations;
import org.pickcellslab.pickcells.api.img.providers.NotInProductionException;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.mesh.DecimationException;
import org.pickcellslab.pickcells.api.mesh.MeshDecimator;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.pickcellslab.pickcells.features.SurfaceComputer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.Cursor;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.labeling.ConnectedComponents;
import net.imglib2.algorithm.labeling.ConnectedComponents.StructuringElement;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.view.Views;


@Module
public class BoundariesIdentifier extends AbstractAnalysis implements Analysis {

	private static Logger log = LoggerFactory.getLogger(BoundariesIdentifier.class);

	//Session reference
	private final DataAccess session;

	//Activation related Fields
	private boolean isActivable = true;

	private File home;

	private final ProviderFactoryFactory pfFactory;

	private final ImgIO io;

	private UITheme theme;


	public BoundariesIdentifier(UITheme theme, DataAccess access, ProviderFactoryFactory pfFactory, ImgIO io){
		this.theme = theme;
		this.pfFactory = pfFactory;
		this.io = io;
		this.session = access;
	}




	@Override
	public void  launch() throws AnalysisException {


		setStep(0);


		try {


			// Display a dialog alowing the user to choose which class of segmented object to process
			List<MetaClass> available = session.metaModel().classesExtending(SegmentedObject.class);
			if(available.isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no data compatible with this analysis yet, compute intrinsic features first.");
				return;
			}


			QueryableChoiceDialog<MetaClass> dialog = new  QueryableChoiceDialog<>(theme, session.dataRegistry(), available,"Choose the type of object from which to identiify boundaries", false);
			dialog.pack();
			dialog.setLocationRelativeTo(null);
			dialog.setModal(true);
			dialog.setVisible(true);

			if(dialog.wasCancelled())
				return;

			@SuppressWarnings("unchecked")
			Class<? extends SegmentedObject> clazz = (Class<? extends SegmentedObject>) ((MetaClass) dialog.getChoice().get()).itemClass(session.dataRegistry());


			if(home == null){
				//Get the image folder
				String f = UsefulQueries.experimentFolder(session);
				home = new File(f);		
			}


			// Get images from the database including their segmentation result
			List<Image> images = UsefulQueries.imagesWithSegmentations(session);

			if(images.isEmpty()){
				JOptionPane.showMessageDialog(null,"There are no images registered in the database");
				return;
			}





			//TODO remove , debugging only
			session.queryFactory().delete(Boundary.class).completely().getAll().run();




			//TODO ask user for the name to associate with the segmentationResults.
			//Look for segresults with this name + .tif which may already exist

			//TODO choose from create a segmentation pipeline or import segmentation results


			process(images, clazz);


		} catch ( Exception e1) {
			throw new AnalysisException("Unable to read the database", e1);
		}

	}






	@SuppressWarnings("unchecked")
	private <T extends NativeType<T> & RealType<T>, S extends SegmentedObject> void process(List<Image> images, Class<S> clazz) 
			throws IOException, InterruptedException, ExecutionException, NotInProductionException, DataAccessException{

		ProviderFactory pf = pfFactory.create(2);

		for(Image i : images){

			setStep(1);

			SegmentationResult sr = i.getSegmentation(clazz.getSimpleName()).get();

			//Load the segmented image
			pf.addToProduction(sr);

			//Load associated data

			RegeneratedItems items = 
					session.queryFactory().regenerate(SegmentationResult.class).toDepth(1)
					.traverseLink(sr.linkType(), Direction.INCOMING)
					.withOneEvaluation(P.isSubType(clazz), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE)
					.includeOneKey(DataItem.idKey)
					.doNotGetAll()
					.useFilter(P.keyTest(DataItem.idKey, P.equalsTo(sr.getAttribute(DataItem.idKey).get())))
					.run();


			SegmentationImageProvider<UnsignedShortType> sp = pf.get(sr);
			sr = items.getOneTarget(SegmentationResult.class).get();
			Map<Integer, List<S>> data = items.getDependencies(clazz).collect( Collectors.groupingBy(s->s.frame()));
			sp.setKnownAssociatedData(data);


			for(int t = 0; t<i.frames(); t++){		

				//Get a ref to the actual Img
				RandomAccessibleInterval<UnsignedShortType> segmented = sp.getFrame(t);


				//Create a binary copy		
				setStep(2);
				RandomAccessibleInterval<BitType> binary = Operations.convert(io, segmented, new LabelToBinaryConverter<>(), new BitType(false));

				//Invert
				Operations.convert(binary, binary, BinaryMorphology.invertConverter(new BitType(false), new BitType(true)));


				//Check the highest label to start labeling negative structure from this value	

				setStep(3);

				int start = data.get(t).stream().max((d1,d2)->Float.compare(d1.label().get(), d2.label().get())).get().label().get().intValue()+2;

				System.out.println("Start Value: "+start);

				//ConnectedComponents erases the backing image so feed a new image to label
				Img<UnsignedShortType> result = io.createImg(segmented, new UnsignedShortType());;
				ConnectedComponents.labelAllConnectedComponents(binary, result, StructuringElement.FOUR_CONNECTED);

				//Now copy into segmented the non zero pixels from result
				Cursor<UnsignedShortType> sC = Views.iterable(segmented).cursor();
				Cursor<UnsignedShortType> rC = result.cursor();

				while(sC.hasNext()){
					UnsignedShortType rt = rC.next();
					UnsignedShortType st = sC.next();
					if(st.get()==0)
						st.setReal(rt.get()+start);
				}





				//release resource
				result = null;


				//Now for each structure, get the mesh and separate unconnected
				setStep(4);



				final int ft = t;
				sp.labels(t).forEach(f->{//TODO parallel

					System.out.println("-----------------------------------------------------------------------");
					System.out.println("Processing label "+f);

					SegmentedObject segObject = sp.getItem(f,ft).get();
					List<float[]> array = sp.getMesh(segObject, 2, false, false);

					if(!array.isEmpty()){

						TriangleCollection<UnsignedShortType> tc = new TriangleCollection<UnsignedShortType>(
								segmented
								,new UnsignedShortType(f.shortValue())
								,array);

						tc.removeBorderTriangles();
						Map<UnsignedShortType, List<Triangle<UnsignedShortType>>> map = tc.disjoints();

						System.out.println("Number of disjoint boundaries"+map.size());

						if(map.size()!=1){

							for(UnsignedShortType v : map.keySet()){	

								List<Point3f> meshPoints = TriangleCollection.translate((List)map.get(v),  tri-> new Point3f(i.floatCalibrated(tri, false, false)));
								try{
									//Simplify the mesh and assign to Boundary
									meshPoints = MeshDecimator.decimate(meshPoints, 1.2f);

								}catch (DecimationException e){
									log.warn("Could not simplify a mesh");
								}

								List<float[]> mesh = meshPoints.stream().map(p->new float[]{p.x,p.y,p.z}).collect(Collectors.toList());

								Boundary b = null;
								try {
									b = new Boundary(segObject, mesh, v.get(), home);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									return;
								}
								b.setAttribute(SurfaceComputer.surface, TriangleCollection.surface(map.get(v)));


							}
						}
					}
					//TODO compute other features
				});

			} //END of iter over frames




			/*
			//turn off channel
			if(order[Image.c] != -1){
				int cPos = i.channelIndex();
				order[Image.c] = -1;
				for(int j = cPos; j<order.length; j++){
					if(order[j]!=-1)
						order[j]--;
				}				
			}
			//io.save(order, cal, result, home+File.separator+"labeling.tif");
			io.save(order, cal,segmented, home+File.separator+"segmented.tif");
			 */


			//segmented = null;



			//Now save to database
			setStep(5);

			//Create a documented query
			StorageBoxBuilder builder = session.queryFactory().store().add(sr);

			//TODO ? Remove all the attributes that were previously know to 

			//Document keys
			//use the first item attached to labels as a prototype to document the MetaModel


			//The objects created by computers maybe large so add a limit on the size of transactions
			builder.setMaxCommitSize(3000);

			try {
				builder.run();
			} catch (DataAccessException e) {
				log.error("Unable to update the data graph ; cause -> "+e.getMessage());
			}					



		}//END of iter over images

	}



	@Override
	public String name() {
		return "Boundaries Detection";
	}





	@Override
	public boolean isActive() {
		return isActivable;
	}





	@Override
	public String[] categories() {
		return new String[]{"Segmentation"};
	}







	@Override
	public String description() {
		return "<HTML>Extract distinct boundaries in a segmented image."
				+ "</HTML>";
	}



	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/boundary_icon.png"));
	}





	@Override
	public Icon[] icons() {
		return new Icon[7];
	}




	@Override
	public String[] steps() {
		return new String[]{
				"Read DB",
				"Opening Image",
				"Binarization",
				"Labeling",
				"Finding Boundaries",
				"Updating DB",
				"Done"
		};
	}




	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}




}
