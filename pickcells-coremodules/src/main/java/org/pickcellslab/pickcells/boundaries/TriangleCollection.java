package org.pickcellslab.pickcells.boundaries;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.hypersphere.HyperSphere;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.ComplexType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.view.Views;

class TriangleCollection<T extends ComplexType<T>> {

	private Map<T,List<Triangle<T>>> disjoint = new HashMap<>();
	private List<Triangle<T>> all;
	private final RandomAccessibleInterval<T> labels;

	TriangleCollection(RandomAccessibleInterval<T> labels, T value, List<float[]> array){

		this.labels = labels;

		all = new ArrayList<>(array.size()/3);
		System.out.println("New Triangle Collection -> mesh size "+array.size());

		for(int p = 0; p<array.size(); p+=3){

			//Get the summit of the triangle
			float[] p1 = array.get(p);	 
			float[] p2 = array.get(p+1);		
			float[] p3 = array.get(p+2);		
			Triangle<T> t = new Triangle<>(p1,p2,p3,value);
			//if(t.area() == 0)
			//	System.out.println(t);
			//else
			all.add(t);			
		}
		System.out.println("Number of triangles in the collection "+all.size());
	}





	void removeBorderTriangles(){
		List<Triangle<T>> toRemove = new ArrayList<>();
		for(Triangle<T> t : all)
			if(t.isBorder(labels))
				toRemove.add(t);
		System.out.println("Number of removed triangles : "+toRemove.size());
		all.removeAll(toRemove);
	}






	Map<T,List<Triangle<T>>> disjoints(){

		if(!disjoint.isEmpty())
			return disjoint;


		HyperSphere<T> s = new HyperSphere<>(Views.extendZero(labels), labels.randomAccess(), 1);

		for(Triangle<T> t : all){
			T v = t.otherLabel(s);
			if(v != t.getLabel()){
				List<Triangle<T>> list = disjoint.get(v);
				if(null == list){
					list = new ArrayList<>();
					disjoint.put(v.copy(), list);
					System.out.println("New Type -> "+v);
				}
				list.add(t);
			}
		}	

		return disjoint;
	}











	public static <E> List<E> translate(List<Triangle> list, Function<float[],E> translator){

		Objects.requireNonNull(list, "The provided list of triangle is null");
		Objects.requireNonNull(translator, "The translator is null");

		List<E> translation = new ArrayList<>(list.size()*3);

		for(Triangle<?> t : list){
			translation.add(translator.apply(t.a));
			translation.add(translator.apply(t.b));
			translation.add(translator.apply(t.c));
		}

		System.out.println("Size of translated = " + translation.size());

		return translation;

	}









	public static <T extends Type<T>> float[] toMesh(List<Triangle<T>> list){
		float[] mesh = new float[list.size()*9];
		for(int tri = 0, m = 0; tri<list.size(); tri++, m+=9){			

			Triangle<?> t = list.get(tri);

			mesh[m] = t.a[0];
			mesh[m + 1] = t.a[1];
			mesh[m + 2] = t.a[2];

			mesh[m + 3] = t.b[0];
			mesh[m + 4] = t.b[1];
			mesh[m + 5] = t.b[2];

			mesh[m + 6] = t.c[0];
			mesh[m + 7] = t.c[1];
			mesh[m + 8] = t.c[2];
		}		
		return mesh;
	}







	public static double surface(List<Triangle<UnsignedShortType>> list) {		
		double surface = 0;
		for(Triangle<UnsignedShortType> t : list)
			surface+=t.area();
		return surface;
	}


}
