package org.pickcellslab.pickcells.boundaries;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.img.array.ArrayImgs;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.view.Views;
import net.imglib2.view.composite.CompositeIntervalView;
import net.imglib2.view.composite.GenericComposite;




public abstract class Mesh {

	
	private static final Logger log = LoggerFactory.getLogger(Mesh.class);
	
	
	
	/**
	 * Creates a {@link CompositeIntervalView} of the provided mesh. This view allows to use the ImgLib2 data access system
	 * to traverse the mesh with a minimum memory print. Note that the provided array is not copied so changes to this array will 
	 * lead to undetermined results.
	 * @param array
	 * @param dim the number of dimensions of the space in which the mesh is defined
	 * @return A CompositeIntervalView of the mesh. The number of dimension of this view is 2. The first dimension corresponds
	 * to the number of vertices, the second to the number of simplices in the mesh. The CompositeType returned by the
	 * accessors of the view will have a dimension equals to dim
	 */
	public CompositeIntervalView<FloatType, ? extends GenericComposite<FloatType>> asImgLib2View(float[] array, int dim){
		
		long[] dims = new long[3];
		dims[0] = dim; // points dimensions
		dims[1] = array.length/dim; // number of vertices
		dims[2] = dims[1]/dim; // number of simplices
		
		return Views.collapse(Views.permute(ArrayImgs.floats(array, dims),0,2));
		
	}
	

	/**
	 * Folds the provided array into a List of sub arrays
	 * @param array The array to fold
	 * @param dim the length of the subarrays
	 * @return The List of subarrays
	 */
	public static List<float[]> asList(float[] array, int dim){
		
		if(array.length % dim != 0)
			throw new IllegalArgumentException("The length of the array must be a multiple of the provided number of dimension");
		
		List<float[]> result = new ArrayList<>(array.length/dim);
		for(int i = 0; i<array.length; i++){
			float[] f = new float[dim];
			for(int j = 0; j<dim; j++)
				f[j] = array[i+j];
			i+=dim;
		}
			
		return result;
	}
	

		
	
	/**
	 * Translates a list of objects into a 1 dimensional array 
	 * @param mesh A list of objects from which coordinates can be extracted
	 * @param translator A {@link Function} to extract coordinates from the entries of the list
	 * @param dim The number of dimension for which the mesh is defined
	 * @return A 1 dimensional float array of size mesh.size()*dim
	 * @see #asList(float[], int)
	 */
	public static <E> float[] toMesh1d(List<E> mesh, Function<E,float[]> translator, int dim){		
		
		if(null == mesh || mesh.isEmpty())
			return new float[0];
		
		Objects.requireNonNull(translator);
		
		float[] result = new float[mesh.size()*dim];
		int counter = 0;
		for(E e : mesh){			
			float[] t = translator.apply(e);
			for(int i=0;  i<dim; i++)
				result[counter++] = t[i];
		}		
		return result;
	}
	
	
	

}
