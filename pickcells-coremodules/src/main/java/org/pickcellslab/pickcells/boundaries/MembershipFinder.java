package org.pickcellslab.pickcells.boundaries;

import java.net.URL;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.util.MathArrays;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.QueryableChoiceDialog;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.RandomAccess;
import net.imglib2.type.numeric.RealType;


@Module
public class MembershipFinder extends AbstractAnalysis implements Analysis {

	private static Logger log = LoggerFactory.getLogger(MembershipFinder.class);

	private static float minOverlap = 0.9f;

	//Session reference
	private final DataAccess session;

	//Activation related Fields
	private boolean isActivable = true;


	private final ProviderFactoryFactory factory;
	private final NotificationFactory notif;
	private final UITheme theme;


	public MembershipFinder(UITheme theme, NotificationFactory notif, DataAccess access, ProviderFactoryFactory factory){
		this.theme = theme;
		this.session = access;
		this.factory = factory;
		this.notif = notif;
	}


	




	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void  launch() throws AnalysisException {


		setStep(0);



		List<MetaClass> segs = session.metaModel().classesExtending(SegmentedObject.class);
		segs.addAll(session.metaModel().classesExtending(ImageDot.class));

		if(segs.size()<2){
			JOptionPane.showMessageDialog(null, "At least 2 types of segmented objects are required for this module");
			return;
		}

		QueryableChoiceDialog sd = new QueryableChoiceDialog(theme, session.dataRegistry(), segs,"Choose the sources", false);
		sd.setModal(true);
		sd.pack();
		sd.setLocationRelativeTo(null);
		sd.setVisible(true);

		if(sd.wasCancelled())
			return;

		MetaClass source = (MetaClass) sd.getChoice().get();

		segs.remove(source);

		sd = new QueryableChoiceDialog(theme, session.dataRegistry(), segs,"Choose the targets", false);
		sd.setModal(true);
		sd.pack();
		sd.setLocationRelativeTo(null);
		sd.setVisible(true);

		if(sd.wasCancelled())
			return;

		MetaClass target = (MetaClass) sd.getChoice().get();



		boolean isDot = ImageDot.class.isAssignableFrom(source.itemClass(session.dataRegistry()));


		// Ask for the minimum percentage of overlap		
		final SpinnerNumberModel model = new SpinnerNumberModel(minOverlap, 0.01, 1, 0.05);
		final JSpinner spinner = new JSpinner(model);
		JPanel panel = new JPanel();
		panel.add(new JLabel("Choose the minimum percentage of overlap"));
		panel.add(spinner);
		int o = JOptionPane.showConfirmDialog(null, panel, "Overlap options...", JOptionPane.OK_CANCEL_OPTION);
		if(o == JOptionPane.CANCEL_OPTION)
			return;

		minOverlap = model.getNumber().floatValue();


		//Now run for each image
		Set<Image> images = null;
		try {
			images = session.queryFactory()
					.regenerate(DataRegistry.typeIdFor(Image.class))
					.toDepth(0).traverseAllLinks()
					.includeAllNodes().regenerateAllKeys().getAll()
					.getTargets(Image.class).collect( Collectors.toSet());
		} catch (Exception e) {
			throw new AnalysisException("Unable to read the database", e);
		}


		setStep(1);

		int c = 0;
		for(Image i : images){


			Map<Integer, List<ImageLocated>> sources;
			try {
				sources = UsefulQueries.imageToImageLocated(
						session,
						source,
						i.getAttribute(DataItem.idKey).get())
						.getAllItemsFor(source.itemClass(session.dataRegistry()))
						.map(il->(ImageLocated)il)
						.collect(Collectors.groupingBy(s->s.frame()));
			} catch (Exception e1) {
				throw new AnalysisException("Error reading sources : "+source, e1);
			}




			if(sources.size() == 0){
				log.warn("No "+source.name()+" in "+i.toString());
				continue;
			}

			/*
			System.out.println("Sources size = "+sources.size());
			LabelsImage label = ((SegmentedObject) sources.get(0)).origin().get();
			for(NodeItem s : sources)
				assert  ((SegmentedObject)s).origin().get().equals(label);
			System.out.println("Sources all from image = "+label);
			*/
			
			Map<Integer, List<SegmentedObject>> targets;
			try {
				targets = UsefulQueries.imageToImageLocated(
						session,
						target,
						i.getAttribute(DataItem.idKey).get())
						.getAllItemsFor(target.itemClass(session.dataRegistry()))
						.map(il->(SegmentedObject)il)
						.collect( Collectors.groupingBy(so->so.frame()));
			} catch (Exception e1) {
				throw new AnalysisException("Error reading targets : "+target, e1);
			}



			if(targets.size() == 0){
				log.warn("No Structure in "+i.toString());
				continue;
			}
			
			/*
			System.out.println("Targets size = "+targets.size());
			label = ((SegmentedObject) targets.get(0)).origin().get();
			for(DataItem s : targets)
				assert  ((SegmentedObject)s).origin().get().origin().equals(i);
			System.out.println("Targets all from image = "+label);
			*/


			final String vectorName = "Vector To "+target.name();
			boolean save = false;
			try{			
				if(isDot)
					save = findMembersForDots((Map)sources, targets, i.isTimeLapse());
				else
					save = findMembersForSegmented((Map)sources, targets, minOverlap, !i.isZStack(), i.isTimeLapse(), vectorName);
			}catch(InterruptedException | ExecutionException e){
				throw new AnalysisException("Error while reading images", e);
			}


			if(save){

				LabelsImage labels = targets.get(0).get(0).origin().get();
				TraverserConstraints tc = 
						Traversers.newConstraints().fromDepth(0).toDepth(2)
						.traverseLink(labels.linkType(),Direction.INCOMING)
						.and("MEMBER_OF", Direction.INCOMING)
						.includeAllNodes();
				
				StorageBoxBuilder box = session.queryFactory().store().add(labels,tc);

				if(!isDot){
					double[] prototype =  i.isZStack() ? new double[3] : new double[2];
					box.feed(AKey.get(vectorName,double[].class), prototype, "MembershipFinder", DataModel.DIRECTIONAL+": towards center of member structure", source);
					
				}
				try {
					box.run();
				} catch (DataAccessException e) {
					log.error("An error occured while saving the data to the database",e);
					continue;
				}

			}

			setProgress((float)++c/(float)images.size());
		}

		setStep(2);
	}




	private <T extends RealType<T>> boolean findMembersForDots(Map<Integer,List<ImageDot>> dots, Map<Integer,List<SegmentedObject>> structs, boolean isTL) throws InterruptedException, ExecutionException{

		try{

			//Load the LabelsImage for the sources (if not ImageDot)
			ProviderFactory pf = factory.create(1);


			//Load the LabelsImage for the structures
			LabelsImage structLabels = structs.get(0).get(0).origin().get();
			pf.addToProduction(structLabels);

			SegmentationImageProvider<T> sp = pf.get(structLabels);
			sp.setKnownAssociatedData(structs);


			for(int time : dots.keySet()){


				RandomAccess<? extends T> access = sp.getFrame(time).randomAccess();
				for(ImageDot dot : dots.get(time)){
					access.setPosition(dot.location());
					Optional<SegmentedObject> s = sp.getItem(access.get().getRealFloat(), time);
					s.ifPresent(o->{
						Link link = new DataLink("MEMBER_OF", dot, o, true);
						link.setAttribute(AKey.get("Overlap", Float.class), 1f);
					});
				}		

			}

		}catch(Exception e){
			notif.display("Error", "An error occured while finding members for image dots", e, Level.WARNING);
			return false;
		}


		return true;
	}



	private <T extends RealType<T>> boolean findMembersForSegmented(Map<Integer,List<SegmentedObject>> srces, Map<Integer,List<SegmentedObject>> structs, float min, boolean is2D, boolean isTL, String vectorName) throws InterruptedException, ExecutionException{

		try{

			//Load the LabelsImage for the sources (if not ImageDot)
			final ProviderFactory pf = factory.create(2);
			final LabelsImage sourceLabels = srces.get(0).get(0).origin().get();
			pf.addToProduction(sourceLabels);


			//Load the LabelsImage for the structures
			final LabelsImage targetLabels = structs.get(0).get(0).origin().get();
			pf.addToProduction(targetLabels);


			final SegmentationImageProvider<T> sp = pf.get(sourceLabels);
			sp.setKnownAssociatedData(srces);

			final SegmentationImageProvider<T> tp = pf.get(targetLabels);
			tp.setKnownAssociatedData(structs);



			//final MutableInt count = new MutableInt();
			for(int time : srces.keySet()){

				final RandomAccess<? extends T> tA = tp.getFrame(time).randomAccess();

				// Now for each source : 
				// Get all the points of the segmented object and check the value in tA
				// Map each new value to a list of points
				// If the size of the list is above minOverlap create a new link

				for(SegmentedObject source : srces.get(time)){


					final MutableInt roiSize = new MutableInt();
					final Map<Float, MutableInt> overlaps = new HashMap<>();

					final Consumer<long[]> c = l->{
						roiSize.increment();
						tA.setPosition(l);
						float tgLabel = tA.get().getRealFloat();
						if(tgLabel != 0 && tp.getItem(tgLabel, time).isPresent()){
							MutableInt counter = overlaps.get(tgLabel);
							if(counter==null){
								counter = new MutableInt();
								overlaps.put(tgLabel, counter);
							}
							counter.increment();
						}
					};


					sp.processPoints(source.label().get(), time, c);


					final float minSize = roiSize.intValue() * min;

					overlaps.forEach( (tgLabel,counter) -> {
						//System.out.println("--------- Checking length - "+l.size());
						//System.out.println("--------- Checking label - "+f);
						if(counter.intValue()>=minSize){
							Link link = new DataLink("MEMBER_OF", source, tp.getItem(tgLabel, time).get(), true);
							link.setAttribute(AKey.get("Overlap", Float.class), counter.floatValue()/roiSize.floatValue());
							double d = MathArrays.distance(
									link.source().getAttribute(Keys.centroid).get(), 
									link.target().getAttribute(Keys.centroid).get());
							link.setAttribute(Keys.distance, d);
							//count.increment();
						}
					});


					//Now assign the vector towards the highest overlap
					if(overlaps.size() != 0){

						float max = Float.NaN;
						int maxOL = Integer.MIN_VALUE;
						for(float l : overlaps.keySet()){
							if(tp.getItem(l, time).isPresent())
								if( overlaps.get(l).intValue() > maxOL)
									max = l;
						}

						//System.out.println("max = "+max);

						if(!Float.isNaN(max)){

							double[] sC = source.getAttribute(SegmentedObject.centroid).get();
							double[] tC = tp.getItem(max, time).get().getAttribute(SegmentedObject.centroid).get();

							if(is2D){
								Vector2D v = new Vector2D(tC[0]-sC[0],tC[1]-sC[1]);	
								if(v.getNorm()!=0)
									v = v.normalize();
								source.setAttribute(AKey.get(vectorName,double[].class),v.toArray());
							}
							else{
								Vector3D v = new Vector3D(tC[0]-sC[0],tC[1]-sC[1], tC[2]-sC[2]);	
								if(v.getNorm()!=0)
									v = v.normalize();
								source.setAttribute(AKey.get(vectorName,double[].class),v.toArray());
								//System.out.println("vector assigned = "+v.normalize().toString());
							}
						}

					}

				}

			}


			//System.out.println("Number of associations = "+count.intValue());

		}catch(Exception e){
			notif.display("Error", "An error occured while identifying memberships", e, Level.WARNING);
			return false;
		}



		return true;
	}


	
	@Override
	public String name() {
		return "Memberships";
	}




	@Override
	public boolean isActive() {
		return isActivable;
	}






	@Override
	public String[] categories() {
		return new String[]{"Associations"};
	}






	@Override
	public String description() {
		return "<HTML>Create relationships between segmented objects if their labels overlap.</HTML>";
	}



	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/overlap_icon.png"));
	}


	@Override
	public Icon[] icons() {
		return new Icon[3];
	}


	@Override
	public String[] steps() {
		return new String[]{"Inputs","Finding Members","Done"};
	}







	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}







	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}







	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}







	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}







	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}




}
