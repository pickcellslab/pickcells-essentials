package org.pickcellslab.pickcells.boundaries;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.queries.builders.StorageBoxBuilder;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.QueryableChoiceDialog;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Boundary;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.geometry.bvh.BVH;
import org.pickcellslab.pickcells.api.geometry.bvh.BVTTNode;
import org.pickcellslab.pickcells.api.geometry.bvh.ClosestMeshFinder;
import org.pickcellslab.pickcells.api.img.providers.NotInProductionException;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Module
public class RelativeConnector extends AbstractAnalysis {

	private static Logger log = LoggerFactory.getLogger(RelativeConnector.class);


	//Base link type created by this connector
	public final String baseType = "RELATIVE_TO";

	//Session reference
	private final DataAccess access;

	//Activation related Fields
	private boolean isActivable = true;

	private ProviderFactoryFactory pff;
	private final UITheme theme;




	public RelativeConnector(UITheme theme, DataAccess access, ProviderFactoryFactory pff) {
		this.theme = theme;
		this.pff = pff;
		this.access = access;
	}





	@Override
	public void launch() throws AnalysisException {

		setStep(0);

		//Ask for the sources
		QueryableChoiceDialog<MetaClass> sd = new QueryableChoiceDialog<>(theme, access, "Please, choose the source data type", 
				(mq) -> mq instanceof MetaClass && ImageLocated.class.isAssignableFrom(((MetaClass) mq).itemClass(access.dataRegistry())), true);

		sd.setModal(true);
		sd.pack();
		sd.setLocationRelativeTo(null);
		sd.setVisible(true);

		if(sd.wasCancelled()){
			setCancelled();
			sd.dispose();
			return;
		}


		Optional<MetaClass> sOpt = sd.getChoice();

		if(!sOpt.isPresent()) {
			setCancelled();
			return;
		}


		//Ask for the targets
		QueryableChoiceDialog<MetaClass> td = new QueryableChoiceDialog<>(theme, access, "Please, choose the target data type", 
				mq -> mq instanceof MetaClass 
				//&&
				//((MetaClass) mq).itemClass() != sOpt.get().itemClass()
				&&
				(ImageLocated.class.isAssignableFrom(((MetaClass) mq).itemClass(access.dataRegistry()))
						|| Boundary.class == ((MetaClass) mq).itemClass(access.dataRegistry()))
				, true);


		td.setModal(true);
		td.pack();
		td.setLocationRelativeTo(null);
		td.setVisible(true);

		if(td.wasCancelled()){
			setCancelled();
			td.dispose();
			return;
		}

		Optional<MetaClass> tOpt = td.getChoice();
		if(!tOpt.isPresent()) {
			setCancelled();
			return;
		}


		MetaClass target = tOpt.get();


		//Ask for the max distance above which relationships should not be considered
		// As well as a relative variation threshold
		float maxClip = 50f;
		float assoRule = 0f;
		boolean ok = false;
		while(!ok){
			String input = JOptionPane.showInputDialog(
					"Enter the max distance above which relationships should not be considered (real space units)", 
					maxClip
					);
			if(null == input){
				setCancelled();
				return;
			}
			try{
				maxClip = Float.parseFloat(input);
			}catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null, "The value entered is not a valid number");
			}

			input = JOptionPane.showInputDialog(
					"Multiple association rule: Max distance defined as a multiplier of the shortest distance found (\"0\" for unique association)", 
					assoRule
					);
			if(null == input){
				setCancelled();
				return;
			}
			try{
				assoRule = Float.parseFloat(input);
				ok = true;
			}catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null, "The value entered is not a valid number");
			}
		}

		setStep(2);


		//Determine if the sources are ImageDot
		boolean sourceIsDot = ImageDot.class.isAssignableFrom(((MetaClass) sOpt.get()).itemClass(access.dataRegistry()));
		boolean targetIsDot = ImageDot.class.isAssignableFrom(((MetaClass) tOpt.get()).itemClass(access.dataRegistry()));


		//Build the name of the links and vectors which will be created
		StringBuilder sb = new StringBuilder();
		sb.append(baseType);
		sb.append("_"+target.name());
		String linkType = sb.toString();
		sb.replace(0, baseType.length()-1, "Vector");
		String vType = sb.toString();


		
		String chosenType = JOptionPane.showInputDialog("Choose a type for created links", linkType);
		
		if(chosenType == null || chosenType.isEmpty()) {
			this.setCancelled();
			return;
		}
					
		if(access.metaModel().getMetaLinks(chosenType).size()>0) {
			JOptionPane.showMessageDialog(null, 
					"This link type already exist", "", JOptionPane.WARNING_MESSAGE);
			this.setCancelled();
			return;
		}
		linkType = chosenType;
		
		
		
		String chosenName = JOptionPane.showInputDialog("Choose a name for the directional vector ", vType);
		if(chosenName == null || chosenName.isEmpty()) {
			this.setCancelled();
			return;
		}
		vType = chosenName;


		//TODO remove debug only
		try {
			access.queryFactory().deleteLinks(linkType).completely()
			.useFilter(P.sourcePasses(P.isSubType((Class<? extends NodeItem>) ((MetaClass) sOpt.get()).itemClass(access.dataRegistry()))))
			.run();
			
			// TODO remove vType as well
			
		} catch (DataAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			setFailure("Unable to update the database");
			return;
		}





		//Now run for each image
		List<Image> images = null;
		try {

			images = UsefulQueries.imagesWithSegmentations(access);

		} catch (Exception e) {
			setFailure("Unable to read the database");
			throw new AnalysisException("Unable to read the database", e);
		}




		@SuppressWarnings("unchecked")
		final Class<? extends ImageLocated> sClass =  (Class<? extends ImageLocated>)  sOpt.get().itemClass(access.dataRegistry());
		final Class<?> tClass = target.itemClass(access.dataRegistry());




		ProviderFactory factory = pff.create(2);



		//  *********************** Run for each image in the database *************************



		int c = 0;
		for(Image i : images){


			log.info("Processing  "+i.toString());


			// Load the providers if we have SegmentedObjects

			SegmentationImageProvider<?> sourceSp = null;
			LabelsImage sourceLabelImage = null;

			SegmentationImageProvider<?> targetSp = null;
			LabelsImage targetLabelImage = null;

			if(SegmentedObject.class.isAssignableFrom(sClass)){
				// we need to get the segmented result to recreate the mesh

				sourceLabelImage = i.getSegmentation(DataRegistry.typeIdFor(sClass))
						.orElseThrow(()->new AnalysisException("No segmentation was found for the type "+sClass, null));
				factory.addToProduction(sourceLabelImage);
				try {
					sourceSp = factory.get(sourceLabelImage);
				} catch (InterruptedException | ExecutionException | NotInProductionException e) {
					throw new AnalysisException("Failure to load the segmentation provider for "+sClass, e);
				}

			}

			if(SegmentedObject.class.isAssignableFrom(tClass)){
				// we need to get the segmented result to recreate the mesh

				targetLabelImage = i.getSegmentation(DataRegistry.typeIdFor((Class<? extends NodeItem>) tClass))
						.orElseThrow(()->new AnalysisException("No segmentation was found for the type "+tClass, null));
				factory.addToProduction(targetLabelImage);
				try {
					targetSp = factory.get(targetLabelImage);
				} catch (InterruptedException | ExecutionException | NotInProductionException e) {
					throw new AnalysisException("Failure to load the segmentation provider for "+tClass, e);
				}

			}



			// Iterate over time
			for(int time = 0; time<i.frames(); time++){


				// Create the predicate on time frame
				final int frame = time;
				ExplicitPredicate<DataItem> framePredicate = F.select(ImageLocated.frameKey, 0).equalsTo(frame);


				log.info("    At Frame : "+time);




				List<? extends ImageLocated> all = null;
				List<ImageLocated> sources = new ArrayList<>();
				List<NodeItem> targets = new ArrayList<>();

				final List<BVH<DataItem>> allBVH = new ArrayList<>();
				final List<BVH<DataItem>> sourceBVH = new ArrayList<>();				
				final List<BVH<DataItem>> targetsBVH = new ArrayList<>();






				// =============== Handle special case when sources and targets are the same types ==============

				if(sClass == tClass) {


					try {

						// Obtain all the objects in the image
						all = UsefulQueries.imageToImageLocated(access, sOpt.get(), framePredicate, i.getAttribute(DataItem.idKey).get())
								.getAllItemsFor(sClass).collect(Collectors.toList());



					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						continue;
					}



					//Build the bvhs
					// 1- Create a BVH for each mesh
					if(sourceIsDot)
						all.forEach(s -> {
							List<float[]> sMesh = new ArrayList<>(1);
							double[] posL = s.getAttribute(Keys.centroid).get();
							float[] posF = new float[posL.length];
							for(int x = 0; x<posL.length; x++)
								posF[x] = (float) posL[x];
							sMesh.add(posF);
							allBVH.add(new BVH<DataItem>(sMesh, s));
						});
					else{
						for(ImageLocated s : all){
							List<float[]> sMesh = sourceSp.getMesh((SegmentedObject) s, 2, false, true);
							if(sMesh.size()>0)
								allBVH.add(new BVH<DataItem>(sMesh, s));
						}
						log.info("Meshes retrieved");
					}


					final Optional<MetaFilter> sFilter = sd.getFilter();
					if(sFilter.isPresent()) {
						ExplicitPredicate<Object> p = sFilter.get().toFilter();
						for(BVH<DataItem> s : allBVH){
							if(p.test(s.getUserObject())) {
								sourceBVH.add(s);	
								sources.add((ImageLocated)s.getUserObject());
							}
						}
					}
					else
						for(BVH<DataItem> s : allBVH){
							sourceBVH.add(s);	
							sources.add((ImageLocated)s.getUserObject());
						}


					final Optional<MetaFilter> tFilter = td.getFilter();
					if(tFilter.isPresent()) {
						ExplicitPredicate<Object> p = tFilter.get().toFilter();
						for(BVH<DataItem> s : allBVH){
							if(p.test(s.getUserObject())) {
								targetsBVH.add(s);			
								targets.add((NodeItem)s.getUserObject());
							}
						};
					}
					else
						for(BVH<DataItem> s : allBVH){
							targetsBVH.add(s);			
							targets.add((NodeItem)s.getUserObject());
						};

				}// END of Special Case sClass == tClass	


				
				
				
				
				else {


					// ===============  Get the source list  ==========================

					try {

					if(sd.getFilter().isPresent())
						sources = UsefulQueries.imageToImageLocated(access, sOpt.get(), 
								sd.getFilter().get().toFilter()
								.merge(Op.Bool.AND, (ExplicitPredicate)framePredicate)
								, i.getAttribute(DataItem.idKey).get())
						.getAllItemsFor(sClass).collect(Collectors.toList());
					else
						sources = UsefulQueries.imageToImageLocated(access, sOpt.get(), framePredicate, i.getAttribute(DataItem.idKey).get())
						.getAllItemsFor(sClass).collect(Collectors.toList());


					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						continue;
					}


					

					if(sources.size() == 0){
						log.warn("No source found in "+i.toString());
						continue;
					}
					else
						log.info("Number of sources in frame "+time+" : "+sources.size());



					// 1- Create  the sources BVH for each mesh
					if(sourceIsDot)
						sources.forEach(s -> {
							List<float[]> sMesh = new ArrayList<>(1);
							double[] posL = s.getAttribute(Keys.centroid).get();
							float[] posF = new float[posL.length];
							for(int x = 0; x<posL.length; x++)
								posF[x] = (float) posL[x];
							sMesh.add(posF);
							sourceBVH.add(new BVH<DataItem>(sMesh, s));
						});
					else{


						for(ImageLocated s : sources){
							List<float[]> sMesh = sourceSp.getMesh((SegmentedObject) s, 2, false, true);
							if(sMesh.size()>0)
								sourceBVH.add(new BVH<DataItem>(sMesh, s));
						}


						log.info("Source meshes retrieved");
					}





					// ===============  Get the target list  ==========================



					try{

						if(tClass == Boundary.class){
							ExplicitPredicate<DataItem> p = td.getFilter().isPresent() ? 
									td.getFilter().get().toFilter().merge(Op.Bool.AND, (ExplicitPredicate)framePredicate) : framePredicate;
									targets = UsefulQueries.imageToBoundary(access, p, i.getAttribute(DataItem.idKey).get())
											.getAllItemsFor((Class<? extends NodeItem>)tClass).collect(Collectors.toList());
						}
						else{

							log.info("Target type = "+tClass.getSimpleName());
							if(td.getFilter().isPresent())
								targets = 
								UsefulQueries.imageToImageLocated(access, tOpt.get(), td.getFilter().get().toFilter().merge(Op.Bool.AND, (ExplicitPredicate)framePredicate) , i.getAttribute(DataItem.idKey).get())
								.getAllItemsFor((Class<? extends ImageLocated>)tClass).collect(Collectors.toList());
							else
								targets = 
								UsefulQueries.imageToImageLocated(access, tOpt.get(), framePredicate, i.getAttribute(DataItem.idKey).get())
								.getAllItemsFor((Class<? extends ImageLocated>)tClass).collect(Collectors.toList());

						}

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						continue;
					} 


					log.info("Number of targets : "+targets.size());


					if(targets.size() == 0){
						log.info("No targets found in "+i.toString());
						continue;
					}





					// 1- Create the targets BVH for each mesh
					if(tClass == Boundary.class){

						String f = null;
						try {
							f = UsefulQueries.experimentFolder(access);
						} catch (DataAccessException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						final File home = new File(f);		

						targets.forEach(t->{
							List<float[]> tMesh = null;
							try {
								tMesh = ((Boundary)t).mesh(home);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if(tMesh.size()>0)
								targetsBVH.add(new BVH<DataItem>(tMesh, t));
						});
					}
					else if(targetIsDot)
						targets.forEach(s -> {
							List<float[]> tMesh = new ArrayList<>(1);
							double[] posL = s.getAttribute(Keys.centroid).get();
							float[] posF = new float[posL.length];
							for(int x = 0; x<posL.length; x++)
								posF[x] = (float) posL[x];
							tMesh.add(posF);
							targetsBVH.add(new BVH<DataItem>(tMesh, s));
						});
					else{

						for(NodeItem t : targets){
							List<float[]> tMesh = targetSp.getMesh((SegmentedObject) t, 1, false, true);
							if(tMesh.size()>0)
								targetsBVH.add(new BVH<DataItem>(tMesh, t));
						}

					}



					log.info("RelativeConnector: Target meshes retrieved");


				}// END of Building source and targets BVHs




				//================================  Now Use ClosestMeshFinder to identify relationships =========================


				final String lType = linkType;
				final String vName = vType;
				
				//log.info("RelativeConnector: number of targets : "+targets.size());
				//System.out.println("RelativeConnector: number of source BVH : "+sourceBVH.size());
				//System.out.println("RelativeConnector: number of target BVH : "+targetsBVH.size());

				final float max = maxClip;
				final float rule = assoRule;
				sourceBVH.forEach(s ->{

					//Get all the pairs of mesh points selected by ClosestMeshFinder

					final List<BVTTNode<DataItem,DataItem>> pairs = ClosestMeshFinder.closestPoints(s, targetsBVH, 0.01f, rule, max);


					//System.out.print("-");
					//System.out.println("RelativeConnector: number of pairs: "+pairs.size());

					// 1- The pairs may connect the source with different targets
					//Create a map with an entry for each different target considered
					Map<DataItem, List<BVTTNode<DataItem,DataItem>>> entries = new HashMap<>();
					pairs.forEach(bvtt ->{
						List<BVTTNode<DataItem,DataItem>> list = entries.get(bvtt.target().getUserObject());
						if(null == list){
							list = new ArrayList<>();
							entries.put(bvtt.target().getUserObject(), list);
						}
						list.add(bvtt);
					});

					// 2- For each entry calculate:
					// a- The average distance between pairs of points and the stD of this distance
					// b- The source points barycenter and the targets points barycenter
					// c- The max eigenvalue of the covariance matrix of the cloud of point of the target as a measure of dispersion
					final MutableInt counter = new MutableInt();
					entries.forEach((t,bvtt) ->{

						float[] sBary = new float[3];
						float[] tBary = new float[3];
						SummaryStatistics stats = new SummaryStatistics();					
						double[][] matrix = new double[bvtt.size()][3];

						for(int p = 0; p<bvtt.size(); p++){

							// a- add to barycenter
							float[] sPoint = bvtt.get(p).source().center();						
							float[] tPoint = bvtt.get(p).target().center();
							double[] row = new double[3];
							for(int d = 0; d<3; d++){
								sBary[d] += sPoint[d];
								tBary[d] += tPoint[d];
								row[d] = tPoint[d];
							}						
							// b- add to distance stats
							stats.addValue(bvtt.get(p).distance());						
							// c- add to data sample for covariance
							matrix[p] = row;
						}

						for(int d = 0; d<3; d++){
							sBary[d] /= (float)bvtt.size();
							tBary[d] /= (float)bvtt.size();
						}	
						double mDist = stats.getMean();
						double vDist = stats.getStandardDeviation();

						//Create a link between the source and the target which will store the data
						Link l = new DataLink(lType, (NodeItem)bvtt.get(0).source().getUserObject(), (NodeItem)t, true);
						l.setAttribute(Keys.distance, mDist);
						l.setAttribute(AKey.get("distance deviation", Double.class), vDist);
						l.setAttribute(Keys.sourceAnchor, sBary);
						l.setAttribute(Keys.targetAnchor, tBary);

						//Now compute the dispersion
						double disp = 0;
						if(matrix.length>1){
							Covariance cov = new Covariance(matrix);
							RealMatrix covMatrix = cov.getCovarianceMatrix();
							EigenDecomposition decomp = new EigenDecomposition(covMatrix);
							disp = decomp.getRealEigenvalue(0);
						}
						l.setAttribute(AKey.get("uncertainty", Double.class), disp);
						
						counter.increment();
						if((counter.intValue()*100/entries.size() + 1) % 10 == 0)
							log.info("Relative Connector: "+counter.intValue()*100/entries.size()+"% done for "+i.toString());
						
					});


				});


				// if rule is 0, then a unique link must be given
				/*
				sources.forEach(s -> {
					if(s.getDegree(Direction.OUTGOING, linkType)>1) {					
						final Link valid = s.getLinks(Direction.OUTGOING, linkType)
								.sorted((l1,l2)->Double.compare(l2.getAttribute(Keys.distance).get(), l1.getAttribute(Keys.distance).get()))//Sort descending
								.findFirst().get();//shortest to keep
						s.getLinks(Direction.OUTGOING, linkType).forEach(toTest->{
							if(toTest != valid) {
								toTest.delete();
							}
						});
					}
				});
				*/

				//Now for each source compute the mean vector
				
				sources.forEach(s -> {
					Vector3D v = new Vector3D(0, 0, 0);
					final Iterator<Link> it = ((NodeItem)s).getLinks(Direction.OUTGOING, lType).iterator();
					while(it.hasNext()){
						Link l = it.next();
						float[] sV = l.getAttribute(Keys.sourceAnchor).get();
						float[] tV = l.getAttribute(Keys.targetAnchor).get();					
						Vector3D v2 = new Vector3D(tV[0]-sV[0],tV[1]-sV[1],tV[2]-sV[2]);
						if(!v2.equals(Vector3D.ZERO))
							v = v.add(v2.normalize());
					}
					if(!v.equals(Vector3D.ZERO))
						s.setAttribute(AKey.get(vName,double[].class),v.normalize().toArray());
				});



				// ============== Now write to the database ==================



				//Store each subgraph
				TraverserConstraints tc = Traversers.newConstraints().fromMinDepth().toDepth(1)
						.traverseLink(linkType, Direction.INCOMING)
						.includeAllNodes();

				//@SuppressWarnings("unchecked")
				//List<Traversal<NodeItem,Link>> roots = (List)Traversers.disconnected(targets, tc);

				//System.out.println("Number of graphs : "+roots.size());

				//if roots.getdegree == 0

				StorageBoxBuilder b = access.queryFactory().store();
				targets.forEach(s->b.add(s, tc));

				b.feed(AKey.get(vType,double[].class), new double[3], "Relative Connector", DataModel.DIRECTIONAL, sOpt.get());

				try {
					b.run();
				} catch (DataAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}// END of time itr


			setProgress((float)++c/(float)images.size());

		} // END of images itr

		setStep(3);


	}



	@Override
	public String name() {
		return "Spatial Relations";
	}







	@Override
	public boolean isActive() {
		return isActivable;
	}




	@Override
	public String[] categories() {
		return new String[]{"Associations"};
	}




	@Override
	public String description() {
		return "<HTML>Calculate relative positions between source objects and a closest target object.</HTML>";
	}



	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/relative_icon.png"));
	}




	@Override
	public Icon[] icons() {
		return new Icon[4];
	}




	@Override
	public String[] steps() {
		return new String[]{
				"Inputs",
				"Reading DB",
				"Finding Connections",
				"Done"
		};
	}





	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}





	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}





	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}







}
