package org.pickcellslab.pickcells.api.fitting;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Vector;

import org.pickcellslab.foundationj.dataviews.fitting.FitConfig;
import org.pickcellslab.foundationj.dataviews.fitting.UniVariateRealDistributionFitter;

import com.jmef.BregmanSoftClustering;
import com.jmef.MixtureModel;
import com.jmef.PVector;
import com.jmef.UnivariateGaussian;
import com.jmef.tools.KMeans;

public class UVNormalMixtureFitter implements UniVariateRealDistributionFitter<Integer, UVNormalMixtureModel> {

	private String last;

	@Override
	public Double[] sample( UVNormalMixtureModel distribution, int size) {
		return distribution.sample(size);
	}

	@Override
	public  UVNormalMixtureModel fit(List<Double> data, FitConfig<Integer> config) {


		// Variables
		int n = config.options();


		// Convert to PVectors
		PVector[] points = new PVector[data.size()];
		for(int i = 0; i<data.size(); i++){
			points[i] = new PVector(1);
			points[i].array[0] = data.get(i); 
		}
				
		
		
		Vector<PVector>[] clusters = KMeans.run(points, n);

		// Classical EM
		MixtureModel mmc;
		mmc    = BregmanSoftClustering.initialize(clusters, new UnivariateGaussian());
		mmc    = BregmanSoftClustering.run(points, mmc);
		last = "Mixure model estimated using BregmanSoftClustering \n" + mmc + "\n";
		/*
		mmc = ExpectationMaximization1D.initialize(clusters);
		mmc = ExpectationMaximization1D.run(points, mmc);
		last = "Mixure model estimated using classical EM \n" + mmc + "\n";
		*/

		//mmc.normalizeWeights();
		
		return new UVNormalMixtureModel(mmc);
	}

	@Override
	public String lastFitInfos() {
		return last;
	}



}
