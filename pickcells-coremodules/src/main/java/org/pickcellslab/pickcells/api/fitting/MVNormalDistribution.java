package org.pickcellslab.pickcells.api.fitting;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistribution;

public class MVNormalDistribution implements MultiVariateRealDistribution {


	private final MultivariateNormalDistribution nd ;

	MVNormalDistribution(MultivariateNormalDistribution distri){
		nd = distri;
	}


	@Override
	public int dimensions() {
		return nd.getDimension();
	}

	@Override
	public double density(double[] coordinates) {
		return nd.density(coordinates);
	}


	@Override
	public String summary() {
		String s =  "<HTML>Normal Distribution: <br>"
				+ "Number of dimensions : "+dimensions()+"<br>";
		for(int i = 0; i<dimensions(); i++){
			s+= "Means: "+ Arrays.toString(nd.getMeans())+"<br>";
			s+= "Stds: "+ Arrays.toString(nd.getStandardDeviations())+"<br></HTML>";
		}
		
		return s;
	}

}
