package org.pickcellslab.pickcells.api.fitting;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;

import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.dataviews.fitting.FitConfig;
import org.pickcellslab.foundationj.dataviews.fitting.FittingMethod;

public class NormalMixtureMethod implements FittingMethod<Integer>{

	@Override
	public String getName() {
		return "Normal Mixture";
	}

	@Override
	public String getDescription() {
		return "Fits an n-dimensional dataset w";
	}

	@Override
	public int minNumberOfDimensions() {
		return 1;
	}

	@Override
	public int maxNumberOfDimensions() {
		return 10;
	}

	@Override
	public Optional<FitConfig<Integer>> config() {
		// TODO 
		Integer[] values = new Integer[maxNumberOfDimensions()-1];
		for(int i = 1; i<maxNumberOfDimensions(); i++)
			values[i-1] = i;
		Integer k = (Integer) JOptionPane.showInputDialog(null,
				"Please choose the number of components",
				"Normal Mixture", JOptionPane.PLAIN_MESSAGE, null,
				values, 1);
		
		if(k == null)
			return Optional.empty();
		
		FitConfig<Integer> config = new FitConfig<>(this, k);
		return Optional.of(config);
	}

}
