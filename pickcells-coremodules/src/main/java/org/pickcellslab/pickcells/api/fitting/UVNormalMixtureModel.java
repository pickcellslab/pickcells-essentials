package org.pickcellslab.pickcells.api.fitting;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.math3.distribution.NormalDistribution;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.MixtureModel;
import org.pickcellslab.foundationj.dataviews.fitting.UnivariateRealDistribution;

import com.jmef.PVector;

public class UVNormalMixtureModel implements MixtureModel<Double, UVNormalDistribution>, UnivariateRealDistribution {



	private final com.jmef.MixtureModel model;
	private Distribution<Double>[] result;

	public UVNormalMixtureModel(com.jmef.MixtureModel model){
		this.model = model;
	}

	Double[] sample(int size) {
		PVector[] pv = model.drawRandomPoints(size);
		Double[] d = new Double[pv.length];
		for(int i = 0; i<pv.length; i++)
			d[i] = pv[i].array[0];
		return d;
	}

	@Override
	public int dimensions() {
		return model.getDimension();
	}

	@Override
	public int numDistributions() {
		return model.weight.length;
	}

	@Override
	public double[] weights() {
		return model.weight;
	}


	@Override
	public Distribution<Double>[] distributions() {

		if(result == null){
			result = new UVNormalDistribution[numDistributions()];

			for(int i = 0; i<numDistributions(); i++){
				result[i] = new UVNormalDistribution(
						new NormalDistribution(null, ((PVector)model.param[i]).array[0], Math.sqrt(((PVector)model.param[i]).array[1]))
						);
			}
		}

		return result;
	}

	@Override
	public int[] predictions(Double[] coordinates) {
		int[] p = new int[coordinates.length];
		for(int i = 0; i<coordinates.length; i++)
			p[i] = predictions(coordinates[i]);
		return p;
	}

	@Override
	public int predictions(Double coordinate) {		
		double[] m = memberships(coordinate);
		double max = -1;
		int index = 0;
		for(int i = 0; i<m.length; i++)
			if(m[i]>max){
				max = m[i];
				index = i;			
			}
		return index;
	}

	@Override
	public double[][] memberships(Double[] coordinates) {
		double[][] memberships = new double[coordinates.length][numDistributions()];
		for(int i = 0; i<coordinates.length; i++)
			memberships[i] = memberships(coordinates[i]);
		return memberships;
	}



	@Override
	public double[] memberships(Double coordinate) {
		
		double[] m = new double[numDistributions()];
		Distribution<Double>[] components = distributions();
		for(int d = 0; d<m.length; d++)
			m[d] = components[d].density(coordinate);
		
		return m;
	}

	@Override
	public double[] densities(Double[] coordinates) {
		double[] densities = new double[coordinates.length];
		for(int i = 0; i<coordinates.length; i++)
			densities[i] = density(coordinates[i]);
		return densities;
	}

	@Override
	public double density(Double coordinate) {
		PVector p = new PVector(1);
		p.array[0] = coordinate;
		return model.density(p);
	}

	@Override
	public String summary() {
		StringBuilder b = new StringBuilder();
		b.append("Univariate Normal Mixture Model with "+numDistributions()+"<br>");
		for(int i = 0; i<numDistributions(); i++){
			b.append("Component 1 : Mean = "
		+((PVector)model.param[i]).array[0]+
		" ; Std = "+ Math.sqrt(((PVector)model.param[i]).array[1])+
		" ; Weight "+model.weight[i]+"<br>");
		}	
		return b.toString();
	}

}
