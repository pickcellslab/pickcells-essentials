package org.pickcellslab.pickcells.api.fitting;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dataviews.fitting.UniVariateRealDistributionFitterFactory;

@Module
public class UVNormalMixtureFitterFactory implements 
UniVariateRealDistributionFitterFactory<UVNormalMixtureFitter,Integer, UVNormalMixtureModel> {

	NormalMixtureMethod method = null;
	
	@Override
	public NormalMixtureMethod method() {
		if(method == null)
			method = new NormalMixtureMethod();
		return method;
	}

	@Override
	public UVNormalMixtureFitter createFitter() {
		return new UVNormalMixtureFitter();
	}

	@Override
	public String group() {
		return "Linear Mixture Model (Univariate)";
	}

	

}
