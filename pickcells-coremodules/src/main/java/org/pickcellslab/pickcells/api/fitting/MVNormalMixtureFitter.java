package org.pickcellslab.pickcells.api.fitting;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

import org.apache.commons.math3.distribution.MixtureMultivariateNormalDistribution;
import org.apache.commons.math3.distribution.fitting.MultivariateNormalMixtureExpectationMaximization;
import org.pickcellslab.foundationj.dataviews.fitting.FitConfig;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistributionFitter;

public class MVNormalMixtureFitter implements MultiVariateRealDistributionFitter<Integer, MVNormalMixtureModel>  {

	private String last = "";
	
	@Override
	public double[][] sample(MVNormalMixtureModel distribution, int size) {
		return distribution.sample(size);
	}

	@Override
	public MVNormalMixtureModel fit(List<double[]> data, FitConfig<Integer> config) {
		
		double[][] d = data.toArray(new double[data.size()-1][data.get(0).length-1]);
		MixtureMultivariateNormalDistribution estimate = MultivariateNormalMixtureExpectationMaximization.estimate(d, config.options());
		
		MultivariateNormalMixtureExpectationMaximization EM = new MultivariateNormalMixtureExpectationMaximization(d);
		EM.fit(estimate);
		
		MixtureMultivariateNormalDistribution model = EM.getFittedModel();
		
		last = "Multivariate Normal Mixture Model:\n"
				+ "Data size: "+data.size()+"\n"
				+ "Number of components: "+model.getComponents().size()
				+ "LogLikelyhood : "+EM.getLogLikelihood();
		
		return new MVNormalMixtureModel(model);
	}

	@Override
	public String lastFitInfos() {
		return last;
	}

}
