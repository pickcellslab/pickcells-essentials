package org.pickcellslab.pickcells.api.fitting;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.distribution.MixtureMultivariateNormalDistribution;
import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.MixtureModel;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistribution;

public class MVNormalMixtureModel implements MixtureModel<double[], MVNormalDistribution>, MultiVariateRealDistribution {



	final MixtureMultivariateNormalDistribution model;
	
	public MVNormalMixtureModel(MixtureMultivariateNormalDistribution model){
		this.model = model;
	}

	double[][] sample(int size) {
		return model.sample(size);
	}

	@Override
	public int dimensions() {
		return model.getDimension();
	}

	@Override
	public int numDistributions() {
		return model.getComponents().size();
	}

	@Override
	public double[] weights() {
		return model.getComponents().stream().mapToDouble(p->p.getFirst()).toArray();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Distribution<double[]>[] distributions() {
		return
				model.getComponents().stream().map(p->new MVNormalDistribution(p.getSecond()))
				.collect(Collectors.toList()).toArray(new Distribution[numDistributions()]);
	}

	@Override
	public int[] predictions(double[][] coordinates) {
		int[] p = new int[coordinates.length];
		for(int i = 0; i<coordinates.length; i++)
			p[i] = predictions(coordinates[i]);
		return p;
	}

	@Override
	public int predictions(double[] coordinate) {		
		double[] m = memberships(coordinate);
		double max = -1;
		int index = 0;
		for(int i = 0; i<m.length; i++)
			if(m[i]>max){
				max = m[i];
				index = i;			
			}
		return index;
	}

	@Override
	public double[][] memberships(double[][] coordinates) {
		double[][] memberships = new double[coordinates.length][numDistributions()];
		for(int i = 0; i<coordinates.length; i++)
			memberships[i] = memberships(coordinates[i]);
		return memberships;
	}



	@Override
	public double[] memberships(double[] coordinate) {

		double total = density(coordinate);
		List<Pair<Double, MultivariateNormalDistribution>> comp = model.getComponents();
		double[] memberships = new double[comp.size()];
		for(int i = 0; i<comp.size(); i++){
			Pair<Double, MultivariateNormalDistribution> p = comp.get(i);
			memberships[i] = p.getFirst()*p.getSecond().density(coordinate)/total;
		}

		return memberships;
	}

	@Override
	public double[] densities(double[][] coordinates) {
		double[] densities = new double[coordinates.length];
		for(int i = 0; i<coordinates.length; i++)
			densities[i] = model.density(coordinates[i]);
		return densities;
	}

	@Override
	public double density(double[] coordinate) {
		return model.density(coordinate);
	}

	
	
	@Override
	public String summary() {
		StringBuilder b = new StringBuilder();
		b.append("Multivariate Normal Distribution Mixture\n");		
		List<Pair<Double, MultivariateNormalDistribution>> comp = model.getComponents();
		for(int i = 0; i<comp.size(); i++){
			b.append("Component "+i+": \n");
			b.append("Weight = "+comp.get(i).getFirst()+":\n");
			b.append("Means = "+Arrays.toString(comp.get(i).getSecond().getMeans())+":\n");
			b.append("Std = "+Arrays.toString(comp.get(i).getSecond().getStandardDeviations())+":\n");
		}
		return b.toString();
	}

}
