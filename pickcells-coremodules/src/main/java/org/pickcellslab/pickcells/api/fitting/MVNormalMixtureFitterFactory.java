package org.pickcellslab.pickcells.api.fitting;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistributionFitterFactory;

@Module
public class MVNormalMixtureFitterFactory implements 
MultiVariateRealDistributionFitterFactory<MVNormalMixtureFitter,Integer, MVNormalMixtureModel> {

	NormalMixtureMethod method = null;
	
	@Override
	public NormalMixtureMethod method() {
		if(method == null)
			method = new NormalMixtureMethod();
		return method;
	}

	@Override
	public MVNormalMixtureFitter createFitter() {
		return new MVNormalMixtureFitter();
	}

	@Override
	public String group() {
		return "Linear Mixture Model";
	}

	

}
