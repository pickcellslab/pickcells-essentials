package org.pickcellslab.pickcells.api.fitting;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.math3.distribution.NormalDistribution;
import org.pickcellslab.foundationj.dataviews.fitting.UnivariateRealDistribution;

public class UVNormalDistribution implements UnivariateRealDistribution {


	private NormalDistribution nd;

	public UVNormalDistribution(NormalDistribution nd) {
		this.nd = nd;		
	}

	@Override
	public int dimensions() {
		return 1;
	}

	@Override
	public double density(Double coordinates) {
		return nd.density(coordinates);
	}

	@Override
	public String summary() {
		String s =  "<br>Normal Distribution: <br>"+
				"Means : "+ nd.getMean()+"<br>"+
				"Std : "+nd.getStandardDeviation();

		return s;
	}

	

}
