package org.pickcellslab.pickcells.editing;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.threads.ModeratedThread;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.editors.AnnotationManagerWritable;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.view.AbstractAnnotationManager;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatus;
import org.pickcellslab.pickcells.api.img.view.AnnotationStatusListener;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.EllipseNeighborhood;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.view.Views;

public class ImageDotEditorManager extends AbstractAnnotationManager implements AnnotationManagerWritable{


	enum Status implements AnnotationStatus{
		DELETED{
			@Override
			public int preferredARGB() {
				return ARGBType.rgba(255, 0, 0, 255);
			} 
		},
		ADDED{
			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 255, 0, 255);
			} 
		},
		MOVED{
			@Override
			public int preferredARGB() {
				return ARGBType.rgba(0, 0, 255, 255);
			} 
		},

	}




	private final Image image;

	private final ImageDot prototype;

	private final Set<Integer> toDel = new HashSet<>();
	private final Map<Integer, List<DotAnnotation>> frameMap;	
	private final Map<Integer,DotAnnotation> idCache = new HashMap<>();


	private int visibleFrameDepth = 3, radius = 2;


	private int tPos, zPos;

	private RandomAccessibleInterval<IntType> drawnDots;
	private final RandomAccess<IntType> na;

	private final ModeratedThread redrawThread = new ModeratedThread(()->redraw());


	private final DataAccess access;
	private final NotificationFactory notifier;


	private Set<DotAnnotation> handled = new HashSet<>();

	private boolean annotationsVisible = true;
	private final List<AnnotationStatusListener> sLstrs = new ArrayList<>();



	public ImageDotEditorManager(Image image, List<ImageDot> dots, DataAccess access, ImgIO io, NotificationFactory notifier) {

		Objects.requireNonNull(image, "Image is null");
		Objects.requireNonNull(dots, "the list of dots is null");
		Objects.requireNonNull(access, "access is null");

		if(!dots.isEmpty())
			throw new IllegalArgumentException("There are no ImageDot to edit");

		this.image = image;
		this.access = access;
		this.notifier = notifier;

		this.frameMap = dots.stream()
				.map(d->{
					final DotAnnotation da = new DotAnnotation(d);
					idCache.put(d.getAttribute(DataItem.idKey).get(), da);
					return da;
				})
				.collect(Collectors.groupingBy(d->d.dot().frame()));

		this.prototype = dots.get(0);

		drawnDots = io.createImg(
				new FinalInterval(image.getMinimalInfo().removeDimension(Image.c).removeDimension(Image.z).removeDimension(Image.t).imageDimensions()),
				new IntType());

		na = Views.extendZero(drawnDots).randomAccess();

		redrawThread.start();

	}




	@Override
	public MinimalImageInfo annotatedImageInfo() {
		return image.getMinimalInfo().removeDimension(Image.c); 
	}

	@Override
	public List<AnnotationStatus> possibleAnnotationStates() {
		List<AnnotationStatus> list = new ArrayList<>();
		list.add(AnnotationStatus.DESELECTED);
		list.add(AnnotationStatus.SELECTED);
		list.addAll(Arrays.asList(Status.values()));
		return list;
	}



	@Override
	public Map<String,List<Dimension<Annotation, ?>>> possibleAnnotationDimensions(Predicate<Dimension> filter, boolean decompose) {

		List<Dimension<Annotation, ?>> list = null;
		if(!decompose)
			list = (List)prototype.getValidAttributeKeys().map(k->Annotation.createDimension((AKey)k, prototype.getAttribute(k).get())).collect(Collectors.toList());		
		else
			list = (List)prototype.getValidAttributeKeys().flatMap(k->Annotation.createDecomposedDimension((AKey)k, prototype.getAttribute(k).get()).stream()).collect(Collectors.toList());		

		return Collections.singletonMap(prototype.declaredType(), list);
	}



	public void delete(Annotation a) {
		((DotAnnotation) a).setStatus(Status.DELETED);
		this.fireAnnotationRedrawRequired(drawnDots);
	}





	@Override
	public void inverseSelection() {
		this.stream("").forEach(a->{
			DotAnnotation sa = (DotAnnotation)a;			
			sa.toggleSelected();
		});
	}






	@Override
	public void setDragged(Annotation a, long[] newPos) {		
		if(!DotAnnotation.class.isAssignableFrom(a.getClass()))
			return;
		DotAnnotation da = (DotAnnotation) a;
		da.dot.setAttribute(Keys.location, Arrays.copyOf(newPos, newPos.length));
		da.dot.setAttribute(Keys.centroid, image.calibrated(newPos, false, false ));
		da.setStatus(Status.MOVED);
		handled.add(da);
		redrawThread.requestRerun();
	}


	public void add(long[] novel) {

		final int length = image.getMinimalInfo().removeDimension(Image.c).removeDimension(Image.t).numDimensions();

		final ImageDot dot = prototype.create(image, Arrays.copyOf(novel, length));
		dot.setAttribute(ImageLocated.frameKey, tPos);

		//Add to database in order to obtain a new dbId
		try {
			access.queryFactory().store().add(dot, Traversers.newConstraints().fromMinDepth().toDepth(1).traverseAllLinks().includeAllNodes()).run();
		} catch (DataAccessException e) {
			notifier.display("Error", "Unable to create a new Dot", e, Level.WARNING);			
		}

		final DotAnnotation da = new DotAnnotation(dot); //TODO signal!!		
		final int dbId = da.getProperty(DataItem.idKey);
		System.out.println("ImageDotEditorManager : Added id = "+dbId );
		frameMap.get(dot.getAttribute(ImageLocated.frameKey).get()).add(da);
		idCache.put(dbId, da);		
		handled.add(da);
		da.setStatus(Status.ADDED);		
		redrawThread.requestRerun();

	}



	@Override
	public void commitChanges() {

		try {

			new ArrayList<>(handled).forEach(da->da.validate());

			if(!toDel.isEmpty()){
				access.queryFactory().delete(prototype.getClass()).completely().useFilter(P.setContains(DataItem.idKey, toDel)).run();
				toDel.clear();
			}


			redrawThread.requestRerun();

		} catch (DataAccessException e) {
			throw new RuntimeException("Unable to commit changes to the database", e);
		}

	}




	@Override
	public Stream<Annotation> annotations(final long[] min, final long[] max) {
		return Stream.empty();
	}





	@Override
	public DotAnnotation getAnnotationAt(long[] clickPos) {


		final long[] pos = Arrays.copyOf(clickPos, 2);		
		na.setPosition(pos);
		if(na.get().get()!=0){
			return idCache.get(na.get().get());
		}
		return null;
	}



	@Override
	public void toggleSelectedAt(long[] pos) {
		toggleSelected(this.getAnnotationAt(pos));		
	}



	@Override
	public void toggleSelected(Annotation annotation) {
		if(annotation!=null){
			DotAnnotation da = (DotAnnotation) annotation;
			da.toggleSelected();
		}
		redrawThread.requestRerun();
	}




	@Override
	public void clearSelection() {
		new ArrayList<>(handled).forEach(da->{
			if(da.status() == Status.SELECTED){
				da.toggleSelected();
			}
		});
		redrawThread.requestRerun();
	}



	@Override
	public void setSelectedAsDeleted() {		
		new ArrayList<>(handled).forEach(da->{
			if(da.status() == Status.SELECTED){
				da.setStatus(Status.DELETED);
			}
		});
		redrawThread.requestRerun();
	}



	@Override
	public void undeleteAll() {
		final List<Annotation> undeleted = new ArrayList<>();
		new ArrayList<>(handled).forEach(da->{
			if(da.status() == Status.DELETED){
				undeleted.add(da);
				da.setStatus(Status.DESELECTED);
				handled.remove(da);
			}
		});
		redrawThread.requestRerun();
	}



	@Override
	public boolean undoAt(long[] pos) {

		final DotAnnotation da = this.getAnnotationAt(pos);
		if(null == da)
			return false;
		return da.undo();


	}





	@Override
	public void positionHasChanged(ImageDisplay source, DisplayPosition dp) {
		tPos = source.currentTimeFrame();
		zPos = source.currentZSlice();
		redrawThread.requestRerun();
	}




	private void redraw() {


		Views.iterable(drawnDots).forEach(t->t.setZero());

		if(annotationsVisible){

			long[] initCenter = drawnDots.numDimensions()== 2 ?  new long[]{0,0} : new long[]{0,0,0};
			long[] radii = drawnDots.numDimensions()== 2 ?  new long[]{radius, radius} : new long[]{radius, radius, 1};
			final EllipseNeighborhood<IntType> ellipse = new EllipseNeighborhood<>(drawnDots, initCenter, radii);

			getDotsAt(tPos, zPos, visibleFrameDepth).forEach(n->{
				ellipse.setPosition(n.getProperty(Keys.location));
				int value = n.getProperty(DataItem.idKey);
				ellipse.forEach(t->t.set(value));
			});
		}

		this.fireAnnotationRedrawRequired(drawnDots);


	}







	private Stream<DotAnnotation> getDotsAt(int tPos2, int zPos2, int depth) {
		return frameMap.get(tPos).stream().filter(getNodePredicate(zPos2,depth));
	}



	private Predicate<DotAnnotation> getNodePredicate(final int zPos, final int depth){
		if(image.getMinimalInfo().dimension(Image.z)>1)
			return il -> Math.abs(il.dot().location()[2] - zPos) < depth;
			else
				return il -> true;
	}



	@Override
	public Stream<? extends Annotation> stream(String type) {
		return getDotsAt(tPos,zPos,visibleFrameDepth);
	}








	@Override
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer) {
		produceCurrentPlaneIteration(stop,consumer,drawnDots);
	}








	@Override
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer, Interval itrv) {

		final long[] pos = new long[itrv.numDimensions()];

		final Cursor<IntType> lCursor = Views.interval(drawnDots, itrv).localizingCursor();

		//System.out.println("Producing plane");
		while(lCursor.hasNext()){
			final IntType  lT = lCursor.next();
			lCursor.localize(pos);
			if(lT.get()==0)
				consumer.accept(pos, null);
			else
				consumer.accept(pos, idCache.get(lT.get()));

		}
	}




	public void setZVisibilityDepth(int intValue) {
		this.visibleFrameDepth = intValue;
		redrawThread.requestRerun();
	}






	@Override
	public void toggleAnnotationsVisible() {
		annotationsVisible = !annotationsVisible;
		redrawThread.requestRerun();
	}








	@Override
	public boolean annotationsAreVisible() {
		return annotationsVisible;
	}








	@Override
	public void addAnnotationStatusListener(AnnotationStatusListener l) {
		sLstrs.add(l);
	}








	@Override
	public void removeAnnotationStatusListener(AnnotationStatusListener l) {
		sLstrs.remove(l);
	}










	private class DotAnnotation implements Annotation{

		private AnnotationStatus selStatus = AnnotationStatus.DESELECTED;
		private AnnotationStatus status = AnnotationStatus.DESELECTED;
		private final ImageDot dot;

		public DotAnnotation(ImageDot dot) {
			this.dot = dot;
		}



		public boolean undo() {
			if(status == Status.DELETED){
				toDel.remove(dot.getAttribute(DataItem.idKey).get());
				handled.remove(this);				
				status = AnnotationStatus.DESELECTED;
				sLstrs.forEach(l->l.statusChanged(this));
				return true;
			}
			return false;
		}



		public ImageDot dot(){
			return dot;
		}


		public void validate() {
			handled.remove(this);			
			if(status == Status.DELETED){
				toDel.add(dot.getAttribute(DataItem.idKey).get());
				idCache.remove(dot.getAttribute(DataItem.idKey).get());
				frameMap.get(dot.frame()).remove(this);
			}
		}


		public void toggleSelected() {
			if(selStatus == Status.SELECTED){
				selStatus = Status.DESELECTED;
				handled.remove(this);
			}
			else if(selStatus == Status.DESELECTED){
				selStatus = Status.SELECTED;
				handled.add(this);
			}
			sLstrs.forEach(l->l.statusChanged(this));
		}


		void setStatus(AnnotationStatus s){			
			if(s == Status.SELECTED) {
				selStatus = Status.DESELECTED;
				handled.remove(this);
				sLstrs.forEach(l->l.statusChanged(this));
			}
			else if(s == Status.DESELECTED) {
				selStatus = Status.SELECTED;
				handled.add(this);
				sLstrs.forEach(l->l.statusChanged(this));
			}
			else if(status != s){
				this.status = s;
				this.selStatus = Status.DESELECTED;
				if(status==Status.DELETED)
					handled.add(this);
				sLstrs.forEach(l->l.statusChanged(this));
			}			
		}


		@Override
		public AnnotationStatus status() {
			return selStatus == Status.SELECTED ? Status.SELECTED : status;
		}



		@Override
		public Stream<AKey<?>> properties() {
			return dot.getValidAttributeKeys();
		}


		@Override
		public <E> E getProperty(AKey<E> k) {
			return dot.getAttribute(k).orElse(null);
		}

		
		@Override
		public String toString(){
			return dot.toString();
		}

		@Override
		public String representedType() {
			return dot.declaredType();
		}

	}




}
