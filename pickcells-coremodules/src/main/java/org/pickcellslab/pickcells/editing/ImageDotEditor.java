package org.pickcellslab.pickcells.editing;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.modules.ActivationListener;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.app.modules.Task;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.ImageDisplayFactory;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


@Module
public class ImageDotEditor<T extends RealType<T> & NativeType<T>> implements Task, MetaModelListener{
	private static final Logger log = LoggerFactory.getLogger(ImageDotEditor.class);

	private final DataAccess access;
	private final ImgIO io;
	private final ImageDisplayFactory dispFctry;
	private final NotificationFactory notifier;
	
	private final List<ActivationListener> alLstrs = new ArrayList<>();

	private boolean isActive = false;

	
	
	public ImageDotEditor(DataAccess access, ImgIO io, ImageDisplayFactory dispFctry, NotificationFactory notifier) {
		this.access = access;
		this.io = io;
		this.dispFctry = dispFctry;			
		this.notifier = notifier;
	}
	
	

	@Override
	public void launch() throws AnalysisException {

		// Check if there exists elligible ImageDots in the database (no other link than created from)
		List<MetaClass> available = access.metaModel().classesExtending(ImageDot.class);
		if(available.isEmpty()){
			JOptionPane.showMessageDialog(null, "There is no spot detected in the current database");
			return;
		}

		//Now filter
		available = available.stream().filter(mc->mc.metaLinks().collect(Collectors.counting()) == 1).collect(Collectors.toList());
		if(available.isEmpty()){
			JOptionPane.showMessageDialog(null, "There is no elligible spots in the current database. "
					+ "(Spots have been detected but have also been used within analysis. Editing should be the first step as it invalidates"
					+ "every subsequent tasks)");
			return;
		}


		// First allow to choose the type of dots to be edited
		MetaClass dotType = null;
		if(available.size() > 1){
			dotType = 
					(MetaClass) JOptionPane.showInputDialog(null,
							"Choose the type of Spots to be edited",
							"Spots Editor...",
							JOptionPane.PLAIN_MESSAGE,
							icon(),
							available.toArray(), available.get(0));
			if(dotType == null)
				return;
		}
		else
			dotType = available.get(0);


		// Now if there is more than one image, allow to choose which imgae should be edited
		try{

			Object[] images = 
					access.queryFactory().regenerate(Image.class).toDepth(0)
					.traverseAllLinks().includeAllNodes().regenerateAllKeys().getAll()
					.getAllTargets().toArray();


			Image choice = 
					(Image) JOptionPane.showInputDialog(null,
							"Choose the image to be edited",
							"Spots Editor...",
							JOptionPane.PLAIN_MESSAGE,
							icon(),
							images, images[0]);

			if(choice == null)
				return;


			//Now load the image with the dots attached to it
			final MetaClass fdotType = dotType;


			RegeneratedItems imageAndDots = 
					UsefulQueries.imageToImageLocated(access, fdotType, choice.getAttribute(DataItem.idKey).get());


			Image image = imageAndDots.getOneTarget(Image.class).get();
			List<ImageDot> dots = imageAndDots.getAllItemsFor(ImageDot.class).collect(Collectors.toList());



			// Now launch the display

			//Create the display
			final ImageDisplay display = dispFctry.newDisplay();

			//Add the color image
			final Img<T> color = io.open(image);
			display.addImage(image.getMinimalInfo(), color);
			//Name channels
			String[] channelNames = image.channels();
			for(int i=0; i<channelNames.length; i++){
				display.getChannel(0, i).setChannelName(channelNames[i]);
				//TODO display.getChannel(0, i).getAvailableLuts()
			}

			// Add annotations
			final ImageDotEditorManager mgr = new ImageDotEditorManager(image, dots, access, io, notifier);
			display.addAnnotationLayer("Annotations", mgr);

			/*

					//Save color changes in preferences
					final UserPreferences prefs = CrossDBRessources.getPrefsForCurrentUser(access);
					ColorTable[] luts = ImgDimensions.createColorTables(color.firstElement(), image.luts());			
					AnnotationEditorView<T> view = new AnnotationEditorView<>(manager, color, luts, image.order(), prefs);
			 */

			JToolBar toolBar = ImageDotEditorControllers.createControls(mgr, display);

			// Z visibility control
			final SpinnerNumberModel zModel = new SpinnerNumberModel(3, 0, 100, 1);
			final JSpinner zSpinner = new JSpinner(zModel);
			toolBar.add(new JLabel("    Z Visibility"));
			toolBar.add(zSpinner);
			zSpinner.addChangeListener(l-> {
				if(mgr!=null){
					mgr.setZVisibilityDepth(zModel.getNumber().intValue());
				}
			});
			
			
			
			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());
			panel.add(toolBar,BorderLayout.NORTH);
			panel.add(display.getView(), BorderLayout.CENTER);

			JFrame f = new JFrame();
			f.setContentPane(panel);
			f.pack();
			f.setLocationRelativeTo(null);
			f.setVisible(true);

			display.refreshDisplay();	






		}catch(Exception e){
			throw new AnalysisException("An error occured while running Spot Detector", e);
		}

	}




	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/DotEditor_32.png"));
	}



	@Override
	public String[] categories() {
		return new String[]{"Editing"};
	}

	@Override
	public String name() {
		return "Spots Editor";
	}

	@Override
	public String description() {
	    return "<HTML>Spots Editor: View an image with an overlay containing the location of detected spots.</HTML>";
	}

	



	@Override
	public void registerListener(ActivationListener lst) {
		alLstrs.add(lst);
	}



	@Override
	public boolean isActive() {
		return isActive;
	}






	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {

		/* TODO activability
		try {

			isActive = 
					! access.queryFactory().read(SegmentationResult.class).makeList(DataItem.idKey)
					.inOneSet().useFilter(P.hasKey(SegmentationResult.associated).negate()).run().isEmpty();


			if(al!=null)
				al.isNowActive(this, isActive);


		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 */
	}



	@Override
	public void start() {
		
		try {
			
			isActive = 
					! access.queryFactory().read(SegmentationResult.class).makeList(DataItem.idKey)
					.inOneSet().useFilter(P.hasKey(SegmentationResult.associated).negate()).run().isEmpty();
		
		} catch (DataAccessException e) {
			isActive = false;
			alLstrs.forEach(l->l.isNowActive(this, isActive));
			log.warn("ImageDotEditor failed to read the database - set to inactive", e);
		}
		
	}



	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

}
