package org.pickcellslab.pickcells.editing;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.pickcells.api.img.editors.Brushes;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;
import org.pickcellslab.pickcells.api.img.view.Annotation;
import org.pickcellslab.pickcells.api.img.view.BrushRadiiPanel;
import org.pickcellslab.pickcells.api.img.view.DisplayPositionListener;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEvent;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEventListener;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEventModel.SelectionMode;
import org.pickcellslab.pickcells.editing.ImageDotEditorManager.Status;

import com.alee.managers.tooltip.TooltipManager;

import net.imglib2.realtransform.AffineGet;
import net.imglib2.ui.TransformListener;

public final class ImageDotEditorControllers {



	private static final String description = 
			"<HTML><h3>Keyboard Controls :</h3>"
					+"<ul>"					
					+"<li>Set Selection as Deleted : <strong>Ctrl + D</strong></li>"
					+"<li>Clear the current Selection :&nbsp; <strong>Shift + C</strong></li>"
					+"<li>Inverse the current selection :<strong> Shift + I</strong></li>"
					+"<li>Toggle Annotation visibility : <strong>Ctrl + S</strong></li>"
					+"<li>Move 1 Z slice up : <strong>Up arrow</strong></li>"
					+"<li>Move 1 Z slice down :<strong> Down arrow</strong></li>"

					+"</ul>"
					+"<h3>Mouse Controls :</h3>"
					+"<ul>"					
					+"<li>Select Object : <strong>Left click</strong></li>"
					+"<li>Enable/Diable the deletion brush : <strong>Ctrl + E</strong></li>\""
					+"<li>Undelete Object (if not validated) : <strong>Left Click + Maintain Shift while clicking on a deleted object</strong></li>"
					+"<li>Add a New Object : <strong>Left Click + Maintain Shift while clicking on an empty space</strong></li>"
					+"<li>"
					+"</ul></HTML>";







	private ImageDotEditorControllers(){}


	public static JToolBar createControls(ImageDotEditorManager mgr, ImageDisplay view){



		// Bind keys for eraser
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl E"), "Erase", new AbstractAction(){

			DotEraser eraser;

			@Override
			public void actionPerformed(ActionEvent e) {
				if(eraser==null){
					eraser = new DotEraser(view, mgr);
				}
				else{
					eraser.deregister();
					eraser = null;
				}
			}			

		});


		// Bind keys for deletions
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl D"), "Delete", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("ImageDotEditorControllers: deletion called");
				mgr.setSelectedAsDeleted();				
			}			
		});

		//Bind Key for validation
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl V"), "Validate", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.commitChanges();				
			}			
		});

		//Bind Key for toggling visibility
		view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("ctrl S"), "Show", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				mgr.toggleAnnotationsVisible();				
			}			
		});


		// Mouse listener for addition and undelete
		view.getMouseModel().addListener(new MouseDisplayEventListener(){
			@Override
			public void mouseClicked(MouseDisplayEvent e){
				if(SwingUtilities.isLeftMouseButton(e.getMouseEvent()) && e.getMouseEvent().isShiftDown()){
					long[] coords = e.getAnnotationEventLocation(0);
					if(!mgr.undoAt(coords)){
						mgr.add(coords);
					}
				}
			}
		});


		view.getMouseModel().setSelectionMode(SelectionMode.MULTIPLE);
		view.getMouseModel().setRotationEnabled(false);


		JToolBar toolBar = new JToolBar();




		JButton sc = new JButton("Key Bindings");
		TooltipManager.setTooltip(sc, "Display the Keyboard Controls");
		sc.addActionListener(l->{
			JEditorPane txt = new JEditorPane();
			txt.setContentType( "text/html" );
			txt.setText(description);
			txt.setEditable(false);
			JOptionPane.showMessageDialog(view.getView(),txt);
		});


		toolBar.add(sc);



		// Save and commit to the database
		JButton save = new JButton(view.getIconTheme().icon(IconID.Files.SAVE, 16));
		TooltipManager.setTooltip(save, "Commit the changes you have made to the database");
		save.addActionListener( l->	mgr.commitChanges()	);


		toolBar.add(save);




		return toolBar;
	}







	private static class DotEraser extends MouseDisplayEventListener implements DisplayPositionListener, TransformListener<AffineGet>{



		private static final String[] dimNames = {"Radius"};

		protected final ImageDisplay view;
		protected final ImageDotEditorManager mgr;


		private long[] radius = new long[]{3} ;		


		private boolean inDrag;


		private final Set<Annotation> toDel = new HashSet<>();

		private final Consumer<long[]> drawer ;


		public DotEraser(ImageDisplay view, ImageDotEditorManager mgr) {

			this.view = view;
			this.mgr = mgr;	

			view.addTransformListener(this);
			view.getMouseModel().setSelectionMode(SelectionMode.NONE);
			view.getMouseModel().addListener(this);
			view.getImageView().setCursor(getCursor(view.getCurrentTransform()));
			view.addDisplayPositionListener(this);

			drawer = c->{
				final Annotation a = mgr.getAnnotationAt(c);
				if(a!=null && a.status()!=Status.DELETED){
					toDel.add(a);
				}
			};




			view.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("shift D"), "Dialog",  new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e) {
					BrushRadiiPanel brp = new BrushRadiiPanel(radius, dimNames);
					int i = JOptionPane.showConfirmDialog(view.getView(), brp, "Brush Dimensions...", JOptionPane.OK_CANCEL_OPTION);
					if(i == JOptionPane.OK_OPTION){
						radius = brp.getNewDims();
						view.getImageView().setCursor(getCursor(view.getCurrentTransform()));
					}

				}			
			});	



		}






		private void draw(long[] center) {
			ImgGeometry.sampleDisc(center, radius[0], drawer);
			toDel.forEach(a->mgr.delete(a));
			toDel.clear();
		}



		@Override
		public void mouseClicked(MouseDisplayEvent e) {}



		@Override
		public void mousePressed(MouseDisplayEvent e) {

			if(SwingUtilities.isLeftMouseButton(e.getMouseEvent())){
				draw(e.getAnnotationEventLocation(0));
				view.refreshDisplay();
				inDrag = true;
			}
		}


		@Override
		public void transformChanged( AffineGet transform ){
			view.getImageView().setCursor(getCursor(transform));
		}


		@Override
		public void positionHasChanged(ImageDisplay view, DisplayPosition dp) {		
			if(dp == DisplayPosition.FRAME)
				this.deregister();
		}



		@Override
		public void mouseReleased(MouseDisplayEvent e) {
			inDrag = false;
		}

		@Override
		public void mouseDragged(MouseDisplayEvent e) {
			if(!inDrag)
				return;
			draw(e.getAnnotationEventLocation(0));
			view.refreshDisplay();
		}





		private Cursor getCursor(AffineGet transform){
			int w = (int) (transform.get(0, 0) * radius[0]);	
			if(w<2)
				w=2;
			java.awt.Image image = getBrushIcon().getImage().getScaledInstance(w, w, java.awt.Image.SCALE_FAST);
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Cursor c = toolkit.createCustomCursor(image , new Point(w/2, w/2), "Painter ");
			return c;
		}



		private ImageIcon getBrushIcon(){
			return new ImageIcon(Brushes.class.getResource("/icons/circle.png"));
		}



		public void deregister() {
			view.getImageView().setCursor(Cursor.getDefaultCursor());
			view.getMouseModel().removeListener(this);
			view.getMouseModel().setSelectionMode(SelectionMode.SINGLE);
			view.removeTransformListener(this);
			view.removeDisplayPositionListener(this);
		}


	}
















}
