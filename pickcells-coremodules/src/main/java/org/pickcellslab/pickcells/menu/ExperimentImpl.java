package org.pickcellslab.pickcells.menu;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.pickcellslab.pickcells.api.datamodel.types.DataRoot;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;

@Data(typeId="Experiment")
@SameScopeAs(Workbench.class)
public final class ExperimentImpl extends Experiment implements DataRoot{

	
	public ExperimentImpl(String name, String location, String description){
		super(name,location,description);
	}
	
	@SuppressWarnings("unused")
	private ExperimentImpl(){
		super();
	}
		

	
	
}
