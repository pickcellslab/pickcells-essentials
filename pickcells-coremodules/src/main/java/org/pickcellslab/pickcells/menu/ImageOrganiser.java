package org.pickcellslab.pickcells.menu;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.Set;

import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.PreferencesAccess;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.pickcellslab.pickcells.api.app.modules.ActivationListener;
import org.pickcellslab.pickcells.api.app.modules.MenuEntry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.Image.ImageBuilder;
import org.pickcellslab.pickcells.api.datamodel.types.RawFile;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;
import org.pickcellslab.pickcells.api.img.view.ImgLut;
import org.pickcellslab.pickcells.api.img.view.Lut16;
import org.pickcellslab.pickcells.api.img.view.Lut8;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.NumericType;


/**
 * This class allows the user to import and optionally preprocess the images to analyse into the root directory
 * of the experiment. The imported images will be registered in the database as {@link Image}. 
 * 
 * @author Guillaume Blin
 *
 */
@Module
public class ImageOrganiser implements MenuEntry{//TODO MetaModelListener

	private static Logger log = LoggerFactory.getLogger(ImageOrganiser.class);


	//Variables related to the treatment of images
	//private String[] types = {"Tiffs in a folder","Image Library"};
	//private String fileTypes;
	//private double angle = 0;
	//private double bgSubRadius=100;
	//private boolean subtract = false;

	//Variables related to the storage of the status of the image import into the database
	private DataAccess session;

	private String[] cNames;
	private String[] cLuts;


	private final ImgIO io;
	private final NotificationFactory notif;
	private final PreferencesAccess prefs;

	private Experiment experiment;


	/**
	 * @param exp The Experiment to link to the imported images
	 * @param session The session in which the image information should be stored
	 */
	public ImageOrganiser(DataAccess session, ImgIO factory, NotificationFactory notif, PreferencesAccess prefs){
		this.session = session;
		this.io = factory;
		this.notif = notif;
		this.prefs = prefs;
	}





	private boolean showInconsistenciesDialog(Set<AKey<?>> set){

		String s = "";
		s+="The following inconsistencies have been identified:";
		for(AKey<?> i : set){
			s+="\n     -"+i.name;
		}
		s+="\nIt is required that all the images share the same channel number, calibration and bit depth";

		JOptionPane.showMessageDialog(null,s);

		return false;

	}



	private boolean showPreProcessingDialog(ImgsChecker checker) throws Exception {


		//Check pixel type and offer the LUT accordingly
		ImgLut<?>[] luts = null;
		if((int)checker.value(ImgsChecker.bitDepth) == 3)// 8 bits
			luts = Lut8.values();
		else
			luts = Lut16.values();



		String infos = "<HTML>Number of images : "+checker.currentList().numImages();
		for(AKey<?> k : checker.tested()){
			if(k.type().isArray()){
				infos += "<br>"+k.name+": [";
				Object array = checker.value(k);
				infos+= Array.get(array, 0).toString();
				for(int i = 1; i<Array.getLength(array);i++)
					infos+= "," +Array.get(array, i).toString();
				infos += "]";
			}else
				infos += "<br>"+k.name+": "+checker.value(k);
		}



		int nChannels =  checker.value(ImgsChecker.nChannels);

		ImportProcessDialog dialog = new ImportProcessDialog((int)nChannels, infos, luts);
		dialog.setModal(true);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);

		if(dialog.wasCancelled()){
			launch();
			return false;
		}

		cNames = new String[(int) nChannels];
		cLuts = new String[(int) nChannels];

		for(int c = 0; c<nChannels; c++){
			cNames[c] = dialog.channelName(c);
			cLuts[c] = dialog.channelLut(c).name();
		}

		return true;
	}




	private <T extends NumericType<T> & NativeType<T>> void processImages(ImgFileList list, ImgsChecker checker){


		try {
			io.importImages(list, experiment.getDBPath(), null);
		} catch (IOException e1) {
			log.error("An error occured while importing images", e1);
		}


		Image imgPrototype = null;
		RawFile rfPrototype = null;

		for(int i = 0; i<list.numDataSets(); i++){
			for(int j = 0; j<list.numImages(i); j++){



				//Create an Image data object storing info we are interested in

				// 1- get the dimensions of the image, including ordering and calibration
				long[] dims = checker.value(ImgsChecker.dimensions, list , i , j);
				int[] order = checker.value(ImgsChecker.ordering, list, i, j);
				double[] cal = checker.value(ImgsChecker.calibration, list , i , j); 

				int x = index(order,Image.x);
				int y = index(order,Image.y);
				int c = index(order,Image.c);
				int z = index(order,Image.z);
				int t = index(order,Image.t);



				// 2- initialise the builder
				int bitDepth = checker.value(ImgsChecker.bitDepth, list , i , j);
				ImageBuilder builder = new ImageBuilder(list.name(i,j).substring(0, list.name(i,j).lastIndexOf('.'))+io.standard(dims, bitDepth),
						dims[x], dims[y], 
						bitDepth)
						.setLocation(experiment.getDBPath());


				// 3- assign a RawFile
				RawFile rf = new RawFile(
						list.name(i,j),
						checker.value(ImgsChecker.extension, list , i , j),
						list.path(i)+File.separator+list.name(i,j),dims);
				builder.setRawFile(rf);


				// 4- Define the channels
				for(int n = 0; n<cNames.length; n++)
					builder.setChannel(n, cNames[n], cLuts[n]);

				// 5- Set the channel dimension position if more than one channel
				if(cNames.length>1)
					builder.setChannelDimension(c);

				// 6- Define units and calibration in X and Y
				builder.setUnits(cal[x], cal[y], checker.value(ImgsChecker.sUnit, list , i , j));


				// 7- Define Z if image is 3D
				if(z!=-1)
					builder.setZ(dims[z], cal[z], z);					

				// 7- Define Time if image is 3D
				if(t!=-1)
					builder.setT(dims[t],  cal[t], t, checker.value(ImgsChecker.tUnit, list , i , j));


				Image img =	builder.build();
				experiment.addOutgoing(Links.INCLUDES, img);

				imgPrototype = img;
				rfPrototype = rf;


			}
		}



		//Save the updated experiment to the database
		try {
			session.queryFactory().store()
			.add(experiment,Traversers.newConstraints()
					.fromDepth(0).toDepth(2)
					.traverseLink(Links.INCLUDES, Direction.OUTGOING)
					.and(Image.RAW_FILE_LINK, Direction.OUTGOING)
					.includeAllNodes())
			.feed(imgPrototype, "PickCells Core", "An image on the file system of the application which "
					+ "can be used to create segmentation results and analysis")
			.feed(rfPrototype, "PickCells Core", "The file from which an image was imported into the experiment")
			.run();

		} catch (DataAccessException e) {
			log.error("Error : ", e);
			return;
		}

		log.info(list.numImages()+" images imported");

	}



	private int index(int[] order, int i){
		for(int j = 0; j<order.length; j++)
			if(order[j]==i)
				return j;
		return -1;
	}





	@Override
	public void launch() throws Exception {

		log.info("Started : Choosing a set of images to import...");

		//TODO get Experiment in access
		try {

			experiment =
					session.queryFactory().regenerate("Experiment")
					.toDepth(0).traverseAllLinks().includeAllNodes().regenerateAllKeys().getAll()
					.getOneTarget(Experiment.class).get();


		} catch (DataAccessException e) {
			log.error("Unable to find experiment in the database", e);
			JOptionPane.showMessageDialog(null, "There is no experiment in the database \n "
					+ "The error has been logged", "Error", JOptionPane.ERROR);
			return;
		}


		//Ask the user to choose a set of images
		ImgsChooser chooser = io.createChooserBuilder().setTitle("Image Dataset").setHomeDirectory(prefs.getLastDirectory()).build();
		chooser.setModal(true);
		chooser.setVisible(true);
		if(chooser.wasCancelled()){
			int i = JOptionPane.showConfirmDialog(null, "You need to import images first!"
					+ "\n Abandon?");
			if(i == JOptionPane.YES_OPTION)
				return;

			launch();
			return;
		}

		ImgFileList list = chooser.getChosenList();
		if(list.numImages()==0){
			JOptionPane.showMessageDialog(null, "The dataset is empty!");
			launch();
			return;
		}

		//Check the last directory and store in preferences
		File f = new File(list.path(list.numDataSets()-1));
		if(f.isDirectory())
			prefs.setLastDirectory(f);
		else
			prefs.setLastDirectory(f.getParentFile());



		log.info("Checking image consistency");
		ImgsChecker checker = 
				io.createCheckerBuilder()
				.addCheck(ImgsChecker.calibration)
				.addCheck(ImgsChecker.nChannels)
				.addCheck(ImgsChecker.extension)
				.addCheck(ImgsChecker.sUnit)
				.addCheck(ImgsChecker.bitDepth)
				.build(list);





		if(checker.isConsistent())
			log.info("Images are consistent, number of images : "+list.numImages());
		else if(!showInconsistenciesDialog(checker.inconsistencies())){
			launch();
			return;
		}


		if(showPreProcessingDialog(checker)){
			final ImgFileList l = list;
			final ImgsChecker c = checker;
			notif.waitAnimation(new Thread(()->processImages(l, c)), "Importing images...");			
			checker = null;
			list = null; 
		}
		else launch();

	}





	@Override
	public void start() {
		// TODO Auto-generated method stub

	}





	@Override
	public void registerListener(ActivationListener lst) {
		// TODO Auto-generated method stub

	}





	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}





	@Override
	public String name() {
		return "Image Import";
	}





	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public String description() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public String[] menuPath() {
		return new String[] {"Files", "Import...", "Colour Images"};
	}



}



