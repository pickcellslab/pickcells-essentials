package org.pickcellslab.pickcells.menu;

import java.net.URL;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.ActivationListener;
import org.pickcellslab.pickcells.api.app.modules.MenuEntry;

@Module
public class IntegrityCheck implements MenuEntry{

	private final DataAccess access;
	private UITheme theme;
	private NotificationFactory notifier;
	
	public IntegrityCheck(DataAccess access, NotificationFactory notifier, UITheme theme) {
		this.access = access;
		this.notifier = notifier;
		this.theme = theme;
	}
	
	
	@Override
	public void launch() throws Exception {
		access.metaModel().performIntegrityCheck();
		notifier.notify(null, "Integrity Check done", 1000, theme.icon(IconID.Misc.THUMBS_UP, 16));
	}

	@Override
	public void start() {
		// Nothing to do
		
	}

	@Override
	public void registerListener(ActivationListener lst) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String name() {
		return "Integrity Check";
	}

	@Override
	public String authors() {
		return "Guillaume Blin";
	}

	@Override
	public String licence() {
		return "GPLv3";
	}

	@Override
	public String description() {
		return "Search for database corruption events and correct for errors";
	}

	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] menuPath() {
		return new String[] {"Utilities", "Integrity Check"};
	}

}
