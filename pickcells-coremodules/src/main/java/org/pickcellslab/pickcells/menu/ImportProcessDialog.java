package org.pickcellslab.pickcells.menu;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.pickcellslab.pickcells.api.img.view.ImgLut;

@SuppressWarnings("serial")
public class ImportProcessDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private List<ChannelPane> cList = new ArrayList<>();
	private boolean wasCancelled = true;
	


	/**
	 * Create the dialog.
	 */
	public ImportProcessDialog(int numChannels, String imagesInfo, ImgLut<?>[] luts) {
		setResizable(false);
		
		//Definition of controllers
		//------------------------------------------------------------------------------------
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new TitledBorder(null, "Channels Definition", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		
		JPanel channelPanel = new JPanel();
		scrollPane.setViewportView(channelPanel);
		channelPanel.setLayout(new BoxLayout(channelPanel, BoxLayout.PAGE_AXIS));
		
		for(int i = 0; i<numChannels; i++){
			ChannelPane cp = new ChannelPane(i, luts);
			channelPanel.add(cp);
			cList.add(cp);
		}
		
		
		JPanel treatPanel = new JPanel();
		treatPanel.setMaximumSize(new Dimension(300,400));
		treatPanel.setPreferredSize(new Dimension(300,400));
		treatPanel.setBorder(new TitledBorder(null, "Images Info", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JLabel lblPickcellsAnalysesRequire = new JLabel(imagesInfo);
		
		
		
		
		JButton okButton = new JButton("OK");
		
		JButton cancelButton = new JButton("Cancel");
				
		
		//Events
		//------------------------------------------------------------------------------------
		okButton.addActionListener(l->{
			wasCancelled  = false;
			this.dispose();
		});
		cancelButton.addActionListener(l->{
			this.dispose();
		});
		
		//Layout 
		//------------------------------------------------------------------------------------
		
		setBounds(100, 100, 612, 400);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(1, 2, 0, 0));
		
		contentPanel.add(scrollPane);
		contentPanel.add(treatPanel);
		treatPanel.setLayout(new GridLayout(1, 0, 0, 0));
		
		treatPanel.add(lblPickcellsAnalysesRequire);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			
				
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				buttonPane.add(cancelButton);
			
		}
	}
	
	
	boolean wasCancelled(){
		return wasCancelled;
	}
	
	
	String channelName(int pos){
		return cList.get(pos).chosenName();
	}
	
	ImgLut<?> channelLut(int pos){
		return cList.get(pos).chosenLut();
	}
}
