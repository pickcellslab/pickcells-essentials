package org.pickcellslab.pickcells.tools;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JDialog;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.apache.commons.math3.util.MathArrays;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.queryui.QueryableChoiceDialog;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.ValueSelector;
import org.pickcellslab.pickcells.api.app.modules.ActivationListener;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.app.modules.MenuEntry;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Module
public class NeighboursCounter implements MenuEntry{


	private static final Logger log = LoggerFactory.getLogger(NeighboursCounter.class);
	private final DataAccess access;
	private final UITheme theme;
	
	public NeighboursCounter(UITheme theme, DataAccess access) {
		this.theme = theme;
		this.access = access;
	}
	
	
	
	
	@Override
	public void launch() throws Exception {


		//Ask for the type of object for which the counts must be created 
		final QueryableChoiceDialog<MetaClass> sd = new QueryableChoiceDialog<>(theme, access, "Please, choose the data type", 
				(mq) -> mq instanceof MetaClass && ImageLocated.class.isAssignableFrom(((MetaClass) mq).itemClass(access.dataRegistry())), true);

		sd.setModal(true);
		sd.pack();
		sd.setLocationRelativeTo(null);
		sd.setVisible(true);

		if(sd.wasCancelled()){
			sd.dispose();
			return;
		}


		final Optional<MetaClass> sOpt = sd.getChoice();
		@SuppressWarnings("unchecked")
		final Class<? extends ImageLocated> sClass =  (Class<? extends ImageLocated>)  sOpt.get().itemClass(access.dataRegistry());


		// Ask for the radii
		final AKey<double[]> radiiKey = AKey.get("radiiKey", double[].class);
		final ValueSelector<double[]> dialog = new ValueSelector<double[]>(radiiKey);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setModal(true);
		dialog.setVisible(true);

		final double[] radii = dialog.getValue();
		if(radii == null)// if cancelled
			return;

		System.out.println("Radii = "+Arrays.toString(radii));





		//Get the List of db images
		List<Image> images = null;
		try {

			images = UsefulQueries.imagesWithSegmentations(access);

		} catch (Exception e) {
			throw new AnalysisException("Unable to read the database", e);
		}




		final boolean isZStack = images.get(0).isZStack();

		// Create the AKeys to store in Nodes
		@SuppressWarnings("unchecked")
		final AKey<Integer>[] aDensKeys = new AKey[radii.length];
		final AKey<Double>[] nDensKeys = new AKey[radii.length];

		final AKey<Double>[] skewXYKeys = new AKey[radii.length];
		final AKey<Double>[] skewZKeys = new AKey[radii.length];

		final AKey<Double>[] dispKeys = new AKey[radii.length];
		
		final AKey<Double> imgDisKey = AKey.get("Distance From Image Border", double.class);

		for(int r = 0; r<radii.length; r++) {
			aDensKeys[r] = AKey.get("Abs_Density_"+radii[r], int.class);
			nDensKeys[r] = AKey.get("Norm_Density_"+radii[r], double.class);
			skewXYKeys[r] = AKey.get("SkewXY_Density_"+radii[r], double.class);
			if(isZStack)
				skewZKeys[r] = AKey.get("SkewZ_Density_"+radii[r], double.class);
			dispKeys[r] = AKey.get("Disp_Density_"+radii[r], double.class);
		}







		// Iterate over images
		for(Image im : images){
			// Iterate over time
			for(int time = 0; time<im.frames(); time++){


				// Create the predicate on time frame
				final int frame = time;
				final ExplicitPredicate<DataItem> framePredicate = F.select(ImageLocated.frameKey, 0).equalsTo(frame);


				List<ImageLocated> sources = null;

				try {


					if(sd.getFilter().isPresent()) {
						final ExplicitPredicate<DataItem> filter = sd.getFilter().get().toFilter();
						sources = UsefulQueries.imageToImageLocated(access, sOpt.get(), 
								filter.and(framePredicate)
								, im.getAttribute(DataItem.idKey).get())
						.getAllItemsFor(sClass).collect(Collectors.toList());
					}
					else
						sources = UsefulQueries.imageToImageLocated(access, sOpt.get(), framePredicate, im.getAttribute(DataItem.idKey).get())
						.getAllItemsFor(sClass).collect(Collectors.toList());


				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					continue;
				}


				log.trace("Number of sources in frame "+time+" : "+sources.size());

				if(sources.size() == 0){
					log.info("No source found in "+im.toString());
					continue;
				}


				// First compute the density and the barycenter of neighbours
				// Brute force, we could also build a distance matrix but this may require large amounts of memory for large datasets				

				// Also keep track of min and max densities
				// Mins
				final int[] mins = new int[radii.length];
				Arrays.fill(mins, Integer.MAX_VALUE);
				// Maxes
				final int[] maxes = new int[radii.length];
				Arrays.fill(maxes, -Integer.MAX_VALUE);

				
				// Calibrated image size
				final MinimalImageInfo info = im.getMinimalInfo();
				final double calX = (double)info.dimension(Image.x) * info.calibration(Image.x);
				final double calY = (double)info.dimension(Image.y) * info.calibration(Image.y);
				
				for(int i = 0; i<sources.size(); i++) {

					
					final ImageLocated s1 = sources.get(i);
					final double[] s1C = s1.centroid();
					
					// Find shortest distance from image border
					final double x = s1C[0] > calX/2 ? calX - s1C[0] : s1C[0];
					final double y = s1C[1] > calY/2 ? calY - s1C[1] : s1C[1];
					
					s1.setAttribute(imgDisKey, x < y ? x : y);
					
					
					
					
					// Create counters
					final MutableInt[] counters = new MutableInt[radii.length];

					// Create Means
					final double[][] centers = new double[radii.length][isZStack ? 3 : 2];

					// Init arrays
					for(int c = 0; c<counters.length; c++)
						counters[c] = new MutableInt();

					

					for(int j = 0; j<sources.size(); j++) {
						if(i!=j) {
							final ImageLocated s2 = sources.get(j);
							final double[] s2C = s2.centroid();
							final double d = MathArrays.distance(s1C, s2C);

							for(int c = counters.length-1; c>=0; c--) {
								if(d<=radii[c]) {
									counters[c].increment();
									for(int dim = 0; dim<s2C.length; dim++)
										centers[c][dim] += s2C[dim];
								}
								else
									break;
							}
						}
					}


					// Now store in s1 the number of neighbours in each bin and the skew in XY and Z
					for(int r = 0; r<radii.length; r++) {

						s1.setAttribute(aDensKeys[r], counters[r].intValue());

						if(mins[r]>counters[r].intValue())
							mins[r] = counters[r].intValue();

						if(maxes[r]<counters[r].intValue())
							maxes[r] = counters[r].intValue();

						if(counters[r].intValue() != 0) {
							for(int dim = 0; dim<centers[r].length; dim++)
								centers[r][dim] /= counters[r].doubleValue();

							s1.setAttribute(skewXYKeys[r], Math.sqrt(Math.pow(s1C[0]-centers[r][0],2)+Math.pow(s1C[1]-centers[r][1],2)));
							if(isZStack)
								s1.setAttribute(skewZKeys[r], s1C[2]-centers[r][2]);
						}
						else {
							s1.setAttribute(skewXYKeys[r], 0d);
							if(isZStack)
								s1.setAttribute(skewZKeys[r], 0d);
						}


					}


				}// End of first pass



				// 2- Create the normalised density keys and the CV for each bin.
				for(int i = 0; i<sources.size(); i++) {
					// Create Stats
					final Mean[] means = new Mean[radii.length]; 
					final Variance[] vars = new Variance[radii.length]; 
					for(int r = 0; r<radii.length; r++) {
						means[r] = new Mean();
						vars[r] = new Variance();
					}

					final ImageLocated s1 = sources.get(i);
					final double[] s1C = s1.centroid();

					for(int j = 0; j<sources.size(); j++) {
						if(i!=j) {
							final ImageLocated s2 = sources.get(j);
							final double d = MathArrays.distance(s1C, s2.centroid());
							for(int r = radii.length-1; r>=0; r--) {
								if(d<=radii[r]) {
									final int s2Dens = s2.getAttribute(aDensKeys[r]).get();
									means[r].increment(s2Dens);
									vars[r].increment(s2Dens);
								}
								else
									break;
							}
						}
					}


					// Now store in s1 range (dispersion) in each bin as well as the normalised density
					for(int r = 0; r<radii.length; r++) {
						if(s1.getAttribute(aDensKeys[r]).get() != 0) {
							final double disp = Math.sqrt(vars[r].getResult())/means[r].getResult();
							if(!Double.isNaN(disp))
								s1.setAttribute(dispKeys[r], disp);
							else
								s1.setAttribute(dispKeys[r], 0d);
							final double nDens = (double)(s1.getAttribute(aDensKeys[r]).get() - mins[r]) / (double)(maxes[r] - mins[r]) ;
							s1.setAttribute(nDensKeys[r], nDens);
						}
						else {
							s1.setAttribute(dispKeys[r], 0d);
							s1.setAttribute(nDensKeys[r], 0d);
						}
					}
				}













				// ============== Now write to the database ==================

				try {
					access.queryFactory().store().add(sources.get(0).image()).run();

				} catch (DataAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}






			}// END of itr over time frames
		}//END of itr over images







	}


	@Override
	public String name() {
		return "Neighbours Count";
	}

	@Override
	public String description() {
		return "Counts the number of neighbours within specified distances";
	}




	
	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registerListener(ActivationListener lst) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String[] menuPath() {
		return new String[] {"Utilities", "Count Neighbours"};
	}


}
