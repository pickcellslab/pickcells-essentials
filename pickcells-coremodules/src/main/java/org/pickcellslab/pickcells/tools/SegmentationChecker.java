package org.pickcellslab.pickcells.tools;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.ProgressFactory;
import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.TaskProgress;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.DesktopModule;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.detector.LabelGenerator;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.labeling.ConnectedComponents;
import net.imglib2.algorithm.labeling.ConnectedComponents.StructuringElement;
import net.imglib2.img.Img;
import net.imglib2.roi.labeling.ImgLabeling;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.IntegerType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.view.Views;


@Module
public class SegmentationChecker<T extends IntegerType<T>> implements DesktopModule {

	private final ImgIO io;
	private final UITheme theme;
	private final NotificationFactory notif;

	public SegmentationChecker(ImgIO io, UITheme theme, NotificationFactory notif) {
		Objects.requireNonNull(io);
		Objects.requireNonNull(theme);
		Objects.requireNonNull(notif);
		this.io = io;
		this.theme = theme;
		this.notif = notif;
	}


	@Override
	public void launch() throws Exception {


		// Ask user for input images
		final ImgsChooser chooser = io.createChooserBuilder().setTitle("Choose Input Images").build();
		chooser.setModal(true);
		chooser.setVisible(true);


		final ImgFileList list = chooser.getChosenList();
		if(list == null) return;

		/* TODO
		// Check if all images are single channel (as segmentation images should be)		
		final ImgsChecker checker = io.createCheckerBuilder()
				.addCheck(ImgsChecker.nChannels, 1)
				.build(list);

		if(checker.isConsistent()) ...
		 */

		final ProgressFactory<?,Void> progress = notif.newProgressFactory();

		final List<TaskProgress<Void>> tasks = new ArrayList<>();

		for(int i = 0; i<list.numDataSets(); i++){
			for(int j = 0; j<list.numImages(i); j++){				
				tasks.add(progress.newTaskProgress(list.name(i,j), createCallable(io, list, i, j)));
			}			
		}

		
		final ProgressPanel<Void> ui = progress.progressPanel(theme, (List)tasks);
		ui.setTitle("Segmentation Check Progress");
		ui.setVisible(true);
		
		
	}



	private Callable<Void> createCallable(ImgIO io2, ImgFileList list, int i, int j) {
		
		return  ()->{



			// load the image
			@SuppressWarnings({ "unchecked", "rawtypes" })
			final Img<T> original = (Img)list.load(i, j);


			// Count the number of labels in the image
			final Set<Integer> labels = getAllLabels(original);

			// Now relabel check each label individually
			final T value = original.firstElement().createVariable();
			// We always keep the same generator to keep incrementing for all disconnected objects
			final LabelGenerator lGen = new LabelGenerator(0);
			// Create the result image
			final Img<UnsignedShortType> result = io.createImg(original, new UnsignedShortType());
			for(Integer l : labels) {
				value.setInteger(l);
				final RandomAccessibleInterval<BitType> bitImg = getBitTypeImageFor(value, original);
				final ImgLabeling<Integer, UnsignedShortType> partial = getRelabelled(bitImg, lGen);
				this.writeToResult(partial, result);
			}



			// TODO ProgressPanel

			// Finally save the result to disk
			final String dest = list.path(i)+File.separator+formatName(list.name(i,j))+io.standard(result);
			final MinimalImageInfo info = list.getMinimalInfo(i, j);
			io.save(info, result, dest);

			return null;

		};
		
	}


	private Set<Integer> getAllLabels(Img<T> input){

		final Cursor<T> inCursor = input.cursor();
		final Set<Integer> labels = new HashSet<>();

		while(inCursor.hasNext()) {		
			labels.add(inCursor.next().getInteger());
		}	

		labels.remove(0);

		return labels;

	}




	private RandomAccessibleInterval<BitType> getBitTypeImageFor(T value, Img<T> input){

		final RandomAccessibleInterval<BitType> bitImg = io.createImg(input, new BitType());

		final Cursor<T> inCursor = input.cursor();
		final RandomAccess<BitType> bitAccess = bitImg.randomAccess();

		while(inCursor.hasNext()) {

			final T currentValue = inCursor.next();
			if(currentValue.equals(value)) {
				bitAccess.setPosition(inCursor);
				bitAccess.get().setOne();
			}
		}		


		return bitImg;

	}



	private ImgLabeling<Integer, UnsignedShortType> getRelabelled(RandomAccessibleInterval<BitType> input, LabelGenerator lGen) {
		final Img<UnsignedShortType> result = io.createImg(input, new UnsignedShortType());
		ImgLabeling<Integer,UnsignedShortType> labels = new ImgLabeling<>(result);
		ConnectedComponents.labelAllConnectedComponents(input, labels, lGen , StructuringElement.FOUR_CONNECTED);
		return labels;
	}




	private void writeToResult(ImgLabeling<Integer, UnsignedShortType> partial, Img<UnsignedShortType> result) {

		final Cursor<UnsignedShortType> pCursor = Views.iterable(partial.getIndexImg()).cursor();
		final Cursor<UnsignedShortType> rCursor = result.cursor();

		final Map<Integer, Integer> map = new HashMap<>();
		for(int i = 1; i<partial.getMapping().numSets(); i++)
			map.put(i, partial.getMapping().labelsAtIndex(i).iterator().next());

		while(pCursor.hasNext()) {

			pCursor.fwd();
			rCursor.fwd();

			final UnsignedShortType currentValue = pCursor.get();
			if(currentValue.get() != 0) {
				rCursor.get().set(map.get(currentValue.get()));
			}
		}		

	}








	private String formatName(String name) {

		String n = name;

		//Remove file extension
		int index = name.lastIndexOf('.');
		if(index!=-1)
			n = name.substring(0, index);

		//Replace illegal characters in the name
		n = n.replace("\\", "_");
		n = n.replace("/", "_");
		n = n.replace(" ", "_");

		return n;

	}











	@Override
	public String name() {
		return "Segmentation Checker";
	}

	@Override
	public String authors() {
		return "Guillaume Blin";
	}

	@Override
	public String licence() {
		return "GPLv3";
	}

	@Override
	public String description() {
		return "This module takes a segmented image as input and checks that all non-touching shapes are assigned a unique label\n"
				+ "When this is not the case a new relabelled image is generated";
	}

	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/seg_check_icon.png"));
	}

}
