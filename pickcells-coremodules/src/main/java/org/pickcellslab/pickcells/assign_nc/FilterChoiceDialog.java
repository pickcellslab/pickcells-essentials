package org.pickcellslab.pickcells.assign_nc;

import java.util.Objects;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;





@SuppressWarnings("serial")
public class FilterChoiceDialog extends JDialog {
	
	
	private boolean wasCancelled = true;
	private int maxCC = 20;
	private int maxCE = 7;
	private float percent = 0.05f;
	private MetaFilter nF;
	private MetaFilter cF;
	
	
	FilterChoiceDialog(UITheme theme, DataRegistry registry, MetaClass nuc, MetaClass cent) {

		Objects.requireNonNull(nuc, "nuc is null");
		Objects.requireNonNull(cent, "cent is null");
		
		
		JLabel lblFilterOnNuclei = new JLabel("Filter on Nuclei");
		
		JComboBox<MetaFilter> nucFCombo = new JComboBox<>();
		nucFCombo.setRenderer(new MetaRenderer(theme, registry));
		nucFCombo.addItem(null);
		nuc.filters().forEach(f->nucFCombo.addItem(f));
		
		
		JLabel lblFilterOnCentrosomes = new JLabel("Filter on Centrosomes");

		JComboBox<MetaFilter> centFCombo = new JComboBox<>();
		centFCombo.setRenderer(new MetaRenderer(theme, registry));
		centFCombo.addItem(null);
		cent.filters().forEach(f->centFCombo.addItem(f));

		JLabel lblMaxIntercentroidDistance = new JLabel("Max Inter-Centroid Distance");

		JFormattedTextField ICField = new JFormattedTextField();
		ICField.setText(""+maxCC);

		JLabel lblMaxEdgeTo = new JLabel("Max Edge to Centrosome Distance");

		JFormattedTextField ECField = new JFormattedTextField();
		ECField.setText(""+maxCE);

		JLabel lblPercentageOfNucleus = new JLabel("Percentage of nucleus mesh");

		JFormattedTextField perField = new JFormattedTextField();
		perField.setText(""+percent);


		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{
			//First check that textfields are parsable
			try{
				maxCC = Integer.parseInt(ICField.getText());
				maxCE = Integer.parseInt(ECField.getText());
				percent = Float.parseFloat(perField.getText());
			}catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null,"At least one of the text field contains a non numeric input");
				return;
			}
			
			nF = (MetaFilter) nucFCombo.getSelectedItem();
			cF = (MetaFilter) centFCombo.getSelectedItem();
			
			wasCancelled = false;
			this.dispose();
			
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->this.dispose());
		
		

		//Layout
		//-----------------------------------------------------------------------------------------

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(btnOk)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(btnCancel))
										.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
												.addComponent(lblMaxIntercentroidDistance)
												.addPreferredGap(ComponentPlacement.RELATED, 96, Short.MAX_VALUE)
												.addComponent(ICField, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
												.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
														.addComponent(lblFilterOnNuclei)
														.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(nucFCombo, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))
														.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
																.addComponent(lblFilterOnCentrosomes, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(ComponentPlacement.RELATED)
																.addComponent(centFCombo, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))
																.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
																		.addComponent(lblPercentageOfNucleus, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
																		.addComponent(perField, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
																		.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
																				.addComponent(lblMaxEdgeTo)
																				.addPreferredGap(ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
																				.addComponent(ECField, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)))
																				.addGap(25))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblFilterOnNuclei)
								.addComponent(nucFCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(18)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(centFCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(5)
												.addComponent(lblFilterOnCentrosomes)))
												.addGap(18)
												.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
														.addComponent(lblMaxIntercentroidDistance)
														.addComponent(ICField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
																.addComponent(lblMaxEdgeTo)
																.addComponent(ECField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(ComponentPlacement.RELATED)
																.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																		.addComponent(lblPercentageOfNucleus)
																		.addComponent(perField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
																		.addGap(18)
																		.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
																				.addComponent(btnOk)
																				.addComponent(btnCancel))
																				.addGap(27))
				);
		getContentPane().setLayout(groupLayout);
	}
	
	
	boolean wasCancelled(){
		return wasCancelled;
	}
	
	MetaFilter nucleusFilter(){
		return nF;
	}
	
	MetaFilter centFilter(){
		return cF;
	}
	
	int maxCC(){
		return maxCC;
	}
	
	int maxCE(){
		return maxCE;
	}
	
	float percentage(){
		return percent;
	}
	
	
}
