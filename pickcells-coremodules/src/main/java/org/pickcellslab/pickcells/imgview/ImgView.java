package org.pickcellslab.pickcells.imgview;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.picking.AbstractSeriesAndSinglePicker;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPickConsumer;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPicker;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.img.providers.SegmentationManagementException;
import org.pickcellslab.pickcells.api.img.view.AnnotationAppearance;
import org.pickcellslab.pickcells.api.img.view.AnnotationAppearanceListener;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;
import org.pickcellslab.pickcells.api.img.view.Lut8;
import org.pickcellslab.pickcells.api.imgdb.view.ImageContentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.filechooser.WebFileChooser;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;

import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.display.ColorTable;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;

public class ImgView<T extends RealType<T> & NativeType<T>> extends AbstractSeriesAndSinglePicker implements DataView<ImageContentModel,Path<NodeItem,Link>>, AnnotationAppearanceListener{


	private static final Logger log = LoggerFactory.getLogger(ImgView.class);

	private final ImgIO io;
	private final NotificationFactory notif;

	private final ImageContentModel model;
	private final AnnotationAppearance app;
	
	private JPanel view;

	private ImageViewer<T> viewer;
	private RandomAccessibleInterval<T> img;
	private Image image;

	private JPanel channelControl;

	private String[] channels;
	private int[] order;

	private ImgView<T>.SegmentedPicker picker;

	private final Map<String,Map<Float,Integer>> ids = new HashMap<>();



	public ImgView(NotificationFactory notif, ImgIO io, ImageContentModel model) {
		
		this.notif = notif;
		this.io = io;

		this.model = model;
		model.addDataEvtListener(this);

		this.app = new AnnotationAppearance();
		app.addListener(this);
		
		view = new JPanel(new BorderLayout());
		view.setBackground(Color.BLACK);

	}



	JPanel getView(){
		return view;
	}



	@Override
	public void setModel(ImageContentModel model) {
		throw new RuntimeException("Not implemented");
	}

	@Override
	public ImageContentModel getModel() {
		return model;
	}

	@Override
	public void dataSetChanged(DataSet<Path<NodeItem, Link>> oldDataSet, DataSet<Path<NodeItem, Link>> dataset) {

		//TODO Ask for if a heatmap is also required.
		
		if(oldDataSet!=null){
			view.remove(viewer);
			channelControl = null;
			view.repaint();

			app.reset();
			viewer.access(viewer.numChannels()-1).forEach(t->t.setZero());
			
		}


		Thread t = new Thread(()->{

			//Load the image
			if( image==null || !model.currentImage().equals(image) ){
				
				if(viewer!=null){
					viewer.removeViewListener(picker);
					picker = null;
				}
				
				
				image = model.currentImage();


				try {

					img = io.open(image);

				} catch (IOException e) {
					notif.display("Error", "Could not load image "+image.toString(), e, Level.WARNING);
					return;
				}

				//Add a channel for the overlay


				order = image.order();
				img = ImgDimensions.addChannel(io, img, order , 1);

				

				String[] luts = Arrays.copyOf(image.luts(), image.luts().length + 1);
				luts[luts.length-1] = Lut8.RANDOM.name();		

				channels = image.channels();

				ColorTable[] tables = ImgDimensions.createColorTables(img.randomAccess().get().createVariable(), image.luts());
				tables = Arrays.copyOf(tables, tables.length+1);
				tables[tables.length-1] = (ColorTable)app;
				viewer = new ImageViewer<>(img, tables, order);
			}			
			
			
			// Get the time dimension (NB: channel dim is removed since we write on one hyperslice)
			final int tDim = Image.removeDimension(order, Image.c)[Image.t];
			
			//Now load the data
			try {
				for(int q = 0; q<dataset.numQueries(); q++){

					dataset.loadQuery(q, 
							new ImageAction<>(
									model.dataRegistry(),
									viewer.access(viewer.numChannels()-1),
									model.getSegmentationManager(),
									app, ids,
									model.getDataSet().queryInfo(q).get(QueryInfo.INCLUDED),
									tDim));

				}
				viewer.adjustContrast(viewer.numChannels()-1);
		
			} catch (DataAccessException e) {
				notif.display("Error", "Could not load the dataset ", e, Level.WARNING);
				return;
			}


			view.add(viewer,BorderLayout.CENTER);
			view.repaint();
			
		});


		notif.waitAnimation(t, "Loading image, please wait...");

	}




	public void showChannelControl(){
		if(channelControl == null)
			channelControl = viewer.createChannelController(channels);
		JOptionPane.showInputDialog(view, channelControl);
	}




	public AnnotationAppearance getAppearance(){
		return app;
	}
	
	
	

	@Override
	public void appearanceChanged(String id, Color c) {
		viewer.refresh();
	}





	public void setPickingType(DBViewHelp helper, String choice) {
		
		Objects.requireNonNull(helper);
		
		if(viewer != null && picker != null)
			viewer.removeViewListener(picker);
		
		
		picker = new SegmentedPicker(choice);
		viewer.addViewListener(picker);
		helper.registerSingleConsumersFor(picker);

	}

	


	public void saveOverlay() {
		
		if(img == null){
			JOptionPane.showMessageDialog(null, "There is no image loaded yet.");
			return;
		}

		File f = WebFileChooser.showSaveDialog();
		if(f == null)
			return;

		IntervalView<T> interval = viewer.access(viewer.numChannels()-1);
		
		try {
			
			//Remove the channel dimension
			MinimalImageInfo info = image.getMinimalInfo().removeDimension(image.channelIndex());

			io.save(info, interval, f.getAbsolutePath()+io.standard(interval));
		
		} catch (Exception e) {
			notif.display("Error", "An exception has occured while saving the image", e, Level.WARNING);
			return ;
		}

		WebNotification wn = NotificationManager.showNotification("Image saved!");
		wn.setDisplayTime(1000);
	}




	private class SegmentedPicker extends MouseAdapter implements SingleDataPicker{

		protected final List<SingleDataPickConsumer> singlePickers = new ArrayList<>();
		private SegmentationImageProvider<?> prov;
		private RandomAccess<T> rAccess;
		private String type;

		public SegmentedPicker(String type) {
			setType(type);
		}


		@SuppressWarnings("unchecked")
		void setType(String type){
			try {
				
				this.type = type;
				prov = model.getSegmentationManager().getFor(type);
				rAccess = (RandomAccess<T>) prov.provide().randomAccess();

			} catch (SegmentationManagementException e1) {
				notif.display("Error", "The picking process generated this error", e1, Level.WARNING);
				return;
			}
		}


		
		@Override
		public void mouseClicked(MouseEvent e) {

			if(SwingUtilities.isRightMouseButton(e)){

				long[] point = viewer.getImgCoordinates(e.getX(), e.getY());				

				long[] result = new long[point.length-1];
				int p = 0;
				for(int i = 0; i<order.length; i++){
					if(order[i]!=-1 && i!=Image.c){
						result[p] = point[order[i]];
						p++;				
					}
				}

				System.out.println("Location : "+Arrays.toString(point));
				System.out.println("Location : "+Arrays.toString(result));

				rAccess.setPosition(result);
				float label = rAccess.get().getRealFloat();

				System.out.println("Label = "+label);

				Map<Float, Integer> map = ids.get(type);
				if(map!=null){
					Integer i = map.get(label);
					if(i!=null){
						this.fireSingleDataPick(model.getSegmentationManager().getAssociated(type), i);
						System.out.println("id not null");
					}
				}
				log.info("No data picked");




			}

		}


		@Override
		public void addSingleDataPickListener(SingleDataPickConsumer l) {
			if(l!=null)
				singlePickers.add(l);
		}

		@Override
		public void removeSingleDataPicker(SingleDataPickConsumer l) {
			singlePickers.remove(l);
		}

		/**
		 * Notify all listener about a new 
		 * 
		 * @param clazz
		 * @param dbId
		 * @param data.p
		 */
		protected void fireSingleDataPick(DataPointer v, int dbId){
			singlePickers.forEach(l->l.singleDataPicked(v, dbId));
		}



	}



}
