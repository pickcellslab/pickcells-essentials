package org.pickcellslab.pickcells.imgview;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.utils.DataToIconID;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.view.AnnotationAppearanceDialog;
import org.pickcellslab.pickcells.api.imgdb.view.ImageContentModel;







@Module
public class ImageView implements DBViewFactory{ 


	private final ImgIO io;
	private final NotificationFactory notif;
	private final ProviderFactoryFactory pff;
	private final DataRegistry registry;
	private final UITheme theme;



	public ImageView(DataAccess access, ImgIO io, ProviderFactoryFactory pff, NotificationFactory notif, UITheme theme) {
		this.io = io;
		this.notif = notif;
		this.pff = pff;
		this.registry = access.dataRegistry();
		this.theme = theme;
	}


	@Override
	public String toString() {
		return "Image Viewer";
	}




	@Override
	public String description() {
		return "This module lets you load a specific image to visualise and select objects in the image";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}




	@Override
	public String name() {
		return "Image View";
	}





	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) throws ViewFactoryException {

		/*
		// 1- Ask the user which image to load if more than one image
				List<String> images = null;

				try {

					images = access.queryFactory().read(Image.class).makeList(Keys.name).inOneSet().getAll().run();

				} catch (DataAccessException e) {
					throw new ViewFactoryException("Unable to access the database",e);
				}

				String choice = null;
				if(images.isEmpty()){
					JOptionPane.showMessageDialog(null, "There are no image loaded into this experiment");
					return null;
				}

				choice = images.get(0);

				if(images.size()>1){	
					choice =
				(String) JOptionPane.showInputDialog(null, 
						"Select the image to display",
						"Image View...",
						JOptionPane.PLAIN_MESSAGE,
						null,
						images.toArray(), 
						choice);		
				}

				if(choice == null)
					return null;

		 */

		//Create the model
		ImageContentModel model = new ImageContentModel(access, pff);

		//Create the view
		ImgView<?> view = new ImgView<>(notif, io, model);

		// Create Controllers
		JPanel toolBar = helper.newToolBar()		 
				.addChangeDataSetButton(() -> QueryConfigs.newPathOnlyConfig("")
						.setSynchronizeRoots(true)
						.setFixedRootKey(ImageContentModel.fixedRootKey(access))
						.setAllowedQueryables(ImageContentModel.renderableTypes(registry))
						.build(),
						view.getModel())
				.addButton(theme.icon(IconID.Charts.CHART_BAR, 16), "Brightness and Contrast", l->view.showChannelControl())				
				
				// Color Control
				.addButton(theme.icon(IconID.Misc.PAINT, 16), "Colors...", 
						l->{
							if(view.getAppearance().registeredSeries().isEmpty()){
								JOptionPane.showMessageDialog(null, "There is no dataset loaded yet");
								return;
							}
							AnnotationAppearanceDialog dg = new AnnotationAppearanceDialog(view.getAppearance());
							dg.pack();
							dg.setLocationRelativeTo(null);
							dg.setModal(true);
							dg.setVisible(true);
							
						})	
				
				// Picking Control
				.addButton(theme.icon(IconID.Misc.PICK_ONE, 16), "Choose the picking target type", l->{

					if(model.getDataSet().numQueries() == 0){
						JOptionPane.showMessageDialog(null, "There is no dataset loaded yet");
						return;
					}

					Object[] av = model.getSegmentationManager().getIncluded().toArray();

					if(av.length == 0){
						JOptionPane.showMessageDialog(null,"There is no pickable data in the image");
						return;
					}
					
					Object choice = 
							JOptionPane.showInputDialog(null, 
							"Pickable Objects",
							"Loaded :",
							JOptionPane.QUESTION_MESSAGE, 
							null, 
							av, 
							av[0]);
					
					if(choice == null)
						return;
					
					else
						view.setPickingType(helper, (String)choice);
				})
				.addButton(theme.icon(IconID.Files.SAVE, 16), "Save the overlayed channel", l->view.saveOverlay())
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(view.getView()), BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());


	}




	@Override
	public Icon icon() {
		return DataToIconID.getIcon(theme, Image.class, 24);
	}


	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}

}
