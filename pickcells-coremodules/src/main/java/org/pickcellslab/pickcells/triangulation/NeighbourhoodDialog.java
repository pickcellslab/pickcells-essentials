package org.pickcellslab.pickcells.triangulation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.queryui.renderers.MetaRenderer;
import org.pickcellslab.foundationj.services.theme.UITheme;

@SuppressWarnings("serial")
public class NeighbourhoodDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private final JTextField dTField;
	private final JTextField vTField;
	private final JComboBox<MetaFilter> sFCombo;

	//Stores the name/description of searchmethods to find the datasets to process 

	private boolean wasCancelled;
	private MetaClass targetType;
	private MetaFilter sourceFilter;
	private double max = 5;
	private double v = 0.1;
	
	
	



	/**
	 * Create the dialog.
	 */
	public NeighbourhoodDialog(UITheme theme, DataAccess session, Predicate<MetaQueryable> allowed) {


		//Controllers
		//-----------------------------------------------------------------------------------------

		//Labels describing controllers
		JLabel label = new JLabel("DataType :");
		JLabel lblDataset = new JLabel("Filter on sources : ");
		JLabel lblDistanceThreshold = new JLabel("Distance Threshold (real space): ");
		JLabel lblVariationThreshold = new JLabel("Variation Threshold : ");



		//ComboBox for the filter on sources
		sFCombo = new JComboBox<>();
		sFCombo.setRenderer(new MetaRenderer(theme, session.dataRegistry()));
		sFCombo.addItem(null);


		//Distance input
		dTField = new JTextField(""+max);
		dTField.setColumns(10);

		//Variation input
		vTField = new JTextField(""+v);
		vTField.setColumns(10);



		//Define the processable data the user can choose from

		//Get all the classes available implementing segmented object
		Collection<MetaQueryable> processable =  session.metaModel().getQueryable(allowed);

		//Populate the typeCombo
		JComboBox<MetaClass> typeCombo = new JComboBox<>();
		typeCombo.setRenderer(new MetaRenderer(theme, session.dataRegistry()));
		processable.forEach(m->typeCombo.addItem((MetaClass) m));


		//Populate filterCombos
		MetaClass type = (MetaClass) typeCombo.getSelectedItem();
		if(type != null){
			type.filters().forEach(mf -> sFCombo.addItem(mf));
			sFCombo.setSelectedIndex(0);
		}





		// Action events
		typeCombo.addActionListener(l->{
			//Update the filters when the dataType changes
			sFCombo.removeAllItems();
			sFCombo.addItem(null);

			MetaClass target = (MetaClass) typeCombo.getSelectedItem();
			if(target == null)
				return;			
			target.filters().forEach(mf -> sFCombo.addItem(mf));
			sFCombo.setSelectedIndex(0);
		});



		
		
		
			JButton okButton = new JButton("RUN");
			okButton.addActionListener(l->{
				//Get the max distance
				try{
					String d = dTField.getText();
					max = Double.parseDouble(d);
				}catch(Exception e ){
					JOptionPane.showMessageDialog(null, "There seem to be a problem with the "
							+ "maximum distance you entered. \nPlease correct the value.");
					return;
				}

				//Get the max variation
				try{
					String d = vTField.getText();
					v = Double.parseDouble(d);
				}catch(Exception e ){
					JOptionPane.showMessageDialog(null, "There seem to be a problem with the "
							+ "maximum variation you entered. \nPlease correct the value.");
					return;
				}
				

				sourceFilter = (MetaFilter) sFCombo.getSelectedItem();
				targetType = (MetaClass) typeCombo.getSelectedItem();

				wasCancelled = false;
				this.dispose();
			});


		
		
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(l->{
				wasCancelled = true;
				this.dispose();
			});
		
		
		





		//Layout
		//---------------------------------------------------------------------------------------------


		setResizable(false);
		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 415, 221);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);





		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(label)
							.addPreferredGap(ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
							.addComponent(typeCombo, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(lblDataset)
							.addGap(34)
							.addComponent(sFCombo, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE)))
					.addGap(10))
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addGap(10)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(lblDistanceThreshold)
							.addPreferredGap(ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
							.addComponent(dTField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(lblVariationThreshold, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
							.addGap(102)
							.addComponent(vTField, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(typeCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDataset)
						.addComponent(sFCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDistanceThreshold)
						.addComponent(dTField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(vTField)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(2)
							.addComponent(lblVariationThreshold)))
					.addGap(199))
		);
		contentPanel.setLayout(gl_contentPanel);
		
		
		//OK /Cancel Panel
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));		
		buttonPane.add(okButton);
		buttonPane.add(cancelButton);
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
	}





	public boolean wasCancelled(){
		return wasCancelled;
	}

	public Optional<MetaFilter> getSourceFilter(){
		return Optional.ofNullable(sourceFilter);
	}
	

	public MetaClass getTargetType(){
		return targetType;
	}

	public double getMaxDistance(){
		return max;
	}
	
	public double getMaxVariation(){
		return v;
	}
}
