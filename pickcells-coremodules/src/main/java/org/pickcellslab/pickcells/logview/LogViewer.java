package org.pickcellslab.pickcells.logview;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.DefaultDocument;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;



public class LogViewer extends AppenderBase<ILoggingEvent> implements DefaultDocument {

	private final UITheme theme;
	
	private JTextPane pane;
	private StyledDocument doc;
	private Map<Level,Style> styles;
	private Map<Level,Style> icons;

	public LogViewer(UITheme theme){

		this.theme = theme;

		pane = new JTextPane();
		doc = pane.getStyledDocument();
		styles = new HashMap<>();
		icons = new HashMap<>();

		Font font = new Font("Serif", Font.BOLD, 12);		
		pane.setFont(font);
		pane.setEditable(false);

		//Setup the different style for each level
		Style debug = pane.addStyle("debug", null);
		StyleConstants.setForeground(debug, Color.GRAY);
		styles.put(Level.DEBUG, debug);

		//Info
		Style info = pane.addStyle("info", null);
		StyleConstants.setForeground(info, Color.BLACK);	
		styles.put(Level.INFO, info);

		Style infoIcon =  pane.addStyle("info_icon", null);
		StyleConstants.setIcon(infoIcon, theme.icon(IconID.Levels.INFO, 16, Color.BLUE));
		icons.put(Level.INFO, infoIcon);

		//Warning
		Style warn = pane.addStyle("warn", null);
		StyleConstants.setForeground(warn, Color.ORANGE);	
		styles.put(Level.WARN, warn);

		Style warnIcon =  pane.addStyle("warn_icon", null);
		StyleConstants.setIcon(warnIcon, theme.icon(IconID.Levels.WARN, 16, Color.ORANGE));
		icons.put(Level.WARN, warnIcon);

		//Error
		Style error = pane.addStyle("error", null);
		StyleConstants.setForeground(error, Color.RED);	
		styles.put(Level.ERROR, error);

		Style errorIcon =  pane.addStyle("error_icon", null);
		StyleConstants.setIcon(errorIcon, theme.icon(IconID.Levels.ERROR, 16, Color.RED));
		icons.put(Level.ERROR, errorIcon);

	}

	
	
	
	@Override
	public Component getScene() {
		JScrollPane scroll = new JScrollPane(pane);
		scroll.setName("Events Log");
		return scroll;
	}



	@Override
	protected void append(ILoggingEvent loggingEvent) {
		//System.out.println("Logging event received :"+loggingEvent.getMessage());

		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				try {	
					doc.insertString(doc.getLength(), "\n",null);
					doc.insertString(doc.getLength(), " ", icons.get(loggingEvent.getLevel()));
					doc.insertString(doc.getLength(), (String) loggingEvent.getMessage(), styles.get(loggingEvent.getLevel()));
				} catch (BadLocationException e) {
					e.printStackTrace();
				}				
			}

		});
	}
	



	@Override
	public Icon icon() {
		return theme.icon(IconID.Levels.INFO, 24);
	}



	@Override
	public int preferredLocation() {
		return DefaultDocument.DOWN;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		pane.setText("");
	}

	@Override
	public void dispose() {
		pane = null;
		doc = null;
		styles = null;
		icons = null;		
	}



	@Override
	public String name() {
		return "Log View";
	}

}
