package org.pickcellslab.pickcells.clustering;

import java.net.URL;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.queryui.QueryableChoiceDialog;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter2DFactory;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter3DFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.ClusterableAdapter;
import org.pickcellslab.pickcells.api.datamodel.types.Clustering;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.pickcellslab.pickcells.assign_nc.CentrosomeVectorFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Module
public class EuclidianClustering extends AbstractAnalysis  {

	private Logger log = LoggerFactory.getLogger(CentrosomeVectorFinder.class);

	private boolean isActivable = true;
	private boolean isRunning = false;
	private final DataAccess access;

	private int numImages;

	private static final int cancel = 0, ok = 1, redef = 2;

	private final NotificationFactory notif;
	private final SimpleScatter2DFactory twoDScatterFctry; 
	private final SimpleScatter3DFactory threeDScatterFctry;

	private final UITheme theme;

	public EuclidianClustering(UITheme theme, NotificationFactory notif, DataAccess access, SimpleScatter2DFactory twoDScatterFctry, SimpleScatter3DFactory threeDScatterFctry) {
		this.theme = theme;
		this.notif = notif;
		this.access = access;
		this.twoDScatterFctry = twoDScatterFctry;
		this.threeDScatterFctry = threeDScatterFctry;
	}
	
	

	@Override
	public String name() {
		return "Clustering";
	}
	@Override
	public String description() {
		return "Use the DBScan algorithm to cluster objects in space.";
	}




	@Override
	public void launch() {

		try {

			isRunning = true;

			setStep(0);


			//Get images in experiment
			List<Image> images = null;
			try {
				images = access.queryFactory()
						.regenerate(Image.class).toDepth(0)
						.traverseAllLinks().includeAllNodes()
						.regenerateAllKeys().getAll().getTargets(Image.class).collect(Collectors.toList());
			} catch (DataAccessException e) {
				log.error("Unable to read the database");
				setFailure("Unable to read the database");
				return;
			}


			if(images.isEmpty()){
				JOptionPane.showMessageDialog(null,"There are no data in the experiment");
				log.info("Euclidian clustering was cancelled");
				setFailure("No image in the database");
				return;
			}


			QueryableChoiceDialog<MetaClass> sd =new QueryableChoiceDialog<>(
					theme,
					access,
					"Choose which objects to cluster",
					(mq) -> {
						if(!MetaClass.class.isAssignableFrom(mq.getClass()))
							return false;
						return mq.hasKey(Keys.centroid);
					}, false);

			sd.setModal(true);
			sd.pack();
			sd.setLocationRelativeTo(null);
			sd.setVisible(true);



			Optional<MetaClass> sOpt = sd.getChoice();

			if(!sOpt.isPresent()){
				log.info("Euclidian clustering was cancelled");
				setCancelled();
				return;
			}



			String name = JOptionPane.showInputDialog("Give a name to this Clustering", "DBSCAN");
			if(name == null) {
				log.info("Euclidian clustering was cancelled");
				setCancelled();
				return;
			}


			
			
			//Display a dialog to allow the user to choose which image to load for preview
			Image choice = (Image) JOptionPane.showInputDialog(
					null,
					"Choose an image to load for the preview",
					"DBSCAN...",
					JOptionPane.PLAIN_MESSAGE,
					icon(),
					images.toArray(),
					images.get(0)
					);
	
			if(choice == null){
				setCancelled();
				return;
			}



			@SuppressWarnings("unchecked")
			List<ImageLocated> sources =
			UsefulQueries.imageToImageLocated(
					access,
					sOpt.get(),
					choice.getAttribute(WritableDataItem.idKey).get())
			.getAllItemsFor(ImageLocated.class).collect(Collectors.toList());

			//Adapt sources to Clusterable objects
			List<ClusterableAdapter<ImageLocated>> list = 
					sources.stream()
					.map(s->new ClusterableAdapter<>(s,(o)->o.centroid()))
					.collect(Collectors.toList());

			DBSCANDialog<ClusterableAdapter<ImageLocated>> dialog = new DBSCANDialog<>(list, twoDScatterFctry, threeDScatterFctry);
			dialog.setModal(true);
			dialog.pack();
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			if(dialog.wasCancelled()){
				setCancelled();
				return;
			}

			
			
			//debug only

			try {
				access.queryFactory().delete(Clustering.class).completely().getAll().run();
				access.queryFactory().delete(org.pickcellslab.pickcells.api.datamodel.types.Cluster.class).completely().getAll().run();
			} catch (DataAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			
			
			



			//Create 1 clustering node for all clusters
			Clustering clustering = new Clustering(name,"DBSCAN","minPts = "+dialog.minPts()+" ; eps = "+dialog.eps());

			int counter = 0;
			for(Image i : images){

				setStep(++counter);


				sources = 
						UsefulQueries.imageToImageLocated(
								access,
								sOpt.get(),
								i.getAttribute(WritableDataItem.idKey).get())
						.getAllItemsFor(ImageLocated.class).collect(Collectors.toList());


				//Adapt sources to Clusterable objects
				list = sources.stream()
						.map(s->new ClusterableAdapter<ImageLocated>(s,(o)->o.centroid()))
						.collect(Collectors.toList());

				DBSCANClusterer<ClusterableAdapter<ImageLocated>> clusterer = new DBSCANClusterer<>(dialog.eps(),dialog.minPts());
				List<Cluster<ClusterableAdapter<ImageLocated>>> clusters = clusterer.cluster(list);

				int id = 0;
				for(Cluster<ClusterableAdapter<ImageLocated>> c : clusters)				
					new org.pickcellslab.pickcells.api.datamodel.types.Cluster(i, clustering,
							c.getPoints().stream().map(a->a.object()).collect(Collectors.toList()), id++);				


			}


			//Store to the database
			try {
				access.queryFactory().store().add(clustering).run();

				/*
			//Create default filters

			// To Find this clustering
			ExplicitPredicate<DataItem> p = P.keyTest(DataItem.idKey,
					P.equalsTo(clustering.getAttribute(DataItem.idKey).get()));
			MetaFilter fitF = 
					new MetaFilter(
							name,
							"Clustering Analysis",
							"Find a Clustering with name : " + name,
							p, Subtype.ATTRIBUTES, Meta.getMetaModel(Clustering.class, session));
			session.runQuery(QueryFactory.meta(fitF));
				 */
				// To Find each individual cluster


			} catch (DataAccessException e) {
				JOptionPane.showMessageDialog(null,"An error occured while saving to the database");
				e.printStackTrace();
			}

			setStep(numImages+1);
			isRunning = false;

			

		}catch(Exception e){
			notif.display("Error", "An exception occured while running DBSCAN", e, Level.WARNING);
		}
		
		
	}

		



		@Override
		public boolean isActive() {
			return isActivable;
		}




		@Override
		public String[] categories() {
			return new String[]{"Clustering"};
		}

		@Override
		public Icon icon() {
			return new ImageIcon(getClass().getResource("/icons/clustering_icon.png"));
		}



		private class Choice{

			private int c;

			Choice(int c){
				this.c = c;
			}

			void set(int o){
				c = o;
			}

			int get(){
				return c;
			}
		}


	

		@Override
		public Icon[] icons() {
			return new Icon[3];
		}



		@Override
		public String[] steps() {		
			String[]  steps = new String[numImages+2]; 
			for(int i = 0; i<numImages; i++)
				steps[i+1] = "I"+i;
			steps[0] = "Preview";
			steps[steps.length-1] = "Done";
			return steps;
		}
		
		@Override
		public void stop() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void start() {
			//Check if Objects have centroids in the database
			isActivable = !access.metaModel().getQueryable(p->p.hasKey(Keys.centroid)).isEmpty();

			if(isActivable){
				//Get the number of images
				try {
					numImages = access.queryFactory().read(Image.class).makeList(Keys.name).inOneSet().getAll().run().size();
				} catch (DataAccessException e) {
					notif.display("Error", "Euclidian Clustering unable to read the database", e, Level.SEVERE);
					isActivable = false;
				}
			}		
			this.fireIsNowActive();				
		}
		
		
		@Override
		public String authors() {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public String licence() {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public URL url() {
			// TODO Auto-generated method stub
			return null;
		}










	}
