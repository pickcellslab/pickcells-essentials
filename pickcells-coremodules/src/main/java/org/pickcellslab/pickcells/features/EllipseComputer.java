package org.pickcellslab.pickcells.features;

import java.net.URL;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageConsumer;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.type.numeric.RealType;

@Module
public class EllipseComputer<T extends RealType<T>> extends FeaturesComputer implements SegmentationImageConsumer<T> {

	private final Logger log = LoggerFactory.getLogger(EllipseComputer.class);


	private final List<SegmentationImageProvider<T>> segProviders = new ArrayList<>();


	public static final AKey<double[]> pca1 = AKey.get("PCA1", double[].class);
	public static final AKey<double[]> pca2 = AKey.get("PCA2", double[].class);
	public static final AKey<double[]> pca3 = AKey.get("PCA3", double[].class);

	public static final AKey<Double> aniso = AKey.get("Anisotropy", Double.class);

	private static Map<AKey<?>, String> features = new HashMap<>();

	static{		
		features.put(pca1, DataModel.DIRECTIONAL+" : A vector representing the principal axis of the best fit ellipsoid");
		features.put(pca2, DataModel.DIRECTIONAL+" : A vector representing the second axis of the best fit ellipsoid");
		features.put(pca3, DataModel.DIRECTIONAL+" : A vector representing the third axis of the best fit ellipsoid");
		features.put(aniso, "A measure of anisotropy of the shape (ratio of eigenvectors of the covariance matrix)");
	}



	@Override
	public String name() {
		return "Ellipsoid Fitter";
	}

	@Override
	public String description() {
		return "Finds the best fit ellipoid of the shape and writes derived features";
	}


	@Override
	public void run() {


		final AKey[] keys = {pca1, pca2, pca3};

		for(SegmentationImageProvider<T> provider : segProviders){

			log.debug("new provider");

			Image i = provider.origin().origin();

			log.debug("Checking "+i.getAttribute(Keys.name));

			//Use an out of bound strategy to avoid nullPointerException
			//RandomAccessible<RealType> interval = Views.extendZero(img);
			IntStream.range(0, (int)i.frames()).forEach(t->{

				//For each label get the interval defined by the bounding box
				provider.labels(t).forEach(label->{

					log.trace("new label");
					//get the item corresponding to the current label
					SegmentedObject item = provider.getItem(label, t).get();

					//get the centroid
					double[] centroid = item.getAttribute(Keys.centroid).get();

					//Build the covariance matrix
					List<double[]> points = new ArrayList<>();
					provider.processPoints(label, t, l->points.add(i.calibrated(l, false, false)));

					if(points.size()>2){
						Covariance cov = new Covariance(points.toArray(new double[][]{}));
						RealMatrix cM = cov.getCovarianceMatrix();

						// Get Eigen vectors
						EigenDecomposition dec = new EigenDecomposition(cM);

						for(int d = 0; d<centroid.length; d++){
							RealVector rv = dec.getEigenvector(d);
							rv = rv.mapDivide(rv.getNorm());
							double[] v = rv.toArray();
							item.setAttribute(keys[d], v);
						}

						//Get Anisotropy					
						if(i.isZStack() && dec.getRealEigenvalue(2) != 0){
							double lambda1 = dec.getRealEigenvalue(0);
							double an = 1 - (dec.getRealEigenvalue(2)/lambda1 + dec.getRealEigenvalue(1)/lambda1)/2;
							item.setAttribute(aniso, an);
						}
						else{
							double an = 1 - dec.getRealEigenvalue(1)/dec.getRealEigenvalue(0);
							item.setAttribute(aniso, an);
						}
					}
					else{
						
						if(i.isZStack()){
							item.setAttribute(keys[0], new double[]{0,0,1});
							item.setAttribute(keys[1], new double[]{0,1,0});
							item.setAttribute(keys[2], new double[]{1,0,0});
						}else{
							item.setAttribute(keys[0], new double[]{0,1});
							item.setAttribute(keys[1], new double[]{1,0});
						}						
						
						item.setAttribute(aniso, 0d);
					}


				});

			});

		}
	}

	@Override
	public Collection<AKey<?>> requirements() {
		return Collections.emptySet();
	}





	@Override
	public Collection<AKey<?>> capabilities() {
		return features.keySet();
	}



	@Override
	public void setProvider(SegmentationImageProvider<T> one,
			@SuppressWarnings("unchecked") SegmentationImageProvider<T>... others) {
		segProviders.clear();
		segProviders.add(one);
		for(SegmentationImageProvider<T> p: others)
			segProviders.add(p);
	}

	@Override
	public Map<AKey<?>, String> featuresDescription() {
		return features;
	}

	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}





}
