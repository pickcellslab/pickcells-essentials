package org.pickcellslab.pickcells.spots;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.util.MathArrays;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.geometry.triangulation.DataTriangulation;

import net.imglib2.Cursor;
import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.EllipsoidNeighborhood;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.ComplexType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.ExtendedRandomAccessibleInterval;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

/**
 * This class contains methods to identify spots which belong to mitoses based on centrosome staining in an image.
 * @author Guillaume Blin
 *
 */
public final class MitoticSpots {



	//Parameters
	private double delta = 0.6;
	private double T = 0.01;
	private double itr = 8;
	private double lowest = 50;
	private double maxAngle = Math.PI/2;

	/**
	 * Max inter-centrosome distance during mitoses in micron, see 151004_Mitosis
	 */
	private static final double maxD = 14.5;
	/**
	 * Min inter-centrosome distance during mitoses in micron, see 151004_Mitosis
	 */
	private static final double minD = 3;


	//Created Attributes
	/**
	 * A vector which summarises the anisotropy of an expanded region with respect to the origin
	 */
	public final static AKey<double[]> skew = AKey.get("Skew",double[].class);
	/**
	 * The volume of the expanded region
	 */
	public final static AKey<Integer> volume = AKey.get("Volume", Integer.class);
	/**
	 * A confidence weight (sum of angles between region anisotropy and neighbours direction)
	 */
	public final static AKey<Double> confidence = AKey.get("Confidence",Double.class);
	/**
	 * A vector giving the orientation of the mitosis (spot to spot vector)
	 */
	public final static AKey<double[]> orientation = AKey.get("Volume", double[].class);




	public final static String Mitosis = "MITOSIS";


	/**
	 * Grows a labelled region around each given ImageDots based on intensity values and gradients in the image.
	 * Side effects are: 1 The provided label image will contain the grown regions.
	 * 2 the provided dots will be given attributes {@link #skew} and {@link #volume}
	 * @param dots The ImageDots from which growth must be initialised.
	 * @param img The gray valued channel which contains the dots.
	 * @param labelImg An IntervalView used to store the regions grown for each dot. Must be filled with 0 values.
	 */
	public <T extends RealType<T> & NativeType<T>> void growRegions(Image image, Collection<ImageDot> dots, RandomAccessibleInterval<T> img, IntervalView<T> labelImg){

		System.out.println("---------------Expanding Spots-------------------");

		//Extend the gray-valued image
		ExtendedRandomAccessibleInterval<T, RandomAccessibleInterval<T>> gray = Views.extendValue(img, labelImg.firstElement().createVariable());


		//Initialise The label image
		T value = labelImg.firstElement().createVariable();
		value.setReal(-2);
		RandomAccessible<T> labels = Views.extendValue(labelImg, value);


		/*
		float min = Float.MAX_VALUE;
		float max = Float.MIN_VALUE;
		for(ImageDot dot : dots){
			long[] d = ImageDot.noChannelLocation(dot);
			grayAccess.setPosition(d);
			float s = grayAccess.get().getRealFloat();
			if(s<min) min = s;
			if(s>max) max = s;
		}
		System.out.println("Min = " + min);
		System.out.println("Min = " + max);

		float correction = (max-min)/max;
		System.out.println("Correction = " + correction);
		 */



		//Create the predicate in charge of testing if neighbour pixels should be accepted as part of the region or not.
		Predicate<Expansion<T>> p = e->{

			//System.out.println("-----------------------------------");
			//System.out.println("origin: "+e.originSignal());
			//System.out.println("p: "+e.pSignal());
			//System.out.println("n: "+e.nSignal());

			//Check that the signal is above min intensity
			if(e.nSignal()<lowest) return false; 

			//Check maximum difference with origin
			if(e.nSignal() < T*e.originSignal())	return false;

			//Check neighbour delta
			if(e.nSignal() < delta*e.pSignal()) 	return false;
			return true;			
		};




		//Initialise expansions
		List<Expansion<T>> list = new ArrayList<>();
		int counter = 1;
		for(ImageDot dot : dots)
			list.add(new Expansion<T>(dot, Views.interval(gray,img), labels, counter++, p ));

		// run the number of specified iterations
		for(int i = 0; i<itr; i++)
			list.forEach(e->e.run());



		System.out.println("---------------Computing Domains Stats-------------------");

		list.forEach(e->{

			// Get the region as a list of locations
			List<long[]> region = e.currentDomain();

			//Compute skew
			Vector3D c = new Vector3D(e.getDot().getAttribute(Keys.centroid).get());
			Vector3D avg = new Vector3D(0,0,0);
			for(long[] l : region){
				Vector3D v = new Vector3D(image.calibrated(l, false, false));
				v = v.subtract(c);
				avg = avg.add(v);
			}
			avg = avg.scalarMultiply(1d/(double)region.size());


			e.getDot().setAttribute(volume, region.size());
			e.getDot().setAttribute(skew,avg.toArray());

		});

	}






	/**
	 * Find the pairs of spots making a mitosis based on distance and skew orientation.
	 * Please make sure that {@link #growRegions(Collection, Img, IntervalView)} was called on 
	 * given dots before.
	 * @param dots
	 * @return The list of {@link Link}s linking mitotic spots pairs.
	 */
	public <T extends NativeType<T> & ComplexType<T>> List<Link> findMitoses(Image image, IntervalView<T> grays, List<ImageDot> dots){



		List<Link> mitoses = new ArrayList<>();

		if(dots.size()<2)
			return Collections.emptyList();

		if(dots.size()>2){	
			//Delaunay triangulation to define neighbours for each dot		
			System.out.println("---------------Creating Triangulation-------------------");
			DataTriangulation.triangulate(dots, i->i.getAttribute(Keys.centroid).get(), 3, maxD, "TEMP");
		}else{
			double d = MathArrays.distance(dots.get(0).centroid(), dots.get(1).centroid());
			if(d<maxD){
				Link l = new DataLink("TEMP", dots.get(0), dots.get(1), true);
				l.setAttribute(Keys.distance, d);
			}
				
		}


		//Filter based on angles and min distance

		Set<ImageDot> retained = new HashSet<>();


		dots.forEach(cent->{
			cent.getLinks(Direction.BOTH, "TEMP").forEach(l->{
				if(l.getAttribute(Keys.distance).get() >= minD){//Check above minimum distance
					//Check that the angle between skew and link neighbour is below max Angle
					Vector3D A = new Vector3D(((ImageDot) l.source()).getAttribute(Keys.centroid).get());
					Vector3D B = new Vector3D(((ImageDot) l.target()).getAttribute(Keys.centroid).get());
					Vector3D AB = B.subtract(A);
					Vector3D BA = A.subtract(B);
					double a = Vector3D.angle(AB, new Vector3D(((ImageDot) l.source()).getAttribute(skew).get()));
				//	System.out.println("Angle = "+a);
					if(a <= maxAngle){					
						double b = Vector3D.angle(BA, new Vector3D(((ImageDot) l.target()).getAttribute(skew).get()));
						//System.out.println("Angle = "+b);
						if(b <= maxAngle){

							//Now check that the midle of the spots is a dark region
							EllipsoidNeighborhood<T> m =
									new EllipsoidNeighborhood<>(grays, new long[]{0,0,0}, new long[]{2,2,2});

							float size = m.size();

							long[] pos = new long[3];
							long[] I = ((ImageDot) l.source()).location();
							long[] J = ((ImageDot) l.target()).location();
							for(int i = 0; i<3; i++)
								pos[i] = I[i] - (I[i]-J[i])/2;
							m.setPosition(pos);
							float avg = 0;
							for( T t : m){
								avg += t.getRealFloat();
							}
							avg/=size;
							//System.out.println("Avg center = "+avg);

							if(avg<= lowest){
								Link link = new DataLink(Mitosis, l.source(),l.target(), true);
								link.setAttribute(orientation,AB.toArray());
								link.setAttribute(Keys.location,pos);
								link.setAttribute(Keys.centroid,image.calibrated(pos, false, false));
								link.setAttribute(confidence, 2*Math.PI - (a+b));
								mitoses.add(link);
								retained.add(cent);
							}
						}
					}
				}
				l.delete();
			});	
		});



		//Find best matches
		// 1 - any spot with only one link has precedence over others

		for(Link l : mitoses){
			if(l.source() != null){// Check not already deleted
				final List<Link> srcLinks = l.source().getLinks(Direction.BOTH, Mitosis).collect(Collectors.toList());
				if(srcLinks.size() == 1){					
					for(Link tl : l.target().getLinks(Direction.BOTH, Mitosis).collect(Collectors.toList()))
						if(tl != l)
							tl.delete();
					retained.remove(l.source());
					retained.remove(l.target());

				}else{//Check target
					final Collection<Link> trgLinks = l.target().getLinks(Direction.BOTH, Mitosis).collect(Collectors.toList());
					if(trgLinks.size() == 1){
						for(Link sl : l.source().getLinks(Direction.BOTH, Mitosis).collect(Collectors.toList()))
							if(sl != l)
								sl.delete();
						retained.remove(l.source());
						retained.remove(l.target());
					}
				}
			}
		}

		// 2 - For all the other nodes retain the highest confidence
		Comparator<Link> comp = Comparator.comparingDouble(l -> l.getAttribute(confidence).get());
		for(ImageDot d : retained){
			Link best = d.getLinks(Direction.BOTH, Mitosis)
					.sorted(comp).findFirst().orElse(null);
			if(best!=null){
				best.source().getLinks(Direction.BOTH, Mitosis)
				.forEach(l->{
					if(l!=best)
						l.delete();
				});
				best.target().getLinks(Direction.BOTH, Mitosis)
				.forEach(l->{
					if(l!=best)
						l.delete();
				});
			}
		}


		//Clean the list of mitoses and return		
		return mitoses.stream().filter(l->l.source()!=null).collect(Collectors.toList());
	}





	/**
	 * Deletes the attributes and links which may have been added by the methods of MitoticSpots
	 * @param dots The ImageDot to be cleaned
	 */
	public void deleteSideEffects(Collection<ImageDot> dots){
		dots.forEach(d->{
			d.removeAttribute(volume);
			d.removeAttribute(skew);
			d.links().filter(l->l.declaredType().equals(Mitosis)).forEach(l->l.delete());
		});
	}


	/**
	 * Clear the labels which may be present in the given label image
	 * @param labelImg
	 */
	public <T extends RealType<T> & NativeType<T>> void clearLabels(IntervalView<T> labelImg){

		Cursor<T> c = labelImg.cursor();
		while(c.hasNext()){
			c.fwd();
			c.get().setZero();
		}

	}






	/**
	 * Max gradient authorized (percentage of center signal)
	 * @param delta
	 */
	public void setDelta(double delta) {
		this.delta = delta;
	}






	/**
	 * Max difference with origin signal (percentage of origin signal)
	 * @param t
	 */
	public void setT(double t) {
		T = t;
	}






	/**
	 * Max distance from origin (number of iteration) 
	 * @param itr
	 */
	public void setItr(double itr) {
		this.itr = itr;
	}






	/**
	 * Min intensity allowed to accept neighbour pixels during the region growth process
	 * @param lowest
	 */
	public void setLowest(double lowest) {
		this.lowest = lowest;
	}







	public double getDelta() {
		return delta;
	}






	public double getT() {
		return T;
	}






	public double getItr() {
		return itr;
	}






	public double getLowest() {
		return lowest;
	}





}
