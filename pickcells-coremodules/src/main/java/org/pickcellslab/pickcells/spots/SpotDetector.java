package org.pickcellslab.pickcells.spots;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.Point;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

@Module
public class SpotDetector extends AbstractAnalysis implements Analysis{

	private Logger log = LoggerFactory.getLogger(SpotDetector.class);

	private boolean isActivable = true;


	//Session reference
	private final DataAccess access;
	private final ImgIO ioFactory;
	private final NotificationFactory notif;



	public SpotDetector(DataAccess access, NotificationFactory notif, ImgIO factory){
		this.ioFactory = factory;
		this.notif = notif;
		this.access = access;
	}



	@Override
	public String name() {
		return "Spot Detector";
	}

	@Override
	public String description() {
		return "Spots Dectector: Identify bright spots in images.";
	}



	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void launch() {

		try {

			//Get images names
			//Read one image to obtain the name and luts of the channels
			List<String> rt =
					access.queryFactory().read(Image.class)
					.makeList(Keys.name).inOneSet().getAll().run();

			if(rt.isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no images in the database");
				return;
			}

			//Display a dialog to allow the user to choose which image to load for preview
			String choice = (String) JOptionPane.showInputDialog(
					null,
					"Choose an image to preview",
					"Spot Detection...",
					JOptionPane.PLAIN_MESSAGE,
					icon(),
					rt.toArray(),
					rt.get(0)
					);


			if(choice == null)
				return;


			System.out.println("Choice = "+choice);

			setStep(0);





			RegeneratedItems	r = access.queryFactory().regenerate(Image.class).toDepth(0).traverseAllLinks()
					.includeAllNodes().regenerateAllKeys().getAll();



			//Get one image to obtain the name and luts of the channels
			Image im = r.getTargets(Image.class).filter(p->choice.equals(p.getAttribute(Keys.name).orElse(null))).collect(Collectors.toList()).get(0);


			// Dialog to determine which channel and which object to associate
			SpotDetectorDialog dialog = new SpotDetectorDialog(access.dataRegistry(), im, ioFactory, notif);
			dialog.setVisible(true);


			if(dialog.wasCancelled()){
				setCancelled();
				return;
			}

			//Run spot detector with the chosen values on each image
			long channel = dialog.selectedChannel();
			ImageDot prototype;
			try {
				prototype = dialog.getPrototype();
			} catch (InstantiationException | IllegalAccessException e1) {
				notif.display("Error", "Unable to instantiate the selected type",e1, Level.SEVERE);
				setFailure("Unable to instantiate the selected type");
				return;
			}



			//Erase if they already exist
			try {

				int deletions = access.queryFactory().delete(prototype.getClass()).completely().getAll().run();//FIXME take generic type into account
				log.debug("Number of deleted "+prototype.getClass().getSimpleName()+" : "+deletions);

			} catch (DataAccessException e1) {
				notif.display("Error", "Unable to update the database",e1, Level.SEVERE);
				setFailure("Unable to update the database");
				return;
			}


			setStep(1);

			int count = 0;
			Set<Image> images = r.getTargets(Image.class).collect(Collectors.toSet());
			for(Image image : images){

				Img<RealType> img;
				try {
					img = (Img<RealType>) (Img)ioFactory.open(image);
				} catch (IOException e) {
					notif.display("Error", "Unable to open images",e, Level.SEVERE);
					setFailure("Unable to open images");
					return;
				}


				//Get the maxima
				int peakNumber = 0;

				if(image.channelIndex()==-1){//Not multichannel

					if(image.isTimeLapse()){
						for(int t = 0; t<image.frames(); t++){

							List<Point> peaks = dialog.run(Views.hyperSlice(img, image.timeIndex(), t), im.calibration(false, false));
							peakNumber+=peaks.size();

							final int time = t;
							final RandomAccess<RealType> access = img.randomAccess();
							peaks.stream().forEach(p->{ 
								long[] pos = new long[p.numDimensions()];
								p.localize(pos);
								access.setPosition(p);
								ImageDot dot = prototype.create(image,pos);
								dot.setAttribute(ImageDot.signal, new double[]{access.get().getRealDouble()});
								dot.setAttribute(ImageLocated.frameKey, time);
							});
						}
					}

					else{
						List<Point> peaks = dialog.run(img, im.calibration(false, false));
						peakNumber+=peaks.size();
						final RandomAccess<RealType> access = img.randomAccess();
						peaks.stream().forEach(p->{ 
							long[] pos = new long[p.numDimensions()];
							p.localize(pos);
							access.setPosition(p);
							ImageDot dot = prototype.create(image,pos);
							dot.setAttribute(ImageDot.signal, new double[]{access.get().getRealDouble()});
						});
					}

				}
				else{//Multichannel

					if(image.isTimeLapse()){
						int tDim = Image.removeDimension(image.order(),Image.c)[Image.t];
						for(int t = 0; t<image.frames(); t++){

							List<Point> peaks = dialog.run(
									Views.hyperSlice(
											Views.hyperSlice(img, image.channelIndex(), channel),
											tDim, t),
									im.calibration(false, false));

							peakNumber+=peaks.size();

							final int time = t;
							final RandomAccess<RealType>[] channels = new RandomAccess[image.channels().length];
							for(int c = 0; c<channels.length; c++)
								channels[c] = Views.hyperSlice(
										Views.hyperSlice(img, image.channelIndex(), c),
										tDim, t).randomAccess();

							peaks.stream().forEach(p->{
								long[] pos = new long[p.numDimensions()];
								double[] intensities = new double[channels.length];
								p.localize(pos);
								for(int c = 0; c<channels.length; c++){
									channels[c].setPosition(p);
									intensities[c] = channels[c].get().getRealDouble();
								}
								ImageDot dot = prototype.create(image,pos);
								dot.setAttribute(ImageDot.signal, intensities);
								dot.setAttribute(ImageLocated.frameKey, time);
							});

						}
					}

					else{

						List<Point> peaks = dialog.run(Views.hyperSlice(img, image.channelIndex(), channel),im.calibration(false, false));
						peakNumber+=peaks.size();

						final RandomAccess<RealType>[] channels = new RandomAccess[image.channels().length];
						for(int c = 0; c<channels.length; c++)
							channels[c] = Views.hyperSlice(img, image.channelIndex(), c).randomAccess();
						peaks.stream().forEach(p->{
							long[] pos = new long[p.numDimensions()];
							double[] intensities = new double[channels.length];
							p.localize(pos);
							for(int c = 0; c<channels.length; c++){
								channels[c].setPosition(p);
								intensities[c] = channels[c].get().getRealDouble();
							}
							ImageDot dot = prototype.create(image,pos);
							dot.setAttribute(ImageDot.signal, intensities);
						});
					}

				}
				log.info("Number of spots in image : "+peakNumber);


				try {

					image.setAttribute(AKey.get(prototype.getClass().getSimpleName(), Long.class),channel);				

					access.queryFactory().store().add(image)
					.feed(AKey.get(prototype.getClass().getSimpleName(), Long.class), channel,
							"Spot Detector",
							"The index of the channel where "+prototype.getClass().getSimpleName()+
							" were identified", Image.class, "Image").run();

				} catch (DataAccessException e) {
					notif.display("Error", "Unable to update the database",e, Level.SEVERE);
					setFailure("Unable to update the database");
					return;
				}

				setProgress((float)++count/(float)images.size());
			}	
			setStep(2);


		}
		catch(Exception e ){
			notif.display("Error", "An error while processing images occured, see the log", e, Level.WARNING);
		}
	}





	@Override
	public boolean isActive() {
		return isActivable;
	}



	@Override
	public String[] categories() {
		return new String[]{"Segmentation"};
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/DotEditor_32.png"));
	}


	@Override
	public Icon[] icons() {
		return new Icon[2];
	}


	@Override
	public String[] steps() {
		return new String[]{
				"Inputs",
				"Finding Spots",
				"Done"
		};
	}



	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}


}
