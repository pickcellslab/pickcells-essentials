package org.pickcellslab.pickcells.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.dataviews.fitting.Distribution;

public class DistriWriterDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nameField;
	private JTextField outField;


	private boolean wasCancelled = true;

	private String name = "New Classification";
	private String description;
	private double outThresh = 0.001;
	private boolean getOut;


	/**
	 * Create the dialog.
	 */
	DistriWriterDialog(Distribution<?> distri) {



		//==============================================================================================
		// Controllers


		//Name to give to the classification
		JLabel lblName = new JLabel("Name");
		nameField = new JTextField(name);
		nameField.setColumns(10);

		// Description
		JLabel lblDescription = new JLabel("Description:");
		
		
		JTextPane descPane = new JTextPane();
		JScrollPane scroll = new JScrollPane(descPane);
		
		descPane.setContentType("text/html");
		descPane.setText(distri.summary());

		//Add an outlier class
		outField = new JTextField();
		outField.setText(""+outThresh);
		outField.setColumns(10);
		outField.setEnabled(false);

		JCheckBox outCheck = new JCheckBox("Set as \"Outlier\" if density < ");
		outCheck.addActionListener(l-> outField.setEnabled(outCheck.isSelected()));


		JButton okButton = new JButton("OK");
		okButton.addActionListener(l->{
			if(nameField.getText().isEmpty()){
				JOptionPane.showMessageDialog(DistriWriterDialog.this, "The name is empty!");
				return;
			}
			name = nameField.getText();
			if(descPane.getText().isEmpty()){
				if(JOptionPane.NO_OPTION == 
						JOptionPane.showConfirmDialog(DistriWriterDialog.this, "The name is empty!", "Description?",JOptionPane.YES_NO_OPTION))
					return;
			}
			description = descPane.getText();

			if(outCheck.isSelected()){
				if(outField.getText().isEmpty()){
					JOptionPane.showMessageDialog(DistriWriterDialog.this, "The outlier threshold is empty!");
					return;
				}

				try{
					outThresh = Double.parseDouble(outField.getText());
				}catch(NumberFormatException e){
					JOptionPane.showMessageDialog(DistriWriterDialog.this, "The outlier threshold must be a number!");
					return;
				}

			}
			
			getOut = outCheck.isSelected();

			wasCancelled = false;
			this.dispose();
		});

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(l->this.dispose());







		//=====================================================================================================================
		// Layout

		setTitle("Classification");

		setBounds(100, 100, 336, 256);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);



		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addComponent(outCheck)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(outField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addComponent(lblName)
										.addGap(18)
										.addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(lblDescription)
								.addComponent(scroll, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(143, Short.MAX_VALUE))
				);
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addContainerGap()
										.addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
										.addContainerGap(14, Short.MAX_VALUE)
										.addComponent(lblName)
										.addGap(6)))
						.addComponent(lblDescription)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(scroll, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGap(3)
										.addComponent(outCheck))
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGap(5)
										.addComponent(outField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		contentPanel.setLayout(gl_contentPanel);




		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);


		buttonPane.add(okButton);
		buttonPane.add(cancelButton);
		getRootPane().setDefaultButton(cancelButton);


	}


	boolean wasCancelled() {
		return wasCancelled;
	}


	String classificationName() {
		return name;
	}


	String getDescription() {
		return description;
	}

	boolean checkOutliers(){
		return getOut;
	}

	double getOutThresh() {
		return outThresh;
	}
}
