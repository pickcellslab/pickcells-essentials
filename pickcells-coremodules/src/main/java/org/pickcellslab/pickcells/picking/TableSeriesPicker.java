package org.pickcellslab.pickcells.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.JXTable;
import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.ReadTable;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.picking.SeriesPickConsumer;

import com.alee.laf.filechooser.WebFileChooser;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;
import com.alee.managers.tooltip.TooltipManager;


@Module
public class TableSeriesPicker implements SeriesPickConsumer {


	private final DataAccess access;
	private final NotificationFactory notif;

	
	public TableSeriesPicker(DataAccess access, NotificationFactory notif) {
		this.access = access;
		this.notif = notif;
	}
	

	@Override
	public void seriesPicked(String seriesId, DataPointer v) {

		//null check
		Objects.requireNonNull(seriesId,"SeriesId is null");
		Objects.requireNonNull(v,"vignette is null");

		ReadTable model = null;


		try{

			model = v.readData(access, Actions.table(getDimensions(v)), 50); // FIXME table action should also include result of filters


		}catch(Exception e){
			notif.display("Error", "An exception occured while getting info for object in series "+seriesId, e, Level.WARNING);
			return;
		}


		if(model!=null){

			JFrame f = new JFrame(seriesId+" (preview)");
			model.sortColumns((k1,k2)->k1.name.compareTo(k2.name));
			
			JXTable table = new JXTable();			
			table.setModel(model);

			JScrollPane scroll = new JScrollPane(table);			
			table.setColumnControlVisible(true);
			table.setHorizontalScrollEnabled(true);
			table.packAll();

			// Order the columns in alphabetical order
			List<Object> headers = new ArrayList<>();
			for(int c = 0; c<table.getColumnCount(); c++)
				headers.add(table.getColumn(c).getHeaderValue());
			table.setColumnSequence(headers.toArray());

			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());			
			panel.add(scroll,BorderLayout.CENTER);

			JPanel options = new JPanel();
			options.setLayout(new FlowLayout(FlowLayout.RIGHT));

			JButton close = new JButton("Close");
			close.addActionListener(l->	f.dispose()	);
			options.add(close);

			JButton save = new JButton("Save");
			TooltipManager.addTooltip(save, "Export to .txt", TooltipWay.left, 0);
			save.addActionListener(l->{

				File file = WebFileChooser.showSaveDialog();
				if(file == null)
					return;				


				Thread t = new Thread(()->{
					try {
						v.readData(access, new ToFileAction(getDimensions(v), getFilters(v), file), -1);
					} catch (Exception e1) {
						notif.display("Error", "An exception occured while getting info for object in series "+seriesId, e1, Level.WARNING);
						return;
					}


				});

				notif.waitAnimation(t,"Saving");

				try {
					t.join();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(table, "Could not save to file", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}


				WebNotification wn = NotificationManager.showNotification(seriesId+" Saved!");
				wn.setDisplayTime(1000);


			});
			options.add(save);

			panel.add(options, BorderLayout.SOUTH);

			f.setContentPane(panel);
			f.pack();
			f.setLocationRelativeTo(null);
			f.setVisible(true);

		}


	}


	private List<Dimension<DataItem,?>> getDimensions(DataPointer v){

		MetaQueryable mq =  v.queriedMetaObjects().get(0);
		List<DimensionContainer> mrs = mq.getReadables().sorted((m1,m2)->m1.name().compareTo(m2.name())).collect(Collectors.toList());

		System.out.println("TableSeriesPicker : picking received, getting dimensions");

		if(mq instanceof MetaLink){
			System.out.println("TableSeriesPicker : MetaLink Detected");
			mrs.add(((MetaLink)mq).readOnSource(WritableDataItem.idKey));
			mrs.add(((MetaLink)mq).readOnTarget(WritableDataItem.idKey));
		}

		List<Dimension> dims = new ArrayList<>();
		for(DimensionContainer mr : mrs){
			for(int i = 0; i<mr.numDimensions(); i++){
				Dimension<Link, ?> d = mr.getDimension(i);
				dims.add(d);
				//System.out.println("TableSeriesPicker : adding "+d.name());
			}
		}

		return (List)dims;
	}

	
	
	private List<MetaFilter> getFilters(DataPointer v){
		MetaQueryable mq =  v.queriedMetaObjects().get(0);
		return mq.filters().collect(Collectors.toList());
	}



	@Override
	public String name() {
		return "Table Set";
	}




	private class ToFileAction implements Action<DataItem, Void>{

		ToFileMechanism m;


		public ToFileAction(List<Dimension<DataItem,?>> dims, List<MetaFilter> filters, File f) throws FileNotFoundException {
			m = new ToFileMechanism(dims, filters, f);
		}

		@Override
		public ActionMechanism<DataItem> createCoordinate(Object key) {
			return m;
		}

		@Override
		public Set<Object> coordinates() {
			return Collections.emptySet();
		}

		@Override
		public Void createOutput(Object key) throws IllegalArgumentException {
			return null;
		}

		@Override
		public String description() {
			return "Printing series to file";
		}

	}



	private class ToFileMechanism implements ActionMechanism<DataItem>{

		private final PrintWriter os;
		private final List<Dimension<DataItem, ?>> dims;
		private final List<ExplicitPredicate<DataItem>> predicates = new ArrayList<>();

		public ToFileMechanism(List<Dimension<DataItem,?>> dims, List<MetaFilter> filters, File file) throws FileNotFoundException {

			this.os = new PrintWriter(file);
			this.dims = dims;
			
			// Print Headers
			for (Dimension<DataItem,?> dim : dims) {
				os.print(dim.name()+"\t");
			}
			// Add Filter columns
			for(MetaFilter mf : filters) {
				os.print(mf.name()+"\t");
				predicates.add(mf.toFilter());
			}
			
		}


		@Override
		public void performAction(DataItem i) throws DataAccessException {

			// Print a new row
			os.println();	
			for (Dimension<DataItem,?> dim : dims) {
				os.print(AKey.asString(i, dim)+"\t");
			}
			for (ExplicitPredicate<DataItem> p : predicates) {
				os.print(p.test(i)+"\t");
			}
		}


		public void lastAction(){
			os.flush();
			os.close();
		}

	}


}
