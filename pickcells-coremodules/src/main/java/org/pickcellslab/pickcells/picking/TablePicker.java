package org.pickcellslab.pickcells.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.FlowLayout;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.queryui.utils.MetaToIcon;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPickConsumer;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.extended.image.WebImage;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;

@Module
public class TablePicker implements SingleDataPickConsumer {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final ImgIO io;
	private final DataAccess access;
	private final UITheme theme;

	public TablePicker(UITheme theme, DataAccess access, ImgIO io){
		this.theme = theme;
		this.access = access;
		this.io = io;
	}

	@Override
	public String name(){
		return "Table";
	}

	@Override
	public void singleDataPicked(DataPointer v, int dbId) {

		//TODO in new thread

		try {


			//Init pop over
			final JFrame popOver = new JFrame();
			popOver.setLayout ( new FlowLayout () );

			Icon icon = null;

			//TextPane for the description of the object
			JTextPane pane = new JTextPane();

			//Check if we have an ImageLocated
			Optional<MetaQueryable> segmented = v.queriedMetaObjects().stream().filter(m->{
				if(m instanceof MetaClass) {
					return SegmentedObject.class.isAssignableFrom(((MetaClass) m).itemClass(access.dataRegistry()));
				}
				else
					return false;
			}).findAny();


			if(segmented.isPresent()) {	
				SegObjectMechanism mecha = new SegObjectMechanism((MetaClass)segmented.get());
				segmented.get().readOne(access, dbId, (ActionMechanism)mecha);
				icon = (ImageIcon) mecha.icon;
				pane.setText(mecha.text);

			}				
			//If not ImageLocated simply display the description of the object
			else {

				// FIXME we assume only one queried object here
				MetaQueryable mq = v.queriedMetaObjects().get(0);
				
				icon = MetaToIcon.get(mq, access.dataRegistry(), theme, 32);

				final List<Dimension<DataItem,?>> functions = mq.getReadables()
						.map(mr->mr.getRaw()).collect(Collectors.toList());

				//TODO fetch string and ids / type of source and target
				mq.readOne(access, dbId,item -> {
					StringBuilder b = new StringBuilder();
					b.append(item.toString()+"\n");
					functions.forEach(func-> b.append(func.name()+ " : "+AKey.asString(item,func)+"\n"));
					pane.setText(b.toString());
				});
			}
		

		popOver.setTitle( v.queriedMetaObjects().get(0).name());
		if(icon!=null){
			icon = (ImageIcon) UITheme.resize(icon, 128, 128);
			WebImage img = new WebImage(icon);
			popOver.add(img);
		}
		popOver.add ( new JScrollPane(pane) );
		popOver.pack();
		popOver.setLocationRelativeTo(null);
		popOver.setVisible(true);


	} catch (Exception e) {
		WebNotification wn = NotificationManager.showNotification("Unable to display the table, see the log");
		wn.setDisplayTime(1000);
		log.warn("Unable to display the table", e);
	}

}






private class SegObjectMechanism implements ActionMechanism<NodeItem>{

	private Icon icon;
	private String text;
	private final List<Dimension<DataItem,?>> functions;

	public SegObjectMechanism(MetaClass mc) {
		functions = mc.getReadables()
				.map(mr->mr.getRaw())
				.sorted((d1,d2)->d1.name().compareTo(d2.name()))
				.collect(Collectors.toList());
	}

	@Override
	public void performAction(NodeItem item) throws DataAccessException {

		NodeItem i = item.getLinks(Direction.OUTGOING, Links.CREATED_FROM).iterator().next().target()//SegmentationResult
				.getLinks(Direction.OUTGOING, Links.COMPUTED_FROM).iterator().next().target();//Image


		StringBuilder b = new StringBuilder();


		try {

			Image im = Image.createInstance(access.dataRegistry(), i).get();
			b.append("Found in "+im.toString()+"\n");

			functions.forEach(f-> b.append(f.name()+ " : "+AKey.asString(item,f)+"\n"));

			icon = io.thumbnail(im, item.getAttribute(Keys.bbMin).get(), item.getAttribute(Keys.bbMax).get(), item.getAttribute(ImageLocated.frameKey).orElse(0));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		text = b.toString();
	}


}




}
