package org.pickcellslab.pickcells.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.MixtureModel;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.actions.AKeyInfo;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.pickcells.api.app.picking.DistributionPickConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.managers.notification.NotificationManager;

@Module
public class DistributionWriter implements DistributionPickConsumer<WritableDataItem> {

	private static Logger log = LoggerFactory.getLogger(DistributionWriter.class);
	private final DataAccess access;
	
	
	public DistributionWriter(DataAccess access) {
		this.access = access;
	}
	
	@Override
	public <D> void distributionPicked(Distribution<D> d, MetaQueryable v, Function<WritableDataItem, D> f) {
		
				
		final DistriWriterDialog dialog = new DistriWriterDialog(d);
		dialog.pack();
		dialog.setModal(true);
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
		
		if(dialog.wasCancelled())
			return;
		
		Updater updater = null;
		
		final boolean checkOutliers = dialog.checkOutliers();
		final double thresh = dialog.getOutThresh();
		final List<AKeyInfo> map = new ArrayList<>();
		final AKey<String> predict = AKey.get(dialog.classificationName()+" Class", String.class);
		final AKey<Double> density = AKey.get(dialog.classificationName()+" Density", Double.class);
		final AKey<double[]> proba = AKey.get(dialog.classificationName()+" Probability", double[].class);
		
		map.add(new AKeyInfo(density, dialog.getDescription()));
		
		// If d is a mixture -> 3 keys: density, class, membership 
		if(MixtureModel.class.isAssignableFrom(d.getClass())){
			
			//Cast 
			MixtureModel<D,?> mm = (MixtureModel) d;
			
			
			map.add(new AKeyInfo(predict, "Prediction for "+dialog.getDescription()));
			map.add(new AKeyInfo(proba, "Membership for "+dialog.getDescription(), mm.numDistributions()));
			
			updater = new Updater(){

				@Override
				public void performAction(Updatable i) throws DataAccessException {
					D coords = f.apply(i);
					i.setAttribute(density,mm.density(coords));
					i.setAttribute(proba, mm.memberships(coords));
					i.setAttribute(predict, Integer.toString(mm.predictions(coords))); //TODO add function for custom names
					//TODO check outliers
				}

				@Override
				public List<AKeyInfo> updateIntentions() {
					return map;
				}
				
			};
			
		
		}		
		// If not a mixture and outlier -> 2 keys density, class
		else if(dialog.checkOutliers()){
			
			
			map.add(new AKeyInfo(predict, "Prediction for "+dialog.getDescription()));
			
			final String out = "Outlier";
			final String in = "In";
			
			updater = new Updater(){

				@Override
				public void performAction(Updatable i) throws DataAccessException {
					D coords = f.apply(i);
					double dty = d.density(coords);
					i.setAttribute(density, dty);
							if(dty<thresh)
								i.setAttribute(predict, out);
							else
								i.setAttribute(predict, in);
					
				}

				@Override
				public List<AKeyInfo> updateIntentions() {
					return map;
				}
				
			};
			
			
		}
		// If not a mixture and no outliers -> 1 key density  
		else{
			
			updater = new Updater(){

				@Override
				public void performAction(Updatable i) throws DataAccessException {
					D coords = f.apply(i);
					double dty = d.density(coords);
					i.setAttribute(density, dty);
				}

				@Override
				public List<AKeyInfo> updateIntentions() {
					return map;
				}
				
			};
			
			
			
		}
		
		try {
			v.initiateUpdate(access, updater).getAll().run();
		} catch (DataAccessException e) {
			NotificationManager.showNotification("An error occured while updating the database, see the log");
			log.error("An error occured while updating the database",e);
		}
		
	}


	@Override
	public String name() {
		return "Classify";
	}


	
	
	private class DistributionWriterAction<T> implements Action<T,Void>{

		@Override
		public ActionMechanism<T> createCoordinate(Object key) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Set<Object> coordinates() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Void createOutput(Object key) throws IllegalArgumentException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String description() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}


	
	
	
	

}
