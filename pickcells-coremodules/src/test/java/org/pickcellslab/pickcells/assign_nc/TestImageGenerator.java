package org.pickcellslab.pickcells.assign_nc;

import java.io.IOException;
import java.util.Arrays;

import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.NonLinearIdentityTypeFilters;
import org.pickcellslab.pickcells.api.img.process.Operations;
import org.pickcellslab.pickcells.ioImpl.PickCellsImgIO;

import net.imglib2.Dimensions;
import net.imglib2.FinalInterval;
import net.imglib2.algorithm.neighborhood.RectangleShape;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.util.Util;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

public class TestImageGenerator {

	

	public static void main(String[] args) throws IOException {


		ImgIO io = new PickCellsImgIO();
		
		//Create test image 18
		long[] dims = new long[]{249,249,1,20};
		Dimensions d = new FinalInterval(new long[4],dims);
		Img<UnsignedShortType> img = Util.getArrayOrCellImgFactory(d, new UnsignedShortType()).create(d, new UnsignedShortType());

		IntervalView<UnsignedShortType> nChannel = Views.hyperSlice(img, 2, 0);
		IntervalView<UnsignedShortType> cChannel = Views.hyperSlice(img, 2, 1);

		// Nuclei radii
		long[] nRadii = new long[]{25,50,3};
		long[] nRadii2 = new long[]{35,35,3};
		long[] cRadii = new long[]{2,2,2};
		
		long[] n1 = new long[]{75,135,9};
		long[] n2 = new long[]{135,80,9};
		long[] n3 = new long[]{180,120,9};
		long[] n4 = new long[]{170,200,9};
		
		long[] c1 = new long[]{85,100,9};
		long[] c2 = new long[]{125,170,9};
		long[] c3 = new long[]{120,163,9};
		long[] c4 = new long[]{140,80,9};
		long[] c5 = new long[]{145,85,9};
		

		// Nuclei
		ImgGeometry.drawEllipse(nChannel, n1, nRadii2, new UnsignedShortType(150));
		ImgGeometry.drawEllipse(nChannel, n2, nRadii, new double[]{0.5,0.5,0}, new UnsignedShortType(75));
		ImgGeometry.drawEllipse(nChannel, n3, nRadii, new double[]{0.5,0.5,0},  new UnsignedShortType(200));
		ImgGeometry.drawEllipse(nChannel, n4, nRadii, new double[]{0.5,0,0}, new UnsignedShortType(250));
		
		// Centrosomes
		ImgGeometry.drawEllipse(cChannel, c1, cRadii, new UnsignedShortType(255));
		ImgGeometry.drawEllipse(cChannel, c2, cRadii, new UnsignedShortType(255));
		
		ImgGeometry.drawEllipse(cChannel, c3, cRadii, new UnsignedShortType(255));
		ImgGeometry.drawEllipse(cChannel, c4, cRadii, new UnsignedShortType(255));
		ImgGeometry.drawEllipse(cChannel, c5, cRadii, new UnsignedShortType(255));
		//ImgGeometry.drawEllipse(cChannel, c6, cRadii, new UnsignedShortType(255));

		int[] order = new int[]{0,1,2,3,-1};
		long[] dimensions = new long[4];
		img.dimensions(dimensions);


		//Fill holes due to rounding errors

		Operations.inPlace(
				io,
				nChannel,
				(net.imglib2.algorithm.neighborhood.Shape)new RectangleShape(1,false),
				NonLinearIdentityTypeFilters.medianFunction());


		System.out.println(Arrays.toString(dimensions));

		//	ImageViewer view = new ImageViewer<>(img, new ArrayColorTable[]{Lut8.RANDOM.table(), Lut8.Yellow.table()}, order);
		//	view.displayWithChannelsController("Test", new String[]{"Nuclei", "Centrioles"});

		String path = "/home/ghiomm/documents/Analysis/Associations/GeneratedImages/";
		io.save(new MinimalImageInfo(order, dimensions, new double[]{0.14,0.14,1,0.14,1}), img, path+"Test_"+18+".tif");

	}
	
}
