package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

import org.junit.Test;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.graph.ListenableGraph;
import org.pickcellslab.foundationj.optim.csp.GraphOptimization;
import org.pickcellslab.foundationj.optim.cspImpl.DefaultEnumerationFactory;
import org.pickcellslab.foundationj.optim.cspImpl.DefaultEnumerator.SpanPolicy;
import org.pickcellslab.foundationj.optim.cspImpl.DefaultEnumerator.ordering;
import org.pickcellslab.pickcells.api.app.modules.Demand;
import org.pickcellslab.pickcells.api.app.modules.Dependency;
import org.pickcellslab.pickcells.api.app.modules.DependencyFactory;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;



public class DependenciesTest {

	@Test
	public void testFeatures() {



		//Create some random FeaturesComputers
		List<Demand<Collection<AKey<?>>>> computers = new ArrayList<>();

		AKey<String> k1 = AKey.get("k1",String.class);
		AKey<String> k2 = AKey.get("k2",String.class);
		AKey<String> k3 = AKey.get("k3",String.class);
		AKey<String> k4 = AKey.get("k4",String.class);
		AKey<String> k5 = AKey.get("k5",String.class);
		AKey<String> k6 = AKey.get("k6",String.class);
		AKey<String> k7 = AKey.get("k6",String.class);
		AKey<String> k8 = AKey.get("k7",String.class);

		Collection<AKey<?>> c1 = new ArrayList<>();
		c1.add(k1); c1.add(k4);
		Demand<Collection<AKey<?>>> fc1 = new TestComputer("fc1",Collections.emptyList(),c1);
		computers.add(fc1);

		Collection<AKey<?>> c2 = new ArrayList<>();
		c2.add(k1);
		Collection<AKey<?>> c3 = new ArrayList<>();
		c3.add(k2); c1.add(k3);
		Demand<Collection<AKey<?>>> fc2 = new TestComputer("fc2",c2,c3);
		computers.add(fc2);

		Collection<AKey<?>> c4 = new ArrayList<>();
		c4.add(k1); c4.add(k2);
		Collection<AKey<?>> c5 = new ArrayList<>();
		c5.add(k6); c5.add(k7);
		Demand<Collection<AKey<?>>> fc3 = new TestComputer("fc3",c4,c5);
		computers.add(fc3);
		fc3.setEnabled(false);

		Collection<AKey<?>> c6 = new ArrayList<>();
		c6.add(k6); c6.add(k4);
		Collection<AKey<?>> c7 = new ArrayList<>();
		c5.add(k5); c5.add(k8);
		Demand<Collection<AKey<?>>> fc4 = new TestComputer("fc4",c6,c7);
		computers.add(fc4);


		BiFunction<Demand<Collection<AKey<?>>>, Demand<Collection<AKey<?>>>, Boolean> p 
		= (f,s)-> !Collections.disjoint(f.requirements(), s.capabilities());

				ListenableGraph<Demand<Collection<AKey<?>>>,Dependency<Collection<AKey<?>>>> graph 
				= GraphOptimization.createGraphModel(computers, p, new DependencyFactory<Collection<AKey<?>>>());

		//Create the optimization
		GraphOptimization<Demand<Collection<AKey<?>>>,Dependency<Collection<AKey<?>>>> view 
		= new GraphOptimization<>(
				DemandValidation.factory(),
				DemandCost.factory(), 
				new DefaultEnumerationFactory<>(new SpanPolicy(0, 10, ordering.RANDOM)),
				graph //TODO define args
				);

		view.setNamingConvention(d->((FeaturesComputer) d).name());
		
		//solve		
		view.solve(1000,0.5);



		view.getSortedSolvers().forEach(s->{
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Type = "+((FeaturesComputer) s.getNode()).name());
			System.out.println("Cost = "+s.currentCost());
			System.out.println("validity = "+s.currentValidity());
			System.out.println("Selection : ----------");
			s.getSelection().forEach(sel->System.out.println("-----> "+((FeaturesComputer) sel.getNode()).name()));
		});

	}


}
