package org.pickcellslab.pickcells.impl.providers;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.providers.ChannelImageProvider;

import net.imglib2.Point;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;
import net.imglib2.view.composite.Composite;
import net.imglib2.view.composite.CompositeIntervalView;
import net.imglib2.view.composite.CompositeView;
import net.imglib2.view.composite.RealComposite;


public class PickCellChannelProvider<T extends RealType<T>> implements ChannelImageProvider<T>{

	private final RandomAccessibleInterval<T> source;
	private final CompositeIntervalView<T,RealComposite<T>> view;
	private Image image;
	private int numChannels;
	private final int tDim;
	private CompositeView<T, RealComposite<T>>.CompositeRandomAccess access;

	//private static final Logger log = LoggerFactory.getLogger(PickCellChannelProvider.class);
	
	
	
	/**
	 * Creates a new {@link ChannelImageProvider} with the provided Image and RandomAccessibleInterval.
	 * It is the caller responsability to make sure that img cooresponds to Image
	 * @param view
	 * @throws ImgIOException 
	 */
	PickCellChannelProvider(Image image, RandomAccessibleInterval<T> img){
			
		this.image = image;		
		this.source = img;
		
		//test dimension
		numChannels = image.channels().length;
		if(!(numChannels>1)){
			view = Views.collapseReal(Views.addDimension(img, 0, 1));
		}
		else{
			//transform to get channel dimension last
			RandomAccessibleInterval<T> temp = ImgDimensions.moveDimension(img, image.order()[Image.c], img.numDimensions()-1);			
			//Create the view 
			view = 	Views.collapseReal(temp);
		}
				
		access = view.randomAccess();
		
		
		tDim = Image.removeDimension(image.order(), Image.c)[Image.t];

	}

	

	@Override
	public CompositeIntervalView<T,RealComposite<T>> provide() {
		return view;
	}
	
	
	@Override
	public RandomAccessibleInterval<RealComposite<T>> getFrame(int t) {
		if(!image.isTimeLapse())
			return view;
		else
			return Views.hyperSlice(view, tDim, t);
	}
	

	@Override
	public Image info() {
		return image;
	}
	


	public synchronized Composite<T> get(Point p){
		access.setPosition(p);
		return access.get();
	}


	
	

	@Override
	public RandomAccessibleInterval<T> getSource() {
		return source;
	}

	



}
