package org.pickcellslab.pickcells.impl.providers;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ChannelImageProvider;
import org.pickcellslab.pickcells.api.img.providers.ImageProvider;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.composite.Composite;


/**
 * The PickCells implementation for {@link ProviderFactory}
 * @author Guillaume Blin
 *
 */
public class PickCellsProviderFactory implements ProviderFactory{


	private ExecutorService executor;
	private final Map<NodeItem, Future<?>> providers = new HashMap<>();
	private final ImgIO io;
	private final int numThreads;
	
	private final NotificationFactory notifier;
	

	public PickCellsProviderFactory(NotificationFactory notifier, ImgIO factory, int numThreads){
		this.notifier = notifier;
		this.io = factory;
		executor = Executors.newCachedThreadPool();
		this.numThreads = numThreads;
	}


	/**
	 * Adds a {@link SegmentationImageProvider} in the production line
	 * @param labels
	 */
	public <T extends NativeType<T> & RealType<T>> void addToProduction(LabelsImage labels){
		Objects.requireNonNull(labels);
		SegmentationProviderProduction<T> st = new SegmentationProviderProduction<>(labels);
		providers.put(labels, executor.submit(st));
	}
	/**
	 * Adds a {@link ChannelImageProvider} in the production line
	 * @param result
	 */
	public <T extends RealType<T>> void addToProduction(Image img){
		Objects.requireNonNull(img);
		ChannelProviderProduction<T> cpp = new ChannelProviderProduction<>(img);
		providers.put(img, executor.submit(cpp));
	}


	@Override
	public void clearProduction() {
		executor.shutdown();
		providers.clear();
		executor = Executors.newCachedThreadPool();
	}

	/**
	 * Start the production of the {@link ImageProvider} previously added in the production line
	 * @param timeout
	 * @param unit
	 * @throws InterruptedException
	 */
	public void produceAndWait(long timeout, TimeUnit unit) throws InterruptedException{		
		executor.shutdown();
		executor.awaitTermination(timeout, unit);
		executor = Executors.newFixedThreadPool(numThreads);
	}



	/**
	 * Return the {@link SegmentationImageProvider} associated with the specified
	 * {@link SegmentationResult} or {@code null} if this provider was never
	 * added to the production line or if the production was not started
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("unchecked")
	public <T extends RealType<T>> SegmentationImageProvider<T> get(LabelsImage labels) throws InterruptedException, ExecutionException{			
		return (SegmentationImageProvider<T>) providers.get(labels).get();
	}
	/**
	 * Return the {@link ChannelImageProvider} associated with the specified
	 * {@link Image} or {@code null} if this provider was never
	 * added to the production line or if the production was not started
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@SuppressWarnings("unchecked")
	public <T extends RealType<T>> ChannelImageProvider<T> get(Image img) throws InterruptedException, ExecutionException{		
		return (ChannelImageProvider<T>) providers.get(img).get();	
	}


	private interface RunnableGetter<T,D> extends Callable<ImageProvider<T>>{
	}


	class ChannelProviderProduction<T extends RealType<T>> implements RunnableGetter<Composite<T>,WritableDataItem>{

		private final Image img;


		ChannelProviderProduction(Image img){
			this.img = img;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
		public ImageProvider<Composite<T>> call() throws IOException {
			return new PickCellChannelProvider(img, io.open(img));		
		}



	}



	class SegmentationProviderProduction<T extends NativeType<T> & RealType<T>> implements RunnableGetter<T,SegmentedObject>{

		private final LabelsImage result;


		SegmentationProviderProduction(LabelsImage result){
			Objects.requireNonNull(result);
			this.result = result;
		}

		@Override
		public PickCellSegmentationProvider<T> call() throws IOException {
			return new PickCellSegmentationProvider<T>(io, result, io.open(result), notifier);
		}

	}



	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public <T extends RealType<T>> SegmentationImageProvider<T> createFrom(LabelsImage labels,
			RandomAccessibleInterval<T> img) {
		// TODO Verifications of dimensions (consistency check)
		return new PickCellSegmentationProvider(io, labels,img, notifier);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public <T extends RealType<T>> ChannelImageProvider<T> createFrom(Image image, RandomAccessibleInterval<T> img) {
		// TODO Verifications of dimensions (consistency check)
		return new PickCellChannelProvider(image,img);
	}








}
