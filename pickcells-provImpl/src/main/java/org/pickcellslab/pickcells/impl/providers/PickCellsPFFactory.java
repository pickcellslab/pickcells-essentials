package org.pickcellslab.pickcells.impl.providers;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;

@CoreImpl
public class PickCellsPFFactory implements ProviderFactoryFactory {

	private ImgIO factory;
	private final NotificationFactory notifier;

	public PickCellsPFFactory(ImgIO factory, NotificationFactory notifier){
		this.factory = factory;
		this.notifier = notifier;
	}
	
	@Override
	public ProviderFactory create(int numThreads) {
		return new PickCellsProviderFactory(notifier, factory, numThreads);
	}

}
