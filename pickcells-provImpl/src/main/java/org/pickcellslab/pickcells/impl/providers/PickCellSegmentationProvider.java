package org.pickcellslab.pickcells.impl.providers;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedPrototype;
import org.pickcellslab.pickcells.api.img.detector.MCCube;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgWriter;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.process.Outline;
import org.pickcellslab.pickcells.api.img.providers.OutOfLabelException;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.mesh.DecimationException;
import org.pickcellslab.pickcells.api.mesh.MeshDecimator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.imglib2.Cursor;
import net.imglib2.Point;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.ImgView;
import net.imglib2.img.cell.AbstractCellImg;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.type.numeric.integer.UnsignedIntType;
import net.imglib2.type.numeric.integer.UnsignedLongType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.util.Util;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

public class PickCellSegmentationProvider<T extends NativeType<T> & RealType<T>> implements SegmentationImageProvider<T>{

	private static final Logger log = LoggerFactory.getLogger(PickCellSegmentationProvider.class);


	private Img<T> view; 

	private final ImgIO io;
	private final NotificationFactory notifier;
	private final Image image;
	private final MinimalImageInfo info;
	private Map<Integer,Map<Float, SegmentedObject>> dataMap;
	private SegmentedObject prototype;
	private LabelsImage labels;
	/**
	 * Caches the slices which have been updated since the last save to file as a List of  long[]{frame, slice};
	 * 
	 */
	private final Map<Integer, Set<Integer>> dirtySlices = new HashMap<>();
	private ImgWriter<T> w;

	private static Map<AKey<?>, String> features = new HashMap<>();


	//Note here we also document keys that are automatically created by the SegmentationImageProvider (The one found in Keys)

	static{		
		features.put(Keys.location, "The coordinates of the centroid of the object (in image space)");
		features.put(Keys.centroid, "The coordinates of the centroid of the object (in real space)");
		features.put(Keys.second, "The second central moment of the object shape (in real space)");
		features.put(Keys.bbMin, "An array containing the coordinates of the minimum of the bounding box (in px)");
		features.put(Keys.bbMax, "An array containing the coordinates of the maximum of the bounding box (in px)");
		features.put(Keys.volume, "The volume of the object (in real space)");
		features.put(Keys.isborder, "A flag indicating if the object touches the border of the image or not");
	}


	PickCellSegmentationProvider(ImgIO io, LabelsImage result, RandomAccessibleInterval<T> img, NotificationFactory notifier){

		log.info("Provider loaded for "+result.fileName());
		this.io = io;
		this.notifier = notifier;
		image = result.origin();
		labels = result;
		if(image == null)
			throw new IllegalArgumentException("The provided LabelsImage is not attached to any Image");

		view = ImgView.wrap(img, Util.getArrayOrCellImgFactory(img, img.randomAccess().get().createVariable()));	

		info = image.getMinimalInfo().removeDimension(Image.c);


	}


	@Override
	public RandomAccessibleInterval<T> getFrame(int time){//TODO ReadOnly RandomAccessibleInterval
		if(info.dimension(Image.t)==1)
			return view;
		else
			return Views.hyperSlice(view, info.index(Image.t), time);
	}

	@Override
	public RandomAccessibleInterval<T> getSlice(int time, int slice){//TODO ReadOnly RandomAccessibleInterval
		if(info.dimension(Image.z)==1) 
			return getFrame(time);
		else{

			return Views.hyperSlice(getFrame(time), info.removeDimension(Image.t).index(Image.z), slice);
		}
	}



	@Override
	public Map<AKey<?>, String> computedFeatures(){
		return features;
	}


	@Override
	public <S extends SegmentedObject> void setKnownAssociatedData(Map<Integer, List<S>> items) {

		assert items != null : "items is null";

		//Reorder the list into a map
		dataMap = new ConcurrentHashMap<>(items.size());

		items.entrySet().parallelStream().forEach((entry)->{
			final List<? extends SegmentedObject> list = entry.getValue();
			final Map<Float,SegmentedObject> frameMap = new ConcurrentHashMap<Float, SegmentedObject>(list.size());
			list.parallelStream().forEach(i-> {
				final Float l = i.label().get();
				frameMap.put(l,i);
			});
			dataMap.put(entry.getKey(), frameMap);
		});

		if(!dataMap.isEmpty())
			prototype = dataMap.get(0).values().iterator().next();
	}




	@Override
	public <S extends SegmentedObject> void setKnownAssociatedData(int frame, List<S> list) {
		if(dataMap==null)
			dataMap = new ConcurrentHashMap<>();

		final Map<Float,SegmentedObject> frameMap = new ConcurrentHashMap<Float, SegmentedObject>(list.size());
		list.parallelStream().forEach(i-> {
			final Float l = i.label().get();
			frameMap.put(l,i);
		});
		dataMap.put(frame, frameMap);
	}






	@Override
	public Img<T> provide() {//TODO wrap into a ReadOnly Img 
		return view;
	}





	@Override
	public long numFrames() {
		return info.dimension(Image.t);
	}


	@Override
	public Optional<SegmentedObject> getAnyItem() {
		if(dataMap == null){
			createDataMap(0);
			if(dataMap.get(0).isEmpty())
				return Optional.empty();
			else
				return Optional.of(dataMap.get(0).values().iterator().next());
		}
		else{
			return Optional.of(dataMap.values().iterator().next().values().iterator().next());
		}
	}






	@Override
	public float newLabel(int time) throws OutOfLabelException{
		if(dataMap == null)
			createDataMap(time);
		Set<Float> labels = dataMap.get(time).keySet();
		double max = variable(0).getMaxValue();
		for(int i = 1; i<max; i++)
			if(!labels.contains((float)i))
				return i;
		throw new OutOfLabelException("All possible colors have been used in this image");
	}


	@Override
	public Optional<SegmentedObject> getItem(float l, int t) {
		createDataMap(t);
		return Optional.ofNullable(dataMap.get(t).get(l));
	}


	@Override
	public Optional<SegmentedObject> getData(Point p) {		
		RandomAccess<T> access = view.randomAccess();
		access.setPosition(p);
		if(info.dimension(Image.t)>1)
			return getItem(access.get().getRealFloat(),p.getIntPosition(info.index(Image.t)));
		else{
			return getItem(access.get().getRealFloat(),0);
		}
	}


	@Override
	public Optional<SegmentedObject> getData(long[] p, int time){
		RandomAccess<T> access = getFrame(time).randomAccess();
		access.setPosition(p);
		return getItem(access.get().getRealFloat(),time);
	}


	@Override
	public Stream<Float> labels(int time) {
		createDataMap(time);	
		//Return parallel only if not CellImg
		if(this.view instanceof AbstractCellImg){
			System.out.println("Returning sequential stream as backend maybe dynamic caching");
			return dataMap.get(time).keySet().stream();
		}
		else
			return dataMap.get(time).keySet().parallelStream();
	}





	@Override
	public Stream<Float> labels(long[] min, long[] max, int time) {		
		Collection<Float> labels = new HashSet<>();
		Cursor<T> c = Views.interval(Views.extendZero(getFrame(time)), min, max).cursor();
		while(c.hasNext()){
			T t = c.next();
			if(t.getRealFloat()!=0)
				labels.add(t.getRealFloat());	
		}
		return labels.stream();
	}


	@Override
	public Stream<Float> labels(int time, int slice) {
		createDataMap(time);
		return dataMap.get(time).values().stream()
				.filter(sp->sp.getAttribute(Keys.bbMax).get()[info.index(Image.z)]>=slice && sp.getAttribute(Keys.bbMin).get()[info.index(Image.z)]<=slice)
				.map(sp->sp.label().get());
		/*
		Collection<Float> labels = new HashSet<>();
		Cursor<T> c = Views.iterable(getSlice(time, slice)).cursor();
		while(c.hasNext()){
			T t = c.next();
			if(t.getRealFloat()!=0)
				labels.add(t.getRealFloat());	
		}
		return labels;
		 */
	}


	@Override
	public Stream<Float> labels(long[] min, long[] max, int time, int slice) {
		Collection<Float> labels = new HashSet<>();
		Cursor<T> c = Views.interval(Views.extendZero(getSlice(time, slice)), min, max).cursor();
		while(c.hasNext()){
			T t = c.next();
			if(t.getRealFloat()!=0)
				labels.add(t.getRealFloat());	
		}
		return labels.stream();
	}





	@Override
	public IntervalView<T> interval(float label, int time){

		createDataMap(time);			
		SegmentedObject seg = dataMap.get(time).get(label);
		if(seg != null){
			//Check if the segmented item has useful features already
			long[] min = seg.getAttribute(Keys.bbMin).orElse(null);
			if(min == null)
				throw new IllegalStateException("Implementation issue: no bounding box for a known SegmentedObject");

			long[] max = seg.getAttribute(Keys.bbMax).get();
			return Views.interval(getFrame(time), min, max);
		}
		else 
			throw new IllegalArgumentException("The provided label does not exist");

	}






	@Override
	public void processPoints(float label, int time, Consumer<long[]> consumer) {
		createDataMap(time);		
		SegmentedObject seg = dataMap.get(time).get(label);
		if(seg != null){
			processIntervalLocations(seg, consumer);
			return;
		}else
			return; // label not found in the image			

	}



	@Override
	public void processOutline(float label, int time, Consumer<long[]> consumer) {		

		createDataMap(time);			
		SegmentedObject seg = dataMap.get(time).get(label);
		if(seg != null){
			//Check if the segmented item has useful features already
			long[] min = seg.getAttribute(Keys.bbMin).orElse(null);
			if(min == null)
				throw new IllegalStateException("The provided Segmented object does not contain the required information");

			//Create the interval
			long[] max = seg.getAttribute(Keys.bbMax).get(); 
			this.processOutline(min, max, label, time, consumer);		
			return;
		}				
		else 
			return ;// label not found in the image

	}





	@Override
	public T variable(float l){
		T v = view.firstElement().createVariable();
		v.setReal(l);
		return v;
	}




	private void processIntervalLocations(SegmentedObject seg, Consumer<long[]> consumer){

		//log.info("Providing list of points for "+seg.toString());


		//Check if the segmented item has useful features already
		long[] min = seg.getAttribute(Keys.bbMin).orElse(null);
		if(min == null)
			throw new IllegalStateException("The provided Segmented object does not contain the required information");

		//Create the interval

		long[] max = seg.getAttribute(Keys.bbMax).get();

		processIntervalLocations(min,max,seg.label().get(), seg.frame(), consumer);

	}



	private void processIntervalLocations(long[] min, long[] max, float label, int time, Consumer<long[]> consumer){


		Cursor<T> c = Views.interval(getFrame(time), min, max).localizingCursor();

		//Get the label of the SegmentedObject
		T v = view.firstElement().createVariable();
		v.setReal(label);

		try{
			long[] novel = new long[max.length];
			while(c.hasNext()){
				T t = c.next();
				if(t.compareTo(v)==0){					
					c.localize(novel);
					consumer.accept(novel);
				}

			}

		}
		catch(ArrayIndexOutOfBoundsException e){
			log.error("There has been a problem while iterating other the image",e);
		}
	}




	private void processIntervalValues(long[] min, long[] max, float label, int time, Consumer<T> consumer){


		Cursor<T> c = Views.interval(getFrame(time), min, max).cursor();

		//Get the label of the SegmentedObject
		T v = view.firstElement().createVariable();
		v.setReal(label);

		try{
			while(c.hasNext()){
				T t = c.next();
				if(t.compareTo(v)==0)
					consumer.accept(t);
			}

		}
		catch(ArrayIndexOutOfBoundsException e){
			log.error("There has been a problem while iterating other the image",e);
		}
	}








	private synchronized void createDataMap(int t) {

		if(dataMap==null)
			dataMap = new ConcurrentHashMap<>();


		if(dataMap.get(t)!=null)
			return;

		log.info("Creating the map of objects in the segmented image: "+origin().fileName());

		if(prototype == null)
			throw new IllegalStateException("Cannot create a data map the prototype is null");
		//check that our segmeted object is a prototype
		if(!SegmentedPrototype.class.isAssignableFrom(prototype.getClass()))
			throw new IllegalStateException("The segmented object associated to this provider is not a SegmentedPrototype."
					+ "Only SegmentedPrototype objects can be created on the fly for each label found in the LabelsImage");

		SegmentedPrototype p = (SegmentedPrototype) prototype;		

		if(image.isTimeLapse()){				
			Map<Float,SegmentedObject> frameMap = new ConcurrentHashMap<>();
			dataMap.put(t, frameMap);
			createDataMapOnFrame(getFrame(t), p, frameMap, t);
			final int frame = t;
			frameMap.values().forEach(i->i.setAttribute(SegmentedObject.frameKey, frame));
			log.info("Map of objects created for time point "+t+" with "+frameMap.size()+" objects");
		}
		else{
			Map<Float,SegmentedObject> frameMap = new ConcurrentHashMap<>();
			dataMap.put(0, frameMap);
			createDataMapOnFrame(view, p, frameMap, 0);
			log.info("Map of objects created with "+frameMap.size()+" objects");
		}		
	}



	private void createDataMapOnFrame(RandomAccessibleInterval<T> view, SegmentedPrototype p, Map<Float,SegmentedObject> frameMap, int frame){

		HashMap<Float,SummaryStatistics[]> statsMap = new HashMap<>();
		//final SummaryStatistics volumeStats = new SummaryStatistics();
		final Cursor<T> c = Views.iterable(view).localizingCursor();
		while(c.hasNext()){
			c.fwd();
			float l = c.get().getRealFloat();
			if(l != 0f){
				SummaryStatistics[] stats = statsMap.get(l);
				if(stats == null){// if new label
					//Creat stats
					stats = new SummaryStatistics[c.numDimensions()];
					for(int i = 0; i<c.numDimensions(); i++)
						stats[i] = new SummaryStatistics();

					statsMap.put(l, stats);
					//Create the segmented object which will store the stats
					SegmentedPrototype newP = p.create(l);
					new DataLink(labels.linkType(), newP, labels, true);
					frameMap.put(l, newP);	
				}
				//Add value to stats
				for(int i = 0; i<c.numDimensions(); i++)				
					stats[i].addValue(c.getDoublePosition(i));				
			}
		}

		//Store stats to the dataMap objects
		final double v = image.voxelVolume();
		statsMap.entrySet().stream().parallel().forEach(e->{

			SegmentedObject item = frameMap.get(e.getKey());
			double[] center = new double[c.numDimensions()];
			double[] moments = new double[c.numDimensions()];
			long[] loc = new long[c.numDimensions()];
			long[] min = new long[c.numDimensions()];
			long[] max = new long[c.numDimensions()];

			for(int i = 0; i<c.numDimensions(); i++){
				center[i] = e.getValue()[i].getMean();
				moments[i] = e.getValue()[i].getSecondMoment();
				loc[i] = (long) e.getValue()[i].getMean();
				min[i] = (long) e.getValue()[i].getMin();
				max[i] = (long) e.getValue()[i].getMax();
			}

			//perform calibration where necessary
			item.setAttribute(Keys.volume, ((double)e.getValue()[0].getN())*v);
			item.setAttribute(Keys.location, loc);
			item.setAttribute(Keys.centroid, image.calibrated(center, false, false));
			item.setAttribute(Keys.second, image.calibrated(moments, false, false));
			item.setAttribute(Keys.bbMin, min);
			item.setAttribute(Keys.bbMax, max);
			if(frame!=0)
				item.setAttribute(ImageLocated.frameKey, frame);


			//Check if the item touches the border of the image
			boolean isBorder = false;
			for(int i = 0; i<c.numDimensions(); i++){
				if(min[i] == 0 || max[i] == view.dimension(i)-1){
					isBorder = true;
					break;
				}
			}
			item.setAttribute(Keys.isborder, isBorder);
			//if(isBorder)
			//	volumeStats.addValue(item.getAttribute(Keys.volume).get());

		});

		statsMap = null;		

		/*		
		// Now for border items, check their volume and set as not border if volume > Mean
		final double s = volumeStats.getMean() + volumeStats.getStandardDeviation()/2.0;
		frameMap.values().stream().filter(i->i.getAttribute(Keys.isborder).get()).forEach(i->{
			if(i.getAttribute(Keys.volume).get()>s)
				i.setAttribute(Keys.isborder, false);
		});
		 */
	}




	@Override
	public void setPrototype(LabelsImage labels, SegmentedObject prototype, boolean computeFeatures) {
		this.labels = labels;
		this.prototype = prototype;		
		if(computeFeatures){
			for(int i = 0; i<numFrames(); i++)
				this.createDataMap(i);
		}
	}


	@Override
	public LabelsImage origin() {
		return labels;
	}


	@Override
	public List<float[]> getMesh(SegmentedObject item, int resolution, boolean simplify, boolean calibrated) {

		//get the bounding boxes
		long[] minL = item.getAttribute(Keys.bbMin).get();
		long[] maxL = item.getAttribute(Keys.bbMax).get();

		return getMesh(minL,maxL,item.label().get(), item.frame(), resolution,simplify,calibrated);
	}


	@Override
	public List<float[]> getMesh(long[] bbMin, long[] bbMax, float label, int time, int resolution, boolean simplify, boolean calibrated) {


		//TODO time

		//log.debug("Getting mesh or outline for "+label);

		if(image.depth()==1){//2D
			List<float[]> list = new ArrayList<>();
			this.processIntervalLocations(bbMin, bbMax, label, time, f -> list.add(image.floatCalibrated(f, false,false)));
			return list; 
		}

		// TODO this.getOutline(label).get().stream().map(l-> image.floatCalibrated(l, false, false)).collect(Collectors.toList());


		//Obtain the mesh from the Marching cubes method
		List<float[]> points = new MCCube().getTriangles(getFrame(time), bbMin, bbMax, label, resolution);

		//SummaryStatistics stats = new SummaryStatistics();
		//points.stream().forEach(p->stats.addValue(p[0]));
		//System.out.println(stats.toString());

		if(calibrated)
			points = points.stream().map(l-> image.floatCalibrated(l, false, false)).collect(Collectors.toList());

		if(simplify){
			try{
				points = MeshDecimator.decimateFloats(points, 1.2f);
				//log.debug("Simplification success");

			}catch (DecimationException e){
				log.debug("Simplification failed");
				log.warn("Could not simplify the mesh of an object -> mesh size = "+points.size());
			}
		}

		return points;



	}


	@Override
	public void processOutline(long[] min, long[] max, float label, int time, Consumer<long[]> consumer) {
		T value = view.firstElement().createVariable();
		value.setReal(label);
		//long b = System.currentTimeMillis();
		Outline.run(Views.interval(getFrame(time), min, max), value, consumer);
		//System.out.println("Outline run = "+(System.currentTimeMillis()-b)+"ms (min = "+Arrays.toString(min)+" ; max = "+Arrays.toString(max));
	}



	@Override
	public SegmentedObject merge(List<SegmentedObject> toMerge) {

		Objects.requireNonNull(toMerge);


		//Make sure there are at least 2 objects to merge
		if(toMerge.isEmpty())
			return null;
		if(toMerge.size() == 1)
			return toMerge.get(0);


		Iterator<SegmentedObject> it = toMerge.iterator();

		// Compute the new bounding box
		SegmentedObject first = it.next();
		createDataMap(first.frame());
		T lbl = variable(first.label().get());
		long[] bbMin = first.getAttribute(Keys.bbMin).get();
		long[] bbMax = first.getAttribute(Keys.bbMax).get();
		while(it.hasNext()){
			SegmentedObject next = it.next();
			createDataMap(next.frame());
			long[] bbMin2 = next.getAttribute(Keys.bbMin).get();
			long[] bbMax2 = next.getAttribute(Keys.bbMax).get();
			for(int i = 0; i<bbMin.length; i++){
				bbMin[i] = Math.min(bbMin[i], bbMin2[i]);
				bbMax[i] = Math.max(bbMax[i], bbMax2[i]);
			}
			// Replace labels
			this.processIntervalValues( bbMin2, bbMax2, next.label().get(), next.frame(), t->t.set(lbl));
		}



		SegmentedObject merged = ((SegmentedPrototype) prototype).create(first.label().get());

		new DataLink(labels.linkType(), merged, labels, true);
		this.createBasicAttributes(merged, bbMin, bbMax, first.label().get(), first.frame());

		// Now update the dataMap and delete the links with the LabelsImage
		for(SegmentedObject so : toMerge){
			dataMap.get(merged.frame()).remove(so.label().get());
			so.getLinks(Direction.OUTGOING, labels.linkType()).forEach(l->l.delete());
		}
		dataMap.get(merged.frame()).put(first.label().get(), merged);

		//update dirtySlices
		updateDirtySlices(merged);


		return merged;
	}




	@Override
	public Pair<SegmentedObject, SegmentedObject> splitZ(SegmentedObject toSplit, long slice) throws OutOfLabelException{

		long[] bbMin1 = toSplit.getAttribute(Keys.bbMin).get();
		long[] bbMax2 = toSplit.getAttribute(Keys.bbMax).get();

		long[] bbMax1 = Arrays.copyOf(bbMax2, bbMax2.length);
		bbMax1[2] = slice;
		long[] bbMin2 = Arrays.copyOf(bbMin1, bbMin1.length);
		bbMin2[2] = slice+1;

		dataMap.remove(toSplit.label().get());
		toSplit.getLinks(Direction.OUTGOING, labels.linkType()).forEach(l->l.delete());

		SegmentedObject novel1 = ((SegmentedPrototype) prototype).create(toSplit.label().get());
		new DataLink(this.labels.linkType(), novel1, this.labels, true);
		this.createBasicAttributes(novel1, bbMin1, bbMax1, novel1.label().get(), toSplit.frame());
		dataMap.get(novel1.frame()).put(novel1.label().get(), novel1);
		//System.out.println("******  Novel object created!  ******");
		//for(AKey<?> k : novel1.getValidAttributeKeys())
		//	System.out.println(k + " : "+k.asString(novel1));
		//System.out.println("*************************************");


		// Now relabel on the second part of the split

		T l1 = variable(novel1.label().get());
		float label2 = this.newLabel(novel1.frame());
		T l2 = variable(label2);

		Cursor<T> c = Views.interval(getFrame(toSplit.frame()), bbMin2, bbMax2).cursor();
		while(c.hasNext()){
			T t = c.next();
			if(t.equals(l1))
				t.set(l2);
		}

		SegmentedObject novel2 = ((SegmentedPrototype) prototype).create(label2);
		new DataLink(this.labels.linkType(), novel2, this.labels, true);
		this.createBasicAttributes(novel2, bbMin2, bbMax2, novel2.label().get(), toSplit.frame());
		dataMap.get(novel2.frame()).put(label2, novel2);
		//System.out.println("******  Novel object created!  ******");
		//for(AKey<?> k : novel2.getValidAttributeKeys())
		//	System.out.println(k + " : "+k.asString(novel2));
		//System.out.println("*************************************");

		updateDirtySlices(novel2);

		return new Pair<>(novel1, novel2);
	}



	@Override
	public void delete(SegmentedObject toDelete) {
		final RandomAccess<T> access = this.interval(toDelete.label().get(), toDelete.frame()).randomAccess();
		this.processPoints(
				toDelete.label().get(), 
				toDelete.frame(),
				l->{
					access.setPosition(l);
					access.get().setZero();
				}
				);
		updateDirtySlices(toDelete);
	}




	@Override
	public List<SegmentedObject> write(float label, Iterator<long[]> it, int time) {

		// The list of SegmentedObject that are writen over
		final List<SegmentedObject> amended = new ArrayList<>();

		// The set of labels aencounteed during the write operation which will allow us to determine the above list
		final Set<Float> labels = new HashSet<>();

		RandomAccessibleInterval<T> frame = getFrame(time);


		final long[] min = new long[frame.numDimensions()];
		Arrays.fill(min, Long.MAX_VALUE);
		final long[] max = new long[frame.numDimensions()];
		Arrays.fill(max, Long.MIN_VALUE);
		final RandomAccess<T> access = frame.randomAccess();
		final T w = access.get().createVariable();
		w.setReal(label);

		while(it.hasNext()){
			long[] pos = it.next();
			//update bounds
			for(int i = 0; i<min.length; i++){
				min[i] = Math.min(min[i], pos[i]);
				max[i] = Math.max(max[i], pos[i]);
			}
			access.setPosition(pos);
			T t = access.get();
			float f = t.getRealFloat();
			if(f != 0 && f != label)
				labels.add(f); // register the encountered color if not 0
			t.set(w); // write over
		}



		//Now update our cache

		// if the map was not computed, simply create the map and return the SegmentedObject whose labels were overwriten
		if(dataMap == null){			
			createDataMap(time);
			for(Float f : labels)
				amended.add(getItem(f, time).get());
			//System.out.println("DataMap created after write!");
			return amended;
		}


		// Otherwise, it means we need to update the values for objects that are already known
		if (label != 0 ){

			// If this label is already known, simply add to labels to be updated
			if(getItem(label, time).isPresent()){
				labels.add(label);
				//System.out.println("Label was already known");
			}

			// If not known, then create a new object
			else{				
				SegmentedObject novel = ((SegmentedPrototype) prototype).create(label);
				new DataLink(this.labels.linkType(), novel, this.labels, true);
				this.createBasicAttributes(novel, min, max, label, time);
				amended.add(novel);
				dataMap.get(time).put(label, novel);
				System.out.println("******  Novel object created!  ******");
			}

		}	


		// Now recompute features for all encountered labels (and add to amended)
		for(Float f : labels){
			//System.out.println("Encountered value = "+f);
			SegmentedObject so = getItem(f, time).get();
			update(so, min, max, time);
			amended.add(so);			
		}

		amended.forEach(a->updateDirtySlices(a));

		return amended;

	}




	@Override
	public void convertToNextBitDepth() throws IOException {
		// Identify the next bit depth to convert into
		final int bitDepth = this.view.firstElement().getBitsPerPixel();
		final int nextBitDepth = bitDepth * 2;
		if(nextBitDepth <= 64) {
			// Convert and obtain the image
			final Img<T> converted = (Img)convertToType((NativeType & RealType)getTypeForBitDepth(nextBitDepth));

			// Update Labels Image
			labels.setAttribute(AKey.get("bit depth", Integer. class), io.pxCode(converted.firstElement()));

			// Backup old file
			final String dest = labels.path();

			final File f = new File(dest);
			f.renameTo(new File(dest+".bk"));

			try {
				// Update on disk (new file)
				io.save(labels, converted, dest);

			} catch (IOException e) {
				notifier.display("IOException", "Unable to save image", e, Level.SEVERE);
				//roll back file changes
				f.delete();
				new File(dest+".bk").renameTo(new File(dest));
			}


			//FIXME This will render all previous frames obtained from this provider obsolete
			view = converted;


		}
		else
			throw new RuntimeException("It is not possible to increase the bit depth of "+labels.toString());
	}





	@Override
	public void convertToLowestPossibleBitDepth() throws IOException {
		throw new RuntimeException("convertToLowestPossibleBitDepth not implemented");

	}



	private <O extends RealType<O> & NativeType<O>> Img<O> convertToType(O type){
		return ImgDimensions.hardCopy(io, view, (i,o) -> o.setReal(i.getRealDouble()),  type);
	}


	@SuppressWarnings("unchecked")
	private <O extends RealType<O> & NativeType<O>> O getTypeForBitDepth(int bitDepth){
		switch(bitDepth){
		case 1 : return (O) new BitType();
		case 8 : return (O) new UnsignedByteType();
		case 16: return (O) new UnsignedShortType();
		case 32: return (O) new UnsignedIntType();
		case 64: return (O) new UnsignedLongType();
		default: return null;
		}
	} 





	@Override
	public void updateOnDisk() throws IOException {

		if(w==null)
			w = io.createWriter(labels.path());

		for(int d : dirtySlices.keySet()){
			RandomAccessibleInterval<T> plane = getFrame(d);
			System.out.println("Plane Dims = "+plane.numDimensions());
			for(int s : dirtySlices.get(d))
				if(plane.numDimensions()==3){
					System.out.println("Frame = "+d+" ; Plane = "+s);
					w.writePlane(Views.hyperSlice(plane, 2, s), s, 0, d);
				}
				else
					w.writePlane(plane, s, 0, d);

		}
		dirtySlices.clear();

	}




	private void updateDirtySlices(SegmentedObject merged) {

		Set<Integer> set = dirtySlices.get(merged.frame());
		if(set == null){
			if(this.info.index(Image.z) == -1) { // If 2D
				set = Collections.singleton(0);
			}
			else {
				set = new HashSet<>();
			}
			dirtySlices.put(merged.frame(), set);
		}

		// If 2D return now
		if(this.info.index(Image.z) == -1)	return;
		
		// Otherwise find out which slices need to be updated
		long min = merged.getAttribute(Keys.bbMin).get()[2];
		long max = merged.getAttribute(Keys.bbMax).get()[2];
		for(long s = min; s<=max; s++){
			set.add((int) s);
			//System.out.println("DirtySlices: "+s);
		}
		//System.out.println("DirtySlices updated");
	}








	/**
	 * Recomputes basic attributes for the given SegmentedObject.
	 * @param so The SegmentedObject to update
	 * @param min The minimum of the bounding box of the update in the image
	 * @param max The maximum of the bounding box of the update in the image
	 */
	private void update(SegmentedObject so, long[] min, long[] max, int time) {

		long[] bbMin = so.getAttribute(Keys.bbMin).get();
		long[] bbMax = so.getAttribute(Keys.bbMax).get();
		for(int i = 0; i< min.length; i++){
			bbMin[i] = Math.min(min[i], bbMin[i]);
			bbMax[i] = Math.max(max[i], bbMax[i]);
		}

		//Now find the real bounding box of the shape
		final Cursor<T> c = Views.interval(getFrame(time), bbMin, bbMax).localizingCursor();
		Arrays.fill(bbMin, Long.MAX_VALUE);
		Arrays.fill(bbMax, Long.MIN_VALUE);
		final T l = variable(so.label().get());
		final long[] pos = new long[bbMin.length];
		while(c.hasNext()){
			T t = c.next();
			c.localize(pos);
			if(t.equals(l)){
				for(int i = 0; i<min.length; i++){
					bbMin[i] = Math.min(bbMin[i], pos[i]);
					bbMax[i] = Math.max(bbMax[i], pos[i]);
				}
			}
		}

		this.createBasicAttributes(so, bbMin, bbMax, so.label().get(), time);

	}








	private void createBasicAttributes(SegmentedObject so, long[] bbMin, long[] bbMax, float label, int time){

		if(info.dimension(Image.t)>1)
			so.setAttribute(SegmentedObject.frameKey, time);

		final double v = image.voxelVolume();		
		final Cursor<T> c = Views.interval(getFrame(time), bbMin, bbMax).cursor();
		final SummaryStatistics[] stats = new SummaryStatistics[c.numDimensions()];
		for(int i = 0; i<c.numDimensions(); i++)
			stats[i] = new SummaryStatistics();

		while(c.hasNext()){
			c.fwd();
			//Add value to stats
			for(int i = 0; i<c.numDimensions(); i++)				
				stats[i].addValue(c.getDoublePosition(i));	
		}

		double[] center = new double[c.numDimensions()];
		double[] moments = new double[c.numDimensions()];
		long[] loc = new long[c.numDimensions()];

		for(int i = 0; i<c.numDimensions(); i++){
			center[i] = stats[i].getMean();
			moments[i] = stats[i].getSecondMoment();
			loc[i] = (long) stats[i].getMean();
		}

		//perform calibration where necessary
		so.setAttribute(Keys.bbMin, bbMin);
		so.setAttribute(Keys.bbMax, bbMax);
		so.setAttribute(Keys.volume, ((double)stats[0].getN())*v);
		so.setAttribute(Keys.location, loc);
		so.setAttribute(Keys.centroid, image.calibrated(center, false,false));
		so.setAttribute(Keys.second, image.calibrated(moments, false,false));

	}





}
