package org.pickcellslab.pickcells.impl.notifications;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.ProgressFactory;
import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.TaskProgress;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.impl.theme.UIThemeImpl;

import com.alee.laf.WebLookAndFeel;
import com.alee.managers.WebLafManagers;

public class ProgressTest {

	
	public static void main(String[] args) {
		
		// Init Weblaf		
		WebLookAndFeel.install();
		WebLafManagers.initialize ();
		
		
		final UITheme theme = new UIThemeImpl();
		final NotificationFactory fctry = new NotificationFactoryImpl();
		
		new ProgressTest().launchTest(theme, fctry);
		
		
	}
	
	
	private <P extends TaskProgress<Void>> void launchTest(UITheme theme,NotificationFactory fctry) {
		
		final ProgressFactory<P,Void> pFctry = fctry.newProgressFactory();
		
		final List<P> tasks = new ArrayList<>();
		tasks.add(pFctry.newTaskProgress("name 1", newCallable()));
		tasks.add(pFctry.newTaskProgress("name 2", newCallableWithException()));
		tasks.add(pFctry.newTaskProgress("name 3", newCallable()));
		tasks.add(pFctry.newTaskProgress("name 4", newCallable()));
		tasks.add(pFctry.newTaskProgress("name 5", newCallable()));
		tasks.add(pFctry.newTaskProgress("name 6", newCallable()));
		tasks.add(pFctry.newTaskProgress("name 7", newCallable()));
		tasks.add(pFctry.newTaskProgress("name 8", newCallable()));
		tasks.add(pFctry.newTaskProgress("name 9", newCallable()));
		
		final ProgressPanel<Void> ui = pFctry.progressPanel(theme, tasks);
		ui.setTitle("Test in Progress");
		ui.setVisible(true);
		//ui.getControl().start();
	}
	
	
	
	private Callable<Void> newCallable(){
		return () -> {
			System.out.println("Callable runing!");
			Thread.sleep(2000);
			System.out.println("done!");
			return null;
		};
	}
	
	private Callable<Void> newCallableWithException(){
		return () -> {
			System.out.println("CallableWithException runing!");
			Thread.sleep(2000);
			throw new RuntimeException("Testing Exception...");
		};
	}
	
}
