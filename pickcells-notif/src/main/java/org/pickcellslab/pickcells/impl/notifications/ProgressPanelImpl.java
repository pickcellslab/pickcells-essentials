package org.pickcellslab.pickcells.impl.notifications;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalTime;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.pickcellslab.foundationj.services.ProgressControl;
import org.pickcellslab.foundationj.services.ProgressControlListener;
import org.pickcellslab.foundationj.services.ProgressExceptionHandler;
import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.ProgressStatusListener;
import org.pickcellslab.foundationj.services.TaskProgress;
import org.pickcellslab.foundationj.services.TaskProgress.ProgressStatus;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;

import com.alee.extended.layout.ToolbarLayout;
import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.progress.WebProgressOverlay;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;

public class ProgressPanelImpl<V> extends JFrame implements ProgressPanel<V> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -43031146903858055L;

	private final UITheme theme;
	private final ProgressControlImpl<V> control;



	public ProgressPanelImpl(UITheme theme, ProgressControlImpl<V> control) {

		// Fields
		this.theme = theme;
		this.control = control;



		// Layout UI
		JComponent startAbort = new StartAbortUI(control).getUI();
		startAbort.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		// Create an array of all TaskProgress objects
		final WebPanel list = new WebPanel();
		list.setLayout(new VerticalFlowLayout());

		for(int t = 0; t<control.numTasks(); t++)
			list.add(new TaskProgressUI(control.getProgress(t)).getUI());

		final JScrollPane scroll = new JScrollPane(list);

		final JPanel content = new JPanel();
		content.setLayout(new BorderLayout());
		content.add(startAbort, BorderLayout.NORTH);
		content.add(scroll, BorderLayout.CENTER);

		this.setContentPane(content);


		// Pack and position to center of screen by default
		//this.pack();
		this.setSize(200, 250);
		this.setLocationRelativeTo(null);

	}








	@Override
	public ProgressControl<V> getControl() {
		return control;
	}









	// ====================== UI ======================= //




	private class TaskProgressUI implements ProgressStatusListener {

		private final WebLabel label = new WebLabel();
		private final WebProgressOverlay progressOverlay = new WebProgressOverlay ();



		public TaskProgressUI(TaskProgress<V> task) {

			// Register this UI as a listener
			task.addProgressStatusListener(this);

			// Setup a JLabel to display the name of the task			
			label.setText(task.getName());


			// Create a log for the task
			final TaskInfo log = new TaskInfo(task);

			// Create a JButton to allow the user to display the log for the task
			final WebButton info = new WebButton(theme.icon(IconID.Misc.QUESTION, 10));
			//info.setRound(5);
			info.addActionListener(l->log.show());

			// Group the components in a WebPanel
			final WebPanel group = new WebPanel(new ToolbarLayout());
			group.add(label, ToolbarLayout.START);
			group.add(info, ToolbarLayout.END);
			group.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

			progressOverlay.setConsumeEvents ( false );
			progressOverlay.setComponent ( group );

			updateUI(task);
		}


		private void updateUI(TaskProgress<?> task) {

			// Figure out Icon			
			switch(task.getStatus()) {
			case FAILED: label.setIcon(theme.icon(IconID.Levels.ERROR, 16, Color.RED));
			break;
			case RUNNING: label.setIcon(theme.icon(IconID.Misc.IN_PROGRESS, 16, Color.BLUE));
			break;
			case SUCCESS: label.setIcon(theme.icon(IconID.Misc.VALID, 16, Color.GREEN));
			break;
			case WAITING: label.setIcon(theme.icon(IconID.Misc.CIRCLE, 16, Color.BLACK));
			break;		
			}

			// Update overlay
			progressOverlay.setShowLoad(task.getStatus() == ProgressStatus.RUNNING);

			System.out.println(task.getName() + " is running : " + progressOverlay.isShowLoad());

		}


		public JComponent getUI() {
			return progressOverlay;
		}


		@Override
		public void statusChanged(TaskProgress<?> source) {
			updateUI(source);			
		}



	}


	private class TaskInfo extends JTextPane implements ProgressStatusListener, ProgressExceptionHandler<V> {

		private long startTime;
		private final String title;
		private final TaskProgress<V> source;

		public TaskInfo(TaskProgress<V> source) {
			this.source = source;
			source.addProgressStatusListener(this);
			control.addProgressExceptionHandler(this);
			title = "Log for "+source.getName();			
			this.setEditable(false);
			this.setContentType("text/html");
		}


		@Override
		public void handle(ProgressControl<V> source, TaskProgress<V> task, CancellationException e) {
			if(task == this.source)
				this.append("<br><font color=\"red\">Cancelled by user</font>");			
		}

		@Override
		public void handle(ProgressControl<V> source, TaskProgress<V> task, InterruptedException e) {
			if(task == this.source)
				append("<br><font color=\"red\">Interrupted by user</font>");	
		}

		@Override
		public void handle(ProgressControl<V> source, TaskProgress<V> task, ExecutionException e) {
			if(task == this.source) {
				append("<br><font color=\"red\">An error occured while computing the task</font>");
				final StringWriter stackTraceWriter = new StringWriter();
				e.getCause().printStackTrace(new PrintWriter(stackTraceWriter));
				this.append(e.getCause().toString() + "<br>" + stackTraceWriter.toString());
			}
		}

		@Override
		public void statusChanged(TaskProgress<?> source) {

			final LocalTime time = LocalTime.now();
			if(source.getStatus() == ProgressStatus.RUNNING) {
				append("<br>Started at "+ time.getHour()+" : "+time.getMinute()+" : "+time.getSecond());
				startTime = System.currentTimeMillis();
			}else if(source.getStatus() == ProgressStatus.FAILED) {
				append("Ended at "+ time.getHour()+" : "+time.getMinute()+" : "+time.getSecond());
			}else if(source.getStatus() == ProgressStatus.SUCCESS) {
				append("<br>Ended at "+ time.getHour()+" : "+time.getMinute()+" : "+time.getSecond());
				append("<br>Running time: " + (System.currentTimeMillis()-startTime)/1000 + " sec ");
			}

		}


		public void show() {
			final JScrollPane scrollPane = new JScrollPane(this);
			scrollPane.setPreferredSize(new Dimension(380, 100));
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

			final JFrame frame = new JFrame(title);
			frame.setContentPane(scrollPane);
			//frame.setLocationRelativeTo(ProgressPanelImpl.this);
			frame.setLocation(ProgressPanelImpl.this.getLocation().x + ProgressPanelImpl.this.getWidth(), ProgressPanelImpl.this.getLocation().y);
			
			frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			frame.setSize(300, 250);
			//frame.pack();
			frame.setVisible(true);		 
		}



		private void append(String text) {
			
			HTMLDocument doc = (HTMLDocument) getDocument();
			HTMLEditorKit kit = (HTMLEditorKit) getEditorKit();
			try {

				kit.insertHTML(doc, doc.getLength(), text, 0, 0, null);
				this.repaint();

			} catch (BadLocationException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}







	private class StartAbortUI implements ProgressControlListener{

		final WebProgressOverlay progressOverlay = new WebProgressOverlay ();
		final WebButton button = new WebButton ();



		public StartAbortUI(ProgressControl<?> control) {

			control.addProgressListener(this);
			buildButton();

		}



		public void buildButton() {

			final Icon start = theme.icon(IconID.Data.INCLUDE_CONTINUE, 16);
			final Icon stop = theme.icon(IconID.Levels.ERROR, 16, Color.RED);

			// Progress overlay
			progressOverlay.setConsumeEvents ( false );

			// Progress state change button		
			button.setText(control.isRunning() ? "Click to abort" : "Click to start  ");
			button.setIcon(control.isRunning() ? stop : start );
			button.setRound ( 9 );
			progressOverlay.setComponent ( button );
			progressOverlay.setShowLoad ( control.isRunning() ); // init as running

			// Progress switch
			// Progress switch
			button.addActionListener ( (e)->{

				if(!control.isRunning()) {// Start process
					progressOverlay.setShowLoad ( true );
					button.setText ( "Click to abort" );
					button.setIcon(stop);
					control.start();
				}
				else {

					// Request confirmation
					if(JOptionPane.showConfirmDialog(
							null,
							"Are you sure?",
							"Abort?",
							JOptionPane.YES_NO_OPTION)
							== JOptionPane.NO_OPTION
							)
						return;

					// Changing progress visibility
					progressOverlay.setShowLoad ( true );

					// Changing buttons text
					button.setText ( "Aborting");
					progressOverlay.setShowLoad ( true );
					control.abort();

				}

			});

		}




		public JComponent getUI() {
			return progressOverlay;
		}




		@Override
		public void progressStarted() {
			// Changing progress visibility
			progressOverlay.setShowLoad ( true );
			// Changing buttons text and icons
			button.setText ( "Click to abort" );
		}



		@Override
		public void progressCompleted() {
			button.setIcon(null);
			button.setText(" Done ");
			button.setEnabled( false );	
			progressOverlay.setShowLoad( false );	
			progressOverlay.repaint();
		}



		@Override
		public void progressAborted() {
			button.setIcon(null);
			button.setText(" Cancelled ");
			progressOverlay.setShowLoad( false );
			button.setEnabled( false );
			progressOverlay.repaint();
		}



	}






}
