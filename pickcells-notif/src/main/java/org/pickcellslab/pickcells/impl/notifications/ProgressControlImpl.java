package org.pickcellslab.pickcells.impl.notifications;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import org.pickcellslab.foundationj.services.ProgressControl;
import org.pickcellslab.foundationj.services.ProgressControlListener;
import org.pickcellslab.foundationj.services.ProgressExceptionHandler;
import org.pickcellslab.foundationj.services.TaskProgress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProgressControlImpl<V> implements ProgressControl<V> {

	private static final Logger log = LoggerFactory.getLogger(ProgressControlImpl.class);

	private final List<ProgressControlListener> pLstrs = new ArrayList<>();
	private final List<ProgressExceptionHandler<V>> eHandlers = new ArrayList<>();
	private final List<TaskProgressImpl<V>> tasks = new ArrayList<>();

	private boolean wasStarted = false;
	private boolean aborted = false;


	public ProgressControlImpl(List<TaskProgressImpl<V>> tasks) {
		tasks.forEach(t->{
			//t.addProgressStatusListener(this);
			this.tasks.add(t);
		});
	}




	@Override
	public boolean isSequential() {
		return true; // TODO Enable support for parallelism
	}


	@Override
	public boolean isRunning() {
		if(aborted) 	return false;
		return wasStarted && tasks.stream().anyMatch(t->!t.isDone());
	}




	@Override
	public boolean isDone() {
		return wasStarted && tasks.stream().allMatch(t->!t.isDone());
	}



	@Override
	public int numTasks() {
		return tasks.size();
	}

	@Override
	public TaskProgressImpl<V> getProgress(int index) {
		return tasks.get(index);
	}



	@Override
	public void start() {
		wasStarted = true;
		new Thread(()-> {
			pLstrs.forEach(l->l.progressStarted());
			tasks.forEach(t->{	
				try {
					t.run();
					t.get();
				} catch (CancellationException e) { // thrown when cancelled
					log.info(t.getName() + " was cancelled");
					eHandlers.forEach(h->h.handle(this, t, e));
				} catch (InterruptedException e) { // thrown when cancelled
					log.info(t.getName() + " was interrupted");
					eHandlers.forEach(h->h.handle(this, t, e));
				} catch (ExecutionException e) { // thrown by Callable
					System.out.println("An error has occured while executing task : " + t.getName());
					log.error("An error has occured while executing task : " + t.getName(), e);
					eHandlers.forEach(h->h.handle(this, t, e));
				}
			});
			
			if(tasks.stream().allMatch(t->!t.isCancelled()))
				pLstrs.forEach(t->t.progressCompleted());
			
		}).start();
		
	}



	public void abort() {
		aborted = true;
		tasks.forEach(t->t.cancel(true));
		pLstrs.forEach(l->l.progressAborted());
	}


	@Override
	public void addProgressListener(ProgressControlListener pLstr) {
		Objects.requireNonNull(pLstr);
		if(!pLstrs.contains(pLstr))
			pLstrs.add(pLstr);
	}

	@Override
	public void removeProgressListener(ProgressControlListener pLstr) {
		pLstrs.remove(pLstr);
	}




	@Override
	public void addProgressExceptionHandler(ProgressExceptionHandler<V> handler) {
		Objects.requireNonNull(handler);
		if(!eHandlers.contains(handler))
			eHandlers.add(handler);
	}




	@Override
	public void removeProgressExceptionHandler(ProgressExceptionHandler<V> handler) {
		eHandlers.remove(handler);
	}




}
