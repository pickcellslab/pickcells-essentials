package org.pickcellslab.pickcells.impl.notifications;

import static org.pickcellslab.foundationj.services.TaskProgress.ProgressStatus.WAITING;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import org.pickcellslab.foundationj.services.ProgressLogListener;
import org.pickcellslab.foundationj.services.ProgressStatusListener;
import org.pickcellslab.foundationj.services.TaskProgress;
import org.pickcellslab.foundationj.services.TaskResultConsumer;

public class TaskProgressImpl<V> extends FutureTask<V> implements TaskProgress<V> {


	private ProgressStatus status = WAITING;

	private final String name;
	private final List<ProgressStatusListener> plstrs = new ArrayList<>();
	private final List<TaskResultConsumer<V>> consumers = new ArrayList<>();


	public TaskProgressImpl(String name, Callable<V> callable) {
		super(callable);
		Objects.requireNonNull(name);
		this.name = name;
	}


	@Override
	public String getName() {
		return name;
	}

	@Override
	public ProgressStatus getStatus() {
		return status;
	}


	@Override
	public void addProgressStatusListener(ProgressStatusListener lst) {
		if(lst != null)
			plstrs.add(lst);		
	}


	@Override
	public void addProgressLogListener(ProgressLogListener lst) {
		// TODO Auto-generated method stub

	}


	@Override
	public void addTaskResultConsumer(TaskResultConsumer<V> consumer) {
		if(consumer != null)
			consumers.add(consumer);

	}

	@Override
	public void run() {
		if(!super.isDone()) {
			// Change the status and notify listeners
			status = ProgressStatus.RUNNING;
			plstrs.forEach(l->l.statusChanged(this));
		}
		super.run();
	}
	
	@Override
	public V get() throws CancellationException, InterruptedException, ExecutionException{		
		try {
			return super.get();
		} catch (CancellationException | InterruptedException | ExecutionException e) {
			status = ProgressStatus.FAILED;
			throw e;
		}
	}


	@Override
	protected void done() {
		// update status
		status = super.isCancelled() ? ProgressStatus.FAILED : ProgressStatus.SUCCESS;
		// provide consumers
		if(status == ProgressStatus.FAILED)
			consumers.forEach(c->c.consume(this, null));
		else {
			try {
				final V v = get();
				consumers.forEach(c->c.consume(this, v));
			} catch (InterruptedException | ExecutionException e) {
				// Should never happen
			}			
		}			
		// notify listeners
		plstrs.forEach(l->l.statusChanged(this));
	}




}
