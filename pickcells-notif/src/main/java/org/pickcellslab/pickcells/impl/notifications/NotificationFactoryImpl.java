package org.pickcellslab.pickcells.impl.notifications;

import java.awt.Component;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;

import javax.swing.Icon;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.ProgressControl;
import org.pickcellslab.foundationj.services.ProgressFactory;
import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.TaskProgress;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.extended.window.WebProgressDialog;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.notification.NotificationIcon;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;
import com.alee.managers.tooltip.TooltipManager;
import com.alee.utils.ThreadUtils;

/**
 * Implementation of {@link NotificationFactory} currently using SwingX and WebLaf
 * 
 * @author Guillaume Blin
 *
 */

@CoreImpl
public final class NotificationFactoryImpl implements NotificationFactory {

	private static Logger log = LoggerFactory.getLogger(NotificationFactoryImpl.class);

	
	public NotificationFactoryImpl(){}


	/**
	 * Call this method if you wish to display an error notification window to the user. The method will also log the error to the current 
	 * appenders
	 * 
	 * @param title Title of the window
	 * @param message Message of the error
	 * @param exception A Throwable which contains the exception that occurred
	 * @param level The Level of the error
	 */
	public void display(String title, String message, Throwable exception, Level level){
		log.error(message, exception);
		ErrorInfo error = 
				new ErrorInfo(
						title, 
						message,
						null, null,
						exception,
						level,
						null);

		JXErrorPane.showDialog(null, error);		
		return;
	}






	/**
	 * Start the given Thread and display a "Please wait animation" until the
	 * given Thread finishes 
	 * @param t The Thread to start and run in background
	 * @param message The message to display with the animation
	 */
	public void waitAnimation(Thread t, String message){

		// Load dialog
		final WebProgressDialog progress = new WebProgressDialog ("Process Running ...");
		progress.setAlwaysOnTop(true);
		progress.getProgressBar().setIndeterminate(true);
		progress.setProgressText(message);

		t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

			public void uncaughtException(Thread t, Throwable e) {
				closeAnLogError(progress, e , t);
			}
		});

		Thread waitLabelThread = new Thread(()->{

			try {

				//progress.setModal(true);
				progress.setVisible(true);

				t.start();			
				t.join();


			} catch (Exception e1) {
				closeAnLogError(progress, e1 , t);
			}

			closeDialog(progress);
		});		

		waitLabelThread.start();
	}
	
	
	
	


	private void closeDialog(WebProgressDialog progress){
		progress.setText ( "Done! Closing in 3..." );
		ThreadUtils.sleepSafely ( 250 );
		progress.setText ( "Done! Closing in 2..." );
		ThreadUtils.sleepSafely ( 250 );
		progress.setText ( "Done! Closing in 1..." );
		ThreadUtils.sleepSafely ( 250 );
		progress.setVisible ( false );
		progress.dispose();
	}




	private void closeAnLogError(WebProgressDialog progress, Throwable e, Thread t){

		progress.setText ( "An error has occured ! Closing in 3..." );
		ThreadUtils.sleepSafely ( 750 );
		progress.setText ( "An error has occured ! Closing in 2..." );
		ThreadUtils.sleepSafely ( 750 );
		progress.setText ( "An error has occured ! Closing in 1..." );
		ThreadUtils.sleepSafely ( 750 );
		progress.setVisible ( false );

		progress.dispose();

		display("Error", "An error has occured !", e, Level.WARNING);

		//log.error("exception " + e + " from thread " + t);
	}

	
	@Override
	public void notify(Component comp, String msg) {
		notify(comp, msg, 1000, NotificationIcon.information.getIcon());
	}
	

	@Override
	public void notify(Component comp, String msg, int displayTime, Icon icon) {
		WebNotification wn = NotificationManager.showNotification(comp, msg, icon);
		wn.setDisplayTime(displayTime);		
	}


	@Override
	public void setTooltip(Component comp, String msg, Icon icon, int position) {
		TooltipManager.setTooltip(comp, icon, msg, swingToWay(position));		
	}


	@Override
	public void setTooltip(Component comp, String msg) {
		TooltipManager.setTooltip(comp, msg);	
	}


	@Override
	public void setTooltip(Component comp, String msg, Icon icon) {
		TooltipManager.setTooltip(comp, icon, msg);
	}

	
	private TooltipWay swingToWay(int swingConstant) {
		switch(swingConstant) {
		case SwingConstants.TOP : return TooltipWay.up;
		case SwingConstants.BOTTOM : return TooltipWay.down;
		case SwingConstants.LEFT : return TooltipWay.left;
		case SwingConstants.RIGHT : return TooltipWay.right;
		}
		return TooltipWay.down;
	}


	@Override
	public <P extends TaskProgress<V>, V> ProgressFactory<P, V> newProgressFactory() {
		return new ProgressFactoryImpl();
	}


	
	
	private class ProgressFactoryImpl<V> implements ProgressFactory<TaskProgressImpl<V>,V>{
		
		@Override
		public ProgressPanel<V> progressPanel(UITheme theme, List<TaskProgressImpl<V>> tasks) {
			final ProgressControlImpl<V> control = new ProgressControlImpl<V>(tasks);
			return new ProgressPanelImpl<>(theme, control);
		}

		@Override
		public TaskProgressImpl<V> newTaskProgress(String name, Callable<V> task) {
			return new TaskProgressImpl<>(name, task);
		}

	}

	
	



}
