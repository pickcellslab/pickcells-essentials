package org.pickcellslab.pickcells.impl.presentation.resources;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.pickcells.api.presentation.ImageResources;

@CoreImpl
public class ImageResourcesImpl implements ImageResources{

	// Just a placeholder impl to enable the ImageResource Scope
}
