package org.pickcellslab.pickcells.impl.presentation.overview;

public interface UserSessionViewListener {

	public void createExperimentClicked();
		
	public void deleteExperimentClicked();
	
	public void gotoExperimentClicked();
	
	public void importExperimentClicked();
	
}
