package org.pickcellslab.pickcells.impl.presentation.overview;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.kordamp.ikonli.fontawesome5.FontAwesomeRegular;
import org.kordamp.ikonli.fontawesome5.FontAwesomeSolid;
import org.kordamp.ikonli.swing.FontIcon;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest.Permission;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.data.User.Priviledge;
import org.pickcellslab.pickcells.impl.presentation.admin.ExperimentImplForAdmins;
import org.pickcellslab.pickcells.impl.presentation.admin.LoginDialog;
import org.pickcellslab.pickcells.impl.presentation.admin.NewUserValidator;
import org.pickcellslab.pickcells.impl.presentation.admin.UserImplForAdmins;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.extended.layout.TableLayout;
import com.alee.extended.layout.ToolbarLayout;
import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.WebCollapsiblePane;
import com.alee.laf.button.WebButton;
import com.alee.laf.filechooser.WebFileChooser;
import com.alee.laf.label.WebLabel;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.separator.WebSeparator;
import com.alee.laf.table.WebTable;
import com.alee.laf.table.renderers.WebTableCellRenderer;
import com.alee.utils.SwingUtils;
import com.alee.utils.swing.WebDefaultCellEditor;

/**
 * Swing Implementation for the overview of the user session overview
 *
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("serial")
public class UserSessionView extends WebPanel implements UserEventsListener{

	private static final Logger log = LoggerFactory.getLogger(UserSessionView.class);

	private static final Font userFont = new Font("Times", Font.PLAIN, 24);
	private static final Font expFont = new Font("Times", Font.PLAIN, 16);

	/**
	 * The panel which displays the list of Experiments
	 */
	private WebPanel expPanel;
	private final Map<ExperimentImplForAdmins, WebPanel> exps = new HashMap<>();
	/**
	 * The panel which displays experiments shared with the current user
	 */
	private WebPanel sharedExpPanel;
	private final Map<ExperimentImplForAdmins, WebLabel> shares = new HashMap<>();

	/**
	 * The panel which displays the list of Users managed by the user currently logged in (if admin)
	 */
	private WebPanel usersPanel;
	private final Map<UserImplForAdmins, Component> users = new HashMap<>();

	private final WebPanel contentPanel = new WebPanel ( false );

	private final UITheme theme;
	private final UserSessionModel model;
	private final NotificationFactory notif;
	private final GoToExperimentListener gtLstr;


	public UserSessionView(UITheme theme, NotificationFactory notif, UserSessionModel model, GoToExperimentListener l) {
		this.theme = theme;
		this.notif = notif;
		this.model = model;
		model.addUserEventsListener(this);
		this.gtLstr = l;
	}


	public void start() {

		// Create and layout components

		// background image
		setBackground ( Color.WHITE );


		// White panel centered dropping shadow
		setLayout ( new TableLayout ( new double[][]{
			{ TableLayout.FILL, 0.8, TableLayout.FILL },
			{ TableLayout.FILL, 0.8, TableLayout.FILL }
		} ) );



		// Centered panel with shadow
		final WebPanel shadowPanel = new WebPanel ( true );
		shadowPanel.setWebColoredBackground ( false );
		shadowPanel.setShadeWidth ( 20 );
		shadowPanel.setBackground ( Color.WHITE );



		// Content
		contentPanel.setLayout(new VerticalFlowLayout());
		contentPanel.setBackground ( Color.WHITE );


		// Get the current user
		final UserImplForAdmins user = model.currentUser();



		// ========================================================//
		// Create the Collapsible pane which contains instructions,
		// buttons and experiments of the current user
		// ========================================================//

		final WebCollapsiblePane myExpsPane = new WebCollapsiblePane();
		myExpsPane.setLayout(new VerticalFlowLayout());

		// Header
		final WebLabel header = new WebLabel("  My Experiments");
		header.setFont(userFont);
		header.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		header.setIcon(theme.icon(IconID.App.ADMIN, 24));
		header.setBackground( Color.WHITE );
		header.setToolTip("<HTML>Priviledge: "+user.privilege().name()+ "<br>Since: "+user.creationDate()+"</HTML>");

		myExpsPane.setTitleComponent(header);

		// Panel for instructions to current user
		final WebPanel instructionsPanel = new WebPanel( false );
		instructionsPanel.setLayout(new ToolbarLayout());
		instructionsPanel.setBackground( Color.WHITE );

		// Instructions
		final WebLabel note = new WebLabel("      Right-click on individual experiments for more options");
		instructionsPanel.add(note, ToolbarLayout.START);
		final WebButton createBtn = new WebButton(FontIcon.of(FontAwesomeRegular.LIGHTBULB, 16));
		createBtn.setToolTip("Create a new experiment");
		createBtn.addActionListener(l->createExperiment(user));

		final WebButton importBtn = new WebButton(FontIcon.of(FontAwesomeSolid.DOWNLOAD, 16));
		importBtn.setToolTip("Import an existing experiment");
		SwingUtils.equalizeComponentsSize(createBtn, importBtn);
		importBtn.addActionListener(l->importExperiment(user));

		instructionsPanel.add(createBtn, ToolbarLayout.END);
		instructionsPanel.add(importBtn, ToolbarLayout.END);

		// If the current User is admin then add a button to create a new User
		if(user.privilege() == Priviledge.ADMIN) {
			final WebButton newUserBtn = new WebButton(FontIcon.of(FontAwesomeSolid.USER_PLUS, 16));
			newUserBtn.setToolTip("Add a new user");
			newUserBtn.addActionListener(l->{

				// Create a Set with all user names so the new user does not take an already used name
				final Set<String> knownUsers = new HashSet<>();
				knownUsers.add(user.userName());
				model.allUsersExceptCurrent().forEach( u -> knownUsers.add(u.userName()));

				// Ask for new Credentials
				final LoginDialog d = new LoginDialog(null, true, new NewUserValidator(knownUsers), "New User...", "Create");
				d.pack();
				d.setLocationRelativeTo(null);
				d.setVisible(true);

				if(d.wasCancelled())
					return;

				try {
					model.addUser(new UserImplForAdmins(d.userName(), d.password(), Priviledge.USER));
				} catch (DataAccessException e1) {
					notif.display("User Creation Error", "The new user could not be created", e1, Level.WARNING);
				}

			});

			instructionsPanel.add(newUserBtn, ToolbarLayout.END);
		}

		instructionsPanel.setBorder(BorderFactory.createEmptyBorder(5, 20, 15, 20));


		// Panel which lists experiments for the current user
		expPanel = makeExperimentPanel(user, true);

		myExpsPane.setContent(new GroupPanel(0, false, instructionsPanel, expPanel));
		contentPanel.add(myExpsPane);



		// ========================================================//
		// Create the Collapsible pane which contains
		// buttons and experiments shared with current user
		// ========================================================//

		final WebCollapsiblePane sharedPane = new WebCollapsiblePane();
		myExpsPane.setLayout(new VerticalFlowLayout());

		// Header
		final WebLabel sharedHeader = new WebLabel("  Shared With Me");
		sharedHeader.setFont(userFont);
		sharedHeader.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
		sharedHeader.setIcon(theme.icon(IconID.App.SHARE, 24));
		sharedHeader.setBackground( Color.WHITE );

		sharedPane.setTitleComponent(sharedHeader);

		// Add experiments that are shared
		sharedExpPanel = makeExperimentPanel(user, false);

		sharedPane.setContent(sharedExpPanel);
		contentPanel.add(sharedPane);






		// ========================================================//
		// Create the Collapsible pane which contains
		// the Users managed if the current user is admin
		// ========================================================//
		if(user.privilege() == Priviledge.ADMIN) {

			final WebCollapsiblePane usersPane = new WebCollapsiblePane();
			usersPane.setLayout(new VerticalFlowLayout());

			// Header
			final WebLabel usersHeader = new WebLabel("  Managed Users");
			usersHeader.setFont(userFont);
			usersHeader.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
			usersHeader.setIcon(theme.icon(IconID.App.TEAM, 24));
			usersHeader.setBackground( Color.WHITE );

			usersPane.setTitleComponent(usersHeader);

			// Add experiments that are shared
			usersPanel = new WebPanel( false);
			usersPanel.setLayout(new VerticalFlowLayout());
			usersPanel.setBackground( Color.WHITE );

			user.managedUsers().forEach(u->usersPanel.add(createUserEntry(u)));

			usersPane.setContent(usersPanel);
			contentPanel.add(usersPane);

		}




		// finally add content to this view inside a Scroll
		final WebScrollPane scrollPanel = new WebScrollPane ( contentPanel, false );
		scrollPanel.setShadeWidth ( 20 );
		scrollPanel.setBackground ( Color.WHITE );

		shadowPanel.add ( scrollPanel );

		add(shadowPanel, "1,1");

	}






	/**
	 * @param u The currently logged in user
	 * @param created Whether the panel to create is to display the experiments created by the user
	 * of to display the experiments shared by others with the currently logged in user.
	 * @return
	 */
	private WebPanel makeExperimentPanel(UserImplForAdmins u, boolean created) {
		final WebPanel experimentPanel = new WebPanel( false );
		experimentPanel.setLayout(new VerticalFlowLayout());
		experimentPanel.setBackground( Color.WHITE );
		if(created) {
			u.getCreatedExperimentsLinks().forEach(l->{
				experimentPanel.add(new WebSeparator(true, true, WebSeparator.HORIZONTAL));
				final Component wl = makeExperimentEntry(u, l);
				experimentPanel.add(wl);
			});
		}
		else {
			u.getExperimentsSharedByOthersLinks().forEach(l->{
				experimentPanel.add(new WebSeparator(true, true, WebSeparator.HORIZONTAL));
				final Component wl = makeExperimentEntry(u, l);
				experimentPanel.add(wl);
			});
			if(u.getExperimentsSharedByOthersLinks().count() == 0) {
				experimentPanel.add(new WebLabel("     No experiment has been shared with you yet"));
				experimentPanel.setBorder(BorderFactory.createEmptyBorder(5, 20, 15, 20));
			}
		}
		return experimentPanel;
	}




	/**
	 * Creates a representation for an experiment
	 * @param e A {@link Link} between a {@link UserImplForAdmins} and an {@link ExperimentImplForAdmins}
	 * @return
	 */
	private Component makeExperimentEntry(UserImplForAdmins u, Link l) {

		final ExperimentImplForAdmins e = u.getExperimentFromLink(l);

		Permission perm = u.permission(l);
		final Permission p = perm == null ? Permission.READ_WRITE : perm; // init to READ_WRITE as this should be default when the exp was created by the logged in user.

		final WebPanel expInfoPane = new WebPanel( false );
		expInfoPane.setLayout(new ToolbarLayout());
		expInfoPane.setBackground( Color.WHITE );


		final Border margin = BorderFactory.createEmptyBorder(5, 25, 5, 50);
		expInfoPane.setBorder(margin);
		expInfoPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				expInfoPane.setBorder(new CompoundBorder(
						BorderFactory.createLineBorder(new Color(153,153,153), 1, false),
						BorderFactory.createEmptyBorder(4,24,4,49)));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				if(!expInfoPane.contains(e.getPoint()))
					expInfoPane.setBorder(margin);
			}
		});


		// Icon, Name and path of the experiment
		final WebLabel expInfo = new WebLabel(e.getName()+ "  "+e.getDBPath(), experimentIcon(u.isCreatedLink(l), p));
		expInfo.setFont(expFont);
		expInfo.setToolTip("<HTML>Created by: "+e.getCreator().userName()+
				"<br>Location: "+e.getDBPath()+
				"<br>Description: "+e.getDescription()+
				"</HTML>");
		expInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				if(SwingUtils.isRightMouseButton(evt))
					createMenu(e, u.isCreatedLink(l), p).show(expInfo, evt.getX(), evt.getY());
			}
		});

		expInfoPane.add(expInfo, ToolbarLayout.START);

		// Now add an extra icon with tooltips to indicate whether the experiment is currently being shared
		// Do this only for experiments created by the currently logged in user
		if(u.isCreatedLink(l) && e.isShared()) {
			final WebLabel shareInfo = createShareLabel(e);
			expInfoPane.add(shareInfo, ToolbarLayout.END);
			shares.put(e, shareInfo);
		}

		exps.put(e, expInfoPane); // Add to exp for later removal if required
		return expInfoPane;
	}


	private WebLabel createShareLabel(ExperimentImplForAdmins e) {
		final WebLabel shareInfo = new WebLabel(theme.icon(IconID.App.SHARE, 16));
		shareInfo.setFont(expFont);
		StringBuilder sb = new StringBuilder();
		sb.append("<HTML>Shared With: ");
		e.sharedWith(Permission.READ_WRITE).forEach(other-> sb.append("<br>"+other.userName()+" "+Permission.READ_WRITE));
		e.sharedWith(Permission.READ_ONLY).forEach(other-> sb.append("<br>"+other.userName()+" "+Permission.READ_ONLY));
		sb.append("</HTML>");
		shareInfo.setToolTip(sb.toString());
		return shareInfo;
	}





	private Component createUserEntry(UserImplForAdmins u) {

		final WebPanel userPane = new WebPanel( false );
		userPane.setLayout(new ToolbarLayout());
		userPane.setBackground( Color.WHITE );


		final Border margin = BorderFactory.createEmptyBorder(5, 25, 5, 50);
		userPane.setBorder(margin);
		userPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				userPane.setBorder(new CompoundBorder(
						BorderFactory.createLineBorder(new Color(153,153,153), 1, false),
						BorderFactory.createEmptyBorder(4,24,4,49)));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				if(!userPane.contains(e.getPoint()))
					userPane.setBorder(margin);
			}
		});


		// Icon, Name and path of the experiment
		final int expCount = (int) u.getCreatedExperimentsLinks().count();
		final String expString = expCount <= 1 ? " experiment" : " experiments";
		final WebLabel userInfo = new WebLabel(u.userName()+" ( "+u.getCreatedExperimentsLinks().count()+expString+" created)", theme.icon(IconID.App.USER, 16));
		userInfo.setFont(expFont);
		userInfo.setToolTip("<HTML>"+u.userName()+
				"<br>User since : "+u.creationDate()+
				"<br>Has "+u.getCreatedExperiments().size()+" experiments"+
				"</HTML>");
		userInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				if(SwingUtils.isRightMouseButton(evt)) {
					final JPopupMenu popup = new JPopupMenu();
					final JMenuItem menuItem = new JMenuItem("Delete", theme.icon(IconID.Files.DELETE, 16));
					popup.add(menuItem);
					menuItem.addActionListener(l->{
						new Thread(()->{
							int response = JOptionPane.showConfirmDialog(null,
									"This will delete this user and all associated experiments, Continue?",
									"Delete User "+u.userName(), JOptionPane.YES_NO_OPTION);
							if(response == JOptionPane.YES_OPTION) {
								try {
									model.deleteUser(u);
								} catch (DataAccessException e) {
									notif.display("Error", "Could not delete this user", e, Level.WARNING);
								}
							}
						}).start();
					});
					popup.show(userInfo, evt.getX(), evt.getY());
				}
			}
		});

		userPane.add(userInfo, ToolbarLayout.START);

		users.put(u, userPane);


		return userPane;
	}







	private Icon experimentIcon(boolean created, Permission p) {
		Color c = null;
		if(created)
			c = Color.BLACK;
		else if(p == Permission.READ_WRITE)
			c = new Color(146,198,131);
		else
			c = new Color(255,203,88);
		return theme.icon(IconID.Misc.FLASK, 16, c);
	}



	private JPopupMenu createMenu(ExperimentImplForAdmins e, boolean isCreated, Permission p)
	{
		final JPopupMenu menu = new JPopupMenu ();

		// Load
		final FontIcon loadIcon = FontIcon.of(FontAwesomeSolid.ARROW_ALT_CIRCLE_RIGHT);
		final JMenuItem loadItem = new JMenuItem ("Load", UITheme.toImageIcon(loadIcon));
		loadItem.addActionListener(l-> {
			this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			new Thread(()->{

				try {
					gtLstr.gotoExperiment(model.currentUser(), e);
				}catch(Exception ex) {
					log.error("An error occured while importing the experiment", ex);
					JOptionPane.showMessageDialog(this,
							"An error occured while importing the experiment,"
									+ " the error has been logged", "Import Failed...", JOptionPane.ERROR);
				}
				EventQueue.invokeLater(()->	this.setCursor(Cursor.getDefaultCursor()));
			}).start();

		});
		loadItem.setMargin ( new Insets ( 8, 8, 8, 8 ) );
		loadItem.setToolTipText("Load Experiment: "+e.getName());
		menu.add(loadItem);

		// Edit
		if(isCreated || p==Permission.READ_WRITE) {
			final FontIcon editIcon = FontIcon.of(FontAwesomeSolid.EDIT);
			final JMenuItem editItem = new JMenuItem ("Edit Description", UITheme.toImageIcon(editIcon));
			editItem.addActionListener(l->manageEditing(e));
			editItem.setMargin ( new Insets ( 8, 8, 8, 8 ) );
			editItem.setToolTipText("Edit the Description of this Experiment");
			menu.add(editItem);
		}
		if(isCreated) {
			// Share
			final Icon shareIcon = theme.icon(IconID.App.SHARE, 16);
			final JMenuItem shareItem = new JMenuItem("Share", UITheme.toImageIcon(shareIcon));
			shareItem.addActionListener(l->manageSharing(e));
			shareItem.setMargin ( new Insets ( 8, 8, 8, 8 ) );
			shareItem.setToolTipText("Share this Experiment");
			menu.add(shareItem);


			// Delete
			final FontIcon delIcon = FontIcon.of(FontAwesomeSolid.TRASH);
			final JMenuItem delItem = new JMenuItem ( "Delete", UITheme.toImageIcon(delIcon));
			delItem.addActionListener(l-> {
				int sure = JOptionPane.showConfirmDialog(this, "<HTML>Delete "+e.getName()+" ?"
						+ "<br>( This will not delete the database on disk )</HTML>)");
				if(sure == JOptionPane.OK_OPTION) {
					try {
						model.deleteExperiment(e);
					} catch (DataAccessException e1) {
						notif.display("Error", "Could not delete this experiment", e1, Level.WARNING);
					}
				}
			});
			delItem.setMargin ( new Insets ( 8, 8, 8, 8 ) );
			delItem.setToolTipText("Delete this Experiment");
			menu.add(delItem);
		}

		return menu;
	}






	private void manageEditing(ExperimentImplForAdmins e) {
		final JTextArea tArea = new JTextArea();
		tArea.setText(e.getDescription());
		tArea.setPreferredSize(new Dimension(250,200));
		tArea.setLineWrap(true);
		final JScrollPane scroll = new JScrollPane(tArea);
		final int response = JOptionPane.showConfirmDialog(null, scroll, "Description Editing...",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
		if(response == JOptionPane.OK_OPTION) {
			try {
				model.updateDescription(e, tArea.getText());
			} catch (DataAccessException e1) {
				notif.display("Error", "Could not update this experiment description", e1, Level.WARNING);
			}
		}
	}



	private void manageSharing(ExperimentImplForAdmins e) {
		// Create a Table to choose sharing permissions
		final ShareTableModel tableModel = new ShareTableModel (e);
		final WebTable table = new WebTable ( tableModel );

		// CheckBox renderer for boolean values (shared or not)
		// Custom column
		final TableColumn column = table.getColumnModel().getColumn ( 2 );
		final WebTableCellRenderer renderer = new WebTableCellRenderer();
		renderer.setToolTipText ( "Click to change permission" );
		column.setCellRenderer(renderer);

		// Give a custom editor for Permission
		final JComboBox<Permission> combo = new JComboBox<>(Permission.values());
		column.setCellEditor (new WebDefaultCellEditor<>(combo));
		initColumnSizes(table);
		final WebScrollPane scrollPane = new WebScrollPane (table);
		int response = WebOptionPane.showConfirmDialog(null, scrollPane,
				e.getName()+" : sharing properties",
				WebOptionPane.OK_CANCEL_OPTION,
				WebOptionPane.PLAIN_MESSAGE,
				theme.icon(IconID.App.SHARE, 16));
		if(response != WebOptionPane.CANCEL_OPTION) {
			tableModel.makeChanges();
		}
	}


	@Override
	public void userCreated(UserImplForAdmins created) {
		SwingUtils.invokeLater(()->{
			if(usersPanel == null)	return;
			usersPanel.add(this.createUserEntry(created));
			usersPanel.revalidate();
			usersPanel.repaint();
			System.out.println("UserSessionView: User added "+created.userName());
		});
	}




	@Override
	public void userDeleted(UserImplForAdmins deleted) {
		SwingUtils.invokeLater(()->{
			if(usersPanel == null)	return;
			usersPanel.remove(users.remove(deleted));
			usersPanel.revalidate();
			usersPanel.repaint();
			System.out.println("UserSessionView: User deleted "+deleted.userName());
		});
	}



	@Override
	public void experimentAdded(UserImplForAdmins source, Link linkToExp) {
		SwingUtils.invokeLater(()->{
			if(expPanel == null)	return;
			expPanel.add(this.makeExperimentEntry(source, linkToExp));
			expPanel.revalidate();
			expPanel.repaint();
		});
	}


	@Override
	public void experimentDeleted(UserImplForAdmins owner, ExperimentImplForAdmins experiment) {
		SwingUtils.invokeLater(()->{
			if(expPanel == null)	return;
			if(owner == model.currentUser()){ // current User deleted is own experiment
				expPanel.remove(exps.remove(experiment));
				expPanel.revalidate();
				expPanel.repaint();
			}
			else{ // deleted as a result of user deletion so remove from shared Panel
				sharedExpPanel.remove(exps.remove(experiment));
				// Now update tooltips for experiments currently shared
				exps.keySet().forEach(e->{
					if(shares.containsKey(e))
						sharingPropertiesUpdated(e);
				});
			}
		});
	}


	@Override
	public void experimentUpdated(ExperimentImplForAdmins e) {
		((WebLabel)exps.get(e).getComponent(0)).setToolTip(
				"<HTML>Created by: "+e.getCreator().userName()+
				"<br>Location: "+e.getDBPath()+
				"<br>Description: "+e.getDescription()+
				"</HTML>");
	}



	@Override
	public void sharingPropertiesUpdated(ExperimentImplForAdmins exp){
		if(exp.isShared()) {
			WebLabel comp = shares.get(exp);
			if(comp!=null) {// Update Tooltip
				StringBuilder sb = new StringBuilder();
				sb.append("<HTML>Shared With: ");
				exp.sharedWith(Permission.READ_WRITE).forEach(other-> sb.append("<br>"+other.userName()+" "+Permission.READ_WRITE));
				exp.sharedWith(Permission.READ_ONLY).forEach(other-> sb.append("<br>"+other.userName()+" "+Permission.READ_ONLY));
				sb.append("</HTML>");
				comp.setToolTip(sb.toString());
			}
			else {// Create the Label
				WebLabel label = this.createShareLabel(exp);
				exps.get(exp).add(label, ToolbarLayout.END);
				shares.put(exp, label);
			}
		}
		else {// Check if we need to remove the icon
			Component comp = shares.get(exp);
			if(comp!=null) {
				exps.get(exp).remove(comp);
				exps.get(exp).revalidate();
				exps.get(exp).repaint();
			}
		}
	}



	private void importExperiment(UserImplForAdmins user) {
		// First show a File Chooser
		File workingDirectory = new File(System.getProperty("user.dir"));
		final WebFileChooser chooser = new WebFileChooser(workingDirectory.getParentFile().getParent());
		chooser.setMultiSelectionEnabled ( false );
		chooser.setAcceptAllFileFilterUsed ( false );
		chooser.setFileSelectionMode(WebFileChooser.DIRECTORIES_ONLY);
		chooser.setFileFilter(new FileFilter(){

			@Override
			public boolean accept(File f) {
				return model.isValidDatabase(f);
			}

			@Override
			public String getDescription() {
				return "database folder";
			}

		});

		chooser.setDialogIcon(UITheme.toImageIcon(FontIcon.of(FontAwesomeSolid.DOWNLOAD)));
		File file;
		if ( chooser.showOpenDialog((JComponent)null) == WebFileChooser.APPROVE_OPTION ){
			file = chooser.getSelectedFile ();
			this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			new Thread(()->{
				try {
					model.importExperiment(file);
				}catch(Exception e) {
					log.error("An error occured while importing the experiment", e);
					JOptionPane.showMessageDialog(this,
							"An error occured while importing the experiment,"
									+ " the error has been logged", "Import Failed...", JOptionPane.ERROR);
				}
				EventQueue.invokeLater(()->	this.setCursor(Cursor.getDefaultCursor()));
			}).start();
		}
	}




	private void createExperiment(UserImplForAdmins user) {
		final ExperimentWizard expWizard = new ExperimentWizard(notif);
		Optional<ExperimentImplForAdmins> created = expWizard.showDialog();
		created.ifPresent(e->{
			this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			new Thread(()->{
				try {
					model.addExperiment(e);
				}catch(Exception ex) {
					log.error("An error occured while importing the experiment", ex);
					JOptionPane.showMessageDialog(this,
							"An error occured while importing the experiment,"
									+ " the error has been logged", "Import Failed...", JOptionPane.ERROR);
				}
				SwingUtils.invokeLater(()->this.setCursor(Cursor.getDefaultCursor()));

			}).start();
		});
	}





	// Sharing Properties Table

	private void initColumnSizes ( JTable table )
	{
		ShareTableModel model = ( ShareTableModel ) table.getModel ();
		TableColumn column;
		Component comp;
		int headerWidth;
		int cellWidth;
		Object[] longValues = model.longValues;
		TableCellRenderer headerRenderer = table.getTableHeader ().getDefaultRenderer ();

		for ( int i = 0; i < model.getColumnCount (); i++ )
		{
			column = table.getColumnModel ().getColumn ( i );

			comp = headerRenderer.getTableCellRendererComponent ( null, column.getHeaderValue (), false, false, 0, 0 );
			headerWidth = comp.getPreferredSize ().width;

			comp = table.getDefaultRenderer ( model.getColumnClass ( i ) ).
					getTableCellRendererComponent ( table, longValues[ i ], false, false, 0, i );
			cellWidth = comp.getPreferredSize ().width;

			column.setPreferredWidth ( Math.max ( headerWidth, cellWidth ) );
		}
	}



	private class ShareTableModel extends AbstractTableModel{

		private final ExperimentImplForAdmins exp;
		private final List<UserImplForAdmins> users;

		private final String[] columnNames = {"User Name", "Share", "Permission"};
		private final Object[][] data;
		private final Object[][] initValues;

		public final Object[] longValues = {"Long User Name", Boolean.FALSE, Permission.READ_WRITE};



		public ShareTableModel(ExperimentImplForAdmins exp) {
			this.exp = exp;
			users = model.allUsersExceptCurrent();
			data = new Object[users.size()][columnNames.length];
			initValues = new Object[users.size()][2];
			for(int u = 0; u<users.size(); u++) {
				final UserImplForAdmins user = users.get(u);
				data[u][0] = user.userName();
				data[u][1] = user.getExperimentsSharedByOthers().contains(exp);
				data[u][2] = exp.sharedLinks().filter(l->l.target() == user).map(l->user.permission(l)).findFirst().orElse(Permission.READ_ONLY);
				initValues[u][0] = data[u][1];
				initValues[u][1] = data[u][2];
			}
		}


		public void makeChanges() {

			boolean changesWereMade = false;
			final Set<Integer> deletedLinks = new HashSet<>();
			for(int i = 0; i<data.length; i++) {
				// do we need to create or to delete a link?
				if(data[i][1]!=initValues[i][0]) {
					changesWereMade = true;
					if((boolean)data[i][1]){//create
						exp.shareWith(getUserByName(data[i][0].toString()), (Permission) data[i][2]);
					}
					else {
						deletedLinks.add(exp.stopSharingWith(getUserByName(data[i][0].toString())));
					}
				}
				else if(data[i][2]!=initValues[i][1]) {
					exp.setPermission(getUserByName(data[i][0].toString()), (Permission) data[i][2]);
				}
			}

			if(changesWereMade)
				try {
					model.updateSharingProperties(exp, deletedLinks);
				} catch (DataAccessException e) {
					notif.display("Error", "Could not write to the database", e, Level.WARNING);
				}
		}

		private UserImplForAdmins getUserByName(String name){
			for(UserImplForAdmins u : users)
				if(u.userName().equals(name))
					return u;
			return null;
		};


		@Override
		public int getColumnCount ()
		{
			return columnNames.length;
		}

		@Override
		public int getRowCount ()
		{
			return data.length;
		}

		@Override
		public String getColumnName ( int col )
		{
			return columnNames[ col ];
		}

		@Override
		public Object getValueAt ( int row, int col )
		{
			return data[ row ][ col ];
		}

		@Override
		public Class getColumnClass ( int c )
		{
			return longValues[ c ].getClass();
		}

		@Override
		public boolean isCellEditable ( int row, int col )
		{
			return col >= 1;
		}

		@Override
		public void setValueAt ( Object value, int row, int col )
		{
			data[ row ][ col ] = value;
			fireTableCellUpdated ( row, col );
		}


	}



	public void stop() {
		// TODO Auto-generated method stub

	}



}
