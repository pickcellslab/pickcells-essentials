package org.pickcellslab.pickcells.impl.presentation.toolbar;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.Objects;

import javax.swing.JComboBox;
import javax.swing.JDialog;

import org.pickcellslab.foundationj.dataviews.mvc.CameleonView;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.ui.utils.CardPanel;
import org.pickcellslab.pickcells.api.app.charts.AppearanceFactory;
import org.pickcellslab.pickcells.api.app.charts.AppearancePreviewController;
import org.pickcellslab.pickcells.api.app.charts.AppearancePreviewListener;

@SuppressWarnings("serial")
class CameleonChoiceDialog<V extends CameleonView<A> & DataView<?,?>, A>  extends JDialog implements AppearancePreviewListener<A>{

	private V view;

	private boolean wasCancelled = true;


	@SuppressWarnings("unchecked")
	public CameleonChoiceDialog(V view, AppearanceFactory<A> fctry) {
		
		Objects.requireNonNull(view, "view is null");
		Objects.requireNonNull(fctry, "fctry is null");
		this.view = view;
		
		// Build the dialog
		DataSet<?> ds = view.getModel().getDataSet();
		JComboBox<Series> series = new JComboBox<>();
		for(int q = 0; q < ds.numQueries(); q++){
			for(int i = 0; i<ds.numSeries(q); i++){
				series.addItem(new Series(ds.queryID(q), i, ds.seriesID(q, i)));
			}
		}
		
		
		CardPanel<Series,AppearancePreviewController<A>> card = new CardPanel<>((s)-> fctry.create(s.query, s.index, s.currentAppearance()));
		card.show((Series) series.getSelectedItem());
		
		
		series.addActionListener(l->{
			card.show((Series) series.getSelectedItem());
			this.repaint();
		});
		
		
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(series,BorderLayout.NORTH);		
				
		getContentPane().add(card, BorderLayout.CENTER);
		
	}
	
	
	
	

	@Override
	public void AppearancePreviewEvent(String query, int series, A newApp) {
		view.setSeriesAppearance(query, series, newApp);
	}
	
	
	public boolean wasCancelled(){
		return wasCancelled;
	}
	
	
	private final class Series{
		
		private final String query, name;
		private final int index;
		
		Series(String query, int index, String name) {
			this.query = query;
			this.index = index;
			this.name = name;
		}
		
		@Override
		public String toString(){
			return name;
		}
		
		A currentAppearance(){
			return view.getSeriesAppearance(query, index);
		}
	}
	
	
}
