package org.pickcellslab.pickcells.impl.presentation;

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest.Permission;
import org.pickcellslab.foundationj.scope.AppController;
import org.pickcellslab.foundationj.scope.RuntimeScope;
import org.pickcellslab.foundationj.scope.ScopeConfigurationException;
import org.pickcellslab.foundationj.scope.ScopeManager;
import org.pickcellslab.pickcells.api.presentation.PickCellsPresentation;
import org.pickcellslab.pickcells.api.presentation.PickCellsPresentation.Perspective;
import org.pickcellslab.pickcells.api.presentation.PickCellsPresentationListener;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;
import org.pickcellslab.pickcells.impl.presentation.admin.AdminController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@CoreImpl
public class PickCellsControl extends AppController implements PickCellsPresentationListener, PresentationEntry{

	private static final Logger log = LoggerFactory.getLogger(PickCellsControl.class);

	private final PickCellsPresentation view;

	private final List<RuntimeScope> wbScopes = new ArrayList<>();
	private RuntimeScope userScope;


	public PickCellsControl(ScopeManager scopeMgr, PickCellsPresentation view) { //TODO add UI service
		super(scopeMgr);

		this.view = view;
		this.view.addPresentationListener(this);
		scopeMgr.addScopeEventListener(this.view);




	}



	public void start() {
		log.debug("--------------------------------");
		log.debug(" PickCells Control started");
		log.debug("--------------------------------");
	}



	//==========================================================================//
	//                        Scope Control										//
	//==========================================================================//
	// Controlling SANDBOX / ADMIN-ACCESS





	@Override
	public void sandboxClicked() {
		// Start SANDBOX
		RuntimeScope sandboxScope = scopeMgr.getScope(KnownScopes.SANDBOX, KnownScopes.SANDBOX);
		// If first time to sandbox
		if(sandboxScope == null) {


			try {

				// Get the ImageResources to use as parent
				final RuntimeScope resourcesScope = getOrStartSharedResources();

				// Get the Scope
				sandboxScope = scopeMgr.newScopeInstance(KnownScopes.SANDBOX, KnownScopes.SANDBOX, resourcesScope);


				scopeMgr.startScope(sandboxScope);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
			view.setPerspective(Perspective.SANDBOX);

	}


	@Override
	public void showWelcomeClicked() {
		log.debug(" PickCells going to overview screen ");
		view.setPerspective(Perspective.WELCOME);
	}


	@Override
	public void showOverviewClicked() {
		log.debug(" PickCells going to overview screen ");
		view.setPerspective(Perspective.OVERVIEW);
	}

	@Override
	public void showWorkbenchClicked() {
		view.setPerspective(Perspective.WORKBENCH);
	}




	@Override
	public void loginClicked() {

		// Start ADMIN-ACCESS
		RuntimeScope adminScope = scopeMgr.getScope(KnownScopes.ADMIN_ACCESS, KnownScopes.ADMIN_ACCESS);


			// If first logging
			if(adminScope == null) {

				// Get the Scope
				adminScope = scopeMgr.newScopeInstance(KnownScopes.ADMIN_ACCESS, KnownScopes.ADMIN_ACCESS, null);//scopeMgr.getApplicationScope());
				
				// Start scope with the application's ConnexionRequest
				final ConnexionRequest request = new ConnexionRequest(
						scopeMgr.getRootPath(), 
						"admindb",
						"appliUser",
						"appliPassword",
						Permission.READ_WRITE);

				try {
					scopeMgr.startScope(adminScope, scopeMgr, request);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			final AdminController adminControl = adminScope.getComponentInstance(AdminController.class);
			
			new Thread(()-> adminControl.performLogging()).start();				


	}




	@Override
	public void closeWorkbenchClicked(Workbench wb) {
				
		final RuntimeScope wbScope = scopeMgr.getScope(KnownScopes.WORKBENCH, wb.title());
		scopeMgr.stopScope(wbScope);
		
	}
	




	@Override
	public void logOffClicked() {

		// Ask for confirmation - ask view to show the welcome screen disable buttons 

		int n;
		if(userScope!=null ) {


			if((n = userScope.numberOfChildren(KnownScopes.WORKBENCH))!=0) {


				boolean sure = view.showConfirm(n+" workbench window(s) opened."
						+ "\nLogging off will close opened experiment(s), Continue ?");
				if(!sure) return;


			}

			log.debug(" PickCells logging off ");

			// Stop user session -> will lead to Scope event where we remove wb refs
			scopeMgr.stopScope(userScope);

		}


	}








	//==========================================================================//
	//                        Scope Events										//
	//==========================================================================//
	// Listening on SANDBOX / USER-SESSION / WORKBENCH




	private RuntimeScope getOrStartSharedResources() throws ScopeConfigurationException, Exception {

		// Start SANDBOX
		RuntimeScope resourceScope = scopeMgr.getScope(KnownScopes.IMAGE_RESOURCES, KnownScopes.IMAGE_RESOURCES);
		// If first time to start
		if(resourceScope == null) {
			// Get the Scope
			resourceScope = scopeMgr.newScopeInstance(KnownScopes.IMAGE_RESOURCES, KnownScopes.IMAGE_RESOURCES, scopeMgr.getApplicationScope());
			scopeMgr.startScope(resourceScope);
		}
		return resourceScope;		
	}



	@Override
	public void beforeStop(RuntimeScope willStop) {
		log.debug("PickCellsControl: "+willStop+" with id "+willStop.id()+" will stop ");
		if(willStop.name().equals(KnownScopes.USER_SESSION)) {			
			new ArrayList<>(wbScopes).forEach(wb->scopeMgr.stopScope(wb));
			wbScopes.clear();
		}
	}




	@Override
	public void afterStop(RuntimeScope hasStopped) {
		log.debug("PickCellsControl: "+hasStopped+" has stopped ");

		if(hasStopped.name().equals(KnownScopes.USER_SESSION)) {
			view.setPerspective(Perspective.WELCOME);
			userScope = null;
		}
		else if(hasStopped.name().equals(KnownScopes.WORKBENCH)) {
			wbScopes.remove(hasStopped);
		}
	}




	@Override
	public void beforeStart(RuntimeScope willStart) {
		log.debug("PickCellsControl: "+willStart+" with id "+willStart.id()+" will start ");

	}




	@Override
	public void afterStart(RuntimeScope hasStarted) {

		log.debug("PickCellsControl: "+hasStarted+" has started ");

		if(hasStarted.name().equals(KnownScopes.USER_SESSION)) {
			userScope = hasStarted;
			view.setPerspective(Perspective.OVERVIEW);
		}
		else if(hasStarted.name().equals(KnownScopes.WORKBENCH)) {
			wbScopes.add(hasStarted);
		}
	}





}
