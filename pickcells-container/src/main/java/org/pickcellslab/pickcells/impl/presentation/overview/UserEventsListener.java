package org.pickcellslab.pickcells.impl.presentation.overview;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.pickcells.impl.presentation.admin.ExperimentImplForAdmins;
import org.pickcellslab.pickcells.impl.presentation.admin.UserImplForAdmins;

public interface UserEventsListener {

	public void userCreated(UserImplForAdmins deleted);
	
	public void userDeleted(UserImplForAdmins deleted);
	
	public void experimentAdded(UserImplForAdmins source, Link linkToExp);
	
	public void experimentDeleted(UserImplForAdmins loggedIn, ExperimentImplForAdmins experiment);
	
	public void sharingPropertiesUpdated(ExperimentImplForAdmins shared);

	public void experimentUpdated(ExperimentImplForAdmins e);
	
}
