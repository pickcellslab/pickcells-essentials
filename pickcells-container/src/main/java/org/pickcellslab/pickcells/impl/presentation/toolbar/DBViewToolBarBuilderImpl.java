package org.pickcellslab.pickcells.impl.presentation.toolbar;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Level;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.dataviews.mvc.CameleonView;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.dataviews.mvc.DataViewModel;
import org.pickcellslab.foundationj.dataviews.mvc.DefaultDataSet;
import org.pickcellslab.foundationj.dataviews.mvc.DimensionBroker;
import org.pickcellslab.foundationj.dataviews.mvc.FittableDimensionBroker;
import org.pickcellslab.foundationj.dataviews.mvc.FittableModel;
import org.pickcellslab.foundationj.dataviews.mvc.ReconfigurableView;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableDimensionsModel;
import org.pickcellslab.foundationj.dataviews.mvc.TransformableDimensionBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.queryui.QueryWizard;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.DimensionDialog;
import org.pickcellslab.foundationj.ui.MultiDimensionChoice;
import org.pickcellslab.pickcells.api.app.charts.AppearanceFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewToolBarBuilder;
import org.pickcellslab.pickcells.api.app.charts.DataToFileAction;
import org.pickcellslab.pickcells.api.app.charts.FittingChoiceDialog;
import org.pickcellslab.pickcells.api.app.charts.QueryWizardConfigFactory;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebToggleButton;
import com.alee.laf.filechooser.WebFileChooser;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;
import com.alee.managers.tooltip.TooltipManager;

public class DBViewToolBarBuilderImpl implements DBViewToolBarBuilder {

	private final JPanel toolBar = new JPanel(); //TODO Change to list and build on demand the JPanel

	private final DataAccess access;
	private final UITheme theme;
	private final NotificationFactory notif;


	public DBViewToolBarBuilderImpl(DataAccess access, UITheme theme, NotificationFactory notif){
		this.access = access;
		this.theme = theme;
		this.notif = notif;
	}


	@Override
	public DBViewToolBarBuilder addButton(Icon icon, String tooltip, ActionListener l) {

		Objects.requireNonNull(icon, "icon is null");
		Objects.requireNonNull(l, "listener is null");

		JButton button = new JButton(icon);
		TooltipManager.addTooltip(button, tooltip, TooltipWay.down, 0);
		button.addActionListener(l);

		toolBar.add(button);

		return this;
	}


	@Override
	public <T,D> DBViewToolBarBuilder addChangeDataSetButton(QueryWizardConfigFactory<T,D> configFactory, DataViewModel<T> model) {

		Objects.requireNonNull(configFactory, "factory is null");
		Objects.requireNonNull(model, "model is null");


		JButton button = new JButton(theme.icon(IconID.Misc.DATA_TABLE, 16));
		TooltipManager.addTooltip(button, "Select data", TooltipWay.down, 0);
		button.addActionListener(new ActionListener(){	

			private QueryWizard<T,D> w;

			@Override
			public void actionPerformed(ActionEvent e) {
				try {

					if(w == null)
						w = new QueryWizard<>(theme, access, configFactory.createConfig());
					w.pack();
					w.setLocationRelativeTo(null);
					w.setModal(true);
					w.setVisible(true);

					if(!w.wasCancelled())
						model.setDataSet(new DefaultDataSet<>(access, w.getDataset(), w.getInfos()));

				} catch (Exception e1) {
					notif.display("Error", "An error occured while building the dataset", e1, Level.WARNING);
					return;
				}
			}

		});

		toolBar.add(button);

		return this;
	}



	public JPanel build() {
		((FlowLayout)toolBar.getLayout()).setAlignment(FlowLayout.LEFT);
		return toolBar;
	}


	@Override
	public <M extends RedefinableDimensionsModel<T,B,D>, B extends DimensionBroker<T,D>,D,T> DBViewToolBarBuilder addChangeDimensionsButton(
			ReconfigurableView<M,T,B,D> view) {


		M model = view.getModel();

		// Create the button and make sure it is enabled only when the model contains a dataset
		JButton button = new JButton(theme.icon(IconID.Charts.AXES, 16));
		TooltipManager.addTooltip(button, "Change dimensions", TooltipWay.down, 0);
		button.setEnabled(model.getDataSet()!=null);
		model.addDataEvtListener((o,n)-> button.setEnabled(model.getDataSet()!=null));

		//Create the actionlistener when the button is clicked
		button.addActionListener(l->{
			//Redefine the dimensions 
			String queryId = view.getQueryUnderFocus();
			if(queryId == null){
				JOptionPane.showMessageDialog(null, "No series selected");
				return;
			}

			int queryIndex = model.getDataSet().queryIndex(queryId);		
			final List<DimensionContainer<T,D>> dims = model.possibleDimensions(queryIndex);

			if(dims.isEmpty()){
				JOptionPane.showMessageDialog(null, "The target of the current query has no displayable dimensions");
				return;
			}

			//Display UI to select the desired dims
			final String[] names = new String[view.numDimensionalAxes()];
			final Dimension[] selected = new Dimension[view.numDimensionalAxes()];
			for(int i = 0; i<names.length; i++){
				names[i] = view.axisName(i);
				selected[i] = view.currentAxisDimension(i);
			}



			final DimensionDialog<T,D> ui = new DimensionDialog<>(dims, names, selected, model.getDimensionMode(), theme);
			if(JOptionPane.showConfirmDialog(null,ui,"",JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
				return;		

			model.setBroker(queryId, model.createBroker(ui.orderedChoice()));
		});

		toolBar.add(button);


		return this;

	}





	@Override
	public <M extends RedefinableDimensionsModel<T,B,D>, B extends DimensionBroker<T,D>,D,T extends DataItem> DBViewToolBarBuilder addExportDataButton(
			ReconfigurableView<M,T,B,D> view) {

		M model = view.getModel();

		// Create the button and make sure it is enabled only when the model conatins a dataset
		JButton button = new JButton(theme.icon(IconID.Files.EXPORT, 16));
		TooltipManager.addTooltip(button, "Export the current data", TooltipWay.down, 0);
		button.setEnabled(model.getDataSet()!=null && view.currentAxisDimension(0)!=null);

		model.addBrokerChangedListener((s,q,o,n)-> button.setEnabled(model.getDataSet()!=null && view.currentAxisDimension(0)!=null));		

		button.addActionListener(l->{



			File f = WebFileChooser.showSaveDialog();
			if(f==null)
				return;
			else{
				String path = f.getParent();
				String prefix = f.getName();
				List<Dimension<T,?>> dims = new ArrayList<>();
				for(int i = 0; i<view.numDimensionalAxes(); i++)
					dims.add(view.currentAxisDimension(i));		

				//Allow the user to choose other dimensions				
				final MetaQueryable mq = model.getDataSet().queryInfo(0).get(QueryInfo.TARGET);
				if(mq!=null){
					final List<Dimension<?,?>> toChooseFrom = new ArrayList<>();
					mq.getReadables().forEach(mr->{
						for(int d = 0; d<mr.numDimensions(); d++) {
							final Dimension<?,?> dim = mr.getDimension(d);
							if(!dims.contains(dim))
								toChooseFrom.add(dim);
						}
					});
					@SuppressWarnings({ "unchecked", "rawtypes" })
					MultiDimensionChoice choices = new MultiDimensionChoice((List)toChooseFrom, theme);
					if(JOptionPane.showConfirmDialog(null, choices, "Choose other dimensions to be written to file", JOptionPane.OK_CANCEL_OPTION)==JOptionPane.OK_OPTION){
						dims.addAll(choices.get());
					}else
						return;
				}



				for(int q = 0; q<model.getDataSet().numQueries(); q++){
					try {						
						model.getDataSet().loadQuery(q, new DataToFileAction<>(dims,path,prefix));
					} catch (Exception e) {
						notif.display("Error exporting data", "", e, Level.WARNING);
						return;
					}
				}
				WebNotification wn = NotificationManager.showNotification("Data Exported!");
				wn.setDisplayTime(1000);
			}
		});

		toolBar.add(button);

		return this;
	}







	@Override
	public <V extends CameleonView<A> & DataView<?,?>, A> DBViewToolBarBuilder addCameleonControl(V view, AppearanceFactory<A> factory) {

		JButton button = new JButton(theme.icon(IconID.Misc.PAINT, 16));
		TooltipManager.addTooltip(button, "Change appearance", TooltipWay.down, 0);

		button.addActionListener(l->{

			if(factory == null){
				JOptionPane.showMessageDialog(null, "Not yet implemented, Coming Soon!");
				return;
			}


			if(view.getModel().getDataSet() == null){
				JOptionPane.showMessageDialog(null, "No data displayed just yet.");
				return;
			}
			// Identify all series in the datase and use combobox to determine the focus
			//Create a wizard with a card panel using the AppearanceFactory

			CameleonChoiceDialog<V,A> dg = new CameleonChoiceDialog<>(view,factory);
			dg.pack();
			dg.setLocationRelativeTo(null);
			dg.setModal(true);
			dg.setVisible(true);


		});


		toolBar.add(button);

		return this;
	}


	@Override
	public <T,R,D> DBViewToolBarBuilder addDistributionFittingControl(FittableModel<T,R,D> model) {

		JButton button = new JButton(theme.icon(IconID.Charts.FIT, 16));
		TooltipManager.addTooltip(button, "Fit data with a statistical distribution", TooltipWay.down, 0);

		button.addActionListener(l->{
			//TODO UI -> select
			// Identify all series in the dataset and use combobox to determine the focus
			// display available fitting methods

			FittingChoiceDialog dialog = new FittingChoiceDialog(model);
			dialog.pack();
			dialog.setLocationRelativeTo(null);
			dialog.setModal(true);
			dialog.setVisible(true);

			if(!dialog.wasCancelled()){
				FittableDimensionBroker<T,R,D> b = model.dimensionBroker(dialog.getChosenQuery());

				int[] dims = new int[b.numFittableDimensions()-1];
				for(int i = 1; i<b.numFittableDimensions(); i++)
					dims[i-1] = i;

				Thread t = new Thread(()->{
					try {
						b.fitRequest(dialog.getChosenQuery(), dialog.getChosenMethod(), 0, dims);
					} catch (Exception e) {
						notif.display("Error","An exception occured while fitting the data", e, Level.WARNING);
					}
				});

				notif.waitAnimation(t, "Fit in progress...");

			}

		});


		toolBar.add(button);	


		return this;
	}

	@Override
	public <M extends RedefinableDimensionsModel<T,B,D>, B extends TransformableDimensionBroker<T,D>,D,T> 
	DBViewToolBarBuilder addTransformDimensionControl(ReconfigurableView<M,T,B,D> view, String tooltip){

		return this.addButton(theme.icon(IconID.Misc.MAGIC, 16),
				tooltip,

				l->{

					B broker = view.getModel().dimensionBroker(view.getQueryUnderFocus());
					if(broker == null){
						JOptionPane.showMessageDialog(null, "No dimensions selected yet!");
						return;
					}

					int n = broker.numAvailableTransforms(0);
					final String none = "None";
					final String[] tr = new String[n+1];
					tr[0] = none;
					for(int i = 0; i<n; i++)
						tr[i+1] = broker.transformName(i);

					//		System.out.println("Transform names : "+Arrays.toString(tr));

					String choice = (String) JOptionPane.showInputDialog(null, 
							"Choose a Transform...",
							"Transform",
							JOptionPane.QUESTION_MESSAGE, 
							null, 
							tr, 
							tr[0]);

					//	System.out.println("Choice is : "+Objects.toString(choice));

					if(choice == null)
						return;

					if(choice == none){
						broker.removeTransform(0);
						return;
					}


					for(int i = 0; i<n; i++)
						if(tr[i+1].equals(choice))
							broker.setTransform(0, i);

				});		
	}




	/**
	 * Adds a group of toggle buttons allowing the user to choose between objects a bit like radio buttons
	 * @param icons The icons to display for each button
	 * @param choices The objects corrsponding to the provided icons
	 * @param tooltips The tooltips for each button
	 * @param consumer The consumer which will process the object when selected
	 * @return The updated DBViewToolBarBuilder
	 */
	@Override
	public <T> DBViewToolBarBuilder addToggleControl(Icon[] icons, String[] tooltips, T[] choices, Consumer<T> consumer){

		Objects.requireNonNull(icons,"icons is null");
		Objects.requireNonNull(choices,"choices is null");
		Objects.requireNonNull(consumer,"consumer is null");

		if(icons.length != tooltips.length || icons.length != choices.length)
			throw new IllegalArgumentException("Arrays do not have the same length");

		// Iconed buttons group

		WebToggleButton[] btns = new WebToggleButton[icons.length];
		for(int i = 0; i<icons.length; i++){
			btns[i] = new WebToggleButton (icons[i]);
			final int j = i;
			btns[i].addActionListener(l->consumer.accept(choices[j]));
			TooltipManager.addTooltip(btns[i], tooltips[i]);

		}
		WebButtonGroup iconsGroup = new WebButtonGroup ( true, btns );
		toolBar.add(iconsGroup);

		return this;
	}





}
