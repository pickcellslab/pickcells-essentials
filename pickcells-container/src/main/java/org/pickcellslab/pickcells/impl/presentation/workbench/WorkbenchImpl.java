package org.pickcellslab.pickcells.impl.presentation.workbench;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.annotations.ScopeOption;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.ManuallyStored;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.queryui.ChoicePanel;
import org.pickcellslab.foundationj.queryui.ChoicePanels;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Wizard;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.pickcellslab.pickcells.api.app.modules.Categorized;
import org.pickcellslab.pickcells.api.app.modules.DefaultDocument;
import org.pickcellslab.pickcells.api.app.modules.MenuEntry;
import org.pickcellslab.pickcells.api.app.modules.StepwiseProcess;
import org.pickcellslab.pickcells.api.app.modules.Task;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;
import org.pickcellslab.pickcells.api.presentation.workbench.WorkbenchElement;
import org.pickcellslab.pickcells.api.util.UsefulQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.tab.DocumentData;
import com.alee.extended.tab.DocumentListener;
import com.alee.extended.tab.PaneData;
import com.alee.extended.tab.WebDocumentPane;
import com.alee.laf.button.WebButton;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.progressbar.WebProgressBar;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.tooltip.TooltipManager;

@CoreImpl
public class WorkbenchImpl implements Workbench{

	private static Logger log = LoggerFactory.getLogger(WorkbenchImpl.class);


	// Load preferences and default geometry
	//private final Preferences cfg = Preferences.userNodeForPackage(Workbench.class);
	public static final Rectangle GEOMETRY = new Rectangle(0,0,640,480);

	private final UITheme theme;
	private final NotificationFactory notif;

	private final WebPanel mainContainer;
	private final WebPanel documentsContainer;
	private final JMenuBar menuBar;
	private boolean sceneLoaded = false;

	//Stores the components to place into the toolbar grouped by categories
	private final TreeMap<String,Map<Categorized,Component>> tools = new TreeMap<>();



	private Map<String, UIDocument> activeDocuments = new HashMap<>();


	private final String title;






	/**
	 * Initialize the workbench
	 * @param elements 
	 */
	public WorkbenchImpl(UITheme theme, NotificationFactory notif, DBViewHelp helper, DataAccess access, Collection<WorkbenchElement> elements, @ScopeOption Experiment e) {

		this.theme = theme;
		this.notif = notif;

		Objects.requireNonNull(e);
		this.title = e.getName();


		new ExperimentIntegrityCheck(e, access).performCheck();

		// Contains menubar and documentsContainer in the center
		mainContainer = new WebPanel();
		mainContainer.setLayout(new BorderLayout());


		//Setup the Menu 
		menuBar = buildMenuBar(access, elements.stream()
				.filter(we->MenuEntry.class.isAssignableFrom(we.getClass()))
				.map(we->(MenuEntry)we)
				.collect(Collectors.toList()));		
		mainContainer.add(menuBar, BorderLayout.NORTH);



		documentsContainer = new WebPanel();
		documentsContainer.setLayout(new BorderLayout());

		final WebPanel tasksBar = buildTaskBar(access, elements.stream()
				.filter(we->Task.class.isAssignableFrom(we.getClass()))
				.map(we->(Task)we)
				.collect(Collectors.toList()));
		// Add the vertical scrollbar, it always appears and can be scrolled with bar or mouse wheel
		JScrollPane spVert = new JScrollPane(tasksBar, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		documentsContainer.add(spVert, BorderLayout.WEST);
		
		final WebDocumentPane<DocumentData<Component>> web = buildDefaultView(access, elements.stream()
				.filter(we->DefaultDocument.class.isAssignableFrom(we.getClass()))
				.map(we->(DefaultDocument)we)
				.collect(Collectors.toList()));
		documentsContainer.add(web, BorderLayout.CENTER);

		final WebPanel viewsBar = buildViewsBar(helper, access, elements.stream()
				.filter(we->DBViewFactory.class.isAssignableFrom(we.getClass()))
				.map(we->(DBViewFactory)we)
				.collect(Collectors.toList()),
				web);
		// Add the horizontal scrollbar, it only appears if needed
		JScrollPane spHorz = new JScrollPane(viewsBar, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		documentsContainer.add(spHorz, BorderLayout.NORTH);


		// Add documentContainer to mainContainer
		mainContainer.add(documentsContainer, BorderLayout.CENTER);

		// Finally add listener to handle closed documents
		web.addDocumentListener(new DocumentListener<DocumentData<Component>>(){
			@Override
			public void closed(DocumentData<Component> arg0, PaneData<DocumentData<Component>> arg1, int arg2) {
				UIDocument doc = activeDocuments.get(arg0.getId());
				if(doc!=null){
					doc.close();
					activeDocuments.remove(arg0.getId());
				}
			}

			@Override
			public boolean closing(DocumentData<Component> arg0, PaneData<DocumentData<Component>> arg1, int arg2) {
				return true;
			}

			@Override
			public void opened(DocumentData<Component> arg0, PaneData<DocumentData<Component>> arg1, int arg2) {}

			@Override
			public void selected(DocumentData<Component> document, PaneData<DocumentData<Component>> pane, int index) {}

		});




	}



	//MenuBar
	//-------------------------------------------------------------------------------------------------
	private JMenuBar buildMenuBar(DataAccess session, List<MenuEntry> entries) { 



		// Menus from entries
		//-----------------------------------------
		final WorkbenchMenu menuBar = new WorkbenchMenu(entries);	


		// Default Data
		//-----------------------------------------

		JMenu mnData = new JMenu("Data");
		menuBar.add(mnData);

		JMenuItem filterMenuItem = new JMenuItem("New Filter");
		mnData.add(filterMenuItem);

		filterMenuItem.addActionListener(l->{

			// First choose the MetaQueryable
			try {

				ChoicePanel<MetaQueryable> mqChoice = ChoicePanels.allQueryable(theme, session, "Choose a type of object" );
				mqChoice.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				if(JOptionPane.showConfirmDialog(null, mqChoice, "Custom Filter ...",JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION){
					Wizard.showWizardDialog(
							ChoicePanels.customFilter(theme, session, mqChoice.getSelected().get()).create())
					.ifPresent(mf ->{
						try {
							session.queryFactory().meta(mf).run();
						} catch (Exception e) {
							notif.display("Error", "Unable to save the new Attribute", e, Level.WARNING);
						}
					});
				}			

			} catch (Exception e) {
				notif.display("Error", "Unable to create a new Filter", e, Level.WARNING);
			}

		});


		JMenuItem attMenuItem = new JMenuItem("New Attribute");
		mnData.add(attMenuItem);

		attMenuItem.addActionListener(l->{

			// First choose the MetaQueryable
			try {

				ChoicePanel<MetaQueryable> mqChoice = ChoicePanels.allQueryable(theme, session, "Choose a type of object" );
				mqChoice.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				if(JOptionPane.showConfirmDialog(null, mqChoice, "Custom Attribute ...",JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION){
					Wizard.showWizardDialog(
							ChoicePanels.customFeatureFactory(theme, session, () -> mqChoice.getSelected().get()).create())
					.ifPresent(mr ->{
						try {
							session.queryFactory().meta((ManuallyStored) mr).run();
						} catch (Exception e) {
							notif.display("Error", "Unable to save the new Attribute", e, Level.WARNING);
						}
					});
				}			

			} catch (Exception e) {
				notif.display("Error", "Unable to create a new Attribute", e, Level.WARNING);
			}


		});


		/*
		JMenuItem hideMenuItem = new JMenuItem("MetaModel Visibility");
		mnData.add(hideMenuItem);

		hideMenuItem.addActionListener(l->{
			//TODO but do this in prefuse package as a MenuEntry
			JOptionPane.showMessageDialog(null, "Coming soon (Workbench.class)");		
		});
		 */




		// Locations
		//-----------------------------------------



		JMenu mnLocs = new JMenu("Locations");
		menuBar.add(mnLocs);

		JMenuItem openMenuItem = new JMenuItem("Logs Folder");
		openMenuItem.addActionListener(l->{

			File file = null;

			try {

				if (Desktop.isDesktopSupported()) {

					Desktop desktop = Desktop.getDesktop();
					String path = System.getProperty("user.dir")+File.separator+"logs";
					file = new File(path);
					desktop.open(file);
				}
				else {
					JOptionPane.showMessageDialog(null, "Sorry, not supported on your plateform");
				}
			}
			catch (Exception e){
				notif.display("Error", "Unable to open "+file.toString(), e, Level.WARNING);
			}

		});
		mnLocs.add(openMenuItem);

		JMenuItem openDBItem = new JMenuItem("Database Folder");
		openDBItem.addActionListener(l->{

			File file = null;

			try {

				if (Desktop.isDesktopSupported()) {

					Desktop desktop = Desktop.getDesktop();
					String path = UsefulQueries.experimentFolder(session);
					file = new File(path);
					desktop.open(file);
				}
				else {
					JOptionPane.showMessageDialog(null, "Sorry, not supported on your plateform");
				}
			}
			catch (Exception e){
				notif.display("Error", "Unable to open "+file.toString(), e, Level.WARNING);
			}

		});
		mnLocs.add( openDBItem);






		// Help
		//-----------------------------------------


		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);


		JMenuItem wikiMenuItem = new JMenuItem("Wiki");
		mnHelp.add(wikiMenuItem);
		wikiMenuItem.addActionListener(l->{
			if(Desktop.isDesktopSupported()){
				try {
					Desktop.getDesktop().browse(new URI("https://www.wiki.ed.ac.uk/display/PCUW/PickCells+Users+Wiki+Home"));
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Sorry this operation is not supported for your OS. The URL is : https://www.wiki.ed.ac.uk/display/PCUW/PickCells+Users+Wiki+Home");
				}
			}
			else
				JOptionPane.showMessageDialog(null, "Sorry this operation is not supported for your OS. The URL is : https://www.wiki.ed.ac.uk/display/PCUW/PickCells+Users+Wiki+Home");
		});



		JMenuItem mailMenuItem = new JMenuItem("Mail Author...");
		mailMenuItem.addActionListener(l->{
			try {

				if (Desktop.isDesktopSupported()) { //TODO replace by bug tracker
					mailto(
							Arrays.asList("guillaume.blin@ed.ac.uk"), 
							"PickCells Help Request",
							"Describe your problem..."
							);
				}
				else {
					JOptionPane.showMessageDialog(null, "Sorry, not supported on your plateform");
				}
			}
			catch (Exception e){
				notif.display("Error", "", e, Level.WARNING);
			}
		});
		mnHelp.add(mailMenuItem);

		JMenuItem mntmAbout = new JMenuItem("About...");
		mntmAbout.addActionListener(l->JOptionPane.showMessageDialog(null, "Not yet implemented")); //TODO licenses per module
		mnHelp.add(mntmAbout);

		return menuBar;
	}










	private WebPanel buildTaskBar(DataAccess access, List<Task> collect) {


		collect.forEach(task->addTask(task));


		final WebPanel taskBar = new WebPanel(new VerticalFlowLayout());

		for(String category : tools.navigableKeySet()){			

			log.debug("Adding category in tool bar : " + category);

			Map<Categorized, Component> categoryMap = tools.get(category);
			WebToolBar bar = new WebToolBar(JToolBar.VERTICAL);
			bar.setToolbarStyle(ToolbarStyle.attached);

			for(Entry<Categorized, Component> module : categoryMap.entrySet())
				bar.add(module.getValue());

			taskBar.add(bar);
		}



		return taskBar;
	}









	private WebDocumentPane<DocumentData<Component>> buildDefaultView(DataAccess access,
			List<DefaultDocument> collect) {

		//System.out.println("WorkbenchImpl: number of default docs : "+collect.size());

		final WebDocumentPane<DocumentData<Component>> web = new WebDocumentPane<>();

		for(DefaultDocument factory : collect) {

			//System.out.println("WorkbenchImpl: DefaultDocument name : "+factory.name());

			DocumentData<Component> p = new DocumentData<>( 
					factory.name(),
					UITheme.resize(factory.icon(),12, 12),
					factory.name(),
					Color.WHITE,
					factory.getScene());
			p.setCloseable(false);

			if(web.getAllSplitPanes().isEmpty()){		
				web.openDocument (p);	
				if(collect.size()>1) {
					if(factory.preferredLocation() == DefaultDocument.UP){
						web.split(p, WebDocumentPane.TOP);
					}
					else{
						web.split(p, WebDocumentPane.BOTTOM);
					}
					web.setSelected(p);
				}
			}

			else{

				if(factory.preferredLocation() == DefaultDocument.UP)
					web.getAllSplitPanes().get(0).getFirst().findClosestPane().open(p);
				else
					web.getAllSplitPanes().get(0).getLast().findClosestPane().open(p);

				web.setSelected(p);
			}
		}

		//System.out.println("WorkbenchImpl: Number of split panes : "+web.getAllSplitPanes().size());

		//layout defaultdocuments
		if(web.getAllSplitPanes().size()!=0)
			web.getAllSplitPanes().get(0).setDividerLocation(0.9);


		web.setBorder(BorderFactory.createLineBorder(Color.GRAY));

		return web;
	}






	private WebPanel buildViewsBar(DBViewHelp helper, DataAccess access, List<DBViewFactory> dbFactories, WebDocumentPane<DocumentData<Component>> web) {


		final WebPanel toolBar = new WebPanel();
		toolBar.setLayout(new FlowLayout());

		for(DBViewFactory f : dbFactories){

			final WebButton b = new WebButton(UITheme.resize(f.icon(), 24, 24));
			//b.setRolloverDecoratedOnly(true);
			TooltipManager.setTooltip(b, f.description(), TooltipWay.down, 0);
			b.addActionListener(l->{
				try {
					UIDocument doc;
					doc = f.createView(access, helper);
					if(null != doc) {
						//Set the top pane selected to make sure we add the new document to the top view
						if(web.getAllSplitPanes().size()!=0)
							web.setSelected(web.getAllSplitPanes().get(0).getFirst().findClosestPane().get(0));
						String id = doc.id();
						id += " "+web.getDocuments().stream().filter(d->d.getId().contains(doc.id())).count()+1;
						DocumentData<Component> p = new DocumentData<>( 
								id,
								UITheme.resize(doc.icon(), 12, 12),
								doc.id(),
								Color.WHITE,
								doc.scene());
						activeDocuments.put(id,doc);
						web.openDocument (p);
						web.setSelected(p);
					}
				} catch (ViewFactoryException e) {
					notif.display("View Initialisation Error", "", e, Level.WARNING);
					return;
				}
			});

			toolBar.add(b);

		}	


		return toolBar;
	}







	@Override
	public String title() {
		return title;
	}











	@Override
	public JComponent getPanel() {
		return mainContainer;
	}



	private void addTask(Task task){

		Objects.requireNonNull(task, "The provided ViewFactory is null");

		//Check the factory does not return a null component or a component that is too large
		Icon comp = task.icon();
		if(null == comp) {
			log.warn("The Analysis with name \""+task.name()+"\" has returned a null icon and was therefore disabled");
			return;
		}
		if(comp.getIconWidth() != 24)
			comp = UITheme.resize(UITheme.toImageIcon(comp), 24, 24);

		WebButton btn = new WebButton(comp);
		TooltipManager.setTooltip(btn, task.description());
		//btn.setRolloverDecoratedOnly(true);

		btn.addActionListener(l-> new Thread(()->{

			WebProgressBar progressBar = new WebProgressBar ();
			WebToolBar bar = new WebToolBar();
			bar.setToolbarStyle(ToolbarStyle.attached);
			bar.add(progressBar);
			try{

				progressBar.setIndeterminate ( true );
				progressBar.setStringPainted ( true );
				progressBar.setString ( task.name()+" is running..." );				
				documentsContainer.add(bar,BorderLayout.SOUTH);
				task.launch();				

			}catch(Exception e){
				notif.display("Error", "An exception occured while running "+task.name(), e, Level.WARNING);
				documentsContainer.remove(bar);
				documentsContainer.revalidate();
				documentsContainer.repaint();
				if(StepwiseProcess.class.isAssignableFrom(task.getClass())){
					((StepwiseProcess)task).getPublishers().forEach(p->p.failure((StepwiseProcess) task, e.getMessage()));
				}
				return;
			}

			documentsContainer.remove(bar);
			documentsContainer.revalidate();
			documentsContainer.repaint();

			if(StepwiseProcess.class.isAssignableFrom(task.getClass())){
				((StepwiseProcess)task).getPublishers().forEach(pub->pub.setStep((StepwiseProcess) task, ((StepwiseProcess) task).steps().length));
			}

			//NotificationManager.showNotification(task.name()+" Done!", task.icon());			

		}).start());
		sortCategory(task,btn);


	}






	private void sortCategory(Categorized c, Component comp){		
		Map<Categorized,Component> cat = tools.get(c.categories()[0]);
		if(null == cat){
			cat = new HashMap<>();
			cat.put( c, comp);
			tools.put((String) c.categories()[0], cat);
		}
		else
			cat.put(c, comp);		
	}






















	/*
	 * 
	 * Obtained from http://www.2ality.com/2010/12/simple-way-of-sending-emails-in-java.html
	 * 
	 */


	private static void mailto(List<String> recipients, String subject,
			String body) throws IOException, URISyntaxException {
		String uriStr = String.format("mailto:%s?subject=%s&body=%s",
				join(",", recipients), // use semicolon ";" for Outlook!
				urlEncode(subject),
				urlEncode(body));
		Desktop.getDesktop().browse(new URI(uriStr));
	}

	private static final String urlEncode(String str) {
		try {
			return URLEncoder.encode(str, "UTF-8").replace("+", "%20");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static final String join(String sep, Iterable<?> objs) {
		StringBuilder sb = new StringBuilder();
		for(Object obj : objs) {
			if (sb.length() > 0) sb.append(sep);
			sb.append(obj);
		}
		return sb.toString();
	}




}
