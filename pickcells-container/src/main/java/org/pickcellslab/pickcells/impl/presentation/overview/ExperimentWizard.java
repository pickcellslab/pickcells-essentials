package org.pickcellslab.pickcells.impl.presentation.overview;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.File;
import java.util.Optional;
import java.util.logging.Level;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.impl.presentation.admin.ExperimentImplForAdmins;


@SuppressWarnings("serial")
public class ExperimentWizard extends JDialog {

	private final NotificationFactory notif;
	private final JPanel contentPanel = new JPanel();
	private JTextField nameField;
	private JTextField locationField;


	private ExperimentImplForAdmins experiment;



	/**
	 * Create the dialog.
	 */
	public ExperimentWizard(NotificationFactory notif) {

		this.notif = notif;

		setTitle("Experiment Wizard");
		setBounds(100, 100, 387, 252);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		JLabel lblName = new JLabel("Name");

		nameField = new JTextField();
		nameField.setColumns(10);

		JLabel lblLocation = new JLabel("Location");

		locationField = new JTextField();
		locationField.setColumns(10);

		JButton browsebtn = new JButton("Browse...");
		browsebtn.addActionListener(a->{
			File workingDirectory = new File(System.getProperty("user.dir"));
			JFileChooser chooser = new JFileChooser(workingDirectory.getParentFile().getParent());
			chooser.setDialogTitle("Select the folder in which to create your database for this experiment");
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);

			if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
				locationField.setText(chooser.getSelectedFile().toString());
		});




		JLabel lblDescription = new JLabel("Description");

		JTextArea descriptionField = new JTextArea();
		JScrollPane scroll = new JScrollPane(descriptionField);
		GroupLayout groupLayout = new GroupLayout(contentPanel);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblLocation)
								.addComponent(lblName)
								.addComponent(lblDescription))
								.addGap(12)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
												.addComponent(locationField, GroupLayout.DEFAULT_SIZE, 175, Short.MAX_VALUE)
												.addGap(18)
												.addComponent(browsebtn))
												.addComponent( scroll , GroupLayout.DEFAULT_SIZE, 272, Short.MAX_VALUE)
												.addComponent(nameField, GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE))
												.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblName)
								.addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblLocation)
										.addComponent(locationField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(browsebtn))
										.addGap(18)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblDescription)
												.addComponent( scroll , GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE))
												.addContainerGap(53, Short.MAX_VALUE))
				);
		contentPanel.setLayout(groupLayout);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(a->{
					String name = nameField.getText();
					if(name.isEmpty()){
						JOptionPane.showMessageDialog(null, "Please define a name for your experiment");
						return;
					}
					String location = locationField.getText()+File.separator+name;
					if(location.isEmpty()){
						JOptionPane.showMessageDialog(null, "Please define the location of your experiment");
						return;
					}
					String description = descriptionField.getText();
					if(description.isEmpty()){
						descriptionField.setText("Not available");
					}

					//Create an empty folder
					if(!new File(location).mkdir()){
						notif.display("Access Denied", "Unable to create a directory at "+location, null, Level.SEVERE);
						return;
					}

					experiment = new ExperimentImplForAdmins(name, location, description);
					this.dispose();

				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(e->{
					this.dispose();
				});
				buttonPane.add(cancelButton);
			}
		}
	}

	public Optional<ExperimentImplForAdmins> showDialog(){
		this.setModal(true);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		return Optional.ofNullable(experiment);
	}




}
