package org.pickcellslab.pickcells.impl.presentation.admin;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.scope.AppController;
import org.pickcellslab.foundationj.scope.RuntimeScope;
import org.pickcellslab.foundationj.scope.ScopeManager;
import org.pickcellslab.pickcells.api.app.data.User;
import org.pickcellslab.pickcells.api.app.data.User.Priviledge;
import org.pickcellslab.pickcells.api.presentation.admin.AdminAccess;
import org.pickcellslab.pickcells.impl.presentation.KnownScopes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CoreImpl
public class AdminController extends AppController implements AdminAccess{

	// Access to the admin database
	private final DataAccess access;
	
	private static final Logger log = LoggerFactory.getLogger(AdminController.class);

	public AdminController(ScopeManager model, DataAccess access) {
		super(model);
		this.access = access;
	}


	//==========================================================================//
	//                        Scope Control										//
	//==========================================================================//
	// Controlling USER-SESSION (on admin db)

	// Start login and if returns Connexion Request --> startScope USER-SESSION

	public void performLogging() {

		// Check if this is the first connexion
		try {

			User loggedIn = null;

			long numberOfUsers = access.queryFactory().read(DataRegistry.typeIdFor(UserImplForAdmins.class)).count().inOneSet().getAll().run();

			if(numberOfUsers == 0) { // We need to ask for a new Administrator Credentials

				final LoginDialog d = new LoginDialog(null, true, new NewUserValidator(Collections.emptySet()));
				d.pack();
				d.setLocationRelativeTo(null);
				d.setVisible(true);

				if(!d.wasCancelled()) {
					// create the new user as administrator
					loggedIn = new UserImplForAdmins(d.userName(), d.password(), Priviledge.ADMIN);
					access.queryFactory().store().addStandAlone((UserImplForAdmins)loggedIn).run();
				}

			}
			else {
				// Load all users
				final RegeneratedItems users =
						access.queryFactory().regenerate(DataRegistry.typeIdFor(UserImplForAdmins.class)).toDepth(2)
						.traverseAllLinks().includeAllNodes()
						.regenerateAllKeys().getAll();

				final Map<String,String> credentials = new HashMap<>();
				final AKey<String> pKey = AKey.get("password", String.class);
				users.getTargets(UserImplForAdmins.class).forEach(u->credentials.put(u.userName(), u.getAttribute(pKey).get()));

				final LoginDialog d = new LoginDialog(null, false, new CredentialsValidator(credentials));
				d.pack();
				d.setLocationRelativeTo(null);
				d.setVisible(true);

				if(!d.wasCancelled()) {
					// Figure out the user that has just logged in
					loggedIn = users.getTargets(UserImplForAdmins.class).filter(u->u.userName().equals(d.userName())).findFirst().get();
								
				}

			}




			if(loggedIn == null) {
				scopeMgr.stopScope(scopeMgr.getScope(KnownScopes.ADMIN_ACCESS, KnownScopes.ADMIN_ACCESS));
			}
			else {

				// Start a User Session
				try {

					// Set the logged in user as ScopeOption 
					// The DataAccess will be resolved using the parent child relationship of the user session scope
					// to this Admin-access scope.
					log.debug("Starting the user session for user : "+loggedIn.userName());
					final RuntimeScope adminAccess = scopeMgr.getScope(KnownScopes.ADMIN_ACCESS, KnownScopes.ADMIN_ACCESS);
					final RuntimeScope userSession = scopeMgr.newScopeInstance(KnownScopes.USER_SESSION, KnownScopes.USER_SESSION, adminAccess);
					scopeMgr.startScope(userSession, loggedIn);


				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Could not start the user session. The error has been logged", "Error", JOptionPane.ERROR_MESSAGE);
					scopeMgr.stopScope(scopeMgr.getScope(KnownScopes.ADMIN_ACCESS, KnownScopes.ADMIN_ACCESS));
				}
			}


		} catch (DataAccessException e) {
			log.error("An error occured while accessing the admin database",e);
			throw new RuntimeException("An error occure while accessing the admin database", e);
		}


	}
	
		
	
	
	
	


	//==========================================================================//
	//                        Scope Events										//
	//==========================================================================//
	// Listening only for our own scope in order to remove listener




	@Override
	public void beforeStop(RuntimeScope willStop) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterStop(RuntimeScope hasStopped) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeStart(RuntimeScope willStart) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterStart(RuntimeScope hasStarted) {
		// TODO Auto-generated method stub

	}


	
}
