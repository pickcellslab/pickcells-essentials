package org.pickcellslab.pickcells.impl.presentation.overview;

import javax.swing.JPanel;
import java.awt.Image;
import java.awt.Graphics;

public class ImagePanel extends JPanel {

  private final Image bg;

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawImage(bg, 0, 0, this);
  }

  public ImagePanel(Image bg) {
    this.bg = bg;

  }

}
