package org.pickcellslab.pickcells.impl.presentation.services;

import java.io.File;
import java.util.Objects;
import java.util.prefs.Preferences;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.services.PreferencesAccess;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CoreImpl
public class PreferencesAccessImpl implements PreferencesAccess {

	
	private final Logger log = LoggerFactory.getLogger(PreferencesAccessImpl.class);
	private final DataAccess access;
	
	
	public PreferencesAccessImpl(DataAccess access) {
		this.access = access;
	}
	
	
	public String getLastDirectory(){

		String ld = "";
		try {
			Preferences prefs = Preferences.userRoot();
			ld = prefs.get(
					access.queryFactory()
					.read("Experiment")
					.makeList(Experiment.dbNameKey)
					.inOneSet()
					.getAll()
					.run().get(0),"");

		} catch (DataAccessException e) {
			log.warn("Unable to read the database to retrieve user preferences", e);
		}
		return ld;
	}



	public void setLastDirectory(File directory){


		Objects.requireNonNull(directory);

		String pathToDirectory = directory.getPath();

		if(directory.isDirectory())
			pathToDirectory = directory.getParent();

		try {
			Preferences prefs = Preferences.userRoot();
			prefs.put(
					access.queryFactory()
					.read("Experiment")
					.makeList(Experiment.dbNameKey)
					.inOneSet()
					.getAll()
					.run().get(0), pathToDirectory);
		} catch (DataAccessException e) {
			log.warn("Unable to read the database to retrieve user preferences", e);
		}
	}
	
	
}
