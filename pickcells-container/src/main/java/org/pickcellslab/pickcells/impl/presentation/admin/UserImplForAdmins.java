package org.pickcellslab.pickcells.impl.presentation.admin;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest.Permission;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.pickcellslab.pickcells.api.app.data.User;
import org.pickcellslab.pickcells.api.presentation.admin.AdminAccess;


/**
 * This class defines a User of the application
 * 
 * @author Guillaume
 * 
 */
@Data(typeId = "User")
@SameScopeAs(AdminAccess.class)
public class UserImplForAdmins extends DataNode implements User {


	static final AKey<String> name = AKey.get("name", String.class);
	static final String HAS_ACCESS="HAS ACCESS", MANAGES="MANAGES", CREATED="CREATED";



	/**
	 * @param username
	 * @param password The (encrypted) password - same as {@link ConnexionRequest#userPassword()}
	 * @param p ADMIN or USER
	 */
	public UserImplForAdmins(String username, String password, Priviledge p) {

		setAttribute(AKey.get("name", String.class), username);
		setAttribute(AKey.get("password", String.class), password);
		setAttribute(AKey.get("privilege", String.class), p.name());
		setAttribute(AKey.get("date", String.class), LocalDate.now().toString());

	}

	@SuppressWarnings("unused")
	// used by persistence by reflection
	private UserImplForAdmins(){
		System.out.println("User created from DataBase");		
	}



	@Override
	public Priviledge privilege() {
		return Enum.valueOf(Priviledge.class, getAttribute(AKey.get("privilege", String.class)).get());
	}


	@Override
	public String userName() {
		return getAttribute(name).get();
	}

	public String encryptedPassword() {
		return getAttribute(AKey.get("password", String.class)).get();
	}



	@Override
	public String creationDate() {
		return getAttribute(AKey.get("date", String.class)).orElse("Unknown");
	}




	@Override
	public String toString() {
		return userName();
	}



	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(idKey, AKey.get("privilege", String.class), name, AKey.get("password", String.class));
	}



	//---------------------------------------------------------------------//
	//  Protected methods with Scope Mechanism although methods are public
	//---------------------------------------------------------------------//


	/**
	 * @return A Stream of {@link User}s managed by this User. <b>NB:</b> If this user has no admin {@link Priviledge}, then
	 * this user should return an empty set.
	 */	
	public Stream<UserImplForAdmins> managedUsers(){
		return this.getLinks(Direction.OUTGOING, MANAGES).map(l->(UserImplForAdmins)l.target());
	}



	public List<ExperimentImplForAdmins> getExperiments(){
		Set<Link> set = typeMap.get(HAS_ACCESS);
		if(set == null)
			return Collections.emptyList();
		return set.stream().map(l->(ExperimentImplForAdmins)l.target())
				.sorted((e1,e2)->e1.getName().compareTo(e2.getName()))
				.collect(Collectors.toList());
	}


	/**
	 * @return A Stream of {@link Link}s between this {@link User} (source) and the {@link Experiment}s (target) created by this {@link User}
	 */
	public Stream<Link> getCreatedExperimentsLinks(){
		return getLinks(Direction.OUTGOING, CREATED);
	}



	/**
	 * @return A Stream of {@link Link}s between this {@link User} (target) and the {@link Experiment} (source) shared with this {@link User}
	 */
	public Stream<Link> getExperimentsSharedByOthersLinks() {
		return getLinks(Direction.INCOMING, ExperimentImplForAdmins.SHARED);
	}





	/**
	 * @return A List of {@link Experiment}s created by this {@link User}
	 */
	public List<ExperimentImplForAdmins> getCreatedExperiments(){
		return getCreatedExperimentsLinks()
				.map(l->(ExperimentImplForAdmins)l.target())
				.sorted((e1,e2)->e1.getName().compareTo(e2.getName()))
				.collect(Collectors.toList());
	}



	/**
	 * @return A List of {@link Experiment}s shared by other {@link User}s with this {@link User}.
	 */
	public List<ExperimentImplForAdmins> getExperimentsSharedByOthers() {
		return getExperimentsSharedByOthersLinks()
				.map(l-> (ExperimentImplForAdmins) l.source())
				.collect(Collectors.toList());
	}


	public boolean isCreatedLink(Link l) {
		return l.declaredType().equals(CREATED);
	}


	public boolean isSharedLink(Link l) {
		return l.declaredType().equals(ExperimentImplForAdmins.SHARED);
	}


	public ExperimentImplForAdmins getExperimentFromLink(Link l) {
		if(isCreatedLink(l))
			return (ExperimentImplForAdmins) l.target();
		else if (isSharedLink(l)) {
			return (ExperimentImplForAdmins) l.source();
		}
		else
			return null;
	}

	public Permission permission(Link l) {
		final String perm = l.getAttribute(ExperimentImplForAdmins.permission).orElse(null);
		if(perm ==null)
			return null;
		else
			return Permission.valueOf(perm);
	}



	public Link setAsCreated(ExperimentImplForAdmins root) {
		if(root.getCreator()!=null)
			throw new IllegalArgumentException("Cannot set an experiment as created as this experiment already has a creator");
		return this.addOutgoing(CREATED, root);
	}


	public void addUser(User root) {
		if(this.privilege() == Priviledge.ADMIN)
			this.addOutgoing(MANAGES, root);
		else
			throw new IllegalArgumentException("Cannot add a managed user, this user "+this.userName()+" is not administrator");

	}

	/**
	 * Deletes this User from the database and all experiments created by this User.
	 * @param access The {@link DataAccess} to the database where this User needs to be deleted.
	 * @throws DataAccessException
	 */
	public Set<ExperimentImplForAdmins> delete(DataAccess access) throws DataAccessException {
		
		final int id = getAttribute(idKey).orElse(-1);
		if(id==-1) {
			System.out.println("UserImplForAdmins: This user was never saved to the database...");
			links().forEach(l->l.delete());
			return Collections.emptySet();
		}
		
		// delete all created experiments
		final Set<Integer> set = this.getCreatedExperimentsLinks()
				.map(l->l.target().getAttribute(idKey).get())
				.collect(Collectors.toSet());
		
		access.queryFactory().delete("Experiment").completely()
		.useFilter(P.setContains(idKey, set)).run();
		
		// Keep a ref to all deleted Exps for reference
		final Set<ExperimentImplForAdmins> delExps = this.getCreatedExperimentsLinks()
				.map(l->(ExperimentImplForAdmins)l.target())
				.collect(Collectors.toSet());
				
		// Now delete this User
		access.queryFactory().delete("User").completely()
		.useFilter(F.select(idKey).equalsTo(id)).run();
				
		links().forEach(l->l.delete());
		
		return delExps;
		
	}


}
