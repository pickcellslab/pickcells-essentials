package org.pickcellslab.pickcells.impl.presentation.admin;

import java.awt.Color;
import java.awt.Window;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;

import org.pickcellslab.foundationj.datamodel.tools.MutableBool;

import com.alee.extended.layout.TableLayout;
import com.alee.extended.panel.CenterPanel;
import com.alee.extended.panel.GroupPanel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebDialog;
import com.alee.laf.text.WebPasswordField;
import com.alee.laf.text.WebTextField;
import com.alee.managers.hotkey.Hotkey;
import com.alee.managers.hotkey.HotkeyManager;
import com.alee.utils.SwingUtils;
import com.alee.utils.general.Pair;
import com.alee.utils.swing.DocumentChangeListener;

public class LoginDialog extends WebDialog{

	private final Color invalid = new Color(243,182,177); 
	private final Color valid = new Color(179,247,154); 


	/**
	 * cancelled=-1 / valid = numTextFields / other = invalid
	 */
	private MutableBool wasCancelled = new MutableBool(false);
	private MutableBool confirmValid = new MutableBool(true);

	private Pair<String,String> credentials = new Pair<>();

	
	public LoginDialog ( Window owner, boolean confirmPassword , LoginValidator validator) {
		this(owner, confirmPassword, validator, "Login", "Login");
	}

	public LoginDialog ( Window owner, boolean confirmPassword , LoginValidator validator, String title, String okBtn){

		super ( owner, title );
		//setIconImages ( WebLookAndFeel.getImages () );
		setDefaultCloseOperation ( WebDialog.DISPOSE_ON_CLOSE );
		setResizable ( false );
		setModal ( true );

		TableLayout layout = null;

		if(confirmPassword)
			layout = new TableLayout ( new double[][]{ 
				{ TableLayout.PREFERRED, TableLayout.FILL },// columns width
				{ TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED } // rows height
			} );
		else
			layout = new TableLayout ( new double[][]{ 
				{ TableLayout.PREFERRED, TableLayout.FILL },// columns width
				{ TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED } // rows height
			} );

		layout.setHGap ( 5 );
		layout.setVGap ( 5 );

		final WebPanel content = new WebPanel ( layout );
		content.setMargin ( 15, 30, 15, 30 );
		content.setOpaque ( false );

		final WebTextField nameField = new WebTextField ( 15 );
		nameField.setBackground(invalid);
		content.add ( new WebLabel ( "User Name", WebLabel.TRAILING ), "0,0" );
		content.add ( nameField, "1,0" );

		nameField.getDocument().addDocumentListener(new DocumentChangeListener() {
			@Override
			public void documentChanged(DocumentEvent arg0) {
				if(validator.isUserNameValid(nameField.getText())) {
					nameField.setBackground(valid);
					credentials.setKey(nameField.getText());
				}
				else {
					nameField.setBackground(invalid);
					credentials.setKey(null);
				}

			}			
		});



		final WebPasswordField passField = new WebPasswordField ( 15 );
		passField.setBackground(invalid);
		content.add ( new WebLabel ( "Password", WebLabel.TRAILING ), "0,1" );
		content.add ( passField, "1,1" );

		passField.getDocument().addDocumentListener(new DocumentChangeListener() {
			@Override
			public void documentChanged(DocumentEvent arg0) {
				if(validator.isPasswordValid(nameField.getText(), new String(passField.getPassword()))) {
					passField.setBackground(valid);
					credentials.setValue(validator.getEncrypted(new String(passField.getPassword())));
				}
				else {
					passField.setBackground(invalid);
					credentials.setValue(null);
				}				
			}			
		});


		final WebButton login = new WebButton ( "Login" );
		final WebButton cancel = new WebButton ( "Cancel" );
		cancel.addActionListener(l->{
			wasCancelled.set(true);
			dispose();
		});

		// TODO Setup Listeners
		if(confirmPassword) {

			// Add confirmation field
			final WebPasswordField confirmField = new WebPasswordField ( 15 );
			confirmField.setBackground(invalid);
			confirmValid.set(false);
			content.add ( new WebLabel ( "Confirm Password", WebLabel.TRAILING ), "0,2" );
			content.add ( confirmField, "1,2" );

			// Setup listeners
			passField.getDocument().addDocumentListener(new DocumentChangeListener() {
				@Override
				public void documentChanged(DocumentEvent arg0) {
					confirmField.setText("");
				}
			});

			confirmField.getDocument().addDocumentListener(new DocumentChangeListener() {
				@Override
				public void documentChanged(DocumentEvent arg0) {
					if(Arrays.equals(confirmField.getPassword(),passField.getPassword())){
						confirmField.setBackground(valid);
						confirmValid.set(true);
					}
					else {
						confirmField.setBackground(invalid);
						confirmValid.set(false);
					}
				}
			});
		}


		login.addActionListener(l->{
			// Is userName valid?
			if(!validator.isUserNameValid(nameField.getText())) {
				blink(nameField);
			}
			else if(!validator.isPasswordValid(nameField.getText(), new String(passField.getPassword()))) {
				blink(passField);
			}
			else if(!confirmValid.get()) {
				JOptionPane.showMessageDialog(this, "Please confirm your password","", JOptionPane.WARNING_MESSAGE);
			}
			else
				dispose();
		});



		content.add ( new CenterPanel ( new GroupPanel ( 5, login, cancel ) ), confirmPassword ? "0,3,1,3" : "0,2,1,2" );

		SwingUtils.equalizeComponentsWidths ( login, cancel );

		add ( content );

		HotkeyManager.registerHotkey ( this, login, Hotkey.ESCAPE );
		HotkeyManager.registerHotkey ( this, login, Hotkey.ENTER );
	}



	private void blink(JComponent nameField) {
		// TODO Auto-generated method stub

	}
	public boolean wasCancelled() {
		return wasCancelled.get();
	}


	public String userName() {
		return credentials.getKey();
	}

	public String password() {
		return credentials.getValue();
	}



	public static void main(String[] args) {
		LoginDialog d = new LoginDialog(null, true, new NewUserValidator(Collections.singleton("UserName")));
		d.pack();
		d.setLocationRelativeTo(null);
		d.setVisible(true);

		if(!d.wasCancelled()) {
			System.out.println("Logged in!");
			System.out.println("username : "+d.userName());
			System.out.println("password : "+d.password());
		}
		else {
			System.out.println("Login cancelled!");
		}
	}

}
