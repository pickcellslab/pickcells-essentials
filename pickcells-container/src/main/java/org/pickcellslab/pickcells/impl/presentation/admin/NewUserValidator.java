package org.pickcellslab.pickcells.impl.presentation.admin;

import java.util.Set;

public class NewUserValidator implements LoginValidator {

	private final Set<String> forbiddenNames;
	
	
	public NewUserValidator(Set<String> forbiddenNames) {
		this.forbiddenNames = forbiddenNames;
	}
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.pickcells.impl.presentation.admin.LoginValidator#isUserNameValid(java.lang.String)
	 */
	@Override
	public boolean isUserNameValid(String text) {
		return text != null && text.length()>=minUserNameLength && !forbiddenNames.contains(text);
	};
	
	/* (non-Javadoc)
	 * @see org.pickcellslab.pickcells.impl.presentation.admin.LoginValidator#isPasswordValid(java.lang.String)
	 */
	@Override
	public boolean isPasswordValid(String user, String text){
		return text != null && text.length()>=minPasswordLength;
	}
	
	
}
