package org.pickcellslab.pickcells.impl.presentation.overview;

import org.pickcellslab.pickcells.impl.presentation.admin.ExperimentImplForAdmins;
import org.pickcellslab.pickcells.impl.presentation.admin.UserImplForAdmins;

public interface GoToExperimentListener {

	void gotoExperiment(UserImplForAdmins currentUser, ExperimentImplForAdmins e);

}
