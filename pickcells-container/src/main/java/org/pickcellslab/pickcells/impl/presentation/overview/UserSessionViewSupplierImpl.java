package org.pickcellslab.pickcells.impl.presentation.overview;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopeOption;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest.Permission;
import org.pickcellslab.foundationj.dbm.db.DataStoreConnector;
import org.pickcellslab.foundationj.scope.AppController;
import org.pickcellslab.foundationj.scope.RuntimeScope;
import org.pickcellslab.foundationj.scope.ScopeConfigurationException;
import org.pickcellslab.foundationj.scope.ScopeManager;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.data.User;
import org.pickcellslab.pickcells.api.presentation.admin.UserSessionViewSupplier;
import org.pickcellslab.pickcells.impl.presentation.KnownScopes;
import org.pickcellslab.pickcells.impl.presentation.admin.ExperimentImplForAdmins;
import org.pickcellslab.pickcells.impl.presentation.admin.UserImplForAdmins;
import org.slf4j.LoggerFactory;

/**
 * 
 * Entry class for the USER-SESSION {@link Scope}
 * 
 * @author Guillaume Blin
 *
 */
@CoreImpl
public class UserSessionViewSupplierImpl extends AppController implements UserSessionViewSupplier, GoToExperimentListener{

	
	

	final UserSessionView view;
	

	public UserSessionViewSupplierImpl(ScopeManager model, DataStoreConnector<?,?> connector, DataAccess access, UITheme theme, NotificationFactory notif, @ScopeOption User user) {
		super(model);
		this.view = new UserSessionView(theme, notif, new UserSessionModel(connector, access, user), this);		
	}

	
	@Override
	public JPanel getPanel() {
		return view;
	}


	public void start() {
		view.start();
	}
	
	public void stop() {
		view.stop();
	}
	
	
	public static void main(String[] args) {
		
	}


	@Override
	public void gotoExperiment(UserImplForAdmins currentUser, ExperimentImplForAdmins e) {
		// This is where we start the Workbench Scope!
		RuntimeScope wbScope = scopeMgr.getScope(KnownScopes.WORKBENCH, e.getName());
		if(wbScope!=null) {
			JOptionPane.showMessageDialog(null,"This experiment is already loaded");
			return;
		}
		try {
			// To start the Workbench scope, we need:
				// 1- Make sure that IMAGE_RESOURCES is started 
				// 2- A ConnexionRequest
			
			wbScope = scopeMgr.newScopeInstance(KnownScopes.WORKBENCH, e.getName(), getOrStartSharedResources());	
			
			final ConnexionRequest request = new ConnexionRequest(
					e.getDBPath(),
					e.getDBName(),
					currentUser.userName(),
					currentUser.encryptedPassword(),
					Permission.READ_WRITE);// TODO Permission
			
			
			scopeMgr.startScope(wbScope, request, e);// new Object[]{e, request});
	
		
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null,"There was an error loading this experiment, please check the log");
			LoggerFactory.getLogger(getClass()).error("There was an error loading experiment "+e.getName(), e1);
		}
	}
	
	
	private RuntimeScope getOrStartSharedResources() throws ScopeConfigurationException, Exception {

		// Start SANDBOX
		RuntimeScope resourceScope = scopeMgr.getScope(KnownScopes.IMAGE_RESOURCES, KnownScopes.IMAGE_RESOURCES);
		// If first time to start
		if(resourceScope == null) {
			// Get the Scope
			resourceScope = scopeMgr.newScopeInstance(KnownScopes.IMAGE_RESOURCES, KnownScopes.IMAGE_RESOURCES, scopeMgr.getApplicationScope());
			scopeMgr.startScope(resourceScope);
		}
		return resourceScope;		
	}
	
	
	

	@Override
	public void beforeStop(RuntimeScope willStop) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterStop(RuntimeScope hasStopped) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeStart(RuntimeScope willStart) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterStart(RuntimeScope hasStarted) {
		// TODO Auto-generated method stub
		
	}
	
	
}
