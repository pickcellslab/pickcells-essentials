package org.pickcellslab.pickcells.impl.presentation.admin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.ConnexionRequest.Permission;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.pickcellslab.pickcells.api.app.data.User;
import org.pickcellslab.pickcells.api.presentation.admin.AdminAccess;

@Data(typeId="Experiment")
@SameScopeAs(AdminAccess.class)
public class ExperimentImplForAdmins extends Experiment {

	public static final String SHARED="SHARED_WITH";
	static final AKey<String> permission = AKey.get("permission",String.class);
	
	
	@SuppressWarnings("unused")
	private ExperimentImplForAdmins() {}
	

	public ExperimentImplForAdmins(String name, String location, String description) {
		super(name, location, description);
	}	
	
	/*
	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}
	*/
	
	
	//------------------------------------------------------------------------//
	// Scope specific methods
	//------------------------------------------------------------------------//

	
	public boolean isShared() {
		return this.getLinks(Direction.OUTGOING, SHARED).count() != 0;
	}

	
	public List<User> sharedWith(Permission p) {
		return this.getLinks(Direction.OUTGOING, SHARED)
				.filter(l->l.getAttribute(permission).get().equals(p.name()))
				.map(l->(User)l.target()).collect(Collectors.toList());
	}

	
	public Stream<Link> sharedLinks(){
		return this.getLinks(Direction.OUTGOING, SHARED);
	}
	
	
	public UserImplForAdmins getCreator() {
		return this.getLinks(Direction.INCOMING, UserImplForAdmins.CREATED).map(l->(UserImplForAdmins)l.source()).findFirst().orElse(null);
	}

	

	//---------------------------------------------------------------------//
	//  Protected methods with Scope Mechanism although methods are public
	//---------------------------------------------------------------------//


	public void shareWith(UserImplForAdmins other, Permission p) {
		Link l = new DataLink(SHARED, this, other, true);
		l.setAttribute(permission, p.name());
	}

	
	public int stopSharingWith(UserImplForAdmins userName) {
		final Link link =
		this.sharedLinks().filter(l-> l.target() == userName)
		.findFirst().orElse(null);
		if(link!=null) {
			int id = link.getAttribute(idKey).get();
			link.delete();
			return id;
		}
		else return -1;
	}
	
	
	
	public void setDescription(String description) {
		this.setAttribute(descriptionKey, description);
	};
	
	
	public void setPermission(UserImplForAdmins user, Permission p) {
		final Link link =
				this.sharedLinks().filter(l-> l.target() == user)
				.findFirst().orElse(null);
				if(link!=null) {
					link.setAttribute(permission, p.name());
				}
	}
	

	public void delete(DataAccess access) throws DataAccessException {
		
		final int id = getAttribute(idKey).orElse(-1);
		if(id==-1) {
			System.out.println("ExperimentImplForAdmins: This experiment was never saved to the database...");
			links().forEach(l->l.delete());
			return;
		}
			
		
		access.queryFactory().delete("Experiment").completely()
		.useFilter(F.select(idKey).equalsTo(id)).run();
		
		
		links().forEach(l->l.delete());
		
	}


	


}
