package org.pickcellslab.pickcells.impl.presentation.workbench;

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.Updatable;
import org.pickcellslab.foundationj.dbm.queries.actions.AKeyInfo;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.pickcells.api.app.data.Experiment;
import org.pickcellslab.pickcells.api.datamodel.types.Image;

public class ExperimentIntegrityCheck {

	private final DataAccess access;
	private final Experiment e;

	public ExperimentIntegrityCheck(Experiment e, DataAccess access) {
		this.e = e;
		this.access = access;
	}

	public void performCheck() {


		try {

			// Is this the first time we access the database?
			if(access.queryFactory().read("Experiment").count().inOneSet().getAll().run()==0) {
				// If so just store the new experiment
				access.queryFactory().store().forceUnknownId().add(e,Traversers.newConstraints().fromMinDepth().toDepth(0)
						.traverseAllLinks().includeAllNodes()).run();
			}
			else {
				// Otherwise make sure that paths are consistent
				final Experiment storedExperiment = access.queryFactory().regenerate("Experiment").toDepth(0)
						.traverseAllLinks().includeAllNodes().regenerateAllKeys()
						.getAll().getOneTarget(Experiment.class).get();

				if(!e.getDBPath().equals(storedExperiment.getDBPath())){

					//Update the experiment dbPath
					access.queryFactory().update("Experiment", new Updater(){

						@Override
						public void performAction(Updatable i) throws DataAccessException {
							i.setAttribute(Experiment.dbPathKey, e.getDBPath());
						}

						@Override
						public List<AKeyInfo> updateIntentions() {
							List<AKeyInfo> updates = new ArrayList<>(1);						
							updates.add(new AKeyInfo(Experiment.dbPathKey, "Database Path"));
							return updates;
						}

					}).getAll().run();


					//Update the location of images
					access.queryFactory().update("Image", new Updater(){

						@Override
						public void performAction(Updatable i) throws DataAccessException {
							i.setAttribute(Image.loc, e.getDBPath());
						}

						@Override
						public List<AKeyInfo> updateIntentions() {
							List<AKeyInfo> updates = new ArrayList<>(1);						
							updates.add(new AKeyInfo(Image.loc, "Location"));
							return updates;
						}

					}).getAll().run();

				}				

			}

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
