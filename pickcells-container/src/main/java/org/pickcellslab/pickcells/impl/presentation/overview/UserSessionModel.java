package org.pickcellslab.pickcells.impl.presentation.overview;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.db.DataStoreConnector;
import org.pickcellslab.pickcells.api.app.data.User;
import org.pickcellslab.pickcells.api.app.data.User.Priviledge;
import org.pickcellslab.pickcells.impl.presentation.admin.ExperimentImplForAdmins;
import org.pickcellslab.pickcells.impl.presentation.admin.UserImplForAdmins;

/**
 * 
 * Takes care of User's management - providing permissions, maintaining the experiment list.
 * Notifies listeners when a modification occurs.
 * 
 * @author Guillaume Blin
 *
 */
public class UserSessionModel {

	private final List<UserEventsListener> lstrs = new ArrayList<>();
	private final UserImplForAdmins loggedIn;

	private final DataStoreConnector<?,?> connector;
	private final DataAccess access;

	public UserSessionModel(DataStoreConnector<?,?> connector, DataAccess access, User user) {
		loggedIn = (UserImplForAdmins) user; // provided by parent so safe casting in principle
		this.access = access;
		this.connector = connector;		
	}


	public UserImplForAdmins currentUser() {
		return loggedIn;
	}


	public List<UserImplForAdmins> allUsersExceptCurrent(){
		final TraverserConstraints tc = 
				Traversers.newConstraints()
				.fromDepth(1).toMaxDepth()
				.traverseAllLinks()
				.withOneEvaluation(P.isDeclaredType(loggedIn.declaredType()),
						Decision.INCLUDE_AND_CONTINUE,
						Decision.EXCLUDE_AND_CONTINUE);
		final List<UserImplForAdmins> list = new ArrayList(Traversers.breadthfirst(loggedIn, tc).traverse().nodes());
		Collections.sort(list, (c1,c2)->c1.userName().compareTo(c2.userName()));
		return list;
	}



	public List<UserImplForAdmins> usersManagedByCurrentUser(){

		if(loggedIn.privilege()==Priviledge.USER)
			return Collections.singletonList(loggedIn);

		return loggedIn.managedUsers().sorted((c1,c2)->c1.userName().compareTo(c2.userName())).collect(Collectors.toList());

	}



	public void addUser(UserImplForAdmins u) throws DataAccessException {
		if(u == null) return;
		loggedIn.addUser(u);
		access.queryFactory().store().add(loggedIn).turnMetaModelOff().run();		
		lstrs.forEach(l->l.userCreated(u));
	}




	public void addExperiment(ExperimentImplForAdmins e) throws DataAccessException {
		if(e == null) return;
		final Link link = loggedIn.setAsCreated(e);
		access.queryFactory().store().add(loggedIn).turnMetaModelOff().run();		
		lstrs.forEach(l->l.experimentAdded(loggedIn, link));
	}





	public void deleteExperiment(ExperimentImplForAdmins e) throws DataAccessException {
		if( e == null) return;
		e.delete(access);
		lstrs.forEach(l->l.experimentDeleted(loggedIn, e));
	}


	/**
	 * @param exp The Experiment for which the sharing properties were updated
	 * @param deletedLinks A Set containing the database ids of the 'shared' links that were deleted
	 * @throws DataAccessException
	 */
	public void updateSharingProperties(ExperimentImplForAdmins exp, Set<Integer> deletedLinks) throws DataAccessException {

		if(exp == null) return;

		// Delete the 'unshared' links
		access.queryFactory().deleteLinks(ExperimentImplForAdmins.SHARED)
		.completely()
		.useFilter(P.setContains(DataItem.idKey, deletedLinks))
		.run();

		// Store new permissions and sharing properties
		access.queryFactory().store().add(exp).turnMetaModelOff().run();	

		// Notify listeners
		lstrs.forEach(l->l.sharingPropertiesUpdated(exp));

	}





	public void deleteUser(UserImplForAdmins e) throws DataAccessException {
		if( e == null) return;
		final Set<ExperimentImplForAdmins> delExps = e.delete(access);
		delExps.forEach(exp->lstrs.forEach(lst->lst.experimentDeleted(e, exp)));
		lstrs.forEach(l->l.userDeleted(e));

	}



	public void addUserEventsListener(UserEventsListener l) {
		if(l!=null)
			lstrs.add(l);
	}


	public boolean isValidDatabase(File f) {
		return connector.isValidDatabase(f);
	}




	public void importExperiment(File file) throws DataAccessException {		
		// Create a new Experiment 
		ExperimentImplForAdmins experiment = new ExperimentImplForAdmins(file.getParentFile().getName(), file.getParent(), "Imported Experiment");
		addExperiment(experiment);		
	}


	public void updateDescription(ExperimentImplForAdmins e, String text) throws DataAccessException {
		e.setDescription(text);
		access.queryFactory().store().addStandAlone(e).run();
		lstrs.forEach(l->l.experimentUpdated(e));
	}








}
