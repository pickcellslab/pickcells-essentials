package org.pickcellslab.pickcells.impl.presentation.toolbar;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.dataviews.mvc.ReconfigurableView;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableDimensionsModel;
import org.pickcellslab.foundationj.dataviews.mvc.SelectableDimensionBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.DimensionDialog;
import org.pickcellslab.pickcells.api.app.charts.AppearanceFactory;
import org.pickcellslab.pickcells.api.app.charts.AppearancePreviewController;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.DBViewToolBarBuilder;
import org.pickcellslab.pickcells.api.app.picking.DistributionPickConsumer;
import org.pickcellslab.pickcells.api.app.picking.DistributionPicker;
import org.pickcellslab.pickcells.api.app.picking.SeriesPickConsumer;
import org.pickcellslab.pickcells.api.app.picking.SeriesPicker;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPickConsumer;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPicker;

/**
 * A helper class to create a toolbar for a Chart or any data view 
 * 
 * @author Guillaume Blin
 *
 */
@CoreImpl
public final class DBViewHelpImpl implements DBViewHelp{

	private List<SingleDataPickConsumer> singleDataPickers;
	private List<DistributionPickConsumer> distriPickers;
	private List<SeriesPickConsumer> multis;
	private NotificationFactory notif;
	private DataAccess access;
	private UITheme theme;

	/**
	 * This constructor is public only for the purpose of constructor injection at boot time, 
	 * declare in your constructor instead of instantiating directly.
	 */
	public DBViewHelpImpl(UITheme theme, NotificationFactory notif, DataAccess access, List<SingleDataPickConsumer> singles, List<SeriesPickConsumer> multis, List<DistributionPickConsumer> dPickers){
		this.access = access;
		this.theme = theme;
		this.notif = notif;
		this.singleDataPickers = singles  == null ? Collections.emptyList() : singles;
		this.distriPickers = dPickers == null ? Collections.emptyList() : dPickers;
		this.multis = multis == null ? Collections.emptyList() : multis;
	}


	public <M extends RedefinableDimensionsModel<T,B,D>, B extends SelectableDimensionBroker<T,D>,D,T> Optional<Dimension<T,D>> 
	displayDimensionChoice(ReconfigurableView<M,T,B,D> view, String axis){

		M model = view.getModel();

		//Redefine the dimensions 
		String queryId = view.getQueryUnderFocus();
		if(queryId == null){
			JOptionPane.showMessageDialog(null, "No series selected");
			return Optional.empty();
		}

		Predicate<DimensionContainer<T,?>> p = model.dimensionPredicate().and(c->c.numDimensions()<50);			
		int queryIndex = model.getDataSet().queryIndex(queryId);		
		List<DimensionContainer<T,D>> dims = model.getDataSet().possibleDimensions(queryIndex,p);

		if(dims.isEmpty()){
			JOptionPane.showMessageDialog(null, "The target of the current query has no displayable dimensions");
			return Optional.empty();
		}

		//Display UI to select the desired dims
		String[] names = new String[1];
		names[0] = axis;
		Dimension<T,?>[] selected = new Dimension[1];
		selected[0] = view.currentAxisDimension(0);
				
		DimensionDialog<T,D> ui = new DimensionDialog<>(dims, names,  selected, model.getDimensionMode(), theme);
		if(JOptionPane.showConfirmDialog(null,ui,"",JOptionPane.OK_CANCEL_OPTION)==JOptionPane.CANCEL_OPTION)
			return Optional.empty();			
		else 
			return Optional.of(ui.orderedChoice().get(0));
	}

	
	
	public AppearanceFactory<Color> simpleColorFactory(){
		return new AppearanceFactory<Color>() {
			
			@Override
			public AppearancePreviewController<Color> create(String query, int series, Color previous) {
				
				return new ColorPreviewController(query, series, previous);
				
			}
		};
	}
	
	
	
	


	public DBViewToolBarBuilder newToolBar(){
		return new DBViewToolBarBuilderImpl(access, theme, notif);
	}


	
	public void registerSingleConsumersFor(SingleDataPicker picker){

		Objects.requireNonNull(picker, "picker is null");	

		for(SingleDataPickConsumer i : singleDataPickers){
			picker.addSingleDataPickListener(i);
		}
				
	}

	
	public void registerSeriesConsumersFor(SeriesPicker picker){

		Objects.requireNonNull(picker, "picker is null");	

		for(SeriesPickConsumer i : multis){
			picker.addSeriesPickListener(i);
		}
				
	}



	public void registerDistributionConsumerFor(DistributionPicker picker){

		Objects.requireNonNull(picker, "picker is null");	

		for(DistributionPickConsumer c : distriPickers){
			picker.addDistributionPickListener(c);
		}
		
		/*
		picker.addDistributionPickListener(

				new DistributionPickListener(){

					@Override
					public <D> void distributionPicked(Distribution<D> d, Vignette<? extends DataItem> v,
							Function<DataItem, D> f) {

						final JPopupMenu menu = new JPopupMenu();
						for (DistributionPickConsumer c : distriPickers){
							JMenuItem item = new JMenuItem(c.name());
							item.addActionListener(l->c.distributionPicked(d, v, f));
							menu.add(item);
						}			
						menu.show(invoker, invoker.getX(), invoker.getY());
					}

				});
				*/

	}


}

