package org.pickcellslab.pickcells.impl.presentation;

import org.pickcellslab.foundationj.annotations.Scope;

public final class KnownScopes {

	private KnownScopes() {}
	
	public static final String APPLICATION = Scope.APPLICATION;
	public static final String ADMIN_ACCESS = "ADMIN-ACCESS";	
	public static final String USER_SESSION = "USER-SESSION";
	public static final String IMAGE_RESOURCES = "IMAGE-RESOURCES";
	public static final String WORKBENCH = "WORKBENCH";
	public static final String SANDBOX = "SANDBOX";
	
}
