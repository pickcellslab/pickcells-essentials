package org.pickcellslab.pickcells.impl.presentation.workbench;

import java.awt.Component;
import java.util.List;
import java.util.Objects;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.modules.MenuEntry;
import org.pickcellslab.pickcells.api.app.modules.WithIcon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.managers.tooltip.TooltipManager;

/**
 * 
 * A JMenuBar which bilds itself dynamically depending on the {@link MenuEntry} provided in constructor.
 * (Code largelly inspired from <a href="https://stackoverflow.com/questions/18056878/dynamically-building-a-menu">Stack Overflow</a>)
 * 
 * @author Guillaume Blin
 *
 */
@SuppressWarnings("serial")
public class WorkbenchMenu extends JMenuBar {

	private static final Logger log = LoggerFactory.getLogger(WorkbenchMenu.class);

	public WorkbenchMenu(List<MenuEntry> entries) {

		Objects.requireNonNull(entries);

		final String[][] menuHierarchies = new String[entries.size()][];
		for(int i = 0; i<entries.size(); i++)
			menuHierarchies[i] = entries.get(i).menuPath();


		// relevant code from here. It builds the menu removing redundancies
		for (int rootIndex = 0; rootIndex < menuHierarchies.length; ++rootIndex) {
			JMenu parentMenu = null;

			for(int menuLevel = 0; menuLevel < menuHierarchies[rootIndex].length; ++menuLevel) {
				// if it is root menu
				if (menuLevel == 0) {
					// checks if the root menu already exists 
					for (int i = 0; i < getMenuCount(); ++i) {
						if (getMenu(i).getText().equals(menuHierarchies[rootIndex][menuLevel])) {
							parentMenu = getMenu(i);
							break;
						}
					}

					if (parentMenu == null) {
						parentMenu = new JMenu(menuHierarchies[rootIndex][menuLevel]);
						add(parentMenu);
					}
				} else if (menuLevel < menuHierarchies[rootIndex].length - 1) { // if it is a non-leaf (and, of course, not a root menu)
					Component[] menuComponents = parentMenu.getMenuComponents();
					JMenu currentMenu = null;

					// checks if the menu already exists 
					for (Component component : menuComponents) {
						if (component instanceof JMenu) {
							if (((JMenu) component).getText().equals(menuHierarchies[rootIndex][menuLevel])) {
								currentMenu = (JMenu) component;
								break;
							}
						}
					}

					if (currentMenu == null) {
						currentMenu = new JMenu(menuHierarchies[rootIndex][menuLevel]);
						parentMenu.add(currentMenu);
					}

					parentMenu = currentMenu;
				} else { // if it is a leaf
					Component[] menuComponents = parentMenu.getMenuComponents();
					JMenuItem currentItem = null;

					for (Component component : menuComponents) {
						if (component instanceof JMenuItem) {
							if (((JMenuItem) component).getText().equals(menuHierarchies[rootIndex][menuLevel])) {
								currentItem = (JMenuItem) component;
								break;
							}
						}
					}

					if (currentItem == null) {
						final JMenuItem newItem = new JMenuItem(menuHierarchies[rootIndex][menuLevel]);
						parentMenu.add(newItem);

						final MenuEntry entry = entries.get(rootIndex);
						TooltipManager.setTooltip(newItem, buildTooltip(entry));
						if(WithIcon.class.isAssignableFrom(entry.getClass()))
							newItem.setIcon(UITheme.resize(((WithIcon) entry).icon(), 16, 16));
						// TODO entry.registerListener((l)->());
						newItem.addActionListener(l->{
							new Thread(()->{
								try {
									entry.launch();
								} catch (Exception e) {
									log.error("Error from "+entry.name(), e);
									JOptionPane.showMessageDialog(null, "There was an error with "+entry.name()+" an error has been logged");
								}
							}).start();
						});
					}
				}
			}
		}


	}


	private String buildTooltip(MenuEntry entry) {
		StringBuilder b = new StringBuilder();
		b.append("<HTML>");		
		b.append("Description"+ Objects.toString(entry.description())+"<br>");
		b.append("Url: "+ Objects.toString(entry.url())+"<br>");
		b.append("Authors"+ Objects.toString(entry.authors())+"<br>");
		b.append("License"+ Objects.toString(entry.licence())+"<br>");
		b.append("</HTML>");
		return null;
	}


	public void printMenu() {
		for (int i = 0; i < getMenuCount(); ++i) {
			printMenu(getMenu(i), 0);
		}
	}

	public void printMenu(JMenuItem menuItem, int level) {
		for (int i = 0; i < level; ++i) {
			System.out.print("\t");
		}

		System.out.println("\\" + menuItem.getText());

		if (menuItem instanceof JMenu) {
			JMenu menu = (JMenu) menuItem;

			Component[] menuComponents = menu.getMenuComponents();

			for (Component component : menuComponents) {
				if (component instanceof JMenuItem) {
					printMenu((JMenuItem) component, level+1);
				}
			}
		}
	}





}
