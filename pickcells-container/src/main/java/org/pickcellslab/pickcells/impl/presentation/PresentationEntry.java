package org.pickcellslab.pickcells.impl.presentation;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Scope;

@Core
@Scope
public interface PresentationEntry {

}
