package org.pickcellslab.pickcells.impl.presentation.admin;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface LoginValidator {

	public static final int minUserNameLength = 2;
	public static final int minPasswordLength = 2;
	
	boolean isUserNameValid(String text);

	boolean isPasswordValid(String userName, String password);

	default String getEncrypted(String str) {
		try {

			final MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(str.getBytes());

			byte byteData[] = md.digest();

			//Convert bytes
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Unable to encrypt the provided string ", e);
		}
	};




}