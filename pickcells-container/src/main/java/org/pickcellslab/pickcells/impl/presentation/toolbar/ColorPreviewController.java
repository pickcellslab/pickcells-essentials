package org.pickcellslab.pickcells.impl.presentation.toolbar;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;

import org.pickcellslab.pickcells.api.app.charts.AppearancePreviewController;

import com.alee.laf.colorchooser.ColorChooserListener;
import com.alee.laf.colorchooser.WebColorChooser;

@SuppressWarnings("serial")
public class ColorPreviewController extends AppearancePreviewController<Color> implements ColorChooserListener {

	private final Color initial;
	
	public ColorPreviewController(String query, int series, Color previous) {
		super(query, series);
		
		
		this.initial = previous;
		
		WebColorChooser chooser = new WebColorChooser(previous);
			
		this.setLayout(new BorderLayout());
		this.add(chooser, BorderLayout.CENTER);
		
		chooser.addColorChooserListener(this);
		
	}

	@Override
	public void okPressed(ActionEvent e) {
		this.fireAppearanceSelected(((WebColorChooser )e.getSource()).getColor());
	}

	@Override
	public void resetPressed(ActionEvent e) {
		this.fireAppearanceSelected(((WebColorChooser )e.getSource()).getOldColor());
	}

	@Override
	public void cancelPressed(ActionEvent e) {
		this.fireAppearanceSelected(initial);
	}

}
