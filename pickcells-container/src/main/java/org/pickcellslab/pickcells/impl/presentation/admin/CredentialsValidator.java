package org.pickcellslab.pickcells.impl.presentation.admin;

import java.util.Map;
import java.util.Objects;

public class CredentialsValidator implements LoginValidator {

	private final Map<String,String> users;
	
	public CredentialsValidator(Map<String,String> users) {
		Objects.requireNonNull(users);
		if(users.isEmpty())
			throw new IllegalArgumentException("users is empty");
		this.users=users;	
	}
	
	@Override
	public boolean isUserNameValid(String text) {
		return users.containsKey(text);
	}
	
	// userNameInvalidMessage(String invalidUserName)

	@Override
	public boolean isPasswordValid(String userName, String password) {
		final String known = users.get(userName);
		if(known==null)
			return false;
		return this.getEncrypted(password).equals(known);
	}

}
