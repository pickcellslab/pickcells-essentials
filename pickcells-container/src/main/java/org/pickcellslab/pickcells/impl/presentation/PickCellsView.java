package org.pickcellslab.pickcells.impl.presentation;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.util.EnumMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.kordamp.ikonli.fontawesome5.FontAwesomeRegular;
import org.kordamp.ikonli.fontawesome5.FontAwesomeSolid;
import org.kordamp.ikonli.swing.FontIcon;
import org.pickcellslab.pickcells.impl.presentation.overview.ImagePanel;
import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.scope.RuntimeScope;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.presentation.PickCellsPresentation;
import org.pickcellslab.pickcells.api.presentation.PickCellsPresentationListener;
import org.pickcellslab.pickcells.api.presentation.Sandbox;
import org.pickcellslab.pickcells.api.presentation.admin.UserSessionViewSupplier;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.extended.label.WebVerticalLabel;
import com.alee.extended.layout.ToolbarLayout;
import com.alee.extended.panel.GroupPanel;
import com.alee.extended.statusbar.WebMemoryBar;
import com.alee.extended.statusbar.WebStatusBar;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.button.WebButton;
import com.alee.laf.label.WebLabel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.separator.WebSeparator;
import com.alee.laf.tabbedpane.WebTabbedPane;
import com.alee.managers.WebLafManagers;
import com.alee.managers.tooltip.TooltipManager;
import com.alee.utils.SwingUtils;

@SuppressWarnings("serial")
@CoreImpl
public class PickCellsView extends WebFrame implements PickCellsPresentation {

	private static final Logger log = LoggerFactory.getLogger(PickCellsView.class);


	private PickCellsPresentationListener listener;

	private WebStatusBar statusBar;

	// Controls
	private final Map<Controls, JButton> statusControls = new EnumMap<>(Controls.class);
	private final Map<Controls, JButton> welcomeControls = new EnumMap<>(Controls.class);

	// Perspectives
	private final JPanel mainPanel = new JPanel();
	private final CardLayout cardLayout = new CardLayout();
	private final Map<Perspective, JComponent> perspectives = new EnumMap<>(Perspective.class);

	public PickCellsView(UITheme theme) {
		super("PickCells");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(UITheme.toImage(theme.icon(IconID.App.APP, 24)));
		this.setExtendedState(JFrame.MAXIMIZED_BOTH );
	}


	public void start() {

		//Setup the look and feel
		WebLookAndFeel.install();
		// WebLookAndFeel.initializeManagers ();
		WebLafManagers.initialize ();

		// WebLookAndFeel.setDecorateDialogs ( true );
		//WebLookAndFeel.setDecorateFrames ( false );


		// Setup main panels
		//this.setContentPane(contentPane);
		//his.setUndecorated(true);
		//ComponentMoveAdapter.install ( getRootPane (), this );
		//this.setShowTitleComponent(true);

		getContentPane().setLayout(new BorderLayout());
		mainPanel.setLayout(cardLayout);
		getContentPane().add(mainPanel, BorderLayout.CENTER);

		// Layout Controllers
		setStatusBar();
		setWelcome();

		// Load the background image


		setSize(750, 500);
		setLocationRelativeTo(null);
		setVisible(true);


		System.out.println("PickCells View was started");


	}



	private void setStatusBar() {

		// Init
		statusBar = new WebStatusBar();

		// Add controls
		// Sandbox
		statusControls.put(Controls.SANDBOX, new JButton(FontIcon.of(FontAwesomeSolid.CUBES, 16)));
		statusControls.get(Controls.SANDBOX).addActionListener(a->listener.sandboxClicked());
		TooltipManager.setTooltip(statusControls.get(Controls.SANDBOX), "Go to Sandbox");

		// Welcome
		statusControls.put(Controls.WELCOME, new JButton(FontIcon.of(FontAwesomeSolid.HOME, 16)));
		statusControls.get(Controls.WELCOME).addActionListener(a->listener.showWelcomeClicked());
		TooltipManager.setTooltip(statusControls.get(Controls.WELCOME), "Go to The welcome screen");

		// User
		statusControls.put(Controls.OVERVIEW, new JButton(FontIcon.of(FontAwesomeRegular.USER, 16)));
		statusControls.get(Controls.OVERVIEW).setDisabledIcon(FontIcon.of(FontAwesomeRegular.USER, 16, Color.lightGray));
		statusControls.get(Controls.OVERVIEW).setEnabled(false);
		statusControls.get(Controls.OVERVIEW).addActionListener(a->listener.showOverviewClicked());
		TooltipManager.setTooltip(statusControls.get(Controls.OVERVIEW), "Go to the User Overview");

		// Workbench
		statusControls.put(Controls.WORKBENCH, new JButton(FontIcon.of(FontAwesomeSolid.FLASK, 16)));
		statusControls.get(Controls.WORKBENCH).setDisabledIcon(FontIcon.of(FontAwesomeSolid.FLASK, 16, Color.lightGray));
		statusControls.get(Controls.WORKBENCH).addActionListener(a->listener.showWorkbenchClicked());
		statusControls.get(Controls.WORKBENCH).setEnabled(false);
		TooltipManager.setTooltip(statusControls.get(Controls.WORKBENCH), "Go to the workbench");

		// Logoff
		statusControls.put(Controls.LOGOFF, new JButton(FontIcon.of(FontAwesomeSolid.EXTERNAL_LINK_ALT, 16)));
		statusControls.get(Controls.LOGOFF).setDisabledIcon(FontIcon.of(FontAwesomeSolid.EXTERNAL_LINK_ALT, 16, Color.lightGray));
		statusControls.get(Controls.LOGOFF).addActionListener(a->listener.logOffClicked());
		statusControls.get(Controls.LOGOFF).setEnabled(false);
		TooltipManager.setTooltip(statusControls.get(Controls.LOGOFF), "Log off");

		// Memory bar
		final WebMemoryBar memoryBar = new WebMemoryBar ();
		memoryBar.setPreferredWidth ( memoryBar.getPreferredSize ().width + 20 );

		// Add components to the status bar
		statusBar.add(new WebLabel("PickCells v0.8          "), ToolbarLayout.END);

		statusBar.add ( memoryBar, ToolbarLayout.START );
		statusBar.add (new WebSeparator(true, true, SwingConstants.VERTICAL));
		statusBar.add ( statusControls.get(Controls.WELCOME), ToolbarLayout.START );
		statusBar.add ( statusControls.get(Controls.OVERVIEW), ToolbarLayout.START );
		statusBar.add ( statusControls.get(Controls.WORKBENCH), ToolbarLayout.START );
		statusBar.add ( statusControls.get(Controls.LOGOFF), ToolbarLayout.START );
		statusBar.add(new WebSeparator(true, true, SwingConstants.VERTICAL), ToolbarLayout.START);
		statusBar.add ( statusControls.get(Controls.SANDBOX), ToolbarLayout.START );
		statusBar.add(new WebSeparator(true, true, SwingConstants.VERTICAL), ToolbarLayout.START);

		getContentPane().add(statusBar, BorderLayout.SOUTH);

	}




	private void setWelcome() {

		Image bg =  new ImageIcon(getClass().getResource("/landing_page.png")).getImage();
		final JPanel welcome = new ImagePanel(bg);

		welcome.setBorder(BorderFactory.createEmptyBorder(200,200,200,200));

		final JButton sandbox = new JButton(" Sandbox ");
		sandbox.addActionListener(s->listener.sandboxClicked());

		welcome.add(sandbox, BorderLayout.CENTER);
		welcomeControls.put(Controls.SANDBOX, sandbox);

		final JButton login = new JButton(" Login ");
		login.addActionListener(s->{
			this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			new Thread(()->{
				try {
					listener.loginClicked();
				}catch(Exception e) {
					log.error("");
					JOptionPane.showMessageDialog(this, "An unexpected error has occured when launching the login manager", "Login Service Error", JOptionPane.ERROR_MESSAGE);
				}
				EventQueue.invokeLater(()->this.setCursor(Cursor.getDefaultCursor()));
			}).start();
		});

		welcome.add(login, BorderLayout.CENTER);
		welcomeControls.put(Controls.LOGIN, login);


		SwingUtils.equalizeComponentsWidths(sandbox,login);


		mainPanel.add(welcome, Perspective.WELCOME.toString());
		perspectives.put(Perspective.WELCOME, welcome);
	}


	private void addWorbench(Workbench wb) {


		WebTabbedPane tabs = (WebTabbedPane) perspectives.get(Perspective.WORKBENCH);
		if(tabs == null) {
			tabs = new WebTabbedPane(WebTabbedPane.RIGHT);
			mainPanel.add(tabs, Perspective.WORKBENCH.toString());
			perspectives.put(Perspective.WORKBENCH, tabs);
			statusControls.get(Controls.WORKBENCH).setEnabled(true);
		}
		tabs.addTab(null, wb.getPanel());

		// Create the tab component with a button to allow closing the experiment
		final WebButton close = new WebButton(FontIcon.of(FontAwesomeRegular.WINDOW_CLOSE));
		close.setRolloverDecoratedOnly(true);
		close.addActionListener(b->{
			if(this.showConfirm(" Close Workbench "+wb.title()+" ?"))
				listener.closeWorkbenchClicked(wb);
		});
		final GroupPanel tabComp = new GroupPanel(10, false, close, createTabLabel(wb.title()));

		tabs.setTabComponentAt(tabs.getTabCount()-1, tabComp);
		tabs.setSelectedIndex(tabs.getTabCount()-1);

		cardLayout.show(mainPanel, Perspective.WORKBENCH.toString());

	}

	private void removeWorbench(Workbench wb) {
			final WebTabbedPane tabs = (WebTabbedPane) perspectives.get(Perspective.WORKBENCH);
			tabs.remove(wb.getPanel());
			if(tabs.getTabCount()==0) {
				this.remove(tabs);
				perspectives.remove(Perspective.WORKBENCH);
				statusControls.get(Controls.WORKBENCH).setEnabled(false);
				setPerspective(Perspective.OVERVIEW);
			}
	}




	private WebVerticalLabel createTabLabel(String name) {
		// Counter-clockwise vertical label
		final WebVerticalLabel vl = new WebVerticalLabel ( name, FontIcon.of(FontAwesomeRegular.BOOKMARK, 16, Color.BLACK), WebVerticalLabel.LEFT );
		vl.setClockwise ( false );
		vl.setHorizontalTextPosition ( WebVerticalLabel.RIGHT );
		return vl;
	}




	public void stop() {
		System.out.println(" PickCells View was stoped");
		dispose();
	}






	@Override
	public void addPresentationListener(PickCellsPresentationListener l) {
		this.listener = l;
	}




	@Override
	public boolean showConfirm(String message) {
		final int confirm = JOptionPane.showConfirmDialog(null, message);
		return confirm == JOptionPane.YES_OPTION;
	}



	@Override
	public void beforeStop(RuntimeScope willStop) {



		switch(willStop.name()) {
		case KnownScopes.SANDBOX : {
			EventQueue.invokeLater(()->{
				mainPanel.remove(perspectives.remove(Perspective.SANDBOX));
				cardLayout.show(mainPanel, Perspective.WELCOME.toString());
			});
		}
		break;
		case KnownScopes.WORKBENCH : {
			final Workbench wb = willStop.getComponentInstance(Workbench.class);
			log.debug("removing workbench "+wb.title());
			EventQueue.invokeLater(()->{
				removeWorbench(wb);
			});
		}
		break;
		case KnownScopes.USER_SESSION : {
			// Remove all workbench
			EventQueue.invokeLater(()->{
				log.debug("removing user session");
				JComponent toRm = perspectives.remove(Perspective.OVERVIEW);
				if(toRm != null)
					mainPanel.remove(toRm);
				cardLayout.show(mainPanel, Perspective.WELCOME.toString());
				this.setPerspective(Perspective.WELCOME);
				statusControls.get(Controls.OVERVIEW).setEnabled(false);
				statusControls.get(Controls.LOGOFF).setEnabled(false);
			});

		}
		break;
		default: return;
		}

		mainPanel.revalidate();
		mainPanel.repaint();


	}




	@Override
	public void afterStop(RuntimeScope hasStopped) {
		switch(hasStopped.name()) {
		case KnownScopes.ADMIN_ACCESS : {
			welcomeControls.get(Controls.LOGIN).setEnabled(true);
		}
		break;
		case KnownScopes.USER_SESSION :{
			welcomeControls.get(Controls.LOGIN).setEnabled(true);
			welcomeControls.get(Controls.LOGIN).setText(" Login ");
		}
		break;
		}
	}




	@Override
	public void beforeStart(RuntimeScope willStart) {
		switch(willStart.name()) {
		case KnownScopes.ADMIN_ACCESS : {
			welcomeControls.get(Controls.LOGIN).setEnabled(false);
		}
		break;
		case KnownScopes.USER_SESSION : {
			welcomeControls.get(Controls.LOGIN).setEnabled(false);
		}
		break;
		}
	}




	@Override
	public void afterStart(RuntimeScope hasStarted) {
		EventQueue.invokeLater(()->{

			System.out.println("PickCellsControl: "+hasStarted+" view added ");

			switch(hasStarted.name()) {
			case KnownScopes.SANDBOX : {
				final JComponent sb = hasStarted.getComponentInstance(Sandbox.class).getPanel();
				mainPanel.add(Perspective.SANDBOX.name(), sb);
				cardLayout.show(mainPanel, Perspective.SANDBOX.toString());
				perspectives.put(Perspective.SANDBOX, sb);
			}
			break;
			case KnownScopes.WORKBENCH : {
				final Workbench wb = hasStarted.getComponentInstance(Workbench.class);
				addWorbench(wb);
			}
			break;
			case KnownScopes.USER_SESSION : {
				final JComponent sb = hasStarted.getComponentInstance(UserSessionViewSupplier.class).getPanel();
				mainPanel.add(Perspective.OVERVIEW.toString(), sb);
				cardLayout.show(mainPanel, Perspective.OVERVIEW.toString());
				perspectives.put(Perspective.OVERVIEW, sb);
				statusControls.get(Controls.OVERVIEW).setEnabled(true);
				statusControls.get(Controls.LOGOFF).setEnabled(true);
				welcomeControls.get(Controls.LOGIN).setText(" Logged in ");
			}
			break;
			default: return;
			}
		});

	}


	@Override
	public void setPerspective(Perspective view) {
		EventQueue.invokeLater(()->{
			cardLayout.show(mainPanel, view.toString());
			System.out.println("View now showing "+view);
		});
	}


	@Override
	public void goToExperiment(String expName) {
		EventQueue.invokeLater(()->{
			cardLayout.show(mainPanel, Perspective.WORKBENCH.toString());
			WebTabbedPane tabs = (WebTabbedPane) perspectives.get(Perspective.WORKBENCH);
			for(int t = 0; t<tabs.getTabCount(); t++)
				if(tabs.getTitleAt(t).equals(expName))
					tabs.setSelectedIndex(t);
		});
	}


	@Override
	public void setControlEnabled(Controls control) {

		EventQueue.invokeLater(()->{
			switch(control) {//TODO
			case LOGIN:
				break;
			case LOGOFF:
				break;
			case OVERVIEW:
				break;
			case SANDBOX:
				break;
			case WELCOME:
				break;
			case WORKBENCH:
				break;
			default:
				break;

			}
		});
	}



}
