package org.pickcellslab.pickcells.impl.presentation.toolbar;

/*-
 * #%L
 * foundationj-views
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

import org.pickcellslab.foundationj.dataviews.mvc.DataViewModel;
import org.pickcellslab.foundationj.dataviews.mvc.DefaultDataSet;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;
import org.pickcellslab.foundationj.queryui.QueryWizard;
import org.pickcellslab.foundationj.services.theme.UITheme;

public class DataSetRedefinitionController<T,D> implements ActionListener {

	private final QueryWizardConfig<T,D> config;
	private final DataAccess access;
	private final UITheme theme;
	private final DataViewModel<T> model;

	public DataSetRedefinitionController(UITheme theme, DataAccess access, DataViewModel<T> model, QueryWizardConfig<T,D> config) {
		Objects.requireNonNull(access, "access is null");
		Objects.requireNonNull(theme, "theme is null");
		Objects.requireNonNull(config, "config is null");
		this.access = access;
		this.theme = theme;
		this.config = config;
		this.model = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		try {
			
			QueryWizard<T,D> w = new QueryWizard<>(theme, access, config);
			w.pack();
			w.setLocationRelativeTo(null);
			w.setVisible(true);
			
			
			
			if(!w.wasCancelled())
				model.setDataSet(new DefaultDataSet<>(access, w.getDataset(), w.getInfos()));
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
}
