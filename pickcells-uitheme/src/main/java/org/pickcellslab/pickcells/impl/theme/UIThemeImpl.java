package org.pickcellslab.pickcells.impl.theme;

import java.awt.Color;
import java.net.URL;
import java.util.Objects;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.kordamp.ikonli.Ikon;
import org.kordamp.ikonli.fontawesome5.FontAwesomeBrands;
import org.kordamp.ikonli.fontawesome5.FontAwesomeRegular;
import org.kordamp.ikonli.fontawesome5.FontAwesomeSolid;
import org.kordamp.ikonli.swing.FontIcon;
import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.IconID.App;
import org.pickcellslab.foundationj.services.theme.IconID.Arrows;
import org.pickcellslab.foundationj.services.theme.IconID.Charts;
import org.pickcellslab.foundationj.services.theme.IconID.Data;
import org.pickcellslab.foundationj.services.theme.IconID.Files;
import org.pickcellslab.foundationj.services.theme.IconID.Levels;
import org.pickcellslab.foundationj.services.theme.IconID.Misc;
import org.pickcellslab.foundationj.services.theme.IconID.Queries;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@CoreImpl
public class UIThemeImpl implements UITheme{

	private final Logger log = LoggerFactory.getLogger(UIThemeImpl.class);
	private static final Ikon defaultIcon = FontAwesomeSolid.QUESTION_CIRCLE;


	@Override
	public Icon icon(IconID id, int size) {
		return this.icon(id, size, Color.BLACK); //TODO getDefaultColor
	}

	@Override
	public Icon icon(IconID id, int size, Color color) {
		return asIcon(objectFor(id), size, color);
	}

	


	private Icon asIcon(Object o, int size, Color color) {
		if(o instanceof URL)
			return UITheme.resize(new ImageIcon((URL)o), size, size);
		else
			return UITheme.toImageIcon(FontIcon.of((Ikon) o, size, color)); // transform to ImageIcon so that disabled buttons are rendered properly
	}


	private Object objectFor(IconID id) {		
		if(id!=null) {
			if(id instanceof App)
				return getIkon((App)id);
			if(id instanceof Arrows)
				return getIkon((Arrows)id);
			if(id instanceof Charts)
				return getIkon((Charts)id);
			if(id instanceof Data)
				return getIkon((Data)id);
			if(id instanceof Files)
				return getIkon((Files)id);
			if(id instanceof Levels)
				return getIkon((Levels)id);
			if(id instanceof Misc)
				return getIkon((Misc)id);
			if(id instanceof Queries)
				return getIkon((Queries)id);
		}
		return returnDefault(id);
	}




	private Object getIkon(App id) {
		switch((App)id) {
		case APP: return getClass().getResource("/icons/pickcells_icon.png");
		case ADMIN: return FontAwesomeSolid.USER_CIRCLE;
		case DATABASE: return FontAwesomeSolid.DATABASE;
		case TEAM: return FontAwesomeSolid.USERS;
		case USER: return FontAwesomeSolid.USER;
		case SHARE: return FontAwesomeSolid.SHARE_ALT;
		}
		return returnDefault(id);
	}

	private Object getIkon(Arrows arrows) {
		switch(arrows) {
		case ARROW_DOWN: return FontAwesomeSolid.ARROW_DOWN;
		case ARROW_LEFT: return FontAwesomeSolid.ARROW_LEFT;
		case ARROW_RIGHT: return FontAwesomeSolid.ARROW_RIGHT;
		case ARROW_UP: return FontAwesomeSolid.ARROW_UP;	
		}		
		return returnDefault(arrows);
	}

	private Object getIkon(Data type) {
		switch(type) {
		case BOOLEAN:return FontAwesomeSolid.STAR_HALF;
		case MISC:return FontAwesomeSolid.QUESTION_CIRCLE;
		case NUMERIC: return FontAwesomeSolid.SUPERSCRIPT;
		case TEXT: return FontAwesomeSolid.BOOK;		
		case BOTH: return getClass().getResource("/icons/both_link_24.png");
		case EXCLUDE_CONTINUE: return FontAwesomeSolid.PLAY_CIRCLE;
		case EXCLUDE_STOP: return FontAwesomeSolid.STOP_CIRCLE;
		case INCLUDE_CONTINUE: return FontAwesomeRegular.PLAY_CIRCLE;
		case INCLUDE_STOP: return FontAwesomeRegular.STOP_CIRCLE;
		case INCOMING: return getClass().getResource("/icons/incoming_link_24.png");
		case LINK: return FontAwesomeSolid.LINK;
		case NODE:return FontAwesomeBrands.HUBSPOT;
		case OUTGOING: return getClass().getResource("/icons/outgoing_link_24.png");
		}		
		return returnDefault(type);
	}

	private Object getIkon(Levels levels) {
		switch(levels) {
		case DEBUG: return FontAwesomeSolid.BUG;
		case ERROR: return FontAwesomeRegular.TIMES_CIRCLE;
		case INFO: return FontAwesomeSolid.INFO;
		case WARN: return FontAwesomeSolid.EXCLAMATION_TRIANGLE;
		}
		return returnDefault(levels);
	}

	private Object getIkon(Charts charts) {
		switch(charts) {
		case CHART_AREA: return FontAwesomeSolid.CHART_AREA;
		case CHART_BAR: return FontAwesomeSolid.CHART_BAR;
		case CHART_LINE: return FontAwesomeSolid.CHART_LINE;
		case CHART_PIE: return FontAwesomeSolid.CHART_PIE;
		case AXES: return getClass().getResource("/icons/axes_icon.png");
		case FIT: return getClass().getResource("/icons/fit_icon.png");
		case HISTOGRAM: return getClass().getResource("/icons/histo_icon.png");
		}
		return returnDefault(charts);
	}

	private Object getIkon(Files files) {
		switch(files) {
		case DOWNLOAD: return FontAwesomeSolid.DOWNLOAD;
		case EXPORT: return FontAwesomeSolid.ANGLE_DOUBLE_RIGHT;
		case IMPORT: return FontAwesomeSolid.ANGLE_DOUBLE_DOWN;
		case SAVE: return FontAwesomeSolid.SAVE;
		case UPLOAD: return FontAwesomeSolid.UPLOAD;
		case DELETE: return FontAwesomeSolid.TRASH;
		}
		return returnDefault(files);
	}

	private Object getIkon(Misc common) {

		switch(common) {
		case AT: return FontAwesomeSolid.AT;
		case BINOCULARS: return FontAwesomeSolid.BINOCULARS;
		case BOOK: return FontAwesomeSolid.BOOK;
		case CALENDAR: return FontAwesomeSolid.CALENDAR;
		case CAMERA: return FontAwesomeSolid.CAMERA;
		case CANCEL: return FontAwesomeSolid.TIMES_CIRCLE;
		case CHECK: return FontAwesomeSolid.CHECK;
		case CLIPBOARD: return FontAwesomeSolid.CLIPBOARD;
		case CLOCK: return FontAwesomeSolid.CLOCK;
		case CLONE: return FontAwesomeSolid.CLONE;
		case CLOUD: return FontAwesomeSolid.CLOUD;
		case COG: return FontAwesomeSolid.COG;
		case COGS: return FontAwesomeSolid.COGS;
		case ENVELOPE: return FontAwesomeSolid.ENVELOPE;
		case ENVELOPE_OPEN: return FontAwesomeSolid.ENVELOPE_OPEN;
		case ENVELOPE_SQUARE: return FontAwesomeSolid.ENVELOPE_SQUARE;
		case ERASER: return FontAwesomeSolid.ERASER;
		case EXPAND: return FontAwesomeSolid.EXPAND;
		case FLASK: return FontAwesomeSolid.FLASK;
		case PICK_ONE: return FontAwesomeSolid.CUBE;
		case PICK_SEVERAL: return FontAwesomeSolid.CUBES;
		case THUMBS_UP: return FontAwesomeSolid.THUMBS_UP;
		case UNDO: return FontAwesomeSolid.UNDO;
		case VALID: return FontAwesomeSolid.CHECK_SQUARE;
		case DATA_TABLE: return FontAwesomeSolid.TABLE;
		case FINISH_FLAG: return FontAwesomeSolid.FLAG_CHECKERED;
		case IN_PROGRESS: return FontAwesomeSolid.SPINNER;
		case PAINT: return FontAwesomeSolid.PAINT_BRUSH;
		case PLUS_SIGN: return FontAwesomeSolid.PLUS;
		case QUESTION: return FontAwesomeSolid.QUESTION;
		case START_FLAG: return FontAwesomeSolid.MAP_MARKER_ALT;
		case BATTERY_HALF: return FontAwesomeSolid.BATTERY_HALF;
		case MAGIC: return FontAwesomeSolid.MAGIC;
		case TAG: return FontAwesomeSolid.TAG;
		case CIRCLE: return FontAwesomeRegular.CIRCLE;
		case PIPETTE: return FontAwesomeSolid.EYE_DROPPER;
		}

		return returnDefault(common);

	}


	private Object getIkon(Queries id) {
		switch(id) {
		case FILTER: return FontAwesomeSolid.FILTER;
		case GROUPING: return FontAwesomeSolid.OBJECT_GROUP;
		case MULTIPLE_GROUPS: return FontAwesomeRegular.CLONE;
		case NO_SPLIT: return FontAwesomeSolid.LONG_ARROW_ALT_RIGHT;
		case ONE_GROUP: return FontAwesomeRegular.SQUARE;
		case SORT_ASC: return FontAwesomeSolid.SORT_AMOUNT_UP;
		case SORT_DESC: return FontAwesomeSolid.SORT_AMOUNT_DOWN;
		case SPLIT:return FontAwesomeSolid.CODE_BRANCH;
		case PATH:
			break;
		}
		return returnDefault(id);
	}




	private Object returnDefault(IconID id) {
		log.warn("Unknown IconID: "+Objects.toString(id));
		return defaultIcon;
	}




	@Override
	public Color[] defaultPalette() {		
		return new Color[] {Color.RED, Color.GREEN,  Color.YELLOW, Color.BLUE, Color.GRAY};
	}

	
}
