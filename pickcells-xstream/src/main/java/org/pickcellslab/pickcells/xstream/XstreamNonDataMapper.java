package org.pickcellslab.pickcells.xstream;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.mapping.extra.CustomNonDataConverter;
import org.pickcellslab.foundationj.mapping.extra.NonDataMapper;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;



/**
 * A Factory to obtain DataItem objects that can be persisted by the persistence module from any arbitrary
 * java object
 *
 */
@CoreImpl
public final class XstreamNonDataMapper implements NonDataMapper{

	private static final XStream xstream = new XStream(new DomDriver());


	@Override
	public void setMapping(Class<?> toMap, String mappingId) {
		xstream.alias(mappingId, toMap);
	}

	@Override
	public void ignoreField(Class<?> fromClass, String fieldName) {
		xstream.omitField(fromClass, fieldName);
	}

	@Override
	public void denyMappingFor(Class<?> deniedClass) {
		xstream.denyTypeHierarchy(deniedClass);
	}

	@Override
	public String convertToString(Object object) {
		return xstream.toXML(object);
	}

	@Override
	public Object restoreFromString(String str) {
		return xstream.fromXML(str);
	}

	@Override
	public void setMappingWithCustomConverter(Class<?> mappable, String value, Class<? extends CustomNonDataConverter> converter) {
		try {
			final CustomNonDataConverter instance = converter.newInstance();
			xstream.registerConverter(new ConverterAdapter(mappable, value, instance));	
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(converter + "does not have a public empty constructor");
		}
			
	}
	
}
