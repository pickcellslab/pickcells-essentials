package org.pickcellslab.pickcells.xstream;

import java.util.Objects;

import org.pickcellslab.foundationj.mapping.exceptions.NonDataMappingException;
import org.pickcellslab.foundationj.mapping.extra.CustomNonDataConverter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ConverterAdapter implements Converter {

	private final CustomNonDataConverter delegate;
	private final String id;
	private final Class clazz;

	public ConverterAdapter(Class clazz, String id, CustomNonDataConverter delegate) {
		Objects.requireNonNull(clazz, "Handled class is null");
		Objects.requireNonNull(clazz, "id is null");
		Objects.requireNonNull(clazz, "delegate is null");

		this.clazz = clazz;
		this.id = id;
		this.delegate = delegate;
	}


	@Override
	public boolean canConvert(Class type) {
		return clazz == type;
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		try {
			writer.setValue(delegate.toString(source));
		} catch (NonDataMappingException e) {
			throw new RuntimeException("Non Data conversion to string failed", e);
		}

	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		try {
			return delegate.fromString(reader.getValue());
		} catch (NonDataMappingException e) {
			throw new RuntimeException("Non Data conversion from string failed", e);
		}
	}

}
